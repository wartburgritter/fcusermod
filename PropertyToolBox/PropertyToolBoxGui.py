# -*- coding: utf-8 -*-
# FreeCAD tools of the PropertyToolBox
# (c) 2014 Bernd Hahnebach

# ***************************************************************************
# *   (c) Bernd Hahnebach (bernd@bimstatik.org) 2014                        *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# *   Bernd Hahnebach 2014                                                  *
# ***************************************************************************/


from __future__ import print_function
# needed for print(out, " ", end="")

import importlib
import sys

import FreeCAD
import FreeCADGui
import propertytools


class RELOADMODULES():
    def Activated(self):
        '''reload my modules to develop without restarting FreeCAD'''

        # '''
        if sys.version_info.major >= 3:
            import importIFCprops
            importlib.reload(importIFCprops)
            print('reloaded: importIFCprops')

            import importIFCpropdefs
            importlib.reload(importIFCpropdefs)
            print('reloaded: importIFCpropdefs')

            import ifctools
            importlib.reload(ifctools)
            print('reloaded: ifctools')

            import propertytools
            importlib.reload(propertytools)
            print('reloaded: propertytools')

            import femutils
            importlib.reload(femutils)
            print('reloaded: femutils')

            import npk_getter
            importlib.reload(npk_getter)
            print('reloaded: npk_getter')

            import export2SIA451
            importlib.reload(export2SIA451)
            print('reloaded: export2SIA451')

            import OneAttributColorsGui
            importlib.reload(OneAttributColorsGui)
            print('reloaded: OneAttributColorsGui')

            import BIMStatikTools
            importlib.reload(BIMStatikTools)
            print('reloaded: BIMStatikTools')

            import importIFCmin
            importlib.reload(importIFCmin)
            print('reloaded: importIFCmin')

        else:
            import importIFCprops
            reload(importIFCprops)
            print('reloaded: importIFCprops')

            import importIFCpropdefs
            reload(importIFCpropdefs)
            print('reloaded: importIFCpropdefs')

            import ifctools
            reload(ifctools)
            print('reloaded: ifctools')

            import propertytools
            reload(propertytools)
            print('reloaded: propertytools')

            import femutils
            reload(femutils)
            print('reloaded: femutils')

            import npk_getter
            reload(npk_getter)
            print('reloaded: npk_getter')

            import export2SIA451
            reload(export2SIA451)
            print('reloaded: export2SIA451')

            import OneAttributColorsGui
            reload(OneAttributColorsGui)
            print('reloaded: OneAttributColorsGui')

            import BIMStatikTools
            reload(BIMStatikTools)
            print('reloaded: BIMStatikTools')

            import importIFCmin
            reload(importIFCmin)
            print('reloaded: importIFCmin')

        '''
        # clear Python report prints and console by Python
        # https://forum.freecadweb.org/viewtopic.php?t=33081
        from PySide import QtGui
        mw = Gui.getMainWindow()

        c = mw.findChild(QtGui.QPlainTextEdit, "Python console")
        c.clear

        r = mw.findChild(QtGui.QTextEdit, "Report view")
        r.clear()
        '''

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'reload my modules', 'ToolTip': 'reload my modules'}


class SAVEOBJECT2TMP():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        print('\nPropertyToolBox_SaveObject2tmp')
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            o = selection[0]
            print('Selected Object: ', o.Name)
            if hasattr(o, "Shape"):
                print('Volume = ' + str(o.Shape.Volume * 1E-9))
                print('ist gar nicht so einfach. Da muss ich im aktuellen Dokument ein objekt anlegen!!!')
                print('wenn ich es hier zuweise ist der name tmp auch nur innerhalb der def gueltig!!!')
        else:
            print('Mehr als ein Objekt selektiert!')

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'SaveObject2tm', 'ToolTip': 'Save Selected Object to a temporary object'}


class VOLUMESINGLE():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        print('Command 1')
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            o = selection[0]
            print('Selected Object: ', o.Name)
            if hasattr(o, "Shape"):
                print('Volume = ' + str(o.Shape.Volume * 1E-9))
        else:
            print('Mehr als ein Objekt selektiert!')

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Report Volume Single Object', 'ToolTip': 'Report Volume Single Object'}


class VOLUMEMULTIPLE():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        selection = FreeCADGui.Selection.getSelection()
        for o in FreeCAD.ActiveDocument.Objects:
            o.ViewObject.Visibility = False
        sum_volume = 0
        for o in selection:
            out = 'Object: ' + o.Name
            print(out, " ", end="")
            o.ViewObject.Visibility = True
            if hasattr(o, "Shape"):
                if not hasattr(o, "Proxy") or (hasattr(o, "Proxy") and o.Proxy.Type != 'BuildingPart'):
                    # a simple Part::Feature has no Proxy
                    # a floor obj. is a BuildingPart and a BuildingPart has a Shape (compound of its childs ...)
                    vol = 0
                    try:
                        vol = o.Shape.Volume * 1E-9
                    except:
                        pass
                    print('Volume = ' + str(vol))
                    sum_volume += vol
                else:
                    print('is a BuildingPart: Volume ignored')
            else:
                print('no shape: ', o.Name)
                sum_volume = 0
                break
        print('sum_volume = ' + str(sum_volume))
        print('bitte nochmal machen, doppelselection wird nicht herausgefiltert!!!')

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Report Volume Multiple Objects', 'ToolTip': 'Report Volume Multiple Objects'}


class ONECOLOR():
    def Activated(self):
        propertytools.oneColor()

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'One Color', 'ToolTip': 'One Color for all Objects'}


class COLOROBJECTMATERIAL():
    def Activated(self):
        propertytools.colorObjectMaterial()

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Color ObjectMaterial', 'ToolTip': 'Color regarding Object Material'}


class TEXTUREOBJECTMATERIAL():
    def Activated(self):
        propertytools.textureObjectMaterial()

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Texture ObjectMaterial', 'ToolTip': 'Texture regarding Object Material'}


class DELETETEXTURE():
    def Activated(self):
        propertytools.deleteTextureObjectMaterial()

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Delete Texture', 'ToolTip': 'Delete Textures'}


class ADDSIAATTRIBUTES():
    def Activated(self):
        propertytools.addSiaAttributes()

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Add SIA Attributes', 'ToolTip': 'Add SIA Attributes'}


class ATTRIBUTCOLORSONEGUI():
    def Activated(self):
        import OneAttributColorsGui
        taskd = OneAttributColorsGui.TaskPanelOneAttributColors()
        FreeCADGui.Control.showDialog(taskd)

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'GUI: AttributColorsOneGui', 'ToolTip': 'GUI: AttributColorsOneGui'}


class ATTRIBUTCOLORSONEYAML():
    def Activated(self):
        propertytools.oneAttributColorsYaml()

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'AttributColorsOneYaml', 'ToolTip': 'AttributColorsOneYaml'}


class ATTRIBUTCOLORSTWO():
    def Activated(self):
        propertytools.twoAttributColors()

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'AttributColorsTwo', 'ToolTip': 'AttributColorsTwo'}


FreeCADGui.addCommand('PropertyToolBox_ReloadModules', RELOADMODULES())
FreeCADGui.addCommand('PropertyToolBox_SaveObject2tmp', SAVEOBJECT2TMP())
FreeCADGui.addCommand('PropertyToolBox_VolumeSingle', VOLUMESINGLE())
FreeCADGui.addCommand('PropertyToolBox_VolumeMultiple', VOLUMEMULTIPLE())
FreeCADGui.addCommand('PropertyToolBox_OneColor', ONECOLOR())
FreeCADGui.addCommand('PropertyToolBox_ColorObjectMaterial', COLOROBJECTMATERIAL())
FreeCADGui.addCommand('PropertyToolBox_TextureObjectMaterial', TEXTUREOBJECTMATERIAL())
FreeCADGui.addCommand('PropertyToolBox_DeleteTexture', DELETETEXTURE())
FreeCADGui.addCommand('PropertyToolBox_AddSIAAttributes', ADDSIAATTRIBUTES())
FreeCADGui.addCommand('PropertyToolBox_AttributColorsOneGui', ATTRIBUTCOLORSONEGUI())
FreeCADGui.addCommand('PropertyToolBox_AttributColorsOneYaml', ATTRIBUTCOLORSONEYAML())
FreeCADGui.addCommand('PropertyToolBox_AttributColorsTwo', ATTRIBUTCOLORSTWO())

# ***************************************************************************
# *                                                                         *
# *   Copyright (c) 2016 - Bernd Hahnebach <bernd@bbimstatik.ch>            *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

__title__ = "One attribut color tool task panel"
__author__ = "Bernd Hahnebach"
__url__ = "http://www.freecadweb.org"


import FreeCAD
import FreeCADGui
from PySide import QtGui
from PySide import QtCore
import propertytools


class TaskPanelOneAttributColors:
    '''The editmode TaskPanel for MechanicalMaterial objects'''
    def __init__(self):
        self.form = FreeCADGui.PySideUic.loadUi(FreeCAD.ConfigGet("UserAppData") + "Mod/PropertyToolBox/OneAttributColorsTaskPanel.ui")

        QtCore.QObject.connect(self.form.cb_attributs, QtCore.SIGNAL("activated(int)"), self.choose_attribut)
        QtCore.QObject.connect(self.form.cb_known_values, QtCore.SIGNAL("activated(int)"), self.choose_value)
        QtCore.QObject.connect(self.form.pushButton_apply, QtCore.SIGNAL("clicked()"), self.set_to_one_color)

        # try to retrieve the NPKVolumenPosition from active Document
        print("Search for entries in App.ActiveDocument. Could take some time!")
        known_npk_volumen_positions = []
        known_npk_bauteil = []
        for obj in FreeCAD.ActiveDocument.Objects:
            if hasattr(obj, "NPKVolumenPosition"):
                if obj.NPKVolumenPosition not in known_npk_volumen_positions:
                    known_npk_volumen_positions.append(obj.NPKVolumenPosition)
            if hasattr(obj, "NPKVolumenPosition"):
                if obj.NPKBauteil not in known_npk_bauteil:
                    known_npk_bauteil.append(obj.NPKBauteil)
        print(sorted(known_npk_volumen_positions))
        print(sorted(known_npk_bauteil))

        self.combobox_values = {
            'NPKBauteil': known_npk_bauteil,
            'NPKVolumenPosition': known_npk_volumen_positions}

        self.known_attributes = sorted(self.combobox_values.keys())
        print(self.known_attributes)

        self.form.cb_attributs.removeItem(0)
        for a in self.known_attributes:
            self.form.cb_attributs.addItem(a)
        self.index_attribut = 0
        self.index_value = 0
        self.choose_attribut(self.index_attribut)

    def getStandardButtons(self):
        return int(QtGui.QDialogButtonBox.Close) | int(QtGui.QDialogButtonBox.Apply)
        # return int(QtGui.QDialogButtonBox.Ok) | int(QtGui.QDialogButtonBox.Cancel)| int(QtGui.QDialogButtonBox.Apply)

    def accept(self):
        print('accept')
        FreeCADGui.Control.closeDialog()

    def reject(self):
        print('reject')
        FreeCADGui.Control.closeDialog()

    def clicked(self, button):
        if button == QtGui.QDialogButtonBox.Apply:
            print('apply clicked')
            self.apply_colors()

    def choose_attribut(self, index):
        if index < 0:
            return
        self.form.cb_attributs.setCurrentIndex(index)
        self.index_attribut = index

        for index in range(self.form.cb_known_values.count()):
            # loesche item mit index null so oft wie anzahl items
            self.form.cb_known_values.removeItem(0)
        attribut_key = self.form.cb_attributs.itemText(self.index_attribut)
        if attribut_key in self.combobox_values:
            self.known_values = self.combobox_values[attribut_key]
        for a in sorted(self.known_values):
            self.form.cb_known_values.addItem(a)
        self.form.cb_known_values.setCurrentIndex(self.index_value)

    def choose_value(self, index):
        if index < 0:
            return
        self.form.cb_known_values.setCurrentIndex(index)
        self.index_value = index

    def apply_colors(self):
        # testsetdict = {'NPKVolumenPosition': [[['241.615.311'], [0, 1.0, 0.0, 0.0]]]}
        # propertytools.set_onAttributColor(testsetdict)
        # attribut_key = 'NPKVolumenPosition'
        # attribut_value = '241.615.311'
        attribut_key = self.form.cb_attributs.itemText(self.index_attribut)
        attribut_value = self.form.cb_known_values.itemText(self.index_value)
        color_for_attribut_value = [0, 1.0, 0.0, 0.0]
        attribut_color_data = {attribut_key: [[[attribut_value], color_for_attribut_value]]}
        propertytools.set_onAttributColor(attribut_color_data)

    def set_to_one_color(self):
        propertytools.oneColor()

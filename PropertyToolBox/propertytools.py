# alle Betonbauteile
# NPKVolumen
# NPKBauteil
# NPKKategorie


# Aufpassen, ArchObjekte haben ein VO und die Shape des ArchObjektes hat ein VO
# immer nur das VO des Arch Objektes aendern
# Wenn alle shape eingeschaltet werden liegen die beiden VO uebereinander !!!
#    if '_FreeCAD_shape_body' not in o.Name:    # es waere cooler ein eigenes PythonFeature

# ich koennte auch auf hasattr und proxy suchen
# warum heissen die nicht archobjekte eigentlch auch body, die sollten ohne body heissen,
# dann wurde meine findmethode sie finden
# noch besser jedes body bekommt ein boolsches attribut, body = true, dann klar, das es nur der body eines arch ist.
# ach eine einfache partshape kennt ja keine attribute, dafuer braucht es ein PythonFeature

# noch ne idee ist ein eigenes PythonFeature fue mein import zu schreiben, und dann gar kein Arch Objekt mehr erstellen!!


import sys
import FreeCAD


def oneColor():
    if not FreeCAD.ActiveDocument:
        print('No Active Document !')
        return
        if not FreeCAD.ActiveDocument.Objects:
            print('No Objects!')
            return
    import yaml
    if sys.version_info.major >= 3:
        infile = FreeCAD.ConfigGet('UserAppData') + 'Mod/PropertyToolBox/attributfarben-alleshapes.yml'  # Py3, to be tested on Py2
        with open(infile, 'r') as f:
            shapeColor = yaml.load(f)
    else:
        infile = file(FreeCAD.ConfigGet('UserAppData') + 'Mod/PropertyToolBox/attributfarben-alleshapes.yml')  # Py2
        shapeColor = yaml.load(infile)
    # print(shapeColor)
    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name:
            if hasattr(o, 'Shape'):
                # o.ViewObject.Visibility = True
                o.ViewObject.Transparency = shapeColor[0]
                o.ViewObject.ShapeColor = (shapeColor[1],
                                           shapeColor[2],
                                           shapeColor[3])


def colorObjectMaterial():
    """
    set Color regarding Object Material
    """
    print("Color regarding Object Material for Arch Objects")
    materialandobject2color = {
        'IfcWallStandardCase__Ortbeton':
            [0, 0.0, 1.0, 0.0],
        'IfcWall__Ortbeton':
            [0, 0.0, 1.0, 0.0],
        'IfcWallStandardCase__Beton':
            [0, 0.0, 1.0, 0.0],
        'IfcWall__Beton':
            [0, 0.0, 1.0, 0.0],
        'IfcWallStandardCase__Backstein':
            [0, 1.0, 0.22, 0.22],
        'IfcWall__Backstein':
            [0, 1.0, 0.22, 0.22],
        'IfcWallStandardCase__Monobrick':
            [0, 1.0, 0.22, 0.22],
        'IfcWall__Monobrick':
            [0, 1.0, 0.22, 0.22],
        'IfcWallStandardCase__Kalksandstein':
            [0, 0.624, 0.0, 0.0],
        'IfcWall__Kalksandstein':
            [0, 0.624, 0.0, 0.0],
        'IfcWallStandardCase__Leichtbau':
            [0, 0.98, 0.647, 0.275],
        'IfcWall__Leichtbau':
            [0, 0.98, 0.647, 0.275],
        'IfcWallStandardCase__Elementbeton':
            [0, 0.659, 0.659, 1.0],  # Elementbruestungen
        'IfcWall__Elementbeton':
            [0, 0.659, 0.659, 1.0],
        'IfcWallStandardCase__Recyclingbeton':
            [0, 0.3, 1.0, 0.0],
        'IfcWall__Recyclingbeton':
            [0, 0.3, 1.0, 0.0],
        'IfcWallStandardCase__Beton_bestehend':
            [0, 0.3, 1.0, 0.0],
        'IfcWall__Beton_bestehend':
            [0, 0.3, 0.4, 0.4],
        'IfcSlab__Ortbeton':
            [70, 1.0, 1.0, 0.11],
        'IfcSlab__Beton':
            [70, 1.0, 1.0, 0.11],
        'IfcSlab__Elementbeton':
            [70, 0.0, 1.0, 1.0],  # NAK Bolkone Wohnbau
        'IfcSlab__Holz':
            [70, 0.98, 0.647, 0.275],
        'IfcSlab__ ':
            [70, 1.0, 0.667, 0.941],  # Dachhaut (Auenstein, Joho), steht auch im IfcSlab als Roof drin ToDo: auslesen
        'IfcRoof__ ':
            [70, 1.0, 0.667, 0.941],

        'IfcFooting__Ortbeton':
            [40, 0.51, 0.314, 0.157],
        'IfcFooting__Beton':
            [40, 0.51, 0.314, 0.157],
        'IfcFooting__Magerbeton':
            [30, 1.0, 0.5, 0.0],
        'IfcBuildingElementProxy__Magerbeton':
            [30, 1.0, 0.5, 0.0],
        'IfcColumn__Ortbeton':
            [0, 0.0, 0.431, 0.0],
        'IfcColumn__Beton':
            [0, 0.0, 0.431, 0.0],
        'IfcColumn__Elementbeton':
            [0, 0.0, 1.0, 1.0],
        'IfcColumn__Betonelement':
            [0, 0.0, 1.0, 1.0],
        'IfcColumn__Baustahl':
            [0, 0.0, 0.0, 1.0],
        'IfcColumn__Holzbau':
            [30, 0.98, 0.647, 0.275],
        'IfcBeam__Ortbeton':
            [30, 0.0, 0.431, 0.0],
        'IfcBeam__Beton':
            [30, 0.0, 0.431, 0.0],
        'IfcBeam__Fugenmaterial':
            [30, 1.0, 0.0, 1.0]
    }
    """
    orginalwerte oberes dictionary, sieht in coin nicht schoen aus wohl bei mehreren transparencen hintereinander oder so !!!
    'IfcWallStandardCase__Ortbeton'          : [30, 0.0,1.0,0.0],
    'IfcWall__Ortbeton'                      : [30, 0.0,1.0,0.0],
    'IfcWallStandardCase__Beton'             : [30, 0.0,1.0,0.0],
    'IfcWall__Beton'                         : [30, 0.0,1.0,0.0],
    'IfcWallStandardCase__Backstein'         : [30, 1.0,0.22,0.22],
    'IfcWall__Backstein'                     : [30, 1.0,0.22,0.22],
    'IfcWallStandardCase__Monobrick'         : [30, 1.0,0.22,0.22],
    'IfcWall__Monobrick'                     : [30, 1.0,0.22,0.22],
    'IfcWallStandardCase__Kalksandstein'     : [30, 0.624,0.0,0.0],
    'IfcWall__Kalksandstein'                 : [30, 0.624,0.0,0.0],
    'IfcWallStandardCase__Leichtbau'         : [30, 0.98,0.647,0.275],
    'IfcWall__Leichtbau'                     : [30, 0.98,0.647,0.275],
    'IfcWallStandardCase__Elementbeton'      : [20, 0.659,0.659,1.0],   # Elementbruestungen
    'IfcWall__Elementbeton'                  : [20, 0.659,0.659,1.0],
    'IfcWallStandardCase__Recyclingbeton'    : [30, 0.3,1.0,0.0],
    'IfcWall__Recyclingbeton'                : [30, 0.3,1.0,0.0],
    'IfcSlab__Ortbeton'                      : [30, 1.0,1.0,0.11],
    'IfcSlab__Beton'                         : [30, 1.0,1.0,0.11],
    'IfcSlab__Elementbeton'                  : [30, 0.0,1.0,1.0],       # NAK Bolkone Wohnbau
    'IfcSlab__Holz'                          : [30, 0.98,0.647,0.275],
    'IfcSlab__ '                             : [60, 1.0,0.667,0.941],   # Dachhaut (Auenstein, Joho), steht auch im IfcSlab als Roof drin ToDo: auslesen
    'IfcRoof__ '                             : [60, 1.0,0.667,0.941],
    """

    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name:
            if hasattr(o, 'Shape'):
                o.ViewObject.Visibility = True
                if hasattr(o, 'IfcObjectType') and hasattr(o, 'IfcMaterial1'):
                    changeWrongMaterialDefinitions()
                    objmatstr = o.IfcObjectType + '__' + o.IfcMaterial1
                    if objmatstr in materialandobject2color:
                        o.ViewObject.Transparency = materialandobject2color[objmatstr][0]
                        o.ViewObject.ShapeColor = (materialandobject2color[objmatstr][1],
                                                   materialandobject2color[objmatstr][2],
                                                   materialandobject2color[objmatstr][3])

                    else:
                        o.ViewObject.ShapeColor = (1.0, 1.0, 1.0)  # white looks pretty cool here :-)
                        o.ViewObject.Transparency = 0
                        print('Not in ObjectColorlist --> ', o.Name, ' --> ', o.IfcObjectType, ' --> ', o.IfcMaterial1)
                else:
                    o.ViewObject.Transparency = 0
                    o.ViewObject.ShapeColor = (0.0, 0.0, 0.0)   # black
                    print('No IfcObjectType or No IfcMaterial1 ', o.Name)


def textureObjectMaterial():
    """
    set Texture regarding Object Material
    """
    # import FreeCADGui
    from pivy import coin
    print("Texture regarding Object Material for Arch Objects")
    texturefilepath = FreeCAD.ConfigGet('UserAppData') + 'Texture/'
    # lin /home/username/.FreeCAD/
    # win c:/Users/username/AppData/Roaming/FreeCAD
    material2texture = {
        'noMaterial':
            texturefilepath + 'noMaterial.jpg',
        'Beton':
            texturefilepath + 'ch-beton_bewehrt.jpg',
        'Ortbeton':
            texturefilepath + 'ch-beton_bewehrt.jpg',
        'Betonelement':
            texturefilepath + 'ch-betonelemente.jpg',
        'Elementbeton':
            texturefilepath + 'ch-betonelemente.jpg',
        'Misaporbeton':
            texturefilepath + 'ch-misapor.jpg',
        'Recyclingbeton':
            texturefilepath + 'ch-misapor.jpg',
        'Beton_bestehend':
            texturefilepath + 'ch-beton_bestehend.jpg',
        'Spezialbeton':
            texturefilepath + 'ch-beton_01.jpg',
        'Sichtbeton':
            texturefilepath + 'ch-beton_01.jpg',
        'Beton_unbewehrt':
            texturefilepath + 'ch-beton_01.jpg',

        'Backstein':
            texturefilepath + 'ch-backstein.jpg',
        'Monobrick':
            texturefilepath + 'ch-backstein.jpg',
        'Kalksandstein':
            texturefilepath + 'ch-kalksandstein.jpg',
        'Calmostein':
            texturefilepath + 'ch-calmostein.jpg',
        'Sumostein':
            texturefilepath + 'ch-sumostein.jpg',
        'Naturstein':
            texturefilepath + 'ch-naturstein.jpg',
        'Zementstein':
            texturefilepath + 'ch-zementstein.jpg',
        'Mauerwerk_bestehend':
            texturefilepath + 'ch-mauerwerk_bestehend.jpg',
        'Porenbetonstein':
            texturefilepath + 'ch-porenbetonstein.jpg',
        'Mauerstein':
            texturefilepath + 'ch-backstein.jpg',

        'Baustahl':
            texturefilepath + 'ch-stahl.jpg',
        'Stahl':
            texturefilepath + 'ch-stahl.jpg',
        'Fugenmaterial':
            texturefilepath + 'ch-daemmung_hart.jpg',
        'Kragplattenanschluss':
            texturefilepath + 'ch-daemmung_hart.jpg',
        'Daemmung_hart':
            texturefilepath + 'ch-daemmung_hart.jpg',
        'Daemmstoff_hart':
            texturefilepath + 'ch-daemmung_hart.jpg',
        'Daemmstoff_weich':
            texturefilepath + 'ch-daemmung_weich.jpg',
        'Holzbau':
            texturefilepath + 'ch-holz.jpg',
        'Leichtbau':
            texturefilepath + 'ch-holz.jpg',
        'Holz':
            texturefilepath + 'ch-holz.jpg',
        'Magerbeton':
            texturefilepath + 'ch-magerbeton.jpg',
        'Eindeckung':
            texturefilepath + 'ch-dachziegel.jpg'
    }
    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name:
            if hasattr(o, 'Shape'):
                o.ViewObject.Visibility = True
                if hasattr(o, 'IfcObjectType') and hasattr(o, 'IfcMaterial1'):
                    changeWrongMaterialDefinitions()
                    # find if hight of solid is greater than lenth (texture should be turned than)
                    rootnode = o.ViewObject.RootNode
                    tex = coin.SoTexture2()
                    tex.filename = material2texture['noMaterial']  # white texture
                    if o.IfcMaterial1 in material2texture:
                        # print(material2texture[o.IfcMaterial1])
                        tex.filename = material2texture[o.IfcMaterial1]
                    else:
                        print('Not in Colorlist --> ', o.Name, ' --> ', o.IfcObjectType, ' --> ', o.IfcMaterial1)
                    o.ViewObject.Transparency = 0
                    o.ViewObject.ShapeColor = (1.0, 1.0, 1.0)   # white
                    rootnode.insertChild(tex, 1)
                else:
                    o.ViewObject.Transparency = 0
                    o.ViewObject.ShapeColor = (0.0, 0.0, 0.0)   # black
                    print('No IfcObjectType or No IfcMaterial1 --> ', o.Name)


def deleteTextureObjectMaterial():
    """
    set Texture regarding Object Material
    """
    # import FreeCADGui
    from pivy import coin
    print("Deleting the Texture Nodes")
    for o in FreeCAD.ActiveDocument.Objects:
        # print(o.Name)
        if o.ViewObject.RootNode.getNumChildren() > 1:
            o.ViewObject.RootNode.removeChild(1)

    # Absturz wenn keine Textur vorhanden!!!


'''
import Part
from pivy import coin

box=Part.makeBox(250,1000,3000)
Part.show(box)
rootnode = App.ActiveDocument.Shape.ViewObject.RootNode
tex =  coin.SoTexture2()
tex.filename = '/home/hugo/Documents/projekte--ifc/freecad/BIM--IFC/Texturen--Vorlageproj/ch-backstein.jpg'
App.ActiveDocument.ActiveObject.ViewObject.Transparency = 0
App.ActiveDocument.ActiveObject.ViewObject.ShapeColor = (1.0,1.0,1.0)   # white
rootnode.insertChild(tex,1)

texcoords = coin.SoTextureCoordinatePlane()
texcoords.directionS.setValue(2,0,0)
texcoords.directionT.setValue(0,2,0)
rootnode.insertChild(texcoords,2)

App.ActiveDocument.ActiveObject.ViewObject.RootNode.getNumChildren()
App.ActiveDocument.ActiveObject.ViewObject.RootNode.getChild(1)
App.ActiveDocument.ActiveObject.ViewObject.RootNode.removeChild(1)
App.ActiveDocument.ActiveObject.ViewObject.RootNode.removeChild(2)

'''


def changeWrongMaterialDefinitions():   # better change the materials in my modells !!!
    """
    change wrong material names
    """
    wrongmaterials = [
        'Feuerfestestein', 'Calmo', 'D\u00e4mmstoff hart', 'D\u00e4mmung', 'Holz massiv',
        'Mauerwerk bestehend', 'Dach allgemein', 'Beton bestehend', 'Beton unbewehrt'
    ]
    changematerials = [
        ['Feuerfestestein', 'Sumostein'],
        ['Calmo', 'Calmostein'],
        ['D\u00e4mmstoff hart', 'Daemmung_hart'],
        ['D\u00e4mmung', 'Daemmung_hart'],
        ['Holz massiv', 'Holz'],
        ['Mauerwerk bestehend', 'Mauerwerk_bestehend'],
        ['Dach allgemein', 'Eindeckung'],
        ['Beton bestehend', 'Beton_bestehend'],
        ['Beton unbewehrt', 'Beton_unbewehrt']
    ]
    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name:
            if hasattr(o, 'IfcMaterial1'):
                if o.IfcMaterial1 in wrongmaterials:
                    for cm in changematerials:
                        if o.IfcMaterial1 == cm[0]:
                            o.IfcMaterial1 = cm[1]

            if hasattr(o, 'IfcObjectType') and hasattr(o, 'IfcMaterial1'):
                if (o.IfcObjectType == 'IfcSlab' or 'IfcRoof') and o.IfcMaterial1 == ' ':
                    o.IfcMaterial1 = 'Eindeckung'


def colorPropertyMaterial():
    """
    Farben nach Property Material setzen
    """
    for o in FreeCAD.ActiveDocument.Objects:
        if '_FreeCAD_shape_body' not in o.Name:    # es waere cooler ein eigenes PythonFeature
            if hasattr(o, 'Shape'):
                if hasattr(o, 'IfcImportedProperties'):
                    if 'Material' in o.IfcImportedProperties:
                        o.ViewObject.Transparency = 50
                        if o.IfcImportedProperties['Material'] == 'Backstein':
                            o.ViewObject.ShapeColor = (1.0, 0.0, 0.0)
                        elif o.IfcImportedProperties['Material'] == 'Beton':
                            o.ViewObject.ShapeColor = (0.0, 1.0, 0.0)
                        else:
                            o.ViewObject.ShapeColor = (1.0, 1.0, 1.0)
                            o.ViewObject.Transparency = 0
                else:
                    # make all shapes which have no IfcImportedProperties transparent.
                    o.ViewObject.Transparency = 80
                    print("  Object " + o.Name + " has no IfcImportedProperties")


def farbImport():
    """
    Falls Farben in den propertysets definiert sind. Aeltere ifc-dateien
    """
    for o in FreeCAD.ActiveDocument.Objects:
        if '_FreeCAD_shape_body' not in o.Name:    # es waere cooler ein eigenes PythonFeature
            if hasattr(o, 'Shape'):
                if hasattr(o, 'IfcImportedProperties'):
                    if 'Blue' in o.IfcImportedProperties:
                        print(o.Name)
                        print(o.Name + '   R: ' + o.IfcImportedProperties['Red'] + '   G: ' + o.IfcImportedProperties['Green'] + '   B: ' + o.IfcImportedProperties['Blue'])
                        r = int(o.IfcImportedProperties['Red']) / 255.0
                        # print(r)
                        g = int(o.IfcImportedProperties['Green']) / 255.0
                        b = int(o.IfcImportedProperties['Blue']) / 255.0
                        if int(o.IfcImportedProperties['Red']) == 229 and int(o.IfcImportedProperties['Green']) == 229 and int(o.IfcImportedProperties['Blue']) == 229:
                            print('Mache Grau heller!')
                            r = .999
                            g = .999
                            b = .999
                        o.ViewObject.ShapeColor = (r, g, b)
                        o.ViewObject.Transparency = 0
                else:
                    # make all shapes which have no IfcImportedProperties transparent too.
                    o.ViewObject.Transparency = 75
                    print("  Object " + o.Name + " has no IfcImportedProperties")


def makeOrtbetonVisible():
    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name:
            if hasattr(o, 'Shape'):
                o.ViewObject.Visibility = False
                if hasattr(o, 'IfcMaterial1'):
                    if o.IfcMaterial1 == 'Ortbeton':
                        o.ViewObject.Visibility = True


def makeNPKPropertyVisible():
    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name:
            if hasattr(o, 'Shape'):
                o.ViewObject.Visibility = False
                if hasattr(o, 'IfcObjectType') and hasattr(o, 'IfcMaterial1') and hasattr(o, 'NPKVolumenPosition'):
                    if o.IfcMaterial1 == 'Ortbeton':
                        o.ViewObject.Visibility = True


def makeHasNotNPKPropertyVisible():
    """Ortbeton ohne NPKVolumenPosition sichtbar
    """
    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name:
            if hasattr(o, 'Shape'):
                o.ViewObject.Visibility = False
                if hasattr(o, 'IfcMaterial1') and not hasattr(o, 'NPKVolumenPosition'):
                    if o.IfcMaterial1 == 'Ortbeton':
                        o.ViewObject.Visibility = True


def addSiaAttributes():
    '''
    Damit ich mal einige Attribute habe mit denen ich meine neuen If-Methoden ausprobieren kann !!!
    '''
    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name:
            if hasattr(o, 'Shape'):
                o.ViewObject.Visibility = True
                if hasattr(o, 'IfcObjectType') and hasattr(o, 'IfcMaterial1'):
                    if o.IfcMaterial1 == 'Ortbeton':
                        o.addProperty("App::PropertyString", "NPKVolumen", "NPK", "was das denn")
                        o.NPKVolumen = 'Ortbeton'
                        o.addProperty("App::PropertyString", "NPKBauteil", "NPK", "was das denn")
                        o.addProperty("App::PropertyString", "NPKKategorie", "NPK", "was das denn")
                        o.addProperty("App::PropertyString", "NPKBetonsorte", "NPK", "was das denn")
                        o.NPKBetonsorte = 'NPKC'
                        if o.IfcObjectType == 'IfcFooting':
                            o.NPKBauteil = 'Fundament'
                            o.NPKKategorie = 'Bodenplatte'  # alle Fundamente sind Bodenplatten
                        if o.IfcObjectType == 'IfcWallStandardCase':
                            o.NPKBauteil = 'Wand'
                            o.NPKKategorie = 'Bodenplatte'  # alle Fundamente sind Bodenplatten
                        if o.IfcObjectType == 'IfcSlab':
                            o.NPKBauteil = 'Decke'
                            o.NPKKategorie = 'Standarddecke'  # Kragplatten und Schachtkopfplatte umdefinieren
                        if o.IfcObjectType == 'IfcColumn':
                            o.NPKBauteil = 'Stuetze'
                            o.NPKKategorie = 'Standardstuetze'


def oneAttributColorsYaml():
    """
    ColorMatrix lesen und entsprechende Bauteile einfaerben
    """
    # print(yaml.dump(ListOderDict))  # yaml code ausgeben

    print('Set Color regarding one Attribut')
    attributValueColor = get_oneAttributColorsYaml()
    set_onAttributColor(attributValueColor)


def get_oneAttributColorsYaml():
    import yaml
    infile = file(FreeCAD.ConfigGet('UserAppData') + 'Mod/PropertyToolBox/attributfarben-oneattribut.yml')
    return (yaml.load(infile))


def set_onAttributColor(attributValueColor):
    ''' used in
    Tool: OneAttributColorsYaml
    Tool: OneAttributColorsGui
    '''
    print(attributValueColor)
    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name:
            if hasattr(o, 'Shape'):
                boolAttributIsInValueColor = False
                for attributName in attributValueColor:
                    attributValues = attributValueColor[attributName]
                    if hasattr(o, attributName):
                        shapeAttributValue = getattr(o, attributName)
                        print(shapeAttributValue)
                        for valueColorPair in attributValues:
                            if shapeAttributValue in valueColorPair[0]:
                                print('found: ', attributName, ' --> ', shapeAttributValue, ' for Object : ', o.Name)
                                boolAttributIsInValueColor = True
                                o.ViewObject.Transparency = valueColorPair[1][0]
                                o.ViewObject.ShapeColor = (valueColorPair[1][1],
                                                           valueColorPair[1][2],
                                                           valueColorPair[1][3])
                    if hasattr(o, attributName) and boolAttributIsInValueColor is False:
                        pass
                        # print('not found: ', attributName, ' --> ', shapeAttributValue, ' for Object : ', o.Name)

                        # farbe definieren, wenn der attributWert des ShapeAttributs nicht im Yamlfile ist

                        # je nach dem was man will !!!
                        # der gefundene farbig oderder nicht gefundene farbig
                        # genau gleich fuer ausgbabe bei gefunden ausgabe oder bei nicht gefunden


def twoAttributColors():
    """
    ColorMatrix lesen und entsprechende Bauteile einfaerben
    """
    # print(yaml.dump(ListOderDict))  # yaml code ausgeben

    print('Set Color regarding two Attributs')

    import yaml
    infile = file(FreeCAD.ConfigGet('UserAppData') + 'Mod/PropertyToolBox/attributfarben-twoattribut.yml')

    attributValueColor = yaml.load(infile)
    # print(attributValueColor)
    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name:
            if hasattr(o, 'Shape'):
                boolAttributIsInValueColor = False
                for attributName1 in attributValueColor:
                    for attributName2 in attributValueColor[attributName1]:
                        attributValues = attributValueColor[attributName1][attributName2]
                        if hasattr(o, attributName1) and hasattr(o, attributName2):
                            shapeAttributValue1 = getattr(o, attributName1)
                            shapeAttributValue2 = getattr(o, attributName2)
                            for valueColorTriple in attributValues:
                                # print(shapeAttributValue1, valueColorTriple[0][0], shapeAttributValue2,valueColorTriple[1][0])
                                if shapeAttributValue1 in valueColorTriple[0] and shapeAttributValue2 in valueColorTriple[1]:
                                    boolAttributIsInValueColor = True
                                    o.ViewObject.Transparency = valueColorTriple[2][0]
                                    o.ViewObject.ShapeColor = (valueColorTriple[2][1],
                                                               valueColorTriple[2][2],
                                                               valueColorTriple[2][3])
                        if hasattr(o, attributName1) and hasattr(o, attributName2) and boolAttributIsInValueColor is False:
                            # pass
                            print('not found for Object : ', o.Name)
                            print('    ', attributName1, ' --> ', shapeAttributValue1)
                            print('    ', attributName2, ' --> ', shapeAttributValue2)
                            # farbe definieren, wenn der
                            # attributWert des ShapeAttributs nicht im Yamlfile ist


# ZEIGE NUR ELEMENTE DIE ortbeton aber kein npkbauteil haben

# TEXTUREN ###############
# info texturen, wenn nie textur gesetzt wurde, und es wird versucht diese zu loeschen
# dann stuerzt FreeCAD sofort ab.

# mwaende und decken selectieren (ein- ausschalten mit space)
# IfcObjectType
# IfcWallStandardCase, IfcSlab, IfcBeam, IfcColumn, IfcFooting

#-*- coding=utf8 -*-
import os
from PySide import QtCore, QtGui ,QtSvg
from Plot import getMainWindow	, getMdiArea


data = ['Member', 'Bar\nmark' , 'Type\nand\nSize','' , 'No of\nmbrs.' , 'No of bars in each' ,
        'Total No' , 'Length of each bar\n(mm)' , 'Shape\ncode',
        'A\n(mm)' , 'B\n(mm)' , 'C\n(mm)' , 'D\n(mm)' , 'E\n(mm)' ,
        'F\n(mm)' ,'G\n(mm)' ,'H\n(mm)' ,'R\n(mm)' ,
         'Weight\n(kg)' , 'Bar Shape']

class BarScheduleTableWidget(QtGui.QWidget):
    def __init__(self):
        #QtGui.QWidget.__init__(self, parent, flags)
        super(BarScheduleTableWidget, self).__init__()          
        self.initUI()
    
    def initUI(self): 
        self.setWindowTitle('Bars Schedule')
        self.tablewidget = QtGui.QTableWidget(2, 20)
        
        font = QtGui.QFont() ;font.setBold(True)
        for i in range(len(data)):
            item = QtGui.QTableWidgetItem(str(data[i]))
            self.tablewidget.setItem(0, i, item)
            self.tablewidget.item(0, i).setTextAlignment(
                  QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter )
            self.tablewidget.item(0, i).setFlags(QtCore.Qt.ItemIsEnabled)
            self.tablewidget.item(0, i).setFont(font)

        self.tablewidget.setColumnWidth(0, 100)
        self.tablewidget.setColumnWidth(1, 50)
        self.tablewidget.setColumnWidth(2, 60)
        self.tablewidget.setColumnWidth(3, 40)
        self.tablewidget.setColumnWidth(4, 60)
        self.tablewidget.setColumnWidth(5, 60)
        self.tablewidget.setColumnWidth(6, 60)
        self.tablewidget.setColumnWidth(7, 80)
        self.tablewidget.setColumnWidth(8, 50)  
        for i in range(9,19):
            self.tablewidget.setColumnWidth(i, 60)  
        self.tablewidget.setColumnWidth(18, 100)  
        self.tablewidget.setColumnWidth(19, 320)    
      
        self.tablewidget.setSpan(0,2,1,2)
        self.tablewidget.setRowHeight(0 , 75)

        layout = QtGui.QHBoxLayout()
        layout.addWidget(self.tablewidget)
        self.setLayout(layout)

        
        self.tablewidget.setIconSize(QtCore.QSize(300, 200))
        icon = QtGui.QIcon(os.path.split(os.path.abspath(__file__))[0] + "/images/11.svg" )
        entry = QtGui.QTableWidgetItem()
        entry.setSizeHint(QtCore.QSize(300, 200))
        entry.setIcon(icon)
        entry.setTextAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.tablewidget.setItem(1, 19, entry)
        self.tablewidget.setRowHeight(1,200)

    #data1 = {'member':'B01' , 'barmark':'1' , 'type':'SR24' , 'size':'12' ,
    #         'nummembers':1 , 'numbars':1 , 'shapecode':'11' , 'param':{'A':100 , 'B':1000}}
    def SetData(self,row,dataList):
        iRow = row
        if dataList.has_key('member'):
            entry = QtGui.QTableWidgetItem(dataList['member'])
            entry.setTextAlignment(QtCore.Qt.AlignHCenter)
            self.tablewidget.setItem(iRow, 0 ,entry )
        if dataList.has_key('barmark'):
            entry = QtGui.QTableWidgetItem(dataList['barmark'])
            entry.setTextAlignment(QtCore.Qt.AlignHCenter)
            self.tablewidget.setItem(iRow, 1 ,entry ) 
        if dataList.has_key('type'):
            entry = QtGui.QTableWidgetItem(dataList['type'])
            entry.setTextAlignment(QtCore.Qt.AlignHCenter)
            self.tablewidget.setItem(iRow, 2 ,entry )
        if dataList.has_key('size'):
            entry = QtGui.QTableWidgetItem(dataList['size'])
            entry.setTextAlignment(QtCore.Qt.AlignHCenter)
            self.tablewidget.setItem(iRow, 3 ,entry )
        if dataList.has_key('nummembers'):
            entry = QtGui.QTableWidgetItem( str( dataList['nummembers']) )
            entry.setTextAlignment(QtCore.Qt.AlignHCenter)
            self.tablewidget.setItem(iRow, 4 ,entry )           
        if dataList.has_key('numbars'):
            entry = QtGui.QTableWidgetItem( str( dataList['numbars']) )
            entry.setTextAlignment(QtCore.Qt.AlignHCenter)
            self.tablewidget.setItem(iRow, 5 ,entry )  
        if dataList.has_key('shapecode'):
            entry = QtGui.QTableWidgetItem( str( dataList['shapecode']) )
            entry.setTextAlignment(QtCore.Qt.AlignHCenter)
            self.tablewidget.setItem(iRow, 8 ,entry ) 
        for iCol in range(9,18):
            entry = QtGui.QTableWidgetItem( '-' )
            entry.setTextAlignment(QtCore.Qt.AlignHCenter)
            self.tablewidget.setItem(iRow, iCol ,entry )
            self.tablewidget.item(iRow, iCol).setFlags(QtCore.Qt.ItemIsEnabled)
        for param in dataList['param']:
            if param=='A':
                entry = QtGui.QTableWidgetItem( str( dataList['param'][param]) )
                entry.setTextAlignment(QtCore.Qt.AlignHCenter)
                self.tablewidget.setItem(iRow, 9 ,entry )
                self.tablewidget.item(iRow, 9).setFlags(QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled )
            elif param=='B':
                entry = QtGui.QTableWidgetItem( str( dataList['param'][param]) )
                entry.setTextAlignment(QtCore.Qt.AlignHCenter)
                self.tablewidget.setItem(iRow, 10 ,entry )
                self.tablewidget.item(iRow, 10).setFlags(QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled )

        
    def CalSheet(self):
        for row in range(1, self.tablewidget.rowCount()):
            numMbrs = int( self.tablewidget.item(row,4).text() )
            numBars = int( self.tablewidget.item(row,5).text() )
            entry = QtGui.QTableWidgetItem( str( numMbrs*numBars  ) )
            entry.setTextAlignment(QtCore.Qt.AlignHCenter)
            self.tablewidget.setItem(row, 6 ,entry ) 
            self.tablewidget.item(row, 6).setFlags(QtCore.Qt.ItemIsEnabled)
            
            shapeCode = self.tablewidget.item(row, 8).text()
            Msg('%s\n'%shapeCode)
            if (shapeCode=='00') or (shapeCode=='01'):
                A = self.tablewidget.item(row, 9).text()
                L = A
                entry = QtGui.QTableWidgetItem( str( L  ) )
                entry.setTextAlignment(QtCore.Qt.AlignHCenter)
                self.tablewidget.setItem(row, 7 ,entry ) 
                self.tablewidget.item(row, 7).setFlags(QtCore.Qt.ItemIsEnabled)
            
            elif shapeCode=='11':
                A = float( self.tablewidget.item(row, 9).text() )
                B = float( self.tablewidget.item(row, 10).text() )
                d = float( self.tablewidget.item(row, 3).text() )
                r = 1.5*d
                L = A+B-0.5*r-d
                entry = QtGui.QTableWidgetItem( str( L  ) )
                entry.setTextAlignment(QtCore.Qt.AlignHCenter)
                self.tablewidget.setItem(row, 7 ,entry ) 
                self.tablewidget.item(row, 7).setFlags(QtCore.Qt.ItemIsEnabled)        
            wtData = {'6':0.222 , '9':0.499 , '12':0.888 , '15':1.387 , '19':2.226 , '25':3.853}
            d =  self.tablewidget.item(row, 3).text() 
            wt = wtData[d]
            weight_eachBar = wt*L /1000.
            
            TotalWieght = weight_eachBar *numMbrs*numBars
            entry = QtGui.QTableWidgetItem( "%0.3f"%TotalWieght   )
            entry.setTextAlignment(QtCore.Qt.AlignHCenter)
            self.tablewidget.setItem(row, 18 ,entry ) 
            self.tablewidget.item(row, 18).setFlags(QtCore.Qt.ItemIsEnabled)

ShapeCode = {
  '00':{'formula':'L=A' , 'param':['A'] } ,
  '01':{'formula':'L=A' , 'param':['A'] } ,
  '11':{'formula':'L=A+B-0.5r-d' , 'param':['A','B'] } ,  
}



class RebarDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(RebarDialog, self).__init__(parent)
        self.initUI()
    
    def initUI(self):    
        codeNum = '00'
        self.setWindowTitle("Rebar Shape Data")
        memberLabel = QtGui.QLabel("Member<br />Name",self)
        memberLabel.setGeometry(10,0,50,50)
        barMarkLabel = QtGui.QLabel("Bar<br />mark",self)
        barMarkLabel.setGeometry(200,0,50,50)
        steelTypeLabel = QtGui.QLabel("Steel<br />Type",self)
        steelTypeLabel.setGeometry(400,0,50,50)

        barDiaLabel = QtGui.QLabel("Bar dia<br />in mm",self)
        barDiaLabel.setGeometry(550,0,50,50)
        numMemberLabel = QtGui.QLabel("No of<br />Members",self)
        numMemberLabel.setGeometry(10,45,60,50)
        numBarLabel = QtGui.QLabel("No of<br />bars in",self)
        numBarLabel.setGeometry(200,45,50,50)

        shapeCodeLabel = QtGui.QLabel("Shape Code",self)
        shapeCodeLabel.setGeometry(10,100,80,50)

        totalLengthLabel = QtGui.QLabel("Total Length",self)
        totalLengthLabel.setGeometry(10,320,100,50)




        self.imageLabel = QtGui.QLabel("image",self)
        self.imageLabel.setGeometry(170,115,300,200)
        self.imageLabel.setFrameShape(QtGui.QFrame.Panel)
        self.imageLabel.setFrameShadow(QtGui.QFrame.Plain)
        self.imageLabel.setLineWidth(1)
        self.imageLabel.setPixmap(QtGui.QPixmap(os.path.split(
            os.path.abspath(__file__))[0] + "/images/%s.svg"%codeNum))

        

        x = 550 ; y = 100
        ALabel = QtGui.QLabel("A = ",self)
        ALabel.setGeometry(x,y,50,50)
        BLabel = QtGui.QLabel("B = ",self)
        BLabel.setGeometry(x,y+30,50,50)
        CLabel = QtGui.QLabel("C = ",self)
        CLabel.setGeometry(x,y+30*2,50,50)
        DLabel = QtGui.QLabel("D = ",self)
        DLabel.setGeometry(x,y+30*3,50,50)
        ELabel = QtGui.QLabel("E = ",self)
        ELabel.setGeometry(x,y+30*4,50,50)
        FLabel = QtGui.QLabel("F = ",self)
        FLabel.setGeometry(x,y+30*5,50,50)
        GLabel = QtGui.QLabel("E = ",self)
        GLabel.setGeometry(x,y+30*6,50,50)
        HLabel = QtGui.QLabel("F = ",self)
        HLabel.setGeometry(x,y+30*7,50,50)
        RLabel = QtGui.QLabel("R = ",self)
        RLabel.setGeometry(x,y+30*8,50,50)

        #----------------------------------
        self.memberEdit = QtGui.QLineEdit(self)
        self.memberEdit.setGeometry(65,10,120,25)
        self.barMarkEdit = QtGui.QLineEdit(self)
        self.barMarkEdit.setGeometry(239,10,120,25)
        self.steelTypeCombo= QtGui.QComboBox(self)
        self.steelTypeCombo.setGeometry(440,10,80,25)
        self.steelTypeCombo.addItem( "SR24")
        self.steelTypeCombo.addItem( "SD30")
        self.steelTypeCombo.addItem( "SD40")
        self.steelTypeCombo.addItem( "SD50")

        self.barDiaCombo= QtGui.QComboBox(self)
        self.barDiaCombo.setGeometry(600,10,50,25)
        barSRDiaList = ['6','9','12','15','19','25']
        barSDDiaList = ['12','16','20','25','28','32']
        for dia in barSRDiaList:
            self.barDiaCombo.addItem( dia )

        self.numMemberSpin =QtGui.QSpinBox(self)
        self.numMemberSpin.setGeometry(75,57,70,25)
        self.numMemberSpin.setMaximum(1000)
        self.numMemberSpin.setMinimum(1)

        self.numBarSpin =QtGui.QSpinBox(self)
        self.numBarSpin.setGeometry(250,57,70,25)
        self.numBarSpin.setMaximum(1000)
        self.numBarSpin.setMinimum(1)

        self.shapeCodeCombo= QtGui.QComboBox(self)
        self.shapeCodeCombo.setGeometry(10,140,50,25)
        shapeCodeList = ['00','01','11']
        for code in shapeCodeList:
            self.shapeCodeCombo.addItem( code )

        
        x = 550; y =112
        self.AEdit = QtGui.QLineEdit(self)
        self.AEdit.setGeometry(x+30,y,100,25)
        self.BEdit = QtGui.QLineEdit(self)
        self.BEdit.setGeometry(x+30,y+30,100,25)
        self.CEdit = QtGui.QLineEdit(self)
        self.CEdit.setGeometry(x+30,y+30*2,100,25)
        self.DEdit = QtGui.QLineEdit(self)
        self.DEdit.setGeometry(x+30,y+30*3,100,25)
        self.EEdit = QtGui.QLineEdit(self)
        self.EEdit.setGeometry(x+30,y+30*4,100,25)
        self.FEdit = QtGui.QLineEdit(self)
        self.FEdit.setGeometry(x+30,y+30*5,100,25)
        self.GEdit = QtGui.QLineEdit(self)
        self.GEdit.setGeometry(x+30,y+30*6,100,25)
        self.HEdit = QtGui.QLineEdit(self)
        self.HEdit.setGeometry(x+30,y+30*7,100,25)
        self.REdit = QtGui.QLineEdit(self)
        self.REdit.setGeometry(x+30,y+30*8,100,25)

        self.totalLengthEdit = QtGui.QLineEdit(self)
        self.totalLengthEdit.setGeometry(100,332,400,25)
        self.totalLengthEdit.setEnabled(False)
        self.totalLengthEdit.setText( ShapeCode['00']['formula'] )

        self.OkButton = QtGui.QPushButton("OK",self)
        self.OkButton.setGeometry(450,405,100,35) 
        self.CancelButton = QtGui.QPushButton("Cancel",self)
        self.CancelButton.setGeometry(555,405,100,35) 
        
        self.memberEdit.setFocus() 
        self.setContentsMargins(100,100,100,100);

        # signal
        self.connect(self.shapeCodeCombo, 
           QtCore.SIGNAL("currentIndexChanged(const QString&)"), self.shapeCodeCombo_Change)

        self.shapeCodeCombo_Change()
    
    def shapeCodeCombo_Change(self):
        codeNumber  = self.shapeCodeCombo.currentText()
        #Msg("%s\n"%codeNumber)
        number = codeNumber
        if number=='01' : number='00'
        self.imageLabel.setPixmap(
               QtGui.QPixmap(os.path.split(os.path.abspath(__file__))[0] 
                  + "/images/%s.svg"%number))
        self.totalLengthEdit.setText( ShapeCode[codeNumber]['formula'] )
        
        """
        LineEditList = {'A',self.AEdit ,'B',self.BEdit ,
                        'C',self.CEdit ,'D',self.DEdit ,
                        'E',self.EEdit ,'F',self.FEdit ,
                        'G',self.GEdit ,'H',self.HEdit ,
                        'R',self.REdit  }
        for key in LineEditList:
            Msg("key=%s\n"%key)
            LineEditList[key].setEnabled(False)
        
        for ch in ShapeCode[codeNumber]['param']:
            Msg("ch=%s\n"%ch)
            LineEditList[ch].setEnabled(True)
        """
        self.AEdit.setEnabled(False)
        self.BEdit.setEnabled(False)
        self.CEdit.setEnabled(False)
        self.DEdit.setEnabled(False)
        self.EEdit.setEnabled(False)
        self.FEdit.setEnabled(False)
        self.GEdit.setEnabled(False)
        self.HEdit.setEnabled(False)
        self.REdit.setEnabled(False)   
        Msg(ShapeCode[codeNumber]); Msg('\n')
        for ch in ShapeCode[codeNumber]['param']:
            Msg("ch=%s\n"%ch)
            if ch=='A': self.AEdit.setEnabled(True)
            elif ch=='B': self.BEdit.setEnabled(True)
            elif ch=='C': self.CEdit.setEnabled(True)
            elif ch=='D': self.DEdit.setEnabled(True)
            elif ch=='E': self.EEdit.setEnabled(True)
            elif ch=='F': self.FEdit.setEnabled(True)
            elif ch=='G': self.GEdit.setEnabled(True)
            elif ch=='H': self.HEdit.setEnabled(True)
            elif ch=='R': self.REdit.setEnabled(True)

                

        
def test01():
    data1 = {'member':'B01' , 'barmark':'1' , 'type':'SR24' , 'size':'12' ,
             'nummembers':1 , 'numbars':1 , 'shapecode':'11' , 'param':{'A':100 , 'B':1000}}
    win = BarScheduleTableWidget()
    win.SetData(1 , data1)
    win.CalSheet()

    mdi = getMdiArea()
    sub = mdi.addSubWindow(win)
    sub.show()

def test02():
    form = RebarDialog()
    form.exec_()

if __name__ == "__main__":
    test01()
    
    test02()
    Msg('Done!\n')
# language: en

Feature: Base setup

In order to view the BIM data
As any interested stakeholder
We need an IFC file


Scenario: IFC data

 * IFC data must use the "IFC2X3" schema


Scenario: Project information

 * The project name, code, or short identifier must be "_HB_BIM_Vorlageprojekt"


Scenario: More tests

 * All "IfcWall" elements have a "Qto_WallBaseQuantities.Nettovolumen" quantity


Scenario: application

 * The IFC file must be exported by application full name "Allplan"
 * The IFC file must be exported by application identifier "Allplan"
 * The IFC file must be exported by the application version "2021.1"
 * IFC data header must have a file description of "('no view',)" such as the new Allplan IFC exporter creates it


Scenario: application
 * All buildings have an address
 # erfuellt, weil die datei keine buildings hat ;-)


Scenario: elementeigenschaften

 * There are exclusively "IfcWall" elements only
 * There are exclusively "IfcSlab" elements only
 * There are no "IfcBuildingElementProxy" elements
 * There are no "IfcBuildingElementProxy" elements because "they are not classified elements"
 # * All "IfcWall" elements class attributes have a value
 * All "IfcSlab" elements class attributes have a value
 # there are no slabs ;-)
 * All "IfcWall" elements have a name given
 * All "IfcSlab" elements have a description given
 # there are no slabs ;-)
 * All "IfcWall" elements have a name matching the pattern "Wand"
 * There is an "IfcWall" element with a "GlobalId" attribute with a value of "2ib_I1r0nD5PzTY8ooRKY8"


Scenario: Geometrie

 #* All "IfcWall" elements have an "IfcExtrudeAreaSolid" representation
 # es funktioniert nur IfcFacetedBrep, mal genauer schauen
 * All "IfcBuildingElementProxy" elements have an "IfcFacetedBrep" representation
 # not proxies vorhanden ...

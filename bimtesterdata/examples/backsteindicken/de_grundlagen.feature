# language: de

Funktionalität: Basisdaten

Um BIM-Daten anzusehen
Für alle beteiligten Akteure
Wir brauchen eine IFC-Datei


Szenario: Bereitstellen von IFC-Daten

 * Die IFC-Daten müssen das "IFC2X3" Schema benutzen


Szenario: Projektinformationen

 * The project name, code, or short identifier must be "_HB_BIM_Vorlageprojekt"

# BIMTesterData
## My own information about BIMTester are here
### BIMTester code in FreeCAD
+ ATM bimtester code is copied into the FreeCAD bimtester module for for the sake of convenience
+ TODO bimtester should be installed in conjunction with ifcopenshell
+ to /urs/local by make install


### Generall information
+ This applies to BlenderBIM BIMTester and FreeCAD BIMTester.
+ Neither the ifc nor the ifc path should contain special character like German Umlaute.
+ Neither import nor open a ifc in FreeCAD or BlenderBIM. There is no need for this to run BIMTester.
+ Get Schependomlaan from https://github.com/buildingSMART/Sample-Test-Files/blob/master/IFC%202x3/Schependomlaan/Design%20model%20IFC/IFC%20Schependomlaan.ifc


### BIMTester in FreeCAD:
+ Install module
+ Install missing dependencies on Linux.
    + Do not use behave from Debian buster repo (it is to old)
    + install behave with pip3
+ Install missing dependencies on Windows.
    + copy behave and pystache from BlenderBIM AddOn to FreCAD_xxx/bin/Lib/ 
    + behave could be installed with pip from FreeCAD
    + pystache install with FreeCAD pip results in an error
+ start FreeCAD, switch to BIMTester, the Gui will start
+ choose ifc file (the feature file directory is set to a very simple included example feature file)
+ click on run
+ :-)


### BIMTester in BlenderBIM
+ Install Blender and install BlenderBIM (see my blender notes)
+ Start Blender
+ Set path to feature files (set the path not the file)
+ choose the feature file to run with
   + either the ifc must be in the same directory as the feature file,
   + or the full path to the ifc has to be in the feature file
   + find a simple example in FreeCAD BIMTester module directory features_bimtester/fea_min/features/
   + or use these ones: https://wiki.osarch.org/index.php?title=Category:BIMTester
+ click execute
+ no matter which scenario is choosen, all scenarios will run (TODO)


### Behave code examples
+ directly run behave
+ TODO: add from my local machine


### Remaining issues
+ https://github.com/behave/behave/issues/264
+ https://github.com/behave/behave/issues/549


### Just to keep this code
+ in FreeCAD python console
```this_path = os.path.join(os.path.dirname(__file__))```


### Windows
##### From FreeCAD Python console
+ am besten FreeCADCmd starten, die gui wird nicht aktualisiert, daher heine prozessprints
```python
from os.path import join
bimtestercode = join("C:", os.sep, "Users", "BHA", "AppData", "Roaming", "FreeCAD", "Mod", "bimtester", "code_bimtester")
myargs = {
    "advanced_arguments": "",
    "console": False,
    "featuresdir": join(bimtestercode, "examples", "01_ifcschema_translated"),
    # "featuresdir": "C:/Users/BHA/AppData/Roaming/FreeCAD/Mod/bimtester/code_bimtester/examples/01_ifcschema_translated",
    "feature": "",
    "gui": False,
    "ifcfile": join(bimtestercode, "examples", "01_ifcschema_translated", "IFC2X3_col.ifc"),
    # "ifcfile": "C:/Users/BHA/AppData/Roaming/FreeCAD/Mod/bimtester/code_bimtester/examples/01_ifcschema_translated/IFC2X3_col.ifc",
    "purge": False,
    "path": "",
    "report": False,
    "report_after_run": False,
    "copyintemprun": True
}
from code_bimtester.bimtester.run import run_tests
report_file = run_tests(myargs)
from code_bimtester.bimtester.reports import generate_report
generate_report(report_file=report_file)

```

##### Gui from Python without dirs and with dirs
```python
import sys
sys.path.append("C:/Users/BHA/AppData/Roaming/FreeCAD/Mod/bimtester/code_bimtester")
from startbimtester import show_widget
show_widget()

import sys
sys.path.append("C:/Users/BHA/AppData/Roaming/FreeCAD/Mod/bimtester/code_bimtester")
from startbimtester import show_widget
show_widget("C:/Users/BHA/AppData/Roaming/FreeCAD/Mod/bimtesterdata/features_bimtesterdata/fea_min", "C:/Users/BHA/Desktop/geomtest/Wand_Decke.ifc")

```

##### Gui with Python startscript
```
python C:\Users\BHA\AppData\Roaming\FreeCAD\Mod\bimtester\code_bimtester\startbimtester.py -h
python C:\Users\BHA\AppData\Roaming\FreeCAD\Mod\bimtester\code_bimtester\startbimtester.py -g

python C:\Users\BHA\AppData\Roaming\FreeCAD\Mod\bimtester\code_bimtester\startbimtester.py -g C:/Users/BHA/AppData/Roaming/FreeCAD/Mod/bimtesterdata/features_bimtesterdata/fea_min C:/Users/BHA/Desktop/geomtest/Wand_Decke.ifc
```


### Linux
##### From FreeCAD Python console
```python
from code_bimtester.bimtester.run import run_all
myfeatures = "/home/hugo/.FreeCAD/Mod/bimtesterdata/features_bimtesterdata/fea_min"
myifcfile = "/home/hugo/Documents/zeug_sort/z_some_ifc/example_model.ifc"
run_all(myfeatures, myifcfile)

```

##### Gui from Python without dirs and with dirs
```python
import sys
sys.path.append("/home/hugo/.FreeCAD/Mod/bimtester/code_bimtester")
from startbimtester import show_widget
show_widget()

import sys
sys.path.append("/home/hugo/.FreeCAD/Mod/bimtester/code_bimtester")
from startbimtester import show_widget
show_widget("/home/hugo/.FreeCAD/Mod/bimtesterdata/features_bimtesterdata/fea_min", "/home/hugo/Documents/zeug_sort/z_some_ifc/example_model.ifc")

```

##### Gui with Python startscript
```
python3 /home/hugo/.FreeCAD/Mod/bimtester/code_bimtester/startbimtester.py -h
python3 /home/hugo/.FreeCAD/Mod/bimtester/code_bimtester/startbimtester.py -g

python3 /home/hugo/.FreeCAD/Mod/bimtester/code_bimtester/startbimtester.py -g /home/hugo/.FreeCAD/Mod/bimtesterdata/features_bimtesterdata/fea2_german /home/hugo/Documents/zeug_sort/z_some_ifc/example_model.ifc
python3 /home/hugo/.FreeCAD/Mod/bimtester/code_bimtester/startbimtester.py -g /home/hugo/.FreeCAD/Mod/bimtesterdata/features_bimtesterdata/fea2_german /home/hugo/Documents/zeug_sort/z_some_ifc/example_model.ifc
python3 /home/hugo/Documents/dev/ifcopenshell/ifcopenshell-bhb/ifcos/src/ifcbimtester/startbimtester.py -g /home/hugo/.FreeCAD/Mod/bimtesterdata/features_bimtesterdata/fea2_german /home/hugo/Documents/zeug_sort/z_some_ifc/example_model.ifc

cd ~/Documents/dev/ifcopenshell/ifcopenshell-bhb/ifcos
python3 ./src/ifcbimtester/startbimtester.py -g ~/.FreeCAD/Mod/bimtesterdata/features_bimtesterdata/fea2_german ~/Documents/zeug_sort/z_some_ifc/example_model.ifc

```

### Translation
+ https://jenisys.github.io/behave.example/tutorials/tutorial12.html

```python
behave --lang-list
behave --lang-help de
```


### Dokumentation
+ https://jenisys.github.io/behave.example/index.html
+ https://cucumber.io/docs/gherkin/
+ https://behat.org/en/latest/user_guide/gherkin.html
+ https://riptutorial.com/de/cucumber
+ get the translation words by `behave --lang-help de`

### Translation
#### links
+ https://behave.readthedocs.io/en/latest/api.html#step-macro-calling-steps-from-other-steps
+ https://stackoverflow.com/questions/46735425/how-to-only-show-failing-tests-from-behave
+ https://community.osarch.org/discussion/comment/4533/#Comment_4533
+ http://babel.pocoo.org/en/latest/cmdline.html


#### totally new translation
```
cd ~/Documents/dev/ifcopenshell/ifcopenshell-bhb/ifcos/src/ifcbimtester/bimtester
mkdir locale
pybabel extract . -o locale/messages.pot
pybabel init -l de -i locale/messages.pot -d locale
pybabel init -l fr -i locale/messages.pot -d locale
# edit po files, best with poedit (directly open and save it)
# mo will be created on save of po
```

#### update translation if NEW string in code
```
cd ~/Documents/dev/ifcopenshell/ifcopenshell-bhb/ifcos/src/ifcbimtester/bimtester
# edit pot file or create a new one with
pybabel extract . -o locale/messages.pot
# update po files without deleting existing entries
cd bimtester
pybabel update -i locale/messages.pot -d locale
# edit po files, best with poedit (directly open and save it)
# mo will be created on save of po
```

```
# in short
cd ~/Documents/dev/ifcopenshell/ifcopenshell-bhb/ifcos/src/ifcbimtester/bimtester
pybabel extract . -o locale/messages.pot
pybabel update -i locale/messages.pot -d locale
# edit po files with poedit
```

#### update translation if CHANGED string in code
+ AUFPASSEN stringanpassungen werden geloescht !!!!! 
+ Workflow bei stringanpassung testen
+ vorher po datei kopieren und dann manuell bearbeiten funktioniert ist aber aufwändig


#### recreate mo files from po files
```
cd ~/Documents/dev/ifcopenshell/ifcopenshell-bhb/ifcos/src/ifcbimtester/bimtester/locale
pybabel compile -d .

```


##### flake8
flake8 --builtins="_" geometric_detail_methods.py
import gettext  # noqa


### log file
#### info
+ see environment module for my implementation


##### logging the behave way
+ TODO read and may use 
+ https://stackoverflow.com/a/31545036
+ https://behave.readthedocs.io/en/latest/tutorial.html#environmental-controls
+ https://behave.readthedocs.io/en/latest/api.html#logging-setup

```
def before_all(context):
    # -- SET LOG LEVEL: behave --logging-level=ERROR ...
    # on behave command-line or in "behave.ini".
    context.config.setup_logging()
```


### Most needed command
```
cd ~/Documents/dev/ifcopenshell/ifcopenshell-bhb/ifcos/src/ifcbimtester/
python3  ./startbimtester.py  -t  -d ~/.FreeCAD/Mod/bimtesterdata/features_bimtesterdata/fea2_german  -i ~/Documents/zeug_sort/z_some_ifc/example_model.ifc

cd ~/Documents/dev/ifcopenshell/ifcopenshell-bhb/ifcos/src/ifcbimtester/
python3  ./startbimtester.py  -g  -d ~/.FreeCAD/Mod/bimtesterdata/features_bimtesterdata/fea2_german  -i ~/Documents/zeug_sort/z_some_ifc/example_model.ifc

cd ~/Documents/dev/ifcopenshell/ifcopenshell-bhb/ifcos/src/ifcbimtester/
python3  ./startbimtester.py  -t

cd ~/Documents/dev/ifcopenshell/ifcopenshell-bhb/ifcos/src/ifcbimtester/
python3  ./startbimtester.py  -t  -d ~/.FreeCAD/Mod/bimtesterdata/features_bimtesterdata/fea2_german

cd ~/Documents/dev/ifcopenshell/ifcopenshell-bhb/ifcos/src/ifcbimtester/
python3  ./startbimtester.py  -t  -i ./examples/01_ifcschema_translated/IFC2X3_col.ifc

cd ~/Documents/dev/ifcopenshell/ifcopenshell-bhb/ifcos/src/ifcbimtester/
python3  ./startbimtester.py  -g  -i ./examples/01_ifcschema_translated/IFC2X3_col.ifc

```


### Update ommands
+ in a bash
```
python3 /home/hugo/.FreeCAD/Mod/bimtester/code_bimtester/gui.py
```

+ outside FreeCAD in python console
```
import sys
sys.path.append("/home/hugo/.local/share/FreeCAD/Mod/bimtester/code_bimtester/")
# sys.path.append("/home/hugo/.FreeCAD/Mod/bimtester/code_bimtester/")
sys.path.append("C:/Users/BHA/AppData/Roaming/FreeCAD/Mod/bimtester/code_bimtester/")
from bimtester.guiwidget import run
run()

```

+ in FreeCADs python konsole
```
import sys
sys.path.append("/home/hugo/.local/share/FreeCAD/Mod/bimtester/code_bimtester/")
# sys.path.append("/home/hugo/.FreeCAD/Mod/bimtester/code_bimtester/")
sys.path.append("C:/Users/BHA/AppData/Roaming/FreeCAD/Mod/bimtester/code_bimtester/")
from bimtester.guiwidget import GuiWidgetBimTester
form = GuiWidgetBimTester()
form.show()

```

+ data to test
```
/home/hugo/.local/share/FreeCAD/Mod/bimtester/code_bimtester/examples/01_ifcschema_translated/features/de_grundlagen.feature
/home/hugo/.local/share/FreeCAD/Mod/bimtester/code_bimtester/examples/01_ifcschema_translated/IFC2X3_col.ifc
```


### Repos
##### bimtesterfc
+ user berndhahnebach
+ branch master
+ ohne BIMTestercode --> GPL2
+ Stand Januar 2020
+ wohl AddOn manager
+ Adresse github und gitlab ???

##### bimtesterfc
+ user wartburgritter
+ branch main
+ mit BIMTestercode --> Lizenz sollte GPL3 sein --> dann problemlos mit ifcos mgl, aber nicht mehr mit FreeCAD
+ mein aktuelles entwicklerrepo
+ https://gitlab.com/wartburgritter/bimtesterfc

##### ifcopenshell bernd
+ auf github, mein ifcos dev repo
+ dort branch btlatestdev2
+ ist ein commit vor offiziellem ifcos head (alle meine anpassungen)

##### fbjcode
+ meine fbj code auf priv gitlab


### Run from Python
+ https://community.osarch.org/discussion/comment/9690/#Comment_9690

```
cd /c/Users/BHA/Desktop
git clone https://github.com/IfcOpenShell/IfcOpenShell/
```

```
import os
from os.path import join
ifcos_main_path = join("C:", os.sep, "Users", "BHA", "AppData", "Roaming", "FreeCAD", "Mod") 
ifcbimtester_path = join(ifcos_main_path, "bimtester", "code_bimtester")
examples_path = join(ifcbimtester_path, "examples", "step_tests", "project")
feature_file_name = "project_de"
#feature_file_name = "project_en"
#feature_file_name = "fr_fondamentaux"
#feature_file_name = "it_fondamento"
#feature_file_name = "nl_grondslag"

args = {
    "action": "",
    "advanced_arguments": "",
    "console": False,
    "feature": join(examples_path, feature_file_name + ".feature"),
    "ifc": join(examples_path, "project_ok.ifc"),
    #"feature": "C:/0_BHA_privat/mypythonstuff/scriptBIMTesting/features_jpag/jpag_TRW/features",
    #"ifc": "C:/0_BHA_privat/BIMTesting3/99999/0_backsteindicken.ifc",
    "path": "",
    "report": "",
    "steps": "",
    "schema_file": "",
    "schema_name": "",
    "lang": "",
}

import sys
sys.path.append(ifcbimtester_path)
import bimtester.run
import bimtester.reports
report_json = bimtester.run.TestRunner(args["ifc"], args["schema_file"]).run(args)
report_json

report_html = join(os.path.dirname(os.path.realpath(report_json)), "report.html")
bimtester.reports.ReportGenerator().generate(report_json, report_html)
import webbrowser
webbrowser.open(report_html)

```


"""
 * The project must have an identifier of "2iAYrakL9FABNNwZfj$CbO"
"""

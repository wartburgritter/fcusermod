@german
@mycompany
@lang=de_de
Feature: Base setup

In order to view the BIM data
As any interested stakeholder
We need an IFC file


Scenario: Receiving a file

 * The IFC file "myifc.ifc" must be provided

 * The IFC file must be exported by application full name Allplan

 * The IFC file must be exported by application identifier Allplan

 * The IFC file must be exported by the application version 2020.1
 
 * IFC data must use the IFC2X3 schema

 * IFC data header must have a file description of ('no view',) such as the new Allplan IFC exporter creates it

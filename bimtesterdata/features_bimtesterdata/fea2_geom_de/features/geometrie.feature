# language: de

Funktionalität: Basisdaten

Um BIM-Daten anzusehen
Für alle beteiligten Akteure
Wir brauchen eine IFC-Datei


Szenario: Bereitstellen von IFC-Daten

 * The IFC file "myifc.ifc" must be provided
 * Die IFC Daten müssen das IFC2X3 Schema benutzen


Szenario: Geometrie Fehler
# * All {ifc_class} Bauteile müssen geometrische Repräsentationen ohne Fehler haben
 * Alle IfcBuildingElement Bauteile müssen geometrische Repräsentationen ohne Fehler haben


Szenario: Stelle die richtige geometrische Repräsentation für alle IFC-Bauteilklassen sicher
# * Alle {ifc_class} Bauteile müssen eine geometrische Repräsentation der Klasse {representation_class} verwenden
 * Alle IfcBuildingElementProxy Bauteile müssen eine geometrische Repräsentation der Klasse IfcFacetedBrep verwenden
# * Alle IfcBuildingElement Bauteile müssen eine geometrische Repräsentation der Klasse IfcFacetedBrep verwenden

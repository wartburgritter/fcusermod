# language: de

Funktionalität: Basisdaten

Um BIM-Daten anzusehen
Für alle beteiligten Akteure
Wir brauchen eine IFC-Datei


Szenario: Bereitstellen von IFC-Daten

 * The IFC file "myifc.ifc" must be provided
 # replacement des dateinamens funktioniert nicht nach sprache.
 # Das wird schwierig. Das muss einfach aktuell auf englisch bleiben
 #* Die IFC Datei muss "myifc.ifc" muss bereitgestellt werden

 * IFC data must use the IFC2X3 schema
 * Die IFC Daten müssen das IFC2X3 Schema benutzen

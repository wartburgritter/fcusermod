Feature: Project setup

In order to view the BIM data
As any interested stakeholder
We need an IFC file

Feature: Project setup

In order to ensure quality of the digital built environment
As a responsible digital citizen
We expect compliant OpenBIM deliverables


Scenario: Receiving a file
 * The IFC file "myifc.ifc" must be provided


Scenario: Project metadata is organised and correct
# * The project must have an identifier of 3K5pZ70qH4281XGJUqIX8A



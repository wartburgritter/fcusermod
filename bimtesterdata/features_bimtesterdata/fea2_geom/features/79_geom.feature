Feature: Geometric Representations

In order to correctly identify objects
As any interested stakeholder filtering objects for a particular purpose
All IFC elements must belong to the appropriate IFC class

Scenario: Receiving a file
 * The IFC file "myifc.ifc" must be provided
 * IFC data must use the IFC2X3 schema


Scenario: geometry_error
# * All {ifc_class} elements must have a geometric representation without errors
 * All IfcBuildingElement elements must have a geometric representation without errors

 
 Scenario: Ensure all IFC type elements have correct representation
# * All {ifc_class} elements have an {representation_class} representation
 * All IfcBuildingElementProxy elements have an IfcFacetedBrep representation
# * All IfcBuildingElement elements have an IfcFacetedBrep representation


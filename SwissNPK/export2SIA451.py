# wenn modul manuell geladen wird mit
# import export2SIA451
# braucht nach Aenderungen im modul in FreeCAD nicht neu gestartet zu werden wenn modul wieder geladen wird mit
# reload(export2SIA451)


import tempfile
import sys
from platform import system

import FreeCAD

print("SIA451 export imported")


def export2SIA451():
    import sia451parser
    '''
    export to SIA451
    '''
    print('Get the positionsmengenlist first\n')
    positionsmengenlist = get_positionsmengenlist()

    print('\nCreate appropriate list data for SIA451 export')
    # add to every position a position with 0 instead 1 and sort the list
    mytmplist = []
    for pm in sorted(positionsmengenlist):
        print(pm)
        if pm[0].endswith('1'):
            mytmp = pm[0].rstrip('1') + '0'
            # print(mytmp)
            mytmplist.append([mytmp, ''])

    positionsmengenlisttmp = positionsmengenlist + mytmplist
    positionsmengenlist = []
    for pm in sorted(positionsmengenlisttmp):
        # print(pm)
        positionsmengenlist.append(pm)

    print('\nExport to SIA451')
    if system() == "Linux":
        path_sep = "/"
    elif system() == "Windows":
        path_sep = "\\"
    else:
        path_sep = "/"
    sia_dir = FreeCAD.ConfigGet("UserAppData") + 'Mod' + path_sep + 'SwissNPK' + path_sep
    dataSia451 = sia_dir + 'data-katalog' + path_sep + 'bt.01s'  # r-positionen
    # outfile = sia_dir + 'sia-my.01s'
    outfile = tempfile.gettempdir() + path_sep + 'sia-my.01s'

    if sys.version_info.major >= 3:
        print('SIA export only with Python2!!!')
    else:
        print(outfile)
        wf = open(outfile, 'w')
        # writeSIA451FilePositionen(positionlist, dataSia451, wf)
        # wf.close()
        sia451parser.writeSIA451FileMengen(positionsmengenlist, dataSia451, wf)
        wf.close()
    return True


def get_positionsmengenlist():
    positionlist = []
    einzelpositionsmengenlist = []
    einzelpositionssumme = 0
    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name and hasattr(o, 'Shape'):
            if hasattr(o, 'NPKVolumenPosition') and o.NPKVolumenPosition:
                o.ViewObject.Visibility = True
                pos = o.NPKVolumenPosition.lstrip('241.').replace('.', '')
                vol = o.Shape.Volume * 1E-9
                einzelpositionsmengenlist.append([pos, vol])
                einzelpositionssumme += vol
                if pos not in positionlist:
                    positionlist.append(pos)
            else:
                o.ViewObject.Visibility = False  # deactivate all shapes without a volume position

    if not einzelpositionssumme:
        FreeCAD.Console.PrintError('einzelpositionssumme ist gleich null. Sind wirklich NPKVolumenPositionen vorhanden ?!\n')
        return False

    # print(sorted(positionlist))

    # mengen aufsummieren
    positionsmengenlist = []
    for p in sorted(positionlist):
        positionsumme = 0
        for epm in sorted(einzelpositionsmengenlist):
            if p == epm[0]:
                positionsumme += epm[1]
        positionsmengenlist.append([p, round(positionsumme, 2)])

    positionssummensumme = 0
    for pm in sorted(positionsmengenlist):
        # print(pm)
        positionssummensumme += pm[1]

    # print(einzelpositionssumme, '\n', positionssummensumme, '\n')
    # print(einzelpositionssumme/positionssummensumme, '\n')
    if 0.999 < positionssummensumme / einzelpositionssumme < 1.001:
        positionsmengenlist.append(['790000', ''])
        positionsmengenlist.append(['799001', round(positionssummensumme, 2)])

    return positionsmengenlist


def get_horizontal_area():
    '''
    import export2SIA451
    export2SIA451.get_horizontal_area()
    '''
    import propertytools
    import math
    propertytools.oneColor()
    einzelareamengen = []
    einzelabschalmengen = []
    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name and hasattr(o, 'Shape'):
            o.ViewObject.Visibility = False
            if hasattr(o, 'NPKVolumenPosition') and o.NPKVolumenPosition:
                if hasattr(o, 'NPKBauteil') and o.NPKBauteil:
                    height = 0
                    vol = 0
                    if o.NPKBauteil == 'Fundament':
                        o.ViewObject.Visibility = True
                        if 'NetVolume' in o.IfcImportedQuantities and o.IfcImportedQuantities['NetVolume']:
                            vol = float(o.IfcImportedQuantities['NetVolume'])
                        else:
                            some_quantity_missing(o)
                            continue
                        if 'Height' in o.IfcImportedQuantities and o.IfcImportedQuantities['Height']:
                            height = float(o.IfcImportedQuantities['Height'])
                        else:
                            some_quantity_missing(o)
                            continue
                    elif o.NPKBauteil == 'Decke':
                        o.ViewObject.Visibility = True
                        vol = float(o.IfcImportedQuantities['NetVolume'])
                        height = float(o.IfcImportedQuantities['Width'])

                    if height < 0.15 or height > 0.8:
                        print('unusual height: ', height)
                        o.ViewObject.ShapeColor = (1.0, 0.0, 0.0)  # white
                    else:
                        if vol > 0 and height > 0:
                            area = vol / height  # in Ifc file in m2 already
                            print(round(vol, 2), ' / ', height, ' = ', int(area))
                            einzelareamengen.append((o.NPKVolumenPosition, area))
                            abschalung = math.sqrt(area) * 4 * height
                            einzelabschalmengen.append((o.NPKVolumenPosition, abschalung))

    # ******************
    print('\n\n')
    areamengen = {}
    for p1 in sorted(einzelareamengen):
        if p1[0] not in areamengen:
            areamengen[p1[0]] = p1[1]
        else:
            areamengen[p1[0]] += p1[1]

    for p in sorted(areamengen):
        print(p, ' --> ', areamengen[p])

    # kontrolle
    areasumme = 0
    for p in einzelareamengen:
        areasumme += p[1]
    print(round(areasumme, 2))

    ###
    print('\n')
    abschalmengen = {}
    for p1 in sorted(einzelabschalmengen):
        if p1[0] not in abschalmengen:
            abschalmengen[p1[0]] = p1[1]
        else:
            abschalmengen[p1[0]] += p1[1]

    for p in sorted(abschalmengen):
        print(p, ' --> ', abschalmengen[p])

    # kontrolle
    abschalsumme = 0
    for p in einzelabschalmengen:
        abschalsumme += p[1]
    print(round(abschalsumme, 2))


def some_quantity_missing(freecad_object):
    o = freecad_object
    print(o.IfcImportedQuantities)
    o.ViewObject.ShapeColor = (1.0, 1.0, 1.0)  # white
    o.ViewObject.Transparency = 0

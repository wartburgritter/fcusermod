# file generated by npk_codegenerator.py


def setNPKVolumenPositionNo(o):
    '''position after position is checked if it fit with the positions from the given list

    code generated by npk_codegenerator.py
    license information is still missing
    '''
    import npk_getter
    import npk_setter

    # 241.611.201
    print('  241.611.201')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'Fundament':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieFundament'):
                    valueNPKKategorieFundament = npk_getter.getNPKKategorieFundament(o)
                    if valueNPKKategorieFundament:
                        npk_setter.setNPKKategorieFundament(o, valueNPKKategorieFundament)
                        print('    Set NPKKategorieFundament to ', valueNPKKategorieFundament)
                if hasattr(o, 'NPKKategorieFundament'):

                    if o.NPKKategorieFundament == 'Einzelfundament':
                        print('    Ebene III --> ', o.NPKKategorieFundament)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.611.201'

    # 241.613.201
    print('  241.613.201')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'Fundament':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieFundament'):
                    valueNPKKategorieFundament = npk_getter.getNPKKategorieFundament(o)
                    if valueNPKKategorieFundament:
                        npk_setter.setNPKKategorieFundament(o, valueNPKKategorieFundament)
                        print('    Set NPKKategorieFundament to ', valueNPKKategorieFundament)
                if hasattr(o, 'NPKKategorieFundament'):

                    if o.NPKKategorieFundament == 'Streifenfundament':
                        print('    Ebene III --> ', o.NPKKategorieFundament)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.613.201'

    # 241.615.301
    print('  241.615.301')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'Fundament':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieFundament'):
                    valueNPKKategorieFundament = npk_getter.getNPKKategorieFundament(o)
                    if valueNPKKategorieFundament:
                        npk_setter.setNPKKategorieFundament(o, valueNPKKategorieFundament)
                        print('    Set NPKKategorieFundament to ', valueNPKKategorieFundament)
                if hasattr(o, 'NPKKategorieFundament'):

                    if o.NPKKategorieFundament == 'Bodenplatte':
                        print('    Ebene III --> ', o.NPKKategorieFundament)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.615.301'

    # 241.615.311
    print('  241.615.311')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'Fundament':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieFundament'):
                    valueNPKKategorieFundament = npk_getter.getNPKKategorieFundament(o)
                    if valueNPKKategorieFundament:
                        npk_setter.setNPKKategorieFundament(o, valueNPKKategorieFundament)
                        print('    Set NPKKategorieFundament to ', valueNPKKategorieFundament)
                if hasattr(o, 'NPKKategorieFundament'):

                    if o.NPKKategorieFundament == 'FundamentGeneigt':
                        print('    Ebene III --> ', o.NPKKategorieFundament)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.615.311'

    # 241.631.401
    print('  241.631.401')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'Wand':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieWand'):
                    valueNPKKategorieWand = npk_getter.getNPKKategorieWand(o)
                    if valueNPKKategorieWand:
                        npk_setter.setNPKKategorieWand(o, valueNPKKategorieWand)
                        print('    Set NPKKategorieWand to ', valueNPKKategorieWand)
                if hasattr(o, 'NPKKategorieWand'):

                    if o.NPKKategorieWand == 'Standardwand':
                        print('    Ebene III --> ', o.NPKKategorieWand)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.631.401'

    # 241.631.411
    print('  241.631.411')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'Wand':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieWand'):
                    valueNPKKategorieWand = npk_getter.getNPKKategorieWand(o)
                    if valueNPKKategorieWand:
                        npk_setter.setNPKKategorieWand(o, valueNPKKategorieWand)
                        print('    Set NPKKategorieWand to ', valueNPKKategorieWand)
                if hasattr(o, 'NPKKategorieWand'):

                    if o.NPKKategorieWand == 'Sichtbeton':
                        print('    Ebene III --> ', o.NPKKategorieWand)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.631.411'

    # 241.631.421
    print('  241.631.421')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'Wand':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieWand'):
                    valueNPKKategorieWand = npk_getter.getNPKKategorieWand(o)
                    if valueNPKKategorieWand:
                        npk_setter.setNPKKategorieWand(o, valueNPKKategorieWand)
                        print('    Set NPKKategorieWand to ', valueNPKKategorieWand)
                if hasattr(o, 'NPKKategorieWand'):

                    if o.NPKKategorieWand == 'Kratzbeton':
                        print('    Ebene III --> ', o.NPKKategorieWand)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.631.421'

    # 241.651.301
    print('  241.651.301')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'Stuetze':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieStuetze'):
                    valueNPKKategorieStuetze = npk_getter.getNPKKategorieStuetze(o)
                    if valueNPKKategorieStuetze:
                        npk_setter.setNPKKategorieStuetze(o, valueNPKKategorieStuetze)
                        print('    Set NPKKategorieStuetze to ', valueNPKKategorieStuetze)
                if hasattr(o, 'NPKKategorieStuetze'):

                    if o.NPKKategorieStuetze == 'Standardstuetze':
                        print('    Ebene III --> ', o.NPKKategorieStuetze)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.651.301'

    # 241.721.301
    print('  241.721.301')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'Decke':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieDecke'):
                    valueNPKKategorieDecke = npk_getter.getNPKKategorieDecke(o)
                    if valueNPKKategorieDecke:
                        npk_setter.setNPKKategorieDecke(o, valueNPKKategorieDecke)
                        print('    Set NPKKategorieDecke to ', valueNPKKategorieDecke)
                if hasattr(o, 'NPKKategorieDecke'):

                    if o.NPKKategorieDecke == 'Deckenplatte':
                        print('    Ebene III --> ', o.NPKKategorieDecke)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.721.301'

    # 241.721.311
    print('  241.721.311')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'Decke':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieDecke'):
                    valueNPKKategorieDecke = npk_getter.getNPKKategorieDecke(o)
                    if valueNPKKategorieDecke:
                        npk_setter.setNPKKategorieDecke(o, valueNPKKategorieDecke)
                        print('    Set NPKKategorieDecke to ', valueNPKKategorieDecke)
                if hasattr(o, 'NPKKategorieDecke'):

                    if o.NPKKategorieDecke == 'DeckeGeneigt':
                        print('    Ebene III --> ', o.NPKKategorieDecke)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.721.311'

    # 241.722.301
    print('  241.722.301')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'Decke':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieDecke'):
                    valueNPKKategorieDecke = npk_getter.getNPKKategorieDecke(o)
                    if valueNPKKategorieDecke:
                        npk_setter.setNPKKategorieDecke(o, valueNPKKategorieDecke)
                        print('    Set NPKKategorieDecke to ', valueNPKKategorieDecke)
                if hasattr(o, 'NPKKategorieDecke'):

                    if o.NPKKategorieDecke == 'Kragplatte':
                        print('    Ebene III --> ', o.NPKKategorieDecke)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.722.301'

    # 241.725.001
    print('  241.725.001')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'Decke':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieDecke'):
                    valueNPKKategorieDecke = npk_getter.getNPKKategorieDecke(o)
                    if valueNPKKategorieDecke:
                        npk_setter.setNPKKategorieDecke(o, valueNPKKategorieDecke)
                        print('    Set NPKKategorieDecke to ', valueNPKKategorieDecke)
                if hasattr(o, 'NPKKategorieDecke'):

                    if o.NPKKategorieDecke == 'Schachtkopfplatte':
                        print('    Ebene III --> ', o.NPKKategorieDecke)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.725.001'

    # 241.745.021
    print('  241.745.021')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'UnterzugBruestungTraeger':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieUnterzug'):
                    valueNPKKategorieUnterzug = npk_getter.getNPKKategorieUnterzug(o)
                    if valueNPKKategorieUnterzug:
                        npk_setter.setNPKKategorieUnterzug(o, valueNPKKategorieUnterzug)
                        print('    Set NPKKategorieUnterzug to ', valueNPKKategorieUnterzug)
                if hasattr(o, 'NPKKategorieUnterzug'):

                    if o.NPKKategorieUnterzug == 'Sichtbeton':
                        print('    Ebene III --> ', o.NPKKategorieUnterzug)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.745.021'

    # 241.746.301
    print('  241.746.301')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'UnterzugBruestungTraeger':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieUnterzug'):
                    valueNPKKategorieUnterzug = npk_getter.getNPKKategorieUnterzug(o)
                    if valueNPKKategorieUnterzug:
                        npk_setter.setNPKKategorieUnterzug(o, valueNPKKategorieUnterzug)
                        print('    Set NPKKategorieUnterzug to ', valueNPKKategorieUnterzug)
                if hasattr(o, 'NPKKategorieUnterzug'):

                    if o.NPKKategorieUnterzug == 'Standard':
                        print('    Ebene III --> ', o.NPKKategorieUnterzug)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.746.301'

    # 241.746.321
    print('  241.746.321')
    if not hasattr(o, 'NPKBeton'):
        valueNPKBeton = npk_getter.getNPKBeton(o)
        if valueNPKBeton:
            npk_setter.setNPKBeton(o, valueNPKBeton)
            print('    Set NPKBeton to ', valueNPKBeton)
    if hasattr(o, 'NPKBeton'):

        if o.NPKBeton == 'Ortbeton':
            print('    Ebene I   --> ', o.NPKBeton)
            if not hasattr(o, 'NPKBauteil'):
                valueNPKBauteil = npk_getter.getNPKBauteil(o)
                if valueNPKBauteil:
                    npk_setter.setNPKBauteil(o, valueNPKBauteil)
                    print('    Set NPKBauteil to ', valueNPKBauteil)
            if hasattr(o, 'NPKBauteil'):

                if o.NPKBauteil == 'UnterzugBruestungTraeger':
                    print('    Ebene II  --> ', o.NPKBauteil)
                if not hasattr(o, 'NPKKategorieUnterzug'):
                    valueNPKKategorieUnterzug = npk_getter.getNPKKategorieUnterzug(o)
                    if valueNPKKategorieUnterzug:
                        npk_setter.setNPKKategorieUnterzug(o, valueNPKKategorieUnterzug)
                        print('    Set NPKKategorieUnterzug to ', valueNPKKategorieUnterzug)
                if hasattr(o, 'NPKKategorieUnterzug'):

                    if o.NPKKategorieUnterzug == 'Kratzbeton':
                        print('    Ebene III --> ', o.NPKKategorieUnterzug)
                        print('    !!! Y E A H    F O U N D    T H E    N P K - P O S I T I O N    N U M B E R !!!')
                        if not hasattr(o, 'NPKVolumenPosition'):
                            o.addProperty('App::PropertyString', 'NPKVolumenPosition', 'NPK', 'was das denn')
                        o.NPKVolumenPosition = '241.746.321'
    return True

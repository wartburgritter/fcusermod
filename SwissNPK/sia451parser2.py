# ***************************************************************************
# *   Copyright (c) 2022 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

import json
import os
import pandas as pd
import sys
import yaml
from os.path import join

import FreeCAD


# ************************************************************************************************
# see create_LV_data for data modell of org_npk_data and out_npk_data
# see create_LV_data for TODOs


# ************************************************************************************************
def export2SIA451(npk_lib_path=None, sia451_out_path=None, org_npk_data=None, out_pre_name=""):
    """
    export to SIA451
    """

    print("Start sia451parser2")

    # get the lib data (npk position strings)
    lib_npk_data = read_lib_npk_data(npk_lib_path)

    # get all used positions out of npk data and npk lib
    # print some information, TODO write into log yaml or cvs
    print("Do some diff with position ids from library and from real data")
    position_ids_lib_228 = list(lib_npk_data["228_22"].keys())
    if None in position_ids_lib_228:
        position_ids_lib_228.remove(None)  # TODO: Why is there None in lib posid dict?
    position_ids_data_228 = sorted(list(org_npk_data["228_22"].keys()))
    pos_ids_lib_minus_data = sorted(set(position_ids_lib_228) - set(position_ids_data_228))
    pos_ids_data_minus_lib = sorted(set(position_ids_data_228) - set(position_ids_lib_228))

    pos_diff_out_path = join(os.path.dirname(
        sia451_out_path), out_pre_name + "_NPK_Position_IDs_Diff.txt"
    )
    pos_diff_out_handle = open(pos_diff_out_path, "w")

    pos_diff_out_handle.write("PosNr of NPK228 in data not in lib (Should be empty!):\n")
    # pos_diff_out_handle.write("{}\n\n".format(pos_ids_data_minus_lib))
    yaml.dump(pos_ids_data_minus_lib, pos_diff_out_handle, default_flow_style=False)

    pos_diff_out_handle.write("\n\nPosNr of NPK228 in lib not in data:\n")
    # pos_diff_out_handle.write("{}\n\n".format(pos_ids_lib_minus_data))
    yaml.dump(pos_ids_lib_minus_data, pos_diff_out_handle, default_flow_style=False)

    pos_diff_out_handle.write("\n\nData ... PosNr of NPK228:\n")
    # pos_diff_out_handle.write("{}\n\n".format(position_ids_data_228))
    yaml.dump(position_ids_data_228, pos_diff_out_handle, default_flow_style=False)

    pos_diff_out_handle.write("\n\nLib ... PosNr of NPK228:\n")
    # pos_diff_out_handle.write("{}\n\n".format(position_ids_lib_228))
    yaml.dump(position_ids_lib_228, pos_diff_out_handle, default_flow_style=False)


    pos_diff_out_handle.close()

    # init sia451 export data dict
    sia451_str_data = {}
    
    # (A) add header
    sia451_str_data["Header"] = get_header()
    
    # (B) add gliederungen
    sia451_str_data["Structure"] = get_structure()

    # (C) add konditionen
    sia451_str_data["Contitions"] = get_contitions()

    # (G0) add pre positions
    # baustelleneinrichtung und die ersten zwei zeilen von katalog 228
    # HAAACK **************
    sia451_str_data["PrePositions"] = get_prepositions()

    # (G) add position according NPK Bau, Standard Leistungsverzeichnis
    # TODO das geht nicht durch iterieren und daten hinzufuegen, 
    # aktuell doch am besten ein neues anlegen ... mit klasse geht das evtl.
    # ist auch gut so, dann nur die pos, von denen auch ein string in bibliothek gefunden wurde
    out_npk_data = {}
    for modell_data_catalog_id, modell_data_catalog_pos in org_npk_data.items():
        if modell_data_catalog_id in lib_npk_data:
            out_npk_data[modell_data_catalog_id] = {}
            for modell_data_pos_id, modell_data_posdata in modell_data_catalog_pos.items():
                pos_id = modell_data_pos_id
                if pos_id in lib_npk_data[modell_data_catalog_id]:
                    pos_data = org_npk_data[modell_data_catalog_id][pos_id]
                    pos_string_lib = lib_npk_data[modell_data_catalog_id][pos_id]
                    # fill in menge je gliederung, delete mengenzeilen und generiere diese neu
                    pos_string_out = ""
                    # delete mengen lines if there are some in the library data
                    for ln in pos_string_lib.splitlines():
                        # print(ln)
                        # print(ln[41])
                        if ln[41] != "6":
                            pos_string_out += (ln + "\n")
                    # generate the mengen lines
                    for teil_prj_name, teil_prj_menge in sorted(pos_data["Mengen"].items()):
                        six_char_len_teil_prj_name = teil_prj_name[0:7].ljust(6)
                        # menge hat 13 stellen, 3 kommastellen, ohne komma von hinten geschrieben
                        # 0000000001234 == 1.234
                        mengenstring = format(1000 * teil_prj_menge, ".0f").zfill(13)
                        # print(mengenstring)
                        # print(len(six_char_len_teil_prj_name))
                        # print(teil_prj_name, teil_prj_menge)
                        # G228   121101    EWZ                     6 A+0000000000000
                        # 
                        mengen_line = "G{}   {}    {}{}6 A+{}\n".format(
                            modell_data_catalog_id[0:3],
                            pos_id,
                            six_char_len_teil_prj_name,
                            18*" ",
                            mengenstring, # 13*"0"
                        )
                        # print(mengen_line)
                        pos_string_out += mengen_line
                    pos_data["SIA451OutString"] = pos_string_out
                    out_npk_data[modell_data_catalog_id][pos_id] = pos_data

    sia451_str_data["Positions"] = ""
    # the data needs to be sorted
    for catalog_id in sorted(out_npk_data.keys()):
        catalog_data = out_npk_data[catalog_id]
        for position_id in sorted(catalog_data.keys()):
            position_data = catalog_data[position_id]
            # print(catalog_id, position_id)
            # print(position_data["SIA451OutString"])
            # print(3*"")
            sia451_str_data["Positions"] += position_data["SIA451OutString"]
            # print("Exported: {}.{} Version(20{})".format(catalog_id[0:3], position_id, catalog_id[4:6]))

    # old the real string from Vorlagedatei
    # sia451_str_data["Positions"] = get_positions()

    # create all out string to count the lines
    all_out_string = "{}{}{}{}{}".format(
        sia451_str_data["Header"],
        sia451_str_data["Structure"],
        sia451_str_data["Contitions"],
        sia451_str_data["PrePositions"],
        sia451_str_data["Positions"],
    )
    lines_count = len(all_out_string.splitlines()) + 1
    # print("The export will have {} lines.".format(lines_count))

    # print(all_out_string)
    # print(sia451_str_data["PrePositions"])
    print(3*"")
    # print(sia451_str_data["Positions"])


    # (Z) add footer data
    tmp_footer = get_footer()
    # print(len(tmp_footer))
    # print(tmp_footer[0:90])
    # sys.stdout.flush()
    # print(tmp_footer)
    # print(tmp_footer.rstrip())
    sys.stdout.flush()
    # im BIMTester absturz bei print der sonderzeichen, in FreeCAD nicht.

    sia451_str_data["Footer"] = "{}{} {}".format(
        tmp_footer[0:46],
        str(lines_count).zfill(12),
        tmp_footer[59:],
    )
    # print(sia451_str_data["Footer"])
    all_out_string += sia451_str_data["Footer"]
    
    # print("Decode and Encode")
    # all_out_string.encode("iso-8859-1")  # convert to iso-8859-1
    # glaube nicht noetig, wenn die outdatei mit dem richtigen encoding angegeben ist
    # passiert das automatisch
    # write sia451 data string to file
    print("Write SIA451 file to: {}".format(sia451_out_path))
    # wf = open(sia451_out_path, "w", encoding="utf-8")  # Dartus und BauPlus geben Fehler aus
    wf = open(sia451_out_path, "w", encoding="iso-8859-1")
    wf.write(all_out_string)
    wf.close()
    return True


# ************************************************************************************************
def read_lib_npk_data(npk_lib_path):
    # read library data
    # vorerst nur string speichern
    # data_structure = {
    #     "Katalognummer_Jahr": {
    #         "pos_id": "Positionsstring",
    #         "pos_id": "Positionsstring",
    #         ...
    #         "pos_id": "Positionsstring",
    # 
    #     }
    #     "Katalognummer_Jahr": {
    #         "pos_id": "Positionsstring",
    #         "pos_id": "Positionsstring",
    #         ...
    #         "pos_id": "Positionsstring",
    # 
    #     }
    # }
    lib_npk_data = {}

    # record_art == G and record_typ == 2 is a new position
    # TODO separate method
    rf = open(npk_lib_path, "r", encoding="iso-8859-1")
    line_nr = 0
    catalog_number = None
    catalog_year = None
    position_number = None
    position_string = None
    new_pos_id_on_recordtyp2 = True
    reading_positions = False
    # Rekordart und Rekordtyp
    known_recordarts = ["A", "B", "C", "G", "Z"]
    known_recordtyps = {
        "A": [None],
        "B": ["0", "2"],
        "C": ["0", "1"],
        "G": ["1", "2", "3", "5", "6"],
        "Z": [None]
    }
    for ln in rf:

        line_nr += 1

        # get record_art and record_typ
        record_art = ln[0]
        record_typ = ln[41]
        # print("art and type in file: {}: {}".format(record_art, record_typ))
        if record_art == "A" or record_art == "Z":
            record_typ = None
        if record_art not in known_recordarts:
            print(
                "Error {}: {}, not known record art '{}' in line number {}."
                .format(record_art, record_typ, record_art, line_nr)
            )
            continue
        if record_typ not in known_recordtyps[record_art]:
            print(
                "Error {}: {}, not known record typ '{}' for record art '{}' in line number {}."
                .format(record_art, record_typ, record_typ, record_art, line_nr)
            )

        # Katalognummer
        if record_art == "G" and record_typ == "1":
            reading_positions = True
            catalog_number = ln[1:4]
            catalog_year = ln[5:7]
            catalog_id = "{}_{}".format(catalog_number, catalog_year)
            position_number = None
            new_pos_id_on_recordtyp2 = True
            # print("Katalognummer: {}".format(catalog_number))
            # print(ln)
            if catalog_id not in lib_npk_data:
                lib_npk_data[catalog_id] = {}
            else:
                print("Error in catalog id. Should not happen.")

        # Positionsnummer
        # get position_number
        # get position_string (all lines as string)
        # TODO:
        # ATM when user text in xxx.xx0 or xxx.x00 or similar position
        # these lines belong to the next position
        # if this is missing in data these lines are missing in export
        elif record_art == "G" and record_typ == "2" and new_pos_id_on_recordtyp2 is True:
            # the last record will not be saved, see next elif
            # new position found, thus the old is completely finished, save it
            # print("Posnr: {}.{} ({}):".format(catalog_number, position_number, catalog_year))
            # print("{}\n".format(position_string))
            if position_number not in lib_npk_data[catalog_id]:
                lib_npk_data[catalog_id][position_number] = position_string
            else:
                print("Error in positionsnummer. should not happen: {}".format(ln))

            # set new_pos_id_on_recordtyp2 to False
            new_pos_id_on_recordtyp2 = False
            # init pos number and init position string
            position_number = ""
            position_string = ln
            # print("Positionsnummer: {}".format(position_number))
            # print(ln)
        elif record_art == "Z":
            # save the latest position as save only if a new starts
            if position_number not in lib_npk_data[catalog_id]:
                lib_npk_data[catalog_id][position_number] = position_string
            else:
                print("Error in positionsnummer. should not happen: {}".format(ln))
            # as file is at end no need to reinit values
        elif record_art == "G" and record_typ == "2" and new_pos_id_on_recordtyp2 is False:
            position_string += ln
        elif record_art == "G" and record_typ == "3":
            position_string += ln
        elif record_art == "G" and record_typ == "5":
            position_string += ln
        elif record_art == "G" and record_typ == "6":
            # next record_typ 2 will be a new position
            new_pos_id_on_recordtyp2 = True
            position_number = ln[7:13]
            position_string += ln
        elif record_art == "G":
            # never seen record_art 4 or greater 6
            print("Error in position id. Should not happen. Not known record typ for record art G: {}".format(ln))

        
    # for catid, catdata in lib_npk_data.items():
    #     for posnum, posstr in catdata.items():
    #         print(catalog_id, posnum)
    #         print(posstr)

    # import json
    # print(json.dumps(lib_npk_data, indent=4))
    
    return lib_npk_data


# ************************************************************************************************
def get_header():
    return """A040717451-9211  Stadt ZrichFlckiger+Bo1ABC                 000000000001000000219104      Mhlackerstrasse              19104.5.1      Strassenbau, BehiG Bushaltestea/m/t software servi+41522132313        DARTUS5 Version=5.0.0.1 (2020)
"""


def get_structure():
    return """B002                                     0
B002             EWZ                     2
B002             TAZ                     2                                                  Strassenbau
B002             WVZ                     2
"""


def get_contitions():
    return """C000                                     0  +000000000000000
C001101        00                        1 %+000000000000000+             -000000           Rabatt
C002302        01                  0000301 %+000000000000000+             -000000           Skonto
C003203        02                        1 %+000000000000000+             +000770           Mehrwertsteuer
"""


def get_footer():
    from datetime import datetime
    date_today = datetime.today().strftime("%d%m%y")
    footer = (
        "Z{}110817                                0000000009999   -000000000001       19104      "
        "Muehlackerstrasse                             Flueckiger+Bosshard AG         (unbekannt)         (unbekannt)         (unbekannt)\n"
        .format(date_today)
    )
    return footer
# """Z180822110817                                0000000000389   -000000000001       19104      Muehlackerstrasse                             Flueckiger+Bosshard AG         (unbekannt)         (unbekannt)         (unbekannt)"""
# """Z180822110817                                0000000000389   -000000000001       19104      Mhlackerstrasse                             Flckiger+Bosshard AG         (unbekannt)         (unbekannt)         (unbekannt)
 

def get_prepositions():
    return """G102 22                                  1                                                  Besondere Bestimmungen
G102   000100                            2                                                  Kurztext-Leistungsverzeichnis
G102   0001000101                        3
G228 22                                  1                                                  Zusammengefasste Leistungen im               Strassen- und Leitungsbau
"""


def get_positions():
    return """G228   111001                            2                                                  f.Dauer Leistungen Unternehmer
G228   111001                            5                gl +
G228   111001    WVZ                     6 A+0000000000000
G228   111001    TAZ                     6 A+0000000000000
G228   111001    EWZ                     6 A+0000000000000
G228   121101                            2                                                  fr FG
G228   1211010101                        3
G228   1211010102                        3                                                  Wahl Unternehmer
G228   1211010301                        3                                                       pauschal
G228   121101                            5                LE +
G228   121101    TAZ                     6 A+0000000000000
G228   121101    WVZ                     6 A+0000000000000
G228   121101    EWZ                     6 A+0000000000000
G228   121211                            2                                                  fr FZ 3.5t
G228   1212110101                        3
G228   1212110102                        3                                                  Wahl Unternehmung
G228   1212110301                        3                                                       pauschal
G228   121211                            5                LE +
G228   121211    WVZ                     6 A+0000000000000
G228   121211    TAZ                     6 A+0000000000000
G228   121211    EWZ                     6 A+0000000000000
G228   121221                            2                                                  fr FZ bis 40 t
G228   1212210101                        3
G228   1212210102                        3                                                  Wahl Unternehmung
G228   1212210301                        3                                                       pauschal
G228   121221                            5                LE +
G228   121221    WVZ                     6 A+0000000000000
G228   211102                            2                                                  Belagsdicke mm 51-100
G228   211102                            5                m  +
G228   211102    TAZ                     6 A+0000000000000
G228   211103                            2                                                  Belagsdicke mm 101-150
G228   211103                            5                m  +
G228   211103    EWZ                     6 A+0000000000000
G228   211103    WVZ                     6 A+0000000000000
G228   211103    TAZ                     6 A+0000000000000
G228   231002                            2                                                  Spezifikation
G228   2310020101                        3                                                  Pumpentyp ist dem Unternehmer
G228   2310020102                        3                                                  frei gestellt,
G228   2310020201                        3
G228   2310020202                        3                                                  m bis 5 m
G228   2310020401                        3                                                       globale
G228   2310020501                        3                                                  fr die Dauer der Leistung
G228   2310020502                        3                                                  des Unternehmers
G228   2310020503                        3                                                  Inkl. Erstellen von
G228   2310020504                        3                                                  Pumpensmpfe innerhalb des
G228   2310020505                        3                                                  Leitungsgrabens
G228   231002                            5                LE +
G228   231002    TAZ                     6 A+0000000000000
G228   231002    WVZ                     6 A+0000000000000
G228   243111                            2                                                  Abtragsdicke bis m 0,20
G228   243111                            5                m3 +
G228   243111    TAZ                     6 A+0000000000000
G228   251201                            2                                                  Sondierungstiefe bis m 1,00
G228   251201                            5                m3 +
G228   251201    EWZ                     6 A+0000000000000
G228   251202                            2                                                  Sondierungstiefe m 1,01-1,50
G228   251202                            5                m3 +
G228   251202    WVZ                     6 A+0000000000000
G228   311113                            2                                                  Stellplatten,bis mm 100x300
G228   311113                            5                m
G228   311113    TAZ                     6 A+0000000000000   +
G228   311116                            2                                                  Stellplatten,bis mm 100x300
G228   311116                            5                m  +
G228   311116    TAZ                     6 A+0000000000000
G228   315102                            2                                                  Belagsdicke mm 51-100
G228   315102                            5                m2 +
G228   315102    TAZ                     6 A+0000000000000
G228   315202                            2                                                  Belagsdicke mm 101-150
G228   315202                            5                m2 +
G228   315202    WVZ                     6 A+0000000000000
G228   333001                            2                                                  Spezifikation
G228   3330010101                        3                                                  Art Schachtabdeckung inkl.
G228   3330010102                        3                                                  Betonkragen
G228   3330010301                        3                                                  Abmessung Gussdeckel NW 600 mm
G228   3330010801                        3                                                       xxx
G228   333001                            5                LE +
G228   333001    TAZ                     6 A+0000000000000
G228   333002                            2                                                  Spezifikation
G228   3330020101                        3                                                  Art Gussrost von
G228   3330020102                        3                                                  Strassenablauf und
G228   3330020103                        3                                                  Einlaufschchte inkl.
G228   3330020104                        3                                                  Betonkragen
G228   3330020801                        3                                                       xxx
G228   333002                            5                LE +
G228   333002    TAZ                     6 A+0000000000000
G228   341511                            2                                                  Bis DN 150
G228   341511                            5                m
G228   341511    WVZ                     6 A+0000000000000   +
G228   351101                            2                                                  Aushubtiefe bis m 1,00
G228   351101                            5                St +
G228   351101    WVZ                     6 A+0000000000000
G228   361110                            2                                                  Vollstndig abbrechen
G228   3611100101                        3                                                            Kandelaberfundament
G228   361112                            2                                                  m3 0,51-1,00
G228   361112                            5                St
G228   361112    TAZ                     6 A+0000000000000   +
G228   421131                            2                                                  Grabenbreite bis m 1,00
G228   421131                            5                m  +
G228   421131    WVZ                     6 A+0000000000000
G228   422123                            2                                                  Grabenbreite m 1,21-1,40
G228   422123                            5                m
G228   422123    WVZ                     6 A+0000000000000   +
G228   422165                            2                                                  Grabenbreite m 1,81-2,00
G228   422165                            5                m
G228   422165    TAZ                     6 A+0000000000000   +
G228   451113                            2                                                  Grabenbreite m 0,71-0,90
G228   451113                            5                m  +
G228   451113    WVZ                     6 A+0000000000000
G228   451118                            2                                                  Uebrige Grabenbreiten
G228   4511180101                        3                                                                 xxx
G228   451118                            5                m
G228   451118    TAZ                     6 A+0000000000000   +
G228   481101                            2                                                  Findlinge entfernen
G228   481101                            5                m3 +
G228   481101    WVZ                     6 A+0000000000000
G228   481101    TAZ                     6 A+0000000000000
G228   481102                            2                                                  Fundam.,Mauerreste unbew.Beton
G228   481102                            5                m3 +
G228   481102    WVZ                     6 A+0000000000000
G228   481102    TAZ                     6 A+0000000000000
G228   481104                            2                                                  Aushub im Wurzelbereich
G228   481104                            5                m3 +
G228   481104    TAZ                     6 A+0000000000000
G228   481104    EWZ                     6 A+0000000000000
G228   481201                            2                                                  Leicht abbaub.m.Abbauhammersp.
G228   481201                            5                m3 +
G228   481201    TAZ                     6 A+0000000000000
G228   482121                            2                                                  Spezifikation
G228   4821210101                        3                                                  Diverse Querungen
G228   4821210102                        3                                                  von Leitungen,
G228   482121                            5                St +
G228   482121    TAZ                     6 A+0000000000000
G228   482231                            2                                                  Spezifikation
G228   4822310101                        3                                                  Diverse Querungen
G228   4822310102                        3                                                  von Leitungen,
G228   482231                            5                m  +
G228   482231    TAZ                     6 A+0000000000000
G228   554111                            2                                                  Rohr bis DN/ID 100
G228   554111                            5                m
G228   554111    EWZ                     6 A+0000000000000   +
G228   571321                            2                                                  Bis DN 200
G228   571321                            5                m  +
G228   571321    WVZ                     6 A+0000000000000
G228   615313                            2                                                  t m 1,51-2,00
G228   615313                            5                St +
G228   615313    TAZ                     6 A+0000000000000
G228   615341                            2                                                  Spezifikation
G228   6153410101                        3
G228   6153410102                        3                                                  Beton bis DN 200
G228   6153410201                        3                                                  Strassenablauf gemss TED
G228   6153410202                        3                                                  Norm 13.51
G228   6153410401                        3                                                     700
G228   6153410501                        3                                                                 1.51 bis 2.00
G228   615341                            5                St +
G228   615341    TAZ                     6 A+0000000000000
G228   641131                            2                                                  Kontrollschacht
G228   6411310101                        3                                                            TED Norm
G228   6411310201                        3                                                         D 400
G228   6411310301                        3                                                  Kontrollschacht
G228   641131                            5                St
G228   641131    TAZ                     6 A+0000000000000   +
G228   641132                            2                                                  Spezifikation
G228   6411320101                        3                                                            TED Norm
G228   6411320201                        3                                                         D 400
G228   6411320301                        3                                                  Rost fr Strassenablauf
G228   641132                            5                St
G228   641132                            6 A+0000000000000   +
G228   651221                            2                                                  Spezifikation
G228   6512210701                        3
G228   6512210901                        3                                                  WVZ Schieberkappen vorgngig
G228   6512210902                        3                                                  provisorisch versetzen.
G228   6512211101                        3
G228   651221                            5                St
G228   651221    WVZ                     6 A+0000000000000   +
G228   671101                            2                                                  Spezifikation
G228   6711010101                        3                                                      Kandelaber
G228   6711010201                        3                                                            siehe Angaben ewz
G228   671101                            5                St
G228   671101    EWZ                     6 A+0000000000000   +
G228   684102                            2                                                  Exkl.Lieferung
G228   684102                            5                m  +
G228   684102    WVZ                     6 A+0000000000000
G228   684102    EWZ                     6 A+0000000000000
G228   684302                            2                                                  DN/ID 100
G228   684302                            5                m
G228   684302    EWZ                     6 A+0000000000000   +
G228   722421                            2                                                  Grabenbreite bis m 1,00
G228   722421                            5                m
G228   722421    EWZ                     6 A+0000000000000   +
G228   722426                            2                                                  Uebrige Grabenbreiten
G228   7224260101                        3                                                                 xxx
G228   7224260102                        3                                                  (Hauptgraben)
G228   722426                            5                m
G228   722426    WVZ                     6 A+0000000000000   +
G228   722427                            2                                                  Uebrige Grabenbreiten
G228   7224270101                        3                                                                 bis xxx
G228   7224270102                        3                                                  Umhllung neue
G228   7224270103                        3                                                  Wertstoffsammelstell
G228   722427                            5                m
G228   722427    TAZ                     6 A+0000000000000   +
G228   761201                            2                                                  Spezifikation
G228   7612010301                        3                                                  fr Rabatte
G228   761201                            5                m3 +
G228   761201    TAZ                     6 A+0000000000000
G228   762111                            2                                                  Spezifikation
G228   7621110201                        3                                                                xxx
G228   762111                            5                m2
G228   762111    TAZ                     6 A+0000000000000   +
G228   763111                            2                                                  Spezifikation
G228   7631110301                        3                                                                xxx
G228   763111                            5                m2 +
G228   763111    TAZ                     6 A+0000000000000
G228   783001                            2                                                  Spezifikation
G228   7830010501                        3                                                       m3
G228   783001                            5                LE
G228   783001    WVZ                     6 A+0000000000000   +
G228   811122                            2                                                  Einbaudicke m 0,31-0,40
G228   811122                            5                m2 +
G228   811122    EWZ                     6 A+0000000000000
G228   811223                            2                                                  Einbaudicke m 0,41-0,50
G228   811223                            5                m2 +
G228   811223    WVZ                     6 A+0000000000000
G228   811223    EWZ                     6 A+0000000000000
G228   823102                            2                                                  Planiebreite m 1,01-2,00
G228   823102                            5                m2 +
G228   823102    EWZ                     6 A+0000000000000
G228   823102    WVZ                     6 A+0000000000000
G228   823201                            2                                                  Planiebreite bis m 1,00
G228   823201                            5                m2
G228   823201    EWZ                     6 A+0000000000000   +
G228   823201    WVZ                     6 A+0000000000000   +
G228   824200                            2                                                  In Fahrbahnen
G228   8242000101                        3                                                            ....................
G228   824201                            2                                                  Planiebreite bis m 3,00
G228   824201                            5                m2
G228   824201    TAZ                     6 A+0000000000000   +
G228   824202                            2                                                  Planiebreite .m 3,00
G228   824202                            5                m2
G228   824202                            6 A+0000000000000   +
G228   824301                            2                                                  Uebrige
G228   8243010101                        3                                                  Planie unter neue
G228   8243010102                        3                                                  Randabschlsse
G228   8243010601                        3                                                       m2
G228   824301                            5                LE
G228   824301    TAZ                     6 A+0000000000000   +
G228   825103                            2                                                  Uebrige Ausgleichsdicken
G228   8251030101                        3                                                                     xxx
G228   825103                            5                m2 +
G228   825103    TAZ                     6 A+0000000000000
G228   825203                            2                                                  Uebrige Ausgleichsdicken
G228   8252030101                        3                                                                     xxx
G228   825203                            5                m2
G228   825203    TAZ                     6 A+0000000000000   +
G228   825900                            2                                                  Plattendruckversuche
G228   8259000001                        3                                                  Plattendruckversuche (SN 670
G228   8259000002                        3                                                  317)
G228   825901                            2                                                  Plattendruckversuche
G228   8259010001                        3                                                  Plattendruckversuche
G228   8259010002                        3                                                  durchfhren inkl. allen
G228   8259010003                        3                                                  Einrichtungen wie
G228   8259010004                        3                                                  Gegengewicht etc., Auswertung
G228   8259010005                        3                                                  und Dokumentation
G228   825901                            5                St +
G228   825901    WVZ                     6 A+0000000000000
G228   831120                            2                                                  Bindersteine(Schalensteine)
G228   8311200101                        3                                                  Europa
G228   831122                            2                                                  Typ 12
G228   831122                            5                m  +
G228   831122    TAZ                     6 A+0000000000000
G228   831510                            2                                                  Gerade
G228   8315100101                        3                                                  Europa
G228   831512                            2                                                  SN 8,mm 80x250
G228   831512                            5                m  +
G228   831512    TAZ                     6 A+0000000000000
G228   831710                            2                                                  Gerade
G228   8317100101                        3                                                  Europa
G228   831713                            2                                                  RN 15,mm 150/190x250
G228   831713                            5                m  +
G228   831713    TAZ                     6 A+0000000000000
G228   831781                            2                                                  Spezifikation
G228   8317810101                        3                                                  Randstein fr Bushaltestelle
G228   8317810102                        3                                                  Zrich-Bord Typ G1 20m,
G228   8317810103                        3                                                  R1L/R2L und R1R/R2R je
G228   8317810104                        3                                                  1.5m, gemss Norm 207,  Hhe
G228   8317810105                        3                                                  22cm
G228   8317810301                        3                                                  Herkunft Europa
G228   8317810401                        3                                                            Norm 16.86
G228   831781                            5                m
G228   831781    TAZ                     6 A+0000000000000   +
G228   835212                            2                                                  SN od.SB 8
G228   835212                            5                m
G228   835212    TAZ                     6 A+0000000000000   +
G228   835310                            2                                                  Randsteine o.Wassersteine
G228   8353100101                        3                                                            ....................
G228   835312                            2                                                  RN od.RB 15
G228   835312                            5                m  +
G228   835312    TAZ                     6 A+0000000000000
G228   835314                            2                                                  Spezifikation
G228   8353140101                        3                                                  Randstein fr die
G228   8353140102                        3                                                  Bushaltestelle  aus
G228   8353140103                        3                                                  der Pos: 831.781
G228   835314                            5                m
G228   835314    TAZ                     6 A+0000000000000   +
G228   835321                            2                                                  RN od.RB 15
G228   835321                            5                m
G228   835321    TAZ                     6 A+0000000000000   +
G228   836325                            2                                                  r m 5,00-9,99
G228   836325                            5                m
G228   836325    TAZ                     6 A+0000000000000   +
G228   836501                            2                                                  Spezifikation
G228   8365010101                        3                                                  Randsteine durch den
G228   8365010102                        3                                                  Steinbauer bearbeiten
G228   836501                            5                m
G228   836501    TAZ                     6 A+0000000000000   +
G228   836502                            2                                                  Spezifikation
G228   8365020101                        3                                                  Stellplatten richten
G228   8365020102                        3                                                  Bestehende Platte demontieren
G228   8365020103                        3                                                  und neu versetzen
G228   836502                            5                m
G228   836502    TAZ                     6 A+0000000000000   +
G228   837111                            2                                                  RN 15
G228   837111                            5                m
G228   837111    TAZ                     6 A+0000000000000   +
G228   838301                            2                                                  Splitt-,Rundkornbeton 8/16
G228   838301                            5                m3 +
G228   838301    TAZ                     6 A+0000000000000
G228   864215                            2                                                  Uebrige Schichtdicken
G228   8642150101                        3                                                                  xxx
G228   864215                            5                m2
G228   864215    TAZ                     6 A+0000000000000   +
G228   864525                            2                                                  Schichtdicke mm 100
G228   864525                            5                m2
G228   864525    TAZ                     6 A+0000000000000   +
G228   866213                            2                                                  Schichtdicke mm 30
G228   866213                            5                m2
G228   866213    TAZ                     6 A+0000000000000   +
G228   866422                            2                                                  Schichtdicke mm 30
G228   866422                            5                m2
G228   866422    TAZ                     6 A+0000000000000   +
G228   866900                            2                                                  Prfungen an bitumenhaltigen
G228   8669000001                        3                                                  Prfungen an bitumenhaltigen
G228   8669000002                        3                                                  Materialien und Schichten
G228   866901                            2                                                  Asphaltmischgutprfung
G228   8669010001                        3                                                  Asphaltmischgutprfung
G228   8669010101                        3                                                  Sammelprfung fr Einbau nach
G228   8669010102                        3                                                  Norm und QS TAZ
G228   8669010103                        3                                                  Prfung von
G228   8669010104                        3                                                  Korngrssenverteilung,
G228   8669010105                        3                                                  Rohdichte, Raumdichte
G228   8669010106                        3                                                  lslichem Bindemittelgehalt
G228   8669010107                        3                                                  und Marshall-Kennwerte
G228   8669010201                        3                                                  Inkl. Baustelleneinsatz,
G228   8669010202                        3                                                  Transport, Prfung vom
G228   8669010203                        3                                                  Mischgut inkl.
G228   8669010204                        3                                                  Bindemittelrckgewinnung,
G228   8669010205                        3                                                  inkl. Dokumentation
G228   866901                            5                LE +
G228   866901    TAZ                     6 A+0000000000000
G228   885101                            2                                                  m.Hochdruck-Spritzwagen
G228   885101                            5                m2 +
G228   885101    TAZ                     6 A+0000000000000
G228   885332                            2                                                  Schichtdicke mm 41-80
G228   885332                            5                m  +
G228   885332    TAZ                     6 A+0000000000000
G228   885341                            2                                                  Spezifikation
G228   8853410101                        3                                                  TOK-Fugenband
G228   8853410201                        3                                                               20 x 30
G228   8853410301                        3                                                  Quer- und Lngsnaht Deckbelag
G228   885341                            5                m  +
G228   885341    TAZ                     6 A+0000000000000
G228   887101                            2                                                  Spezifikation
G228   8871010201                        3                                                                    150
G228   8871010401                        3
G228   887101                            5                m  +
G228   887101    TAZ                     6 A+0000000000000
"""

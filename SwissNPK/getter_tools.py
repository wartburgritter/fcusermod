import FreeCAD
import Part


# *************************************************************************************************
def is_quader_bb(o):
    # does not work !!!
    # the box is not rotated in the wall direction if the wall is not in x, y direction (schiefe wand)
    # th BB box is oriented in x,y,z !
    print('BoundBox')
    bb = o.Shape.BoundBox
    bb_lx = bb.XLength
    bb_ly = bb.YLength
    bb_lz = bb.ZLength
    if (bb_lx > 0) and (bb_ly > 0) and (bb_lz > 0):
        bb_box = Part.makeBox(bb_lx, bb_ly, bb_lz)
        bb_box.Placement = FreeCAD.Placement(FreeCAD.Vector(bb.XMin, bb.YMin, bb.ZMin), FreeCAD.Rotation(FreeCAD.Vector(0, 0, 1), 0), FreeCAD.Vector(0, 0, 0))
        BDvol = FreeCAD.ActiveDocument.addObject("Part::Feature", o.Name + "bb_")
        BDvol.Shape = bb_box
        BDvol.ViewObject.Transparency = 80
        # the box is not rotated in the wall direction if the wall is not in x, y direction (schiefe wand)

    else:
        FreeCAD.Console.PrintMessage("Not BoundBox possible" + "\r\n")


def is_quader(o):
    max_len = 0
    # e_dir = None
    f_dir = None
    for f in o.Shape.Faces:
        if -0.001 < f.normalAt(50, 50).z < 0.001:  # Stirn, oder Wandflaeche
            for e in f.Edges:
                # if 0.999 < e.Vertexes[1].Z / e.Vertexes[0].Z < 1.001:  # is edge in x-y-plane, but fivision by zero in one point is in x-y-plane
                if abs(e.Vertexes[1].Z - e.Vertexes[0].Z) < 0.001:  # is edge in x-y-plane
                    if e.Length > max_len:
                        max_len = e.Length
                        # e_dir = e
                        f_dir = f
    # e ist die laengste edge in x-y-ebene --> Annahme: zugehoerige Flaeche ist Wandschalflaeche
    # Part.show(e_dir)  # laengste edge in wandrichtung, die in x-y-ebene liegt

    # alle wandschalflaechen:
    # aber tueren sind nicht durchgeschalt
    w = f_dir.normalAt(50, 50)  # normale der wandschalung
    for f in o.Shape.Faces:
        n = f.normalAt(50, 50)  # normale der aktuellen face
        if (0.0 <= w.getAngle(n)) < 0.1 or (0.0 <= w.getAngle(n.negative()) < 0.1):  # vektoren sind parallel, weil sie keinen winkel bilden
            # Part.show(f)
            pass  # komment
    # es wir nur eine der wand schalungen angezeigt !

    # suche in allen flaechen
    # check --> alle edge aller flaechen die parallel zu der obigen flaeche sind entweder senkrecht oder selbe richtung wie obige edge
    # dann haben alle wandschalflaechen keine schiefen kanten
    # dann wandquader, bei dem auch eine stirnflaeche schief sein kann
    # wanddicke ausrechnen (denke an schiefe stirnflaeche)
    # wandhoehe ausrechnen
    # wandlaenge ausrechnen (brauch ich die?) Wie lang ist wand bei schiefer stirnflaeche
    # wandschalflaeche ausrechnen!!!

    '''
    # speichere alle verschiedenen edgerichtungen in directions, eine quader hat nur drei
    # problem wenn eine stirnflaeche schief ist (winkel zwichen waenden nicht 90 grad) hat wand mehr als drei edgerichtungen
    directions = []
    for e in o.Shape.Edges:
        v = e.Vertexes[1].Point - e.Vertexes[0].Point
        if not directions:
            directions.append(v)
        is_in_dirs = False
        for d in directions:
            if (0.0 <= d.getAngle(v)) < 0.1 or (0.0 <= d.getAngle(v.negative())< 0.1):  # vektoren sind parallel, weil sie keinen winkel bilden
                # print(d.getAngle(v))
                # print(d.getAngle(v.negative()))
                # print(directions)
                is_in_dirs = True
                break
        if not is_in_dirs:
            directions.append(v)
    print(len(directions))
    print(directions)
    if len(directions) == 3:
        return True
    else:
        Part.show(o.Shape)
        return False
    '''


def is_sloped(o):
    '''object is sloped if a normal of one face is not
    == -1.0 --> oberflaeche
    == 1.0  --> unterflaeche
    == 0.0  --> seitenflaeche
    '''
    if hasattr(o, 'Shape'):
        for f in o.Shape.Faces:
            # if f.normalAt(50,50).z != 1.0 and f.normalAt(50,50).z != -1.0 and f.normalAt(50,50).z != 0.0:
            if not 0.999 < f.normalAt(50, 50).z < 1.001 and not -1.001 < f.normalAt(50, 50).z < -0.999 and not -0.001 < f.normalAt(50, 50).z < 0.001:
                FreeCAD.Console.PrintError('YEAH Find some sloped Objekt' + o.Name + '\n\n')
                # Part.show(f)
                return True


# *************************************************************************************************
def setNPKFlaechenEinteilungDecke(o):
    '''NPKFlaechenEinteilungDecke --> unabhaengiges Attribut --> !!! --> dann anders benennen !!!
    dict = {
            oberflaeche : [Face1, Face5],
            unterflaeche : [Face2, Face8],
            seitenflaeche : [FaceX, FaceY, ...]
            }
    '''

    # Aufruf: siehe hinter Funktion, am besten komplett separat!!!
    # funktioniert nur wenn komplett eben !

    # if hasattr(o, 'NPKKategorieFundament') and o.NPKKategorieFundament == 'Bodenplatte':
    if hasattr(o, 'NPKBauteil') and o.NPKBauteil == 'Decke':
        # FreeCAD.Console.PrintError('WE ARE INSIDE')
        print(o.Name)
        if not hasattr(o, "Shape"):
            return []
        # Part.show(o.Shape)
        oberflaechen = []
        schalflaechen = []
        stirnflaechen = []
        for f in o.Shape.Faces:
            if f.normalAt(50, 50).z == 1.0:
                oberflaechen.append(f)
                # Part.show(f)
            elif f.normalAt(50, 50).z == -1.0:
                schalflaechen.append(f)
                # Part.show(f)
            elif f.normalAt(50, 50).z == 0.0:
                stirnflaechen.append(f)
                # Part.show(f)
            else:
                FreeCAD.Console.PrintError('Schiefe Deckenflaeche gefunden!!!' + o.Name + '\n\n')
                # Part.show(f)

        faceeinteilung = {}
        faceeinteilung['oberflaechen'] = oberflaechen
        faceeinteilung['schalflaechen'] = schalflaechen
        faceeinteilung['stirnflaechen'] = stirnflaechen

        return stirnflaechen

    else:
        return []
        # nichts da methode fuer jedes objekt aufgerufen wird, normal fuer jede Decke aufrufen
        # wo aufrufen, im getter, aber evtl. braucht es den getter nicht, weil attribut schon da ?!?
        # FreeCAD.Console.PrintError('kein deckenbauteil: ' + o.Name + '\n\n')
        # raise ValueError('kein deckenbauteil: ' + o.Name + '\n')


'''
import npk_getter
flaechen = []
for o in FreeCAD.ActiveDocument.Objects:
    returned_flaechen = npk_getter.setNPKFlaechenEinteilungDecke(o)
    for f in returned_flaechen:
        flaechen.append(f)

comp = Part.makeCompound(flaechen)
Part.show(comp)
fsum = 0
for f in comp.Faces:
    fsum += f.Area

print(fsum * 1E-6)


######################################################
# Flaechensumme fuer Object Compound
sumf = 0
for f in App.ActiveDocument.Compound.Shape.Faces:
    sumf += f.Area

print(sumf * 1E-6)


# Flaechensumme fuer Object Shape
sumf = 0
for f in App.ActiveDocument.Shape.Shape.Faces:
    sumf += f.Area

print(sumf * 1E-6)
'''


'''
for f in obj.Shape.Faces:
    print(f.normalAt(50,50).z)

# funktioniert nur, wenn sauber modelliert ist, oder the other way around,
# damit lassen sich modellfehler bei nicht schraegen bauteilen sehr schnell finden !!!
# Bei Modellungenauigkeiten weichen die Normalen minimal von diesen Werten ab!
for f in obj.Shape.Faces:
    if f.normalAt(50,50).z != 1.0 and f.normalAt(50,50).z != -1.0 and f.normalAt(50,50).z != 0.0:
        Part.show(f)
        print(f.normalAt(50,50).z)

for f in obj.Shape.Faces:
    if not 0.999 < f.normalAt(50,50).z < 1.001 and not -1.001 < f.normalAt(50,50).z < -0.999 and not -0.001 < f.normalAt(50,50).z < 0.001:
        Part.show(f)
        print(f.normalAt(50,50).z)



obj = App.ActiveDocument.getObject("ID7208__")

face_oben = obj.Shape.Face6
face_unten = obj.Shape.Face5
face_oben.normalAt(50,50)
face_unten.normalAt(50,50)

for f in obj.Shape.Faces:
    print(f.normalAt(50,50))
    print(f.normalAt(50,50).z)
    print(f.Orientation)
    print(f.CenterOfMass)
'''

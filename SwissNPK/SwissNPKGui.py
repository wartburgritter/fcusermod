# -*- coding: utf-8 -*-
# FreeCAD Workbench SwissNPK
# (c) 2014 Bernd Hahnebach

# ***************************************************************************
# *   (c) Bernd Hahnebach (bernd@bimstatik.org) 2014                    *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# *   Bernd Hahnebach 2014                                               *
# ***************************************************************************/


import FreeCAD
import FreeCADGui


def should_object_have_a_postion_number(document_object):
        o = document_object
        if '___body' not in o.Name and hasattr(o, 'Shape') and hasattr(o, 'IfcObjectType'):
            o.ViewObject.Visibility = True
            print(o.Name)
            return True
        else:
            o.ViewObject.Visibility = False
        return False


class MAKESOMEIMPORTANTATTRIBUTES():
    def Activated(self):
        """erstelle einige Attribute fuer die einfache Arbeit mit importierten IFCfiles
        """
        import ifctools
        ifctools.setIfcAttributes(FreeCAD.ActiveDocument)

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Make_some_important_attributes', 'ToolTip': 'Make_some_important_attributes'}


class SETNPKPOSNUMBERSMAT():
    def Activated(self):
        '''rufe fuer alle objekte mit shape und ohne ___body die folgende funktion auf:
        setNPKVolumenPositionNo aus module npk_setNPKPositionNo_mat
        Positiionsebentiefe = 1
        '''
        print('')
        print('')
        print('START TRYING TO ADD POSITION NUMBERS')
        import npk_setNPKPositionNo_mat
        for o in FreeCAD.ActiveDocument.Objects:
            if should_object_have_a_postion_number(o):
                set_pos_no = npk_setNPKPositionNo_mat.setNPKVolumenPositionNo(o)
                print('  ' + str(set_pos_no) + '\n')
                if not set_pos_no:
                    FreeCAD.Console.PrintError('Error\n')
                    break
        FreeCAD.ActiveDocument.recompute()

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Set NPK Position Numbers(Material)', 'ToolTip': 'Set NPK Position Numbers (Material=Ebene1)'}


class SETNPKPOSNUMBERSBT():
    def Activated(self):
        '''rufe fuer alle objekte mit shape und ohne ___body die folgende funktion auf:
        setNPKVolumenPositionNo aus module npk_setNPKPositionNo_bt
        Positiionsebentiefe = 2
        '''
        print('')
        print('')
        print('START TRYING TO ADD POSITION NUMBERS')
        import npk_setNPKPositionNo_bt
        for o in FreeCAD.ActiveDocument.Objects:
            if should_object_have_a_postion_number(o):
                set_pos_no = npk_setNPKPositionNo_bt.setNPKVolumenPositionNo(o)
                print('  ' + str(set_pos_no) + '\n')
                if not set_pos_no:
                    FreeCAD.Console.PrintError('Error\n')
                    break
        FreeCAD.ActiveDocument.recompute()

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Set NPK Position Numbers(Bauteil)', 'ToolTip': 'Set NPK Position Numbers (Bauteil=Ebene2)'}


class SETNPKPOSNUMBERSKT():
    def Activated(self):
        '''rufe fuer alle objekte mit shape und ohne ___body die folgende funktion auf:
        setNPKVolumenPositionNo aus module npk_setNPKPositionNo_bt
        Positiionsebentiefe = 2
        '''
        print('')
        print('')
        print('START TRYING TO ADD POSITION NUMBERS')
        import npk_setNPKPositionNo_kt
        for o in FreeCAD.ActiveDocument.Objects:
            if should_object_have_a_postion_number(o):
                set_pos_no = npk_setNPKPositionNo_kt.setNPKVolumenPositionNo(o)
                print('  ' + str(set_pos_no) + '\n')
                if not set_pos_no:
                    FreeCAD.Console.PrintError('Error\n')
                    break
        FreeCAD.ActiveDocument.recompute()

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Set NPK Position Numbers(Kategorie)', 'ToolTip': 'Set NPK Position Numbers (Kategorie=Ebene3)'}


class SETNPKPOSNUMBERSNPK():
    def Activated(self):
        '''rufe fuer alle objekte mit shape und ohne ___body die folgende funktion auf:
        setNPKVolumenPositionNo aus module npk_setNPKPositionNo_npk
        Positiionsebentiefe = 6
        '''
        print('')
        print('')
        print('START TRYING TO ADD POSITION NUMBERS')
        import npk_setNPKPositionNo_npk
        for o in FreeCAD.ActiveDocument.Objects:
            if should_object_have_a_postion_number(o):
                set_pos_no = npk_setNPKPositionNo_npk.setNPKVolumenPositionNo(o)
                print('  ' + str(set_pos_no) + '\n')
                if not set_pos_no:
                    FreeCAD.Console.PrintError('Error\n')
                    break
        FreeCAD.ActiveDocument.recompute()

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Set NPK Position Numbers(NPK)', 'ToolTip': 'Set NPK Position Numbers (NPK=Ebene6)'}


class WRITESIA451BT():
    def Activated(self):
        import export2SIA451
        print('')
        print('')
        print('START TRYING TO EXPORT SIA451')
        sucess = export2SIA451.export2SIA451()
        if not sucess:
            FreeCAD.Console.PrintError('Error\n')

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Export SIA451 Bauteilpositionen (BT)', 'ToolTip': 'Export SIA451 Bauteilpositionen (BT)'}


FreeCADGui.addCommand('Make_some_important_attributes', MAKESOMEIMPORTANTATTRIBUTES())
FreeCADGui.addCommand('SwissNPK_SetNPKPositionNumber_mat', SETNPKPOSNUMBERSMAT())
FreeCADGui.addCommand('SwissNPK_SetNPKPositionNumber_bt', SETNPKPOSNUMBERSBT())
FreeCADGui.addCommand('SwissNPK_SetNPKPositionNumber_kt', SETNPKPOSNUMBERSKT())
FreeCADGui.addCommand('SwissNPK_SetNPKPositionNumber_npk', SETNPKPOSNUMBERSNPK())
FreeCADGui.addCommand('SwissNPK_Write_SIA451_bt', WRITESIA451BT())

# !!!! posnr und NPK Attribute in FreeCAD werden on rerun nicht neu gesetzt !!!!!!!!!!!!!!
# !!!! das ist gut, kann aber auch verwirrend sein, da nach anpassung sich nichts tut !!!


# doch es macht sinn den getter zu automatisieren, dann weiss ich
# welche get methoden ich brauche und welche returnwerte diese haben ... ok sehe ich an den comments im setter oben
# fuer jedes attribut dort brauche ich eine getter methode, und die returnwerte sind die listeneintraege

# !!! hier nur die puren getter methoden, jegliche hilfsmethoden auslagern nach separaten modul
#

# siehe auch datei npk_data_attribute.py
# reihenfolge hier == reihenfolge in npk_data_attribute.py
# Was zum Teufel macht das modul npk_data_attribute
# glaube gar nichts, das ist ne info, bitte auch lesen ...
# alles hier geschriebene sollte dort sein, hier nur puren getter methoden

# IDEE
# macht es sinn die getter setter auch automatisch und in
# zuordnungsdatei nur die zuornung des setters zu seiner methode
# diese methode dann in englisch mhh weiss noch nicht mal sehn
# erst das Betonleistungsverzeichnis mit den Bauteilrpositionen !!!

# der setter ist ja automatisch


# *************************************************************************************************
# listattribute
# siehe npk_data_attribute.py fuer die listen der moeglichen listattribute, oder den setter !!!


# listenwerte
# ist eine uebersicht aller vergebenen attribute, manuell erstellt, mit Kommentaren. Diese sollte mit Kommentaren im Setter uebereinstimmen !!!

NPKVolumen = ['Ortbeton']

NPKBauteil = ['Decke', 'Fundament', 'Grube', 'Stuetze', 'Treppe', 'Unterzug', 'Wand']

# 61X.XXX --> Fundament
# 62X.XXX --> GrubeSchachtLeitungskanal     --> Grube, Schacht, Leitungskanal
# 63X.XXX --> Wand
# 65X.XXX --> Stuetze
# 71X.XXX --> Treppe
# 72X.XXX --> Decke
# 74X.XXX --> UnterzugTraegerBruestung  --> Unterzug, Traeger, Bruestung

# in yaml fuer NPK ist noch Grube und Unterzug drin, weil in bspfile holderbank so definiert


NPKKategorie = ['Standardeinzelfundament', 'Schachtwand', 'Bodenplatte', 'Standarddecke', 'Standardstuetze', 'Schachtkopfplatte', 'Borduere']
NPKKategorie = ['Einzelfundament',
                'Streifenfundament',
                'Bodenplatte',
                'Schachtwand',
                'Sichtbeton',
                'Kratzbeton',
                'Standardstuetze',
                'TreppeGeraderLauf',
                'TreppePodest',
                'TreppeSpezial',
                'Deckenplatte',
                'Kragplatte',
                'Schachtkopfplatte',
                'Unterzug',
                'Stuetzenkopf',
                'Traeger',
                'Bruestung',
                'Sockel']
# passende liste zu obigem NKKBauteil im setter !!!
# dann auch die enumeration gemaess dem obigen bauteil


NPKBetonsorte = ['NPKA', 'NPKB', 'NPKC', 'NPKD', 'NPKE', 'NPKF', 'nachEigenschaften']


# numerische werte
# grosse frage ist hier noch wie sich bereiche in einer variable definieren lassen, gibt es da einen datentyp in Python !!!

# neue idee, die Attribute sind auch enumerations wie die angebotenen Werte in NPK
# die moeglichen werte stehen auch hier, die getter setter werden auch automatisch erstellt mit verweis auf die uttilty methode, die den wert errechnet

# DESIGNENTSCHEID!!!

# aufpassen in FreeCAD mit mausrad in Attributen, dieses verstellt die enumeration --> NICHT GUT

# alle moeglichen wanddicken egal ob wand oder schacht,
# wie oben es sind immer alle NPKKategorie auswaehlbar egal wie NPKBauteil gesetzt ist
# mhh vorerst so und dann spaeter aendern

NPKDickeWand = ['0.20 .. 0.26', '0.15 .. 0.26', '0.15 .. 0.20', '0.21 .. 0.25']  # in Schachtwandpositionen ist dicke anders definiert als in Wandpositionen

NPKTiefe = ['0.0 .. 1.50']


NPKAnzug = '?'
NPKDickePlatte = 'm'
NPKDickeWand = 'm'
NPKHoeheStuetze = 'm'
NPKHoeheVeraenderlich = 'm'
NPKHoeheWand = 'm'
NPKHoeheWandEtappe = 'm'
NPKOberflaechenneigungPlatteEinseitig = '%'
NPKQuerschnitt = 'm2'
NPKTiefe = 'm'
NPKVolumenbereich = 'm3/St'

# wenn modul manuell geladen wird mit
# import export2SIA451
# braucht nach Aenderungen im modul in FreeCAD nicht neu gestartet zu werden wenn modul wieder geladen wird mit
# reload(export2SIA451)


def export2SIA451():
    import FreeCAD
    import sia451parser
    '''
    export to SIA451
    '''
    print('')
    print('')
    print('START TRYING TO EXPORT SIA451')
    positionlist = []
    einzelpositionsmengenlist = []
    einzelpositionssumme = 0
    for o in FreeCAD.ActiveDocument.Objects:
        if '___body' not in o.Name:
            if hasattr(o, 'Shape'):
                o.ViewObject.Visibility = True
                if hasattr(o, 'NPKVolumenPosition'):
                    pos = o.NPKVolumenPosition.lstrip('241.').replace('.', '')
                    vol = o.Shape.Volume * 1E-9
                    einzelpositionsmengenlist.append([pos, vol])
                    einzelpositionssumme += o.Shape.Volume * 1E-9
                    if pos not in positionlist:
                        positionlist.append(pos)

    # print(sorted(positionlist))

    # mengen aufsummieren
    positionsmengenlist = []
    for p in sorted(positionlist):
        positionsumme = 0
        for epm in sorted(einzelpositionsmengenlist):
            if p == epm[0]:
                positionsumme += epm[1]
        positionsmengenlist.append([p, round(positionsumme, 2)])

    positionssummensumme = 0
    for pm in sorted(positionsmengenlist):
        # print(pm)
        positionssummensumme += pm[1]

    # print(einzelpositionssumme, '\n', positionssummensumme, '\n')
    # print(einzelpositionssumme/positionssummensumme, '\n')
    if 0.999 < positionssummensumme / einzelpositionssumme < 1.001:
        positionsmengenlist.append(['790000', ''])
        positionsmengenlist.append(['799001', round(positionssummensumme, 2)])

    # add to every position a position with 0 instead 1 and sort the list
    mytmplist = []
    for pm in sorted(positionsmengenlist):
        # print(pm)
        if pm[0].endswith('1'):
            mytmp = pm[0].rstrip('1') + '0'
            # print(mytmp)
            mytmplist.append([mytmp, ''])

    positionsmengenlisttmp = positionsmengenlist + mytmplist
    positionsmengenlist = []
    for pm in sorted(positionsmengenlisttmp):
        print(pm)
        positionsmengenlist.append(pm)

    from platform import system
    if system() == "Linux":
        sia_dir = '/home/hugo/.FreeCAD/Mod/SwissNPK/'
        path_sep = "/"
    elif system() == "Windows":
        sia_dir = 'C:\\Users\\bhb\\AppData\\Roaming\\FreeCAD\\Mod\\SwissNPK\\'
        path_sep = "\\"
    else:
        print('Not know OS!')
        return
    dataSia451 = sia_dir + 'data-katalog' + path_sep + 'bt.01s'  # r-positionen
    outfile = sia_dir + 'sia-my.01s'
    wf = open(outfile, 'w')

    # writeSIA451FilePositionen(positionlist, dataSia451, wf)
    # wf.close

    sia451parser.writeSIA451FileMengen(positionsmengenlist, dataSia451, wf)
    wf.close

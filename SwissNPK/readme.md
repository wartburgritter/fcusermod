# Start 
+ Betonkubaturen Ebene2 in reportwindow ausgeben.

# Vorbereitung in Allplan
+ Allplan Ortbeton only, IFC export gemäss Randbedingungen
+ file: *15025_KiGa_ING_TRW_2019-10-09.ifc*
+ import:
    + meine property modul (heillos veraltet)
    + run set properties
    + run sia tool

Allplan etc:
+ standard Hochbau BIM Assistent
+ keine extra Attribute
+ aber auch keine Proxies
+ Ifc Typ stimmt
+ aber kein predefined type vergeben
+ Betontyp steckt in der Farbe drin

+ modell ORB (Ortbeton) mittels reportfavorit aktivieren.
+ falls sich nicht alle TBs des prj aktivieren lassen
    + Projekt kopieren
    + in Zeichnungsstruktur alle Teilbilder 4000 - 8999 löschen
    + Modell Ortbeton (ORB) sollte sich komplett aktivieren lassen
+ alle Exportbauteile oder Reportbauteile sichtbar
+ im Objektmanager prüfen ob es Bauteile ohne Material und NPKBauteil hat, wenn ja setzen
+ Fuer alle Bauteile (wir haben nur Ortbeton) aktieren, Gewerk und Abrchnungsart setzen
    + aufpassen, für Aussparungen das Material nicht setzen
+ export nach IFC


# Ziele
+ Betonkubatur, Schalung, Bewehrung
+ bis auf Einzelpositionen
+ erst Decken Beton
+ dann Decken Schalung
+ was auch wichtig ist
    + Flächen für Spriesen
    + Flächen für Taloschieren
    + Flächen für Stützbügel


# Infos
+ soviel wie möglich als Inklusivpositionen erstellen
+ BSP Sichtbeton keine extra Positionen für
    + Stk. Verschliessen Bindelöcher
    + Stk. Zementabstandhalter
+ Wobei man die wieder auch schnell programmiert hat, wenn die Schalung steht.
+ einfach mal Beton, Schalung, Bewehrung machen.


# Kubaturen mit SIA-Datei
### Allplan:
+ export IFC, siehe oben

### FreeCAD
+ import IFC mit meinem module importIFCprops
+ Workbench SwissNPK --> Tool make_some_important_attributes
+ save as Prjname--import
+ delete the materials
+ run --> make one color --> from PropertyToolBox
+ save as Prjname--work
+ SwissNPK --> Ebene2
+ PropertyToolBox --> AttributeGui --> Screen erstellen
+ SwissNPK --> export SIAdatei ist im Mod verzeichnis in home

### Winbau
+ Devisierung -->  objekt z99901 oeffnen --> Arbeitsgattung is leer --> auf import klicken
+ Sia datei auswaehlen --> es kommt Fehler --> zurück --> Haken bei häufigste Fehler korrigieren setzen --> weiter neues Projekt --> OK
+ Register Leistungsverzeichnis --> drucken --> emaildrucker --> email
+ email.pdf oeffnen --> drucken --> emaildrucker --> 95% --> dateinamen aendern


# Massen in Allplan
+ wertet Abrechnungsart (m3) --> Gewerk(Betonarbeiten) --> Material (Beton) aus
+ im Objektmanager nur Material Beton darstellen
+ Reports --> Büro --> Massen --> einheit-gewerk-material.rdlc
+ Fenster über alle Bauteile aufziehen (nicht alles drücken!)
+ Report speichern

# Variante in Allplan
+ angewendet für Ausmass W23 und W22
+ Gebäude und Geschosse wie NPK-Positionen
+ Modell nach 3D-Körper
+ Diese manuell den Geschossen zugeordnet
+ Objectname oder Bezeichnung wie PositionsNr.
+ Report erstellen
+ siehe auf Server Ausmass W22 und W23
+ Dies sollte ich durch das hier ersetzen können
+ vor allem weil ich die SIA positionen als SIA-Datei habe.
# *************************************************************************************************
# getter, selbst programmiert

# jedes Attribut hat hier eine getter methode --> alle anderen hilsfmethoden sind im modul getter_tools
# Ausswahllisten sind die Attributwerte aus denen der Returnwert kommt: AuswahlAttributname
# zur Absicherung noch Listen fuer die nicht verwendeten objekte: NoAttributname

# die return werte hier der getter methoden muessen mit den setter listen des modules npk_setter uebereinstimmen
# siehe auch nicht tuendes modul npk_data_attributes


import sys
import FreeCAD
import getter_tools as gt


# Beton Ebene 1 ***********************
def getNPKBeton(o):
    AuswahlNPKBeton = [
        'Ortbeton',
        'Beton',
        'Sichtbeton',
        'Spezialbeton'
    ]
    NoNPKBeton = [
        # Jaeger BIM Assistent
        'Ausgleichsschicht',
        'Aussgleichschicht',
        'Daemmung',
        'Fertigelement Beton',
        u'Vergussm\xf6rtel',
        # Jaeger
        'FertigelementBeton',
        # Healy
        'Backstein',
        'Betonelement',
        'Beton bestehend',
        'Daemmstoff_hart',
        'Daemmstoff_weich',
        'Daemmstoff hart',
        'Holz massiv',
        'Kalksandstein',
        'Kragplattenanschluss',
        'Magerbeton',
        'Mauerwerk bestehend',
        'Stahl',
        'Sumostein'
    ]
    if hasattr(o, 'Shape') and hasattr(o, 'IfcMaterial1') and o.IfcMaterial1:
        volumen = o.IfcMaterial1
        if volumen not in AuswahlNPKBeton and volumen not in NoNPKBeton:
            FreeCAD.Console.PrintError('Unbekanntes Volumenmaterial: ' + volumen + '\n\n')
            print(AuswahlNPKBeton)
            print(NoNPKBeton)
            raise ValueError('Unbekanntes Volumenmaterial: ' + volumen + '\n')
            # besseres error handling ueber rueckgabewert
        if volumen in AuswahlNPKBeton:
            return 'Ortbeton'


# Beton Ebene 2 ***********************
def getNPKBauteil(o):
    AuswahlNPKBauteil = [
        'IfcFooting',
        'IfcWallStandardCase',
        'IfcWall',
        'IfcSlab',
        'IfcRoof',
        'IfcColumn',
        'IfcBeam',
        'IfcStair',
        'IfcBeam',
        'Grube'
    ]
    NoNPKBauteil = [
        'IfcPile'
    ]
    if hasattr(o, 'Shape') and hasattr(o, 'IfcObjectType') and o.IfcObjectType:
        bauteil = o.IfcObjectType
        if bauteil not in AuswahlNPKBauteil and bauteil not in NoNPKBauteil:
            FreeCAD.Console.PrintError('Unbekanntes Bauteil: ' + bauteil + '\n\n')
            raise ValueError('Unbekanntes Bauteil: ' + bauteil + '\n')
        elif bauteil == 'IfcFooting':
            return 'Fundament'
        elif bauteil == 'IfcWallStandardCase' or bauteil == 'IfcWall':
            return 'Wand'
        elif bauteil == 'Schachtwaende':
            return 'Grube'
        elif bauteil == 'Borduere':
            return 'Unterzug'
        elif bauteil == 'IfcSlab' or bauteil == 'IfcRoof':
            return 'Decke'
        elif bauteil == 'IfcColumn':
            return 'Stuetze'
        elif bauteil == 'IfcBeam':
            return 'UnterzugBruestungTraeger'
        elif bauteil == 'IfcStair':
            return 'Treppe'


# Beton Ebene 3 ***********************
def getNPKKategorie(o):
    # ist noch in einigen yaml files vorhanden, wenn diese aktualisiert dann hier loeschen
    if hasattr(o, 'Shape') and hasattr(o, 'IfcObjectType') and o.IfcObjectType:
        return ''


def getNPKKategorieFundament(o):
    if hasattr(o, 'NPKBauteil') and o.NPKBauteil == 'Fundament':
        if gt.is_sloped(o):
            return 'FundamentGeneigt'
        elif hasattr(o, 'IfcImportedProperties') and 'NPK241_Kategorie_Fundament' in o.IfcImportedProperties and o.IfcImportedProperties['NPK241_Kategorie_Fundament']:
            return str(o.IfcImportedProperties['NPK241_Kategorie_Fundament'])  # in importIFCprops fixen convert unicode to string
        elif True:
            return 'Bodenplatte'  # habe das attribut wohl nicht bei allen decken in allplan scala???
        else:
            raise ValueError('Fehler in {0} function on object: {1}\n'.format(sys._getframe().f_code.co_name, o.Name))


def getNPKKategorieWand(o):
    if hasattr(o, 'NPKBauteil') and o.NPKBauteil == 'Wand':
        gt.is_quader(o)
        if hasattr(o, 'Shape') and hasattr(o, 'IfcMaterial1') and o.IfcMaterial1:
            material = o.IfcMaterial1
            if material == 'Beton':
                return 'Standardwand'
            elif material == 'Sichtbeton':
                return 'Sichtbeton'
            elif material == 'Spezialbeton':
                return 'Kratzbeton'
        else:
            raise ValueError('Fehler in {0} function on object: {1}\n'.format(sys._getframe().f_code.co_name, o.Name))


def getNPKKategorieStuetze(o):
    if hasattr(o, 'NPKBauteil') and o.NPKBauteil == 'Stuetze':
        return 'Standardstuetze'


def getNPKKategorieDecke(o):
    if hasattr(o, 'NPKBauteil') and o.NPKBauteil == 'Decke':
        if gt.is_sloped(o):
            return 'DeckeGeneigt'
        elif hasattr(o, 'IfcImportedProperties') and 'NPK241_Kategorie_Decke' in o.IfcImportedProperties and o.IfcImportedProperties['NPK241_Kategorie_Decke']:
            return str(o.IfcImportedProperties['NPK241_Kategorie_Decke'])  # in importIFCprops fixen convert unicode to string
        elif True:
            return 'Deckenplatte'  # habe das attribut wohl nicht bei allen decken in allplan scala ???
        else:
            raise ValueError('Fehler in {0} function on object: {1}\n'.format(sys._getframe().f_code.co_name, o.Name))


def getNPKKategorieUnterzug(o):
    if hasattr(o, 'NPKBauteil') and o.NPKBauteil == 'UnterzugBruestungTraeger':
        if hasattr(o, 'Shape') and hasattr(o, 'IfcMaterial1') and o.IfcMaterial1:
            material = o.IfcMaterial1
            if material == 'Beton':
                return 'Standard'
            elif material == 'Sichtbeton':
                return 'Sichtbeton'
            elif material == 'Spezialbeton':
                return 'Kratzbeton'
        else:
            raise ValueError('Fehler in {0} function on object: {1}\n'.format(sys._getframe().f_code.co_name, o.Name))


# Beton Ebene X ***********************
def getNPK241ZweiseitigSpezial(o):
    if hasattr(o, 'NPKKategorieWand') and o.NPKKategorieWand == 'Sichtbeton' or o.NPKKategorieWand == 'Kratzbeton':
        print('look for attribut')
        if hasattr(o, 'IfcImportedProperties') and 'NPK241_Zweiseitig_Spezial' in o.IfcImportedProperties and o.IfcImportedProperties['NPK241_Zweiseitig_Spezial']:
            print('MyTrue')
            return 'MyTrue'
        else:
            print('MyFalse')
            return 'MyFalse'


# *************************************************************************************************
def getNPKBetonsorte(o):
    if hasattr(o, 'Shape') and hasattr(o, 'IfcMaterial1') and hasattr(o, 'IfcObjectType'):
        if o.IfcMaterial1 == 'Ortbeton' or o.IfcMaterial1 == 'Beton':
            if o.IfcObjectType == 'IfcFooting':
                return 'NPKC'
        if o.IfcObjectType == 'IfcWallStandardCase':
            return 'NPKC'
        if o.IfcObjectType == 'IfcColumn':
            return 'NPKA'
        if o.IfcObjectType == 'IfcSlab':
            return 'NPKA'


# *************************************************************************************************
# numerische Attribute
def getNPKAnzug(o):
    if hasattr(o, 'Shape') and hasattr(o, 'IfcObjectType'):
        if o.IfcObjectType == 'IfcWallStandardCase':
            return '0.0'  # 'IfcWallStandardCase' haben Anzug '0.0'


def getNPKDickePlatte(o):
    # PlattenList = ['IfcFooting', 'IfcSlab']
    if hasattr(o, 'Shape') and hasattr(o, 'IfcObjectType'):
        if o.IfcObjectType == 'IfcFooting':
            return '0.21 .. 0.30'  # 'IfcFooting' hat Dicke '0.21 .. 0.30'
        elif o.IfcObjectType == 'IfcSlab':
            return '0.24 .. 0.30'  # 'IfcSlab' hat Dicke '0.24 .. 0.30'


def getNPKDickeWand(o):
    if hasattr(o, 'Shape') and hasattr(o, 'IfcObjectType'):
        if o.IfcObjectType == 'IfcWallStandardCase':
            return '0.21 .. 0.25'  # 'IfcWallStandardCase' haben Dicke '0.21 .. 0.25'


def getNPKHoeheStuetze(o):
    if hasattr(o, 'Shape') and hasattr(o, 'IfcObjectType'):
        if o.IfcObjectType == 'IfcColumn':
            return '0.00 .. 3.50'  # 'IfcColumn' haben Hoehe '0.00 .. 3.50'


def getNPKHoeheVeraenderlich(o):
    pass


def setNPKHoeheVeraenderlich(o, valueNPKHoeheVeraenderlich):
    pass


def getNPKHoeheWand(o):
    if hasattr(o, 'Shape') and hasattr(o, 'IfcObjectType'):
        if o.IfcObjectType == 'IfcWallStandardCase':
            return '2.51 .. 3.50'  # 'IfcWallStandardCase' haben Hoehe '2.51 .. 3.50'


def getNPKHoeheWandEtappe(o):
    pass


def getNPKOberflaechenneigungPlatteEinseitig(o):
    PlattenList = ['IfcFooting', 'IfcSlab']
    if hasattr(o, 'Shape') and hasattr(o, 'IfcObjectType'):
        if o.IfcObjectType in PlattenList:
            return '0.0 .. 0.5'  # 'IfcFooting' und 'IfcSlab' haben Neigung '0.0 .. 0.5'


def getNPKQuerschnitt(o):
    if hasattr(o, 'Shape') and hasattr(o, 'IfcObjectType'):
        if o.IfcObjectType == 'IfcColumn':
            return '0.0  .. 0.10'      # 'IfcColumn' haben Querschnitt '0.0  .. 0.10'


def getNPKTiefe(o):
    return '0.0 .. 1.50'


def getNPKVolumenbereich(o):
    pass

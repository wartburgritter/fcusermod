# ausdruck holderbank
# decken und bpl
# diesen positionen die namen genau geben
# freecad
# import mit namen, altes ifcopenshell
# volumen der positionen aufsummieren
# habe posnr und menge

# siadatei erstellen!!!!
# was passiert bei einlesen denn, wenn die header und footerdaten nicht mit den daten in dem projekt wo eingelesen wird uebereinstimmen


from time import strftime


def gettodaysdate():
    return strftime("%d%m%y")


def myfill(s, c, n):
    """file the string s with caracter c until len l is reached
    """
    if len(s) == n:
        return (s)
    elif len(s) < n:
        while len(s) != n:
            s = s + c
        return (s)
    elif len(s) > n:
        # string abhauen auf n
        return (s)
    else:
        pass
        # error, dass sollte nicht eintreten


def getBetonKatalogLine(dataSia451):
    # partG0 = ("G241 13                                  1                                                  Ortbetonbau")
    rf = open(dataSia451, "r")
    for l in rf:
        tmp_sl = l.split()
        if tmp_sl[0] == "G241" and tmp_sl[2] == "1" and tmp_sl[3] == "Ortbetonbau":
            # print(tmp_sl[1])
            partG0 = (tmp_sl[0] + " " + tmp_sl[1] + " " * 34 + tmp_sl[2] + " " * 50 + tmp_sl[3])
    return partG0


def getprjinfos():
    # aufpassen mit prj nummer und name, bei sia import koennte bestehendes prj ueberschrieben werden
    # prjnr = "123456"
    # prjnr = "2011-07-28"
    prjnr = "z99901"
    prjname = "my export test project"
    # prjname = "MFH Hauptstrasse Holderbank"
    prjinfo = (myfill(prjnr, " ", 11) + myfill(prjname, " ", 30))
    return (prjinfo)


def sia451WritePositions(positionlist, dataSia451, wf):
    """liest positionsliste und sucht diese in data und schreibt diese nach wf
    """
    partG0 = getBetonKatalogLine(dataSia451)
    wf.write(partG0 + "\n")

    for p in positionlist:
        rf = open(dataSia451, "r")
        h = False
        for ln in rf:
            ln = ln.strip()
            # print(ln)
            if ln.startswith("G"):
                s = ln.split()[1]
                s = s[0:6]
                # print(ln)
                if p == s:
                    h = True
                    # print(ln)
                    wf.write(ln + "\n")
        rf.close
        if h is False:
            print("Position " + p + " nicht in Katalogdatei gefunden")


def sia451WritePositionAndMengen(positionsmengenlist, dataSia451, wf):
    """wie sia451WritePositions, aber es werden auch mengen geschrieben
    """
    # es wird nur die Postion in die siadatei geschrieben, die darueberliegenden ebenen generiert das ausschreibungsprogramm automatisch
    # bei R-Postitionen sind die darueberliegenden ebenen evtl. auch r-postionen, die haben andere nummer, es wird aber nur die nummer geschrieben

    partG0 = getBetonKatalogLine(dataSia451)
    wf.write(partG0 + "\n")

    for p in positionsmengenlist:
        rf = open(dataSia451, "r")
        for ln in rf:
            ln = ln.strip()
            # find the position in the catalouge
            if ln.startswith("G"):
                s = ln.split()[1]
                s = s[0:6]
                # print(ln)
                if p[0] == s:
                    # find the line for the menge and generiere zeile mit menge
                    if ln.find("                            6 A") != -1:
                        tmp_p = str(int(1000 * float(p[1])))
                        # bis 13 mit nullen auffuellen
                        # es gibt funktion zfill siehe kleines buch seite 31
                        if len(tmp_p) < 13:
                            while len(tmp_p) != 13:
                                tmp_p = ("0" + tmp_p)
                        # print(tmp_p)
                        ln = (ln + "+" + str(tmp_p))
                        # print(ln)
                    # print(ln)
                    wf.write(ln + "\n")
        rf.close


def sia451WriteHeaderStringOnly(f):
    """write file header, the initial version just copied data from a sia file
    """
    partA0 = ("A160114451-9211                          1 BABaumeister   "
              "000000000000000100000002013-12-12 Zweifamilienhaus Muri         000088         "
              "LV Betonarbeiten              Schneider Software  033 334 00 11       WinBau 1101")
    partC0 = ("C000                                     0")
    partC1 = ("C001101        00                        1 %                +                               Rabatt")
    partC2 = ("C002302        01                  0000301 %                +                               Skonto")
    partC3 = ("C003203        02                        1 %                +             +000800           MWSt")
    f.write(partA0 + "\n")
    f.write(partC0 + "\n")
    f.write(partC1 + "\n")
    f.write(partC2 + "\n")
    f.write(partC3 + "\n")


def sia451WriteHeader(f):
    """write file header
    """
    unternehmertyp = "Baumeister"
    unternehmertyp = myfill(unternehmertyp, " ", 13)

    lvtyp = "LV Betonarbeiten"
    lvtyp = myfill(lvtyp, " ", 30)

    softwarenr = ("001").ljust(15)
    # softwareinfo = "Schneider Software  033 334 00 11       WinBau 1101"
    softwareinfo = "module sia451export www.github.com      sia451.py"

    helpstring1 = "451-9211" + " " * 26 + "1 "
    helpstring2 = "00000000000000010000000"

    partA0 = ("A" + gettodaysdate() + helpstring1 + "BA" + unternehmertyp + helpstring2 + getprjinfos() + softwarenr + lvtyp + softwareinfo)

    # BA, CA, BC  # Was bedeuted das, gehoert zu LVUnternehmerty
    # bei BA = 0, bei BC = 1, bei CA = 0 oder 1 am Ende der vielen Nullen
    # "BC" + unternehmertyp + "00000000000000010000001"
    # "CA" + unternehmertyp + "0000000000000001000000"
    # "CA" + unternehmertyp + "00000000000000010000001"

    # softwarenr = 16 Stellen
    ## winbau --> zu 99% wohl interne nicht sichtbare prjhochzaehlnr.
    ## bauplus --> 001
    ## sorba --> prjnr

    partC0 = ("C000                                     0")
    partC1 = ("C001101        00                        1 %                +                               Rabatt")
    partC2 = ("C002302        01                  0000301 %                +                               Skonto")
    partC3 = ("C003203        02                        1 %                +             +000800           MWSt")
    f.write(partA0 + "\n")
    f.write(partC0 + "\n")
    f.write(partC1 + "\n")
    f.write(partC2 + "\n")
    f.write(partC3 + "\n")


def sia451WriteFooterStringOnly(f):
    """write file footer, the initial version just copied data from a sia file
    """
    partZ0 = ("Z160114                                      0000000000922   -000000000001       2013-12-12 Zweifamilienhaus Muri")
    f.write(partZ0 + "\n")


def sia451WriteFooter(f):
    """write file footer
    """
    helpstring3 = (" " * 38)
    linenumbers = "0000000006969"
    helpstring4 = ("   -000000000001       ")

    partZ0 = ("Z" + gettodaysdate() + helpstring3 + linenumbers + helpstring4 + getprjinfos())
    f.write(partZ0 + "\n")


def readPositionsList(positionlistfile):
    rf = open(positionlistfile, "r")
    pl = []
    for ln in rf:
        ln = ln.strip()
        pl.append(ln)
    # print(pl)
    rf.close
    return pl


def readPositionMengenList(positionsmengenfile):
    rf = open(positionsmengenfile, "r")
    pml = []
    for ln in rf:
        ln = ln.strip()
        s = ln.split()
        pml.append(s)
    #   print(pml)
    rf.close
    return pml


def writeSIA451FilePositionen(positionlist, dataSia451, wf):
    """nur postionen schreiben
    """
    sia451WriteHeader(wf)
    sia451WritePositions(positionlist, dataSia451, wf)
    sia451WriteFooter(wf)


def writeSIA451FileMengen(positionsmengenlist, dataSia451, wf):
    """mengen schreiben impliziert position schreiben
    """
    sia451WriteHeader(wf)
    sia451WritePositionAndMengen(positionsmengenlist, dataSia451, wf)
    sia451WriteFooter(wf)


def writeSIA451FilePreise():
    """preise schreiben impliziert postionen und preise schreiben
    """
    pass


# *************************************************************************************************
if __name__ == "__main__":

    # test the parser
    # python sia451parser.py

    positionlist = [
        "611112", "611113", "613111", "615111", "615112", "615121",
        "615122", "622124", "622401", "631111", "631112", "631122",
        "631131", "631211", "631212", "631222", "631231", "631401",
        "651111", "651131", "711201", "721104", "721105", "721301",
        "722301", "725001", "743001", "746111", "746301"
    ]

    positionsmengenlist = [
        ["615122", "71.1774623247"],
        ["615124", "1.443"],
        ["622401", "21.8656159999"],
        ["631211", "2.5810113739"],
        ["631212", "83.4767472248"],
        ["631222", "40.1193000907"],
        ["631401", "9.43509985889"],
        ["651111", "0.15875"],
        ["721104", "8.37623997387"],
        ["721105", "270.615409428"],
        ["722301", "50.09312"],
        ["725001", "1.17659999999"],
        ["746111", "5.13792000961"]
    ]

    positionsmengenlist = [
        ["619000", ""],
        ["619001", 71.17746232469815],
        ["629000", ""],
        ["629001", 23.308615999922292],
        ["639000", ""],
        ["639001", 138.46876879132077],
        ["659000", ""],
        ["659001", 0.15875000000000014],
        ["72900", ""],
        ["729001", 330.26136940147956],
        ["749000", ""],
        ["749001", 5.137920009611196]
    ]
    # siehe methode  sia451WritePositionAndMengen fuer erlaeuterung derzusaetzlichen zeilen

    # Katalogdatei
    # kann nak nicht als katalog nehmen, weil da teilobjekte drin sind
    # dataSia451 = "muri.01s"
    # dataSia451 = "muri-sia_ohnemengen.01s"
    # dataSia451 = "betonkatalog.01s"
    # dataSia451 = "/home/hugo/Documents/projekte--ifc/sia451/schnittstellenparser/betonkatalog.01s"
    dataSia451 = "/home/hugo/.FreeCAD/Mod/SwissNPK/data-katalog/bt.01s"

    header = """A081214451-9211                          1 BAImport01     00000000000000010000000z98765     SIA451--Projektdaten--fuer--Im000095         SwissNPK generated by FreeCAD Schneider Software  033 334 00 11       WinBau 1101
C000                                     0
C001101        00                        1 %                +                               Rabatt
C002302        01                  0000301 %                +                               Skonto
C003203        02                        1 %                +             +000800           MWSt
"""

    footer = """Z081214                                      0000000000053   -000000000001       z98765     SIA451--Projektdaten--fuer--Im
"""

    # Ausgabedatei
    outfile = "sia-my1.01s"
    wf = open(outfile, "w")

    wf.write(header)
    sia451WritePositionAndMengen(positionsmengenlist, dataSia451, wf)
    wf.write(footer)

    wf.close

    # writeSIA451FilePositionen(positionlist, dataSia451, wf)
    # wf.close

    # writeSIA451FileMengen(positionsmengenlist, dataSia451,wf)
    # wf.close

    # sia451WriteHeaderStringOnly(wf)
    # sia451WriteHeader(wf)
    # sia451WritePositions(positionlist, dataSia451,wf)
    # sia451WritePositionAndMengen(positionsmengenlist, dataSia451,wf)
    # sia451WriteFooterStringOnly(wf)
    # sia451WriteFooter(wf)
    # wf.close

# ***************************************************************************
# *   Copyright (c) 2022 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

"""
# in FreeCAD
import importlib
import create_LV_data

importlib.reload(create_LV_data)
create_LV_data.create_LV_from_testdata_tiefbau()

"""

import importlib
import json
import os
import pandas as pd
import sys
import yaml
from copy import deepcopy
from os.path import join

import FreeCAD
import sia451parser2
importlib.reload(sia451parser2)


# ************************************************************************************************
# TODO
# get rid of the last part of parser module (the real data)
# Vor allem Header und Footer sollten selber geschrieben werden !!!!!
#
# TODO umstieg auf e1s format https://www.crb.ch/Stories/IfA18.html
# https://www.crb.ch/Support/Datenzugriff_Datenaustausch.html
#
# Testprogramm SIATEST des CRB nutzen alle Bausoftwarehersteller
# https://www.crb.ch/Stories/Optimierungen_Datenaustausch.html


# ************************************************************************************************
# TODO long
# anstatt dict eine Klasse, das waere dann der Beginn
# eines richtigen sia451 Python parser
# TODO, Zusatzinformationen fuer SIA451 recordtyp 3 zeilen ???


# ************************************************************************************************
# datenmodell
# npk_data = {
#     "Katalognummer_Jahr": {
#         "Positionsnummer": {
#             "Einheit": "m2",
#             "Mengen" : {
#                 "Gliederung1": Menge_als_float, # "keine" bei ohne Gliederung
#                 "Gliederung2": Menge_als_float,
#             },
#             "SIA451OutString": "The_Out_String", # nur im SIA451 out data dict
#         },
#     },
# }


# ************************************************************************************************
def create_LV_data(npk_data=None, lvdata_export_dir=None, npk_lib_file=None, out_pre_name=""):

    print("Start create_LV_data")

    # round all npk_data mengen to the next bigger integer
    from math import ceil
    # https://realpython.com/python-rounding/

    rounded_npk_data = deepcopy(npk_data)
    # wobei ein copy reichen wuerde
    # da in dem dict keine ref auf eines objekt als identifier
    # https://stackoverflow.com/a/17246744

    for catalog_id, catalog_data in npk_data.items():
        for position_id, position_data in catalog_data.items():
            for teil_prj_name, teil_prj_menge in position_data["Mengen"].items():
                if teil_prj_menge > 1.0:
                    new_teil_prj_menge = ceil(teil_prj_menge)
                else:
                    new_teil_prj_menge = teil_prj_menge
                # print(teil_prj_menge, new_teil_prj_menge)
                rounded_npk_data[catalog_id][position_id]["Mengen"][teil_prj_name] = new_teil_prj_menge

    npk_data = rounded_npk_data

    # print(json.dumps(npk_data, indent=4))

    # export yaml
    out_file_yaml = join(lvdata_export_dir, out_pre_name + "_Mengen_Positionen_aus_BIMModell.yaml")
    # print(out_file_yaml)
    yf = open(out_file_yaml, "w")
    yaml.dump(npk_data, yf, default_flow_style=False)
    yf.close()

    # export to csv 
    # bei mehreren Gliederungen gibt es mehrere zeilen je position
    # 228_22, 211102, m, TAZ, 10.0
    # 228_22, 243111, m3, TAZ, 6.0
    # to be sure sorting is by key use keys() and not items()
    all_position_props = []
    for catalog_id in sorted(npk_data.keys()):
        catalog_data = npk_data[catalog_id]
        for position_id in sorted(catalog_data.keys()):
            position_data = catalog_data[position_id]
            for teil_prj_name in sorted(position_data["Mengen"].keys()):
                teil_prj_menge = position_data["Mengen"][teil_prj_name]
                position_props = {
                    "Katalog": catalog_id,
                    "Position": position_id,
                    "Einheit": position_data["Einheit"],
                    "Teilprojekt": teil_prj_name,
                    "Mengensumme": round(teil_prj_menge, 3),
                }
                all_position_props.append(position_props)
    # print(all_position_props)
    df_positions_props = pd.DataFrame(all_position_props).sort_values(
        by=["Position", "Teilprojekt"],
        ascending=[True, True]  # sort an dritter stelle nach Menge, aber die groesse menge zuerst
    )

    # print(df_positions_props)
    out_file_position_props = join(lvdata_export_dir, out_pre_name + "_Mengen_Positionen_aus_BIMModell.csv")
    df_positions_props.to_csv(out_file_position_props, encoding="utf-8", index=False)

    # keep in mind above are all positions found in modell
    # but only the once found in the npk-library will be exported by the 451 exporter
    # TODO print error if a position has not been found

    # export to SIA451
    print(npk_lib_file)
    if not os.path.isfile(npk_lib_file):
        print("NPK lib file does not exist. SIA451 export is not available.")
        return
    sia451_out_file = join(lvdata_export_dir, out_pre_name + "_NPK_SIA451_aus_BIMModell.01s")
    print(npk_lib_file)
    print(sia451_out_file)
    out = sia451parser2.export2SIA451(npk_lib_file, sia451_out_file, npk_data, out_pre_name=out_pre_name)


# ************************************************************************************************
def create_LV_from_testdata_tiefbau():

    test_npk_data = {"228_22": {}}
    test_npk_228data = test_npk_data["228_22"]
    test_npk_228data["211102"] = {
        "Einheit": "m",
        "Mengen" : {"TAZ": 10.01}
    }
    test_npk_228data["243111"] = {
        "Einheit": "m3",
        "Mengen" : {"TAZ": 6.49}
    }
    test_npk_228data["315102"] = {
        "Einheit": "m2",
        "Mengen" : {"TAZ": 260.5}
    }
    test_npk_228data["481201"] = {
        "Einheit": "m3",
        "Mengen" : {"TAZ": 5.0}
    }

    npk_lib_file = join("C:", os.sep, "0_BHA_privat", "BIMTesterAll", "ztemp_lv_mengen_sia451", "npklib", "19104_SIA451_NPK_library.01S")
    lv_data_out_dir = join("C:", os.sep, "0_BHA_privat", "BIMTesterAll", "ztemp_lv_mengen_sia451")
    create_LV_data(test_npk_data, lv_data_out_dir, npk_lib_file, "test")

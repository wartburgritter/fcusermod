'''
import femexample007
import importlib
importlib.reload(femexample007)
femexample007.make_example_file()

'''


'''
oofem manual example PlaneStress2D.in
'''


def make_example_file(filepath=None):
    if not filepath:
        filepath = u'/home/hugo/Desktop'
    import FreeCAD
    doclabel = u'femexample007'
    docpath = filepath + u'/' + doclabel + u'.FCStd'
    adoc = FreeCAD.newDocument(doclabel)
    create_example(adoc)
    run(adoc)
    print(docpath)
    adoc.saveAs(docpath)
    # FreeCAD.closeDocument(doclabel)


def create_example(doc):
    '''multi body face
    '''
    # parts
    face_obj = doc.addObject('Part::Plane', 'Face')
    face_obj.Width = 4
    face_obj.Length = 9
    import FreeCADGui
    FreeCADGui.ActiveDocument.activeView().viewAxonometric()
    FreeCADGui.SendMsgToActiveView("ViewFit")
    doc.recompute()

    # analysis
    import ObjectsFem
    analysis = ObjectsFem.makeAnalysis(doc, 'Analysis')

    # solver
    solver_object_ccx = analysis.addObject(ObjectsFem.makeSolverCalculixCcxTools(doc, 'CalculiXccxTools'))[0]
    solver_object_ccx.GeometricalNonlinearity = 'linear'
    solver_object_ccx.ThermoMechSteadyState = False
    solver_object_ccx.MatrixSolverType = 'default'
    solver_object_ccx.IterationsControlParameterTimeUse = False
    solver_object_ccx.WorkingDir = u''

    solv_obj_oofem = analysis.addObject(ObjectsFem.makeSolverOOFEM(doc, 'SolverOOFEM'))[0]

    # shell thickness
    thickness_obj = analysis.addObject(ObjectsFem.makeElementGeometry2D(doc, 0, 'ShellThickness'))[0]
    thickness_obj.Thickness = 1.0

    # materials
    material_object = analysis.addObject(ObjectsFem.makeMaterialSolid(doc, 'FemMaterial'))[0]
    mat = {}
    mat['Name'] = "Concrete-Generic"
    mat['YoungsModulus'] = "15 MPa"
    mat['PoissonRatio'] = "0.25"
    mat['Density'] = "0.0 kg/m^3"
    mat['ThermalExpansionCoefficient'] = "1.0 m/m/K"
    material_object.Material = mat

    # displacement_constraints
    # festes lager
    displacement_constraint1 = analysis.addObject(ObjectsFem.makeConstraintDisplacement(doc, name="ConstraintDisplacmentFest"))[0]
    displacement_constraint1.References = [(doc.Face, "Vertex1"), (doc.Face, "Vertex2")]
    displacement_constraint1.xFix = True
    displacement_constraint1.yFix = True
    displacement_constraint1.zFix = False
    displacement_constraint1.xFree = False
    displacement_constraint1.yFree = False
    displacement_constraint1.zFree = True
    displacement_constraint1.rotxFix = False
    displacement_constraint1.rotyFix = False
    displacement_constraint1.rotzFix = False
    displacement_constraint1.rotxFree = True
    displacement_constraint1.rotyFree = True
    displacement_constraint1.rotzFree = True
    displacement_constraint1.xDisplacement = 0.0
    displacement_constraint1.yDisplacement = 0.0
    displacement_constraint1.zDisplacement = 0.0
    displacement_constraint1.xRotation = 0.0
    displacement_constraint1.yRotation = 0.0
    displacement_constraint1.zRotation = 0.0
    # hozizontal bewegliches lager (Gleitlager)
    displacement_constraint1 = analysis.addObject(ObjectsFem.makeConstraintDisplacement(doc, name="ConstraintDisplacmentGleit"))[0]
    displacement_constraint1.References = [(doc.Face, "Vertex3"), (doc.Face, "Vertex4")]
    displacement_constraint1.xFix = False
    displacement_constraint1.yFix = True
    displacement_constraint1.zFix = False
    displacement_constraint1.xFree = True
    displacement_constraint1.yFree = False
    displacement_constraint1.zFree = True
    displacement_constraint1.rotxFix = False
    displacement_constraint1.rotyFix = False
    displacement_constraint1.rotzFix = False
    displacement_constraint1.rotxFree = True
    displacement_constraint1.rotyFree = True
    displacement_constraint1.rotzFree = True
    displacement_constraint1.xDisplacement = 0.0
    displacement_constraint1.yDisplacement = 0.0
    displacement_constraint1.zDisplacement = 0.0
    displacement_constraint1.xRotation = 0.0
    displacement_constraint1.yRotation = 0.0
    displacement_constraint1.zRotation = 0.0

    # force_constraint
    force_constraint = analysis.addObject(ObjectsFem.makeConstraintForce(doc, name="ConstraintForce"))[0]
    force_constraint.References = [(doc.Face, "Vertex3"), (doc.Face, "Vertex4")]
    force_constraint.Force = 5.0  # two nodes each 2.5 N = 5.0 N
    force_constraint.Direction = (doc.Face, ["Edge2"])
    force_constraint.Reversed = False

    # mesh
    import Fem
    fem_mesh = Fem.FemMesh()
    fem_mesh.addNode(0.0, 0.0, 0.0, 1)
    fem_mesh.addNode(0.0, 4.0, 0.0, 2)
    fem_mesh.addNode(2.0, 2.0, 0.0, 3)
    fem_mesh.addNode(3.0, 1.0, 0.0, 4)
    fem_mesh.addNode(8.0, 0.8, 0.0, 5)
    fem_mesh.addNode(7.0, 3.0, 0.0, 6)
    fem_mesh.addNode(9.0, 0.0, 0.0, 7)
    fem_mesh.addNode(9.0, 4.0, 0.0, 8)
    fem_mesh.addFace([1, 4, 3, 2], 1)
    fem_mesh.addFace([1, 7, 5, 4], 2)
    fem_mesh.addFace([4, 5, 6, 3], 3)
    fem_mesh.addFace([3, 6, 8, 2], 4)
    fem_mesh.addFace([5, 7, 8, 6], 5)
    femmesh_obj = analysis.addObject(doc.addObject('Fem::FemMeshObject', 'Face_Mesh'))[0]
    femmesh_obj.FemMesh = fem_mesh
    femmesh_obj.ViewObject.Visibility = False

    # recompute
    doc.recompute()


def run(doc):
    import FemGui
    FemGui.setActiveAnalysis(doc.Analysis)  # set the analysis with Name Analysis to the ActiveAnalysis
    from femtools import ccxtools
    fea = ccxtools.FemToolsCcx()
    fea.purge_results()
    # fea.run()

    # recompute
    doc.recompute()


#######################################################################################
# Wenn in tetra mesh nicht zum shape passe wird keine element fuer facenode gefunden,
'''
import Part
box_obj = App.ActiveDocument.addObject('Part::Feature', 'Box')
box = Part.makeBox(8000, 1000, 1000)
box_obj.Shape = box
App.ActiveDocument.recompute()

import Part
box_obj = App.ActiveDocument.addObject('Part::Box', 'Box')
box_obj.Height = box_obj.Width = 1000
box_obj.Length = 8000
App.ActiveDocument.recompute()
'''

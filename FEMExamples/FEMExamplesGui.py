# -*- coding: utf-8 -*-
# FreeCAD tools of the FEMExamples
# (c) 2016 Bernd Hahnebach

# ***************************************************************************
# *   (c) Bernd Hahnebach (bernd@bimstatik.org) 2016                        *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# *   Bernd Hahnebach 2016                                                  *
# ***************************************************************************/


# import FreeCAD
import FreeCADGui
import importlib

class TOOL1():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        print('Command 1')
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            o = selection[0]
            print('Selected Object: ', o.Name)
            if hasattr(o, "Shape"):
                print('Volume = ', o.Shape.Volume * 1E-9)
        else:
            print('Mehr als ein Objekt selektiert!')

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'Report Volume', 'ToolTip': 'Report Volume'}


class TOOL2():
    def Activated(self):
        import runall
        import importlib
        importlib.reload(runall)
        runall.runall()

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'runall', 'ToolTip': 'Run all examples!'}


FreeCADGui.addCommand('FEMExamples_tool1', TOOL1())
FreeCADGui.addCommand('FEMExamples_tool2', TOOL2())

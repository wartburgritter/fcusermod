import Fem
newmesh = Fem.FemMesh()

import FreeCAD
oldmesh = FreeCAD.ActiveDocument.getObject("Box_Mesh").FemMesh
oldmesh
ns = oldmesh.Nodes
for n in ns:
    # newmesh.addNode(ns[n].z * 1000, ns[n].y * 1000, ns[n].x * 1000, n)  # face normale nach innen
    newmesh.addNode(ns[n].x * 1000, ns[n].y * 1000, ns[n].z * 1000, n)

es = oldmesh.Volumes
for e in es:
    newmesh.addVolume(list(oldmesh.getElementNodes(e)), e)

newmesh
femmesh_obj = FreeCAD.ActiveDocument.addObject('Fem::FemMeshObject', 'New_Mesh')
femmesh_obj.FemMesh = newmesh

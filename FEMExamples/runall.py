'''
import runall
import importlib
importlib.reload(runall)
runall.runall()

'''

import importlib
import FreeCAD
if FreeCAD.GuiUp:
    import FreeCADGui


def runall(filepath=None):
    if not filepath:
        import FreeCAD
        import os
        examplepath = os.path.join("Mod", "FEMExamples")
        filepath = os.path.join(FreeCAD.ConfigGet("UserAppData"), examplepath)
        print(filepath)

    import femexample004
    importlib.reload(femexample004)
    femexample004.make_example_file(filepath)
    if FreeCAD.GuiUp:
        FreeCADGui.updateGui()

    import femexample006
    importlib.reload(femexample006)
    femexample006.make_example_file(filepath)
    if FreeCAD.GuiUp:
        FreeCADGui.updateGui()

    # oofem example
    # import femexample007
    # importlib.reload(femexample007)
    # femexample007.make_example_file(filepath)
    # if FreeCAD.GuiUp:
    #     FreeCADGui.updateGui()

'''
import femexample006
import importlib
importlib.reload(femexample006)
femexample006.make_example_file()

'''


'''
multibody analysis without analysis groups, but with mesh regions (is it really without analysis groups ?!?)
'''


def make_example_file(filepath=None):
    if not filepath:
        filepath = u'/home/hugo/Desktop'
    import FreeCAD
    doclabel = u'femexample006'
    docpath = filepath + u'/' + doclabel + u'.FCStd'
    adoc = FreeCAD.newDocument(doclabel)
    create_example(adoc)
    run(adoc)
    print(docpath)
    adoc.saveAs(docpath)
    # FreeCAD.closeDocument(doclabel)


def create_example(doc):
    '''multi body
    '''
    # parts
    box_obj1 = doc.addObject('Part::Box', 'Box1')
    box_obj1.Height = 10
    box_obj1.Width = 10
    box_obj1.Length = 20
    box_obj2 = doc.addObject('Part::Box', 'Box2')
    box_obj2.Height = 10
    box_obj2.Width = 10
    box_obj2.Length = 20
    box_obj2.Placement.Base = (20, 0, 0)
    box_obj3 = doc.addObject('Part::Box', 'Box3')
    box_obj3.Height = 10
    box_obj3.Width = 10
    box_obj3.Length = 20
    box_obj3.Placement.Base = (40, 0, 0)
    box_obj4 = doc.addObject('Part::Box', 'Box4')
    box_obj4.Height = 10
    box_obj4.Width = 10
    box_obj4.Length = 20
    box_obj4.Placement.Base = (60, 0, 0)
    box_obj5 = doc.addObject('Part::Box', 'Box5')
    box_obj5.Height = 10
    box_obj5.Width = 10
    box_obj5.Length = 20
    box_obj5.Placement.Base = (80, 0, 0)
    box_obj6 = doc.addObject('Part::Box', 'Box6')
    box_obj6.Height = 10
    box_obj6.Width = 10
    box_obj6.Length = 20
    box_obj6.Placement.Base = (100, 0, 0)
    box_obj7 = doc.addObject('Part::Box', 'Box7')
    box_obj7.Height = 10
    box_obj7.Width = 10
    box_obj7.Length = 20
    box_obj7.Placement.Base = (120, 0, 0)
    box_obj8 = doc.addObject('Part::Box', 'Box8')
    box_obj8.Height = 10
    box_obj8.Width = 10
    box_obj8.Length = 20
    box_obj8.Placement.Base = (140, 0, 0)
    box_obj9 = doc.addObject('Part::Box', 'Box9')
    box_obj9.Height = 10
    box_obj9.Width = 10
    box_obj9.Length = 20
    box_obj9.Placement.Base = (160, 0, 0)
    box_obj10 = doc.addObject('Part::Box', 'Box10')
    box_obj10.Height = 10
    box_obj10.Width = 10
    box_obj10.Length = 20
    box_obj10.Placement.Base = (180, 0, 0)
    import FreeCADGui
    FreeCADGui.ActiveDocument.activeView().viewAxonometric()
    FreeCADGui.SendMsgToActiveView("ViewFit")

    # make a CompSolid out of the boxes, to be able to remesh with GUI
    import BOPTools.SplitFeatures
    bf = BOPTools.SplitFeatures.makeBooleanFragments(name='BooleanFragments')
    bf.Objects = [box_obj1, box_obj2, box_obj3, box_obj4, box_obj5, box_obj6, box_obj7, box_obj8, box_obj9, box_obj10]
    bf.Mode = "CompSolid"
    bf.Proxy.execute(bf)
    bf.purgeTouched()
    for obj in bf.ViewObject.Proxy.claimChildren():
        obj.ViewObject.hide()
    doc.recompute()

    # the parametric way to extract the CompSolid
    import CompoundTools.CompoundFilter
    cf = CompoundTools.CompoundFilter.makeCompoundFilter(name='CompoundFilter')
    cf.Base = bf
    cf.FilterType = 'window-volume'
    cf.Proxy.execute(cf)
    cf.purgeTouched()
    cf.Base.ViewObject.hide()
    doc.recompute()
    # the old non parametric way to extract the CompSolid
    # bf.ViewObject.hide()
    # cs_obj = doc.addObject('Part::Feature', 'CompSolid')
    # cs_obj.Shape = bf.Shape.CompSolids[0]

    # analysis
    import ObjectsFem
    analysis = ObjectsFem.makeAnalysis(doc, 'Analysis')

    # solver
    solver_object = analysis.addObject(ObjectsFem.makeSolverCalculixCcxTools(doc, 'CalculiXccxTools'))[0]
    solver_object.GeometricalNonlinearity = 'linear'
    solver_object.ThermoMechSteadyState = False
    solver_object.MatrixSolverType = 'default'
    solver_object.IterationsControlParameterTimeUse = False
    solver_object.WorkingDir = u''

    # materials
    # material1
    material_object1 = analysis.addObject(ObjectsFem.makeMaterialSolid(doc, 'FemMaterial1'))[0]
    material_object1.References = [(doc.Box5, "Solid1"), (doc.Box6, "Solid1")]
    mat = material_object1.Material
    mat['Name'] = "Concrete-Generic"
    mat['YoungsModulus'] = "32000 MPa"
    mat['PoissonRatio'] = "0.17"
    mat['Density'] = "0 kg/m^3"
    material_object1.Material = mat
    # material2
    material_object2 = analysis.addObject(ObjectsFem.makeMaterialSolid(doc, 'FemMaterial2'))[0]
    material_object2.References = [(doc.Box3, "Solid1"), (doc.Box8, "Solid1")]
    mat = material_object2.Material
    mat['Name'] = "PLA"
    mat['YoungsModulus'] = "3640 MPa"
    mat['PoissonRatio'] = "0.36"
    mat['Density'] = "0 kg/m^3"
    material_object2.Material = mat
    # material3
    material_object3 = analysis.addObject(ObjectsFem.makeMaterialSolid(doc, 'FemMaterial3'))[0]
    material_object3.References = [(doc.Box1, "Solid1"), (doc.Box10, "Solid1")]
    mat = material_object3.Material
    mat['Name'] = "Steel-Generic"
    mat['YoungsModulus'] = "200000 MPa"
    mat['PoissonRatio'] = "0.30"
    mat['Density'] = "7900 kg/m^3"
    material_object3.Material = mat
    # material3
    material_object4 = analysis.addObject(ObjectsFem.makeMaterialSolid(doc, 'FemMaterial4'))[0]
    material_object4.References = []
    mat = material_object3.Material
    mat['Name'] = "Wood-Generic"
    mat['YoungsModulus'] = "120000 MPa"
    mat['PoissonRatio'] = "0.05"
    mat['Density'] = "700 kg/m^3"
    material_object4.Material = mat

    # fixed_constraint
    fixed_constraint = analysis.addObject(ObjectsFem.makeConstraintFixed(doc, name="ConstraintFixed"))[0]
    fixed_constraint.References = [(doc.Box1, "Face1"), (doc.Box10, "Face2")]

    # force_constraint
    force_constraint = analysis.addObject(ObjectsFem.makeConstraintForce(doc, name="ConstraintForce"))[0]
    force_constraint.References = [(doc.Box3, "Face6"), (doc.Box8, "Face6")]
    force_constraint.Force = 5000.00
    force_constraint.Direction = (doc.Box1, ["Edge1"])
    force_constraint.Reversed = True

    # fem mesh
    # mesh
    femmesh_obj = analysis.addObject(ObjectsFem.makeMeshGmsh(doc, cf.Name + '_Mesh'))[0]
    femmesh_obj.Part = cf
    femmesh_obj.CharacteristicLengthMax = 8
    cf.ViewObject.Visibility = False
    # mesh regions
    # they are not added to the analysis, because than they would show up twice, there would be some
    # development needed to surpress this, I had a forum topic about this with werner and ickby
    # same for equation object
    mesh_reg1 = ObjectsFem.makeMeshRegion(doc, femmesh_obj, 2.0, 'Region_2mm')
    mesh_reg1.References = [(cf, "Face51"), (cf, "Solid8")]
    mesh_reg2 = ObjectsFem.makeMeshRegion(doc, femmesh_obj, 0.2, 'Region_02mm')
    mesh_reg2.References = [(cf, "Edge38"), (cf, "Edge43"), (cf, "Vertex1")]
    mesh_reg3 = ObjectsFem.makeMeshRegion(doc, femmesh_obj, 1.0, 'Region_1mm')
    mesh_reg3.References = [(cf, "Face14")]

    # meshing
    from femmesh import gmshtools
    gmsh_mesh = gmshtools.GmshTools(femmesh_obj)
    error = gmsh_mesh.create_mesh()
    print(error)

    # recompute
    doc.recompute()


def run(doc):
    import FemGui
    FemGui.setActiveAnalysis(doc.Analysis)  # set the analysis with Name Analysis to the ActiveAnalysis
    from femtools import ccxtools
    fea = ccxtools.FemToolsCcx()
    fea.update_objects()
    fea.purge_results()
    fea.run()

    # recompute
    doc.recompute()


#######################################################################################
# Wenn in tetra mesh nicht zum shape passe wird keine element fuer facenode gefunden,
'''
import Part
box_obj = App.ActiveDocument.addObject('Part::Feature', 'Box')
box = Part.makeBox(8000, 1000, 1000)
box_obj.Shape = box
App.ActiveDocument.recompute()

import Part
box_obj = App.ActiveDocument.addObject('Part::Box', 'Box')
box_obj.Height = box_obj.Width = 1000
box_obj.Length = 8000
App.ActiveDocument.recompute()
'''

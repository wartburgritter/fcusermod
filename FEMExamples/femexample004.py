'''
import femexample004
import importlib
importlib.reload(femexample004)
femexample004.make_example_file()

'''


'''
line load vs. point load
unit test for edge load calculation
'''


def make_example_file(filepath=None):
    if not filepath:
        filepath = u'/home/hugo/Desktop'
    import FreeCAD
    doclabel = u'femexample004'
    docpath = filepath + u'/' + doclabel + u'.FCStd'
    adoc = FreeCAD.newDocument(doclabel)
    create_example(adoc)
    run(adoc)
    print(docpath)
    adoc.saveAs(docpath)
    # FreeCAD.closeDocument(doclabel)


def create_example(doc):
    # parts
    import FreeCADGui
    import Part
    from FreeCAD import Base
    l1 = Part.makeLine((-142.5, -142.5, 0), (142.5, -142.5, 0))
    l2 = Part.makeLine((142.5, -142.5, 0), (142.5, 142.5, 0))
    l3 = Part.makeLine((142.5, 142.5, 0), (-142.5, 142.5, 0))
    l4 = Part.makeLine((-142.5, 142.5, 0), (-142.5, -142.5, 0))
    w = Part.Wire([l1, l2, l3, l4])
    s = w.extrude(Base.Vector(0, 0, 1000))
    forces_obj = doc.addObject('Part::Feature', 'SquareTube')
    forces_obj.Shape = s
    FreeCADGui.ActiveDocument.activeView().viewAxonometric()
    FreeCADGui.SendMsgToActiveView("ViewFit")

    '''
    # brauch ich hier doch gar nicht ?!
    points_forces = []
    points_forces.append(Part.Vertex(-142.5, 142.5, 0.0))
    points_forces.append(Part.Vertex(-142.5, -142.5, 0.0))
    points_forces.append(Part.Vertex(-142.5, 95.0, 0.0))
    points_forces.append(Part.Vertex(-142.5, 47.5, 0.0))
    points_forces.append(Part.Vertex(-142.5, 0.0, 0.0))
    points_forces.append(Part.Vertex(-142.5, -47.5, 0.0))
    points_forces.append(Part.Vertex(-142.5, -95.0, 0.0))
    points_forces.append(Part.Vertex(142.5, -142.5, 0.0))
    points_forces.append(Part.Vertex(-95.0, -142.5, 0.0))
    points_forces.append(Part.Vertex(-47.5, -142.5, 0.0))
    points_forces.append(Part.Vertex(0.0, -142.5, 0.0))
    points_forces.append(Part.Vertex(47.5, -142.5, 0.0))
    points_forces.append(Part.Vertex(95.0, -142.5, 0.0))
    points_forces.append(Part.Vertex(142.5, 142.5, 0.0))
    points_forces.append(Part.Vertex(142.5, -95.0, 0.0))
    points_forces.append(Part.Vertex(142.5, -47.5, 0.0))
    points_forces.append(Part.Vertex(142.5, 0.0, 0.0))
    points_forces.append(Part.Vertex(142.5, 47.5, 0.0))
    points_forces.append(Part.Vertex(142.5, 95.0, 0.0))
    points_forces.append(Part.Vertex(95.0, 142.5, 0.0))
    points_forces.append(Part.Vertex(47.5, 142.5, 0.0))
    points_forces.append(Part.Vertex(0.0, 142.5, 0.0))
    points_forces.append(Part.Vertex(-47.5, 142.5, 0.0))
    points_forces.append(Part.Vertex(-95.0, 142.5, 0.0))
    points_forces.append(Part.Vertex(-142.5, 118.75, 0.0))
    points_forces.append(Part.Vertex(-142.5, -118.75, 0.0))
    points_forces.append(Part.Vertex(-142.5, 71.25, 0.0))
    points_forces.append(Part.Vertex(-142.5, 23.75, 0.0))
    points_forces.append(Part.Vertex(-142.5, -23.75, 0.0))
    points_forces.append(Part.Vertex(-142.5, -71.25, 0.0))
    points_forces.append(Part.Vertex(118.75, -142.5, 0.0))
    points_forces.append(Part.Vertex(-71.25, -142.5, 0.0))
    points_forces.append(Part.Vertex(-118.75, -142.5, 0.0))
    points_forces.append(Part.Vertex(-23.75, -142.5, 0.0))
    points_forces.append(Part.Vertex(23.75, -142.5, 0.0))
    points_forces.append(Part.Vertex(71.25, -142.5, 0.0))
    points_forces.append(Part.Vertex(142.5, 118.75, 0.0))
    points_forces.append(Part.Vertex(142.5, -71.25, 0.0))
    points_forces.append(Part.Vertex(142.5, -118.75, 0.0))
    points_forces.append(Part.Vertex(142.5, -23.75, 0.0))
    points_forces.append(Part.Vertex(142.5, 23.75, 0.0))
    points_forces.append(Part.Vertex(142.5, 71.25, 0.0))
    points_forces.append(Part.Vertex(71.25, 142.5, 0.0))
    points_forces.append(Part.Vertex(118.75, 142.5, 0.0))
    points_forces.append(Part.Vertex(23.75, 142.5, 0.0))
    points_forces.append(Part.Vertex(-23.75, 142.5, 0.0))
    points_forces.append(Part.Vertex(-71.25, 142.5, 0.0))
    points_forces.append(Part.Vertex(-118.75, 142.5, 0.0))


    points_fixes = []
    points_fixes.append(Part.Vertex(-142.5, 142.5, 1000.0))
    points_fixes.append(Part.Vertex(-142.5, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(-142.5, 95.0, 1000.0))
    points_fixes.append(Part.Vertex(-142.5, 47.5, 1000.0))
    points_fixes.append(Part.Vertex(-142.5, 0.0, 1000.0))
    points_fixes.append(Part.Vertex(-142.5, -47.5, 1000.0))
    points_fixes.append(Part.Vertex(-142.5, -95.0, 1000.0))
    points_fixes.append(Part.Vertex(142.5, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(-95.0, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(-47.5, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(0.0, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(47.5, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(95.0, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(142.5, 142.5, 1000.0))
    points_fixes.append(Part.Vertex(142.5, -95.0, 1000.0))
    points_fixes.append(Part.Vertex(142.5, -47.5, 1000.0))
    points_fixes.append(Part.Vertex(142.5, 0.0, 1000.0))
    points_fixes.append(Part.Vertex(142.5, 47.5, 1000.0))
    points_fixes.append(Part.Vertex(142.5, 95.0, 1000.0))
    points_fixes.append(Part.Vertex(95.0, 142.5, 1000.0))
    points_fixes.append(Part.Vertex(47.5, 142.5, 1000.0))
    points_fixes.append(Part.Vertex(0.0, 142.5, 1000.0))
    points_fixes.append(Part.Vertex(-47.5, 142.5, 1000.0))
    points_fixes.append(Part.Vertex(-95.0, 142.5, 1000.0))
    points_fixes.append(Part.Vertex(-142.5, 118.75, 1000.0))
    points_fixes.append(Part.Vertex(-142.5, -118.75, 1000.0))
    points_fixes.append(Part.Vertex(-142.5, 71.25, 1000.0))
    points_fixes.append(Part.Vertex(-142.5, 23.75, 1000.0))
    points_fixes.append(Part.Vertex(-142.5, -23.75, 1000.0))
    points_fixes.append(Part.Vertex(-142.5, -71.25, 1000.0))
    points_fixes.append(Part.Vertex(118.75, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(-71.25, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(-118.75, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(-23.75, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(23.75, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(71.25, -142.5, 1000.0))
    points_fixes.append(Part.Vertex(142.5, 118.75, 1000.0))
    points_fixes.append(Part.Vertex(142.5, -71.25, 1000.0))
    points_fixes.append(Part.Vertex(142.5, -118.75, 1000.0))
    points_fixes.append(Part.Vertex(142.5, -23.75, 1000.0))
    points_fixes.append(Part.Vertex(142.5, 23.75, 1000.0))
    points_fixes.append(Part.Vertex(142.5, 71.25, 1000.0))
    points_fixes.append(Part.Vertex(71.25, 142.5, 1000.0))
    points_fixes.append(Part.Vertex(118.75, 142.5, 1000.0))
    points_fixes.append(Part.Vertex(23.75, 142.5, 1000.0))
    points_fixes.append(Part.Vertex(-23.75, 142.5, 1000.0))
    points_fixes.append(Part.Vertex(-71.25, 142.5, 1000.0))
    points_fixes.append(Part.Vertex(-118.75, 142.5, 1000.0))

    forces_obj = doc.addObject('Part::Feature', 'Forces')
    forces_obj.Shape = Part.makeCompound(points_forces)
    forces_obj.ViewObject.PointColor = (1.0, 0.0, 0.0, 0.0)
    forces_obj.ViewObject.PointSize = 10.0
    fixes_obj = doc.addObject('Part::Feature', 'Fixes')
    fixes_obj.Shape = Part.makeCompound(points_fixes)
    fixes_obj.ViewObject.PointColor = (1.0, 0.0, 0.0, 0.0)
    fixes_obj.ViewObject.PointSize = 10.0
    '''

    # analysis
    import ObjectsFem
    analysis = ObjectsFem.makeAnalysis(doc, 'Analysis')

    # solver
    solver_object = analysis.addObject(ObjectsFem.makeSolverCalculixCcxTools(doc, 'CalculiXccxTools'))[0]
    solver_object.GeometricalNonlinearity = 'linear'
    solver_object.ThermoMechSteadyState = False
    solver_object.MatrixSolverType = 'default'
    solver_object.IterationsControlParameterTimeUse = False
    solver_object.WorkingDir = u''

    # material
    material_object = analysis.addObject(ObjectsFem.makeMaterialSolid(doc, 'FemMaterial'))[0]
    mat = material_object.Material
    mat['Name'] = "Steel-Generic"
    mat['YoungsModulus'] = "200000 MPa"
    mat['PoissonRatio'] = "0.30"
    mat['Density'] = "7900 kg/m^3"
    material_object.Material = mat

    analysis.addObject(ObjectsFem.makeElementGeometry2D(doc, 15, 'ShellThickness'))[0]

    # fixed_constraint
    fixed_constraint = analysis.addObject(ObjectsFem.makeConstraintFixed(doc, name="ConstraintFixed"))[0]
    fixed_constraint.References = [(doc.SquareTube, "Edge4"), (doc.SquareTube, "Edge7"), (doc.SquareTube, "Edge10"), (doc.SquareTube, "Edge12")]

    # force_constraint1
    force_constraint1 = analysis.addObject(ObjectsFem.makeConstraintForce(doc, name="ConstraintForce1"))[0]
    force_constraint1.References = [(doc.SquareTube, "Edge9")]
    force_constraint1.Force = 100000.00
    force_constraint1.Direction = (doc.SquareTube, ["Edge9"])
    force_constraint1.Reversed = True
    # force_constraint2
    force_constraint2 = analysis.addObject(ObjectsFem.makeConstraintForce(doc, name="ConstraintForce2"))[0]
    force_constraint2.References = [(doc.SquareTube, "Edge3")]
    force_constraint2.Force = 100000.00
    force_constraint2.Direction = (doc.SquareTube, ["Edge3"])
    force_constraint2.Reversed = True
    # force_constraint3
    force_constraint3 = analysis.addObject(ObjectsFem.makeConstraintForce(doc, name="ConstraintForce3"))[0]
    force_constraint3.References = [(doc.SquareTube, "Edge1")]
    force_constraint3.Force = 100000.00
    force_constraint3.Direction = (doc.SquareTube, ["Edge11"])
    force_constraint3.Reversed = True
    # force_constraint4
    force_constraint4 = analysis.addObject(ObjectsFem.makeConstraintForce(doc, name="ConstraintForce4"))[0]
    force_constraint4.References = [(doc.SquareTube, "Edge6")]
    force_constraint4.Force = 100000.00
    force_constraint4.Direction = (doc.SquareTube, ["Edge6"])
    force_constraint4.Reversed = True

    # mesh
    from mesh_z88_bsp25_shell_tria6 import create_nodes, create_elements
    import Fem
    fem_mesh = Fem.FemMesh()
    control = create_nodes(fem_mesh)
    if not control:
        print('ERROR')
    control = create_elements(fem_mesh)
    if not control:
        print('ERROR')
    femmesh_obj = analysis.addObject(doc.addObject('Fem::FemMeshObject', 'Box_Mesh'))[0]
    femmesh_obj.FemMesh = fem_mesh
    forces_obj.ViewObject.Visibility = False

    # recompute
    doc.recompute()


def run(doc):
    import FemGui
    FemGui.setActiveAnalysis(doc.Analysis)  # set the analysis with Name Analysis to the ActiveAnalysis
    from femtools import ccxtools
    fea = ccxtools.FemToolsCcx()
    fea.update_objects()
    fea.purge_results()
    fea.run()

    # recompute
    doc.recompute()


#######################################################################################
# Wenn in tetra mesh nicht zum shape passe wird keine element fuer facenode gefunden,
'''
import Part
box_obj = App.ActiveDocument.addObject('Part::Feature', 'Box')
box = Part.makeBox(8000, 1000, 1000)
box_obj.Shape = box
App.ActiveDocument.recompute()

import Part
box_obj = App.ActiveDocument.addObject('Part::Box', 'Box')
box_obj.Height = box_obj.Width = 1000
box_obj.Length = 8000
App.ActiveDocument.recompute()
'''

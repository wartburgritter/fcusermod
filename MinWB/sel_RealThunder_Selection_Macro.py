import FreeCAD, FreeCADGui, Part
from PySide.QtGui import QMessageBox, QDialog,\
        QComboBox, QDialogButtonBox, QVBoxLayout
from PySide.QtCore import Qt, QMetaObject
from collections import defaultdict

class SelDialog(object):

    ShapeMap = { 'Vertex'   : (0, Part.Vertex),
                 'Edge'     : (1, Part.Edge),
                 'Wire'     : (2, Part.Wire),
                 'Face'     : (3, Part.Face),
                 'Shell'    : (4, Part.Shell),
                 'Solid'    : (5, Part.Solid),
                 'CompSolid': (6, Part.CompSolid),
                }

    def __init__(self, shapeName, childName):
        self.logger = FreeCAD.Logger('SelDialog')

        self.shapeType = None
        self.shapeName = None
        self.childName = None
        self.selectable = None

        self.selobj = None
        self.shape = None
        self.children = None
        self.subname = None
        self.subobj = None
        self.element = None

        self.dialog = None
        self.items = []

        if self.getSelection(shapeName, childName):
            self.dialog = QDialog(FreeCADGui.getMainWindow())
            self.dialog.setAttribute(Qt.WA_DeleteOnClose)
            self.dialog.setWindowTitle('Please select a ' + self.shapeName)

            self.layout = QVBoxLayout(self.dialog)
            self.comboBox = QComboBox(self.dialog)
            for item in self.items:
                self.comboBox.addItem(item)
            self.comboBox.highlighted.connect(self.onHighlighted)
            self.layout.addWidget(self.comboBox)
            self.buttons = QDialogButtonBox(QDialogButtonBox.Ok \
                                                |QDialogButtonBox.Cancel)
            self.buttons.accepted.connect(self.accept)
            self.buttons.rejected.connect(self.reject)
            self.layout.addWidget(self.buttons)

    def accept(self):
        if self.dialog:
            self.subobj = self.selobj.getSubObject(self.subname,retType=1)
            self.element = self.comboBox.currentText()
            self.dialog.accept()

    def reject(self):
        if self.dialog:
            self.dialog.reject()

    def getSelection(self, shapeName, childName):
        try:
            if self.ShapeMap[shapeName][0] <= self.ShapeMap[childName][0]:
                self.err('%s is not parent type of %s' % (shapeName, childName))

            self.shapeType = self.ShapeMap[shapeName][1]
            self.shapeName = shapeName
            self.childName = childName

            self.selectable = self.shapeType in (Part.Face, Part.Edge, Part.Vertex)
        except Exception:
            self.err('Unsupported shape type %s, %s' % (shapeName,childName))
            return
        try:
            counter = []
            parents = []

            sels = FreeCADGui.Selection.getSelectionEx('', 0)
            if not sels:
                self.err('Please select one or more %ss of a %s' % \
                        (self.childName, self.shapeName))
                return

            count = 0
            for sel in sels:
                for subname in sel.SubElementNames:
                    subs = subname.split('.')
                    if not subs[-1].startswith(self.childName):
                        self.err('Please select only %ss' % self.childName)
                        return

                    subname = '.'.join(subs[:-1])

                    if self.selobj:
                        if self.selobj != sel.Object or self.subname!=''.join(subs[:-1]):
                            self.err('Please select from one object only')
                            return
                    else:
                        self.selobj = sel.Object
                        self.subname = subname
                        self.shape = Part.getShape(self.selobj,subname)
                        parents = getattr(self.shape, self.shapeName+'s')
                        if not parents:
                            self.err('No %s found' % self.shapeName)
                            return
                        counter = [0]*len(parents)

                    child = getattr(self.shape,subs[-1])

                    found = False
                    for parent in self.shape.ancestorsOfType(child, self.shapeType):
                        for i,s in enumerate(parents):
                            if s.isEqual(parent):
                                counter[i]+=1
                                found = True

                    if not found:
                        self.logger.warn('skip %s.%s%s' \
                                % (self.selobj.Name,subname,subs[-1]))
                        continue

                    count += 1

            if not count:
                self.err('Please select only %s from %ss' % (self.childName,self.shapeName))
                return

            # filter parents that do not own all selected children
            parents = []
            for i,c in enumerate(counter):
                if c == count:
                    element = '%s%s'%(self.shapeName, i+1)
                    self.items.append(element)
                    parents.append(getattr(self.shape, element))

            if not parents:
                self.err('Please select %ss from common %s(s)' % \
                        (self.childName, self.shapeName))
                return

            if not self.selectable:
                # map child elements of the remaining parents
                self.children = []
                for i in range(0,len(parents)):
                    self.children.append([])
                for i,child in enumerate(getattr(self.shape, self.childName+'s')):
                    for ss in self.shape.ancestorsOfType(child,self.shapeType):
                        for j,s in enumerate(parents):
                            if s.isEqual(ss):
                                self.children[j].append(i)
                                break
            return True
        except Exception as e:
            self.err('Unexpected error\n%s' % e.message)
            return

    def err(self, msg):
        QMessageBox.critical(FreeCADGui.getMainWindow(),'Error',msg)

    def onHighlighted(self, idx):
        try:
            docname = self.selobj.Document.Name
            objname = self.selobj.Name

            FreeCADGui.Selection.clearSelection()
            if self.selectable:
                element = self.comboBox.itemText(idx)
                FreeCADGui.Selection.addSelection(docname,objname,self.subname+element)
                return

            # select children of the highlighted parent
            for i in self.children[idx]:
                subname = '%s%s%d'%(self.subname,self.childName,i+1)
                FreeCADGui.Selection.addSelection(docname,objname,subname)

        except Exception as e:
            self.logger.error(e.message)

    def popup(self):
        if self.dialog:
            return self.dialog.exec_()

def getElement(shapeName='Solid', childName='Face'):
    dlg = SelDialog(shapeName, childName)
    if dlg.popup() == QDialog.Accepted:
        return (dlg.subobj, dlg.element)
    del dlg

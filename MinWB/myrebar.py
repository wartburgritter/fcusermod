import FreeCAD, Arch, Draft
myh = 3000


Structure = Arch.makeStructure(None, length=1000, width=1000, height=myh)
Structure.ViewObject.Transparency = 80
FreeCAD.ActiveDocument.recompute()

p1 = FreeCAD.Vector(0, 0, 0)
p2 = FreeCAD.Vector(0, 0, myh)
Wire1 = Draft.makeWire([p1, p2])
FreeCAD.ActiveDocument.recompute()

Rebar = Arch.makeRebar(Structure, Wire1, diameter=30, amount=1, offset=0)
FreeCAD.ActiveDocument.recompute()

Rebar2 = Arch.makeRebar(None, Wire1, diameter=30, amount=1, offset=0)
FreeCAD.ActiveDocument.recompute()

Rebar3 = Arch.makeRebar(None, App.ActiveDocument.Wire, diameter=30, amount=1, offset=0)
FreeCAD.ActiveDocument.recompute()


# code to make a rebar based on a simple wire
import FreeCAD, Arch, Draft
Wire = Draft.makeWire([FreeCAD.Vector(0, 0, 0), FreeCAD.Vector(0, 0, 3000)])
Rebar = Arch.makeRebar(None, Wire, diameter=30, amount=1)
FreeCAD.ActiveDocument.recompute()

# ***************************************************************************
# *   (c) Bernd Hahnebach (bernd@bimstatik.org) 2014                        *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# *   Bernd Hahnebach 2014                                                  *
# ***************************************************************************/


import FreeCAD

import ifcopenshell.util.element as eleutils


def attributes_native_tool(o):
    """prints for the selected object the properties from native opened ifc-file
    """
    # test, ob auch nur ein object uebergeben wurde, fuer aufruf per python
    print("\n\n" + 93 * "-")
    print("Output IfcImportedProperties for Object: ", o.Name)

    # native IFC
    try:
        ifcfile = o.Document.IfcObject.Proxy.ifcfile
    except: 
        print("IfcOS parsed file object 'ifcfile' not found.")
        return

    if hasattr(o, "StepId"):

        # get product
        elem = ifcfile[o.StepId]
        print(elem)

        # psets and qsets
        all_props = eleutils.get_psets(elem)

        # material
        material_entry = eleutils.get_material(elem)
        if material_entry is None:
            material_name = None
            material_id = None
        elif hasattr(material_entry, "Name"):
            material_name = material_entry.Name
            material_id = material_entry.id()
        else:
            material_name = "isMaterialLayer"
            material_id = material_entry.id()
        all_props["Material"] = {
            "Name": material_name,
            "StepId": material_id,
        }

        # geometry
        if not hasattr(elem, "Representation"):
                all_props["Geometry"] = {
                    "ShapeRepresentation": None,
                    "Information": "Could be SpatialStructure, and lots other without represeentation...",
                }
        elif elem.Representation is None:
            if elem.IsDecomposedBy:
                # container for some other elems
                # https://community.osarch.org/discussion/1367
                decomp = elem.IsDecomposedBy[0]
                all_props["Geometry"] = {
                    "ShapeRepresentation": None,
                    "Container": {
                        "StepId": decomp.id(),
                        "StepType": decomp.is_a(),
                        "ElementsStepId": str([p.id() for p in eleutils.get_decomposition(elem)])
                    }
                }
        else:
            if not elem.IsDecomposedBy:
                decomposition = None
            else:
                decomp = elem.IsDecomposedBy[0]
                decomposition = {
                    "StepId": decomp.id(),
                    "StepType": decomp.is_a(),
                    "ElementsStepId": str([p.id() for p in eleutils.get_decomposition(elem)])
                }
            shape_repr = elem.Representation.Representations[0]
            repr_item = shape_repr.Items[0]
            import importIFCHelper as ifchelp
            color_std = ifchelp.getColorFromProduct(elem)
            color_rgb = ifchelp.color2colorRGB(color_std)

            all_props["Geometry"] = {
                "ShapeRepresentation": {
                    "StepId": shape_repr.id(),
                    "RepresentationIdentifier": shape_repr.RepresentationIdentifier,
                    "RepresentationType": shape_repr.RepresentationType,
                },
                "RepresentationItem": {
                    "StepId": repr_item.id(),
                    "StepType": repr_item.is_a(),
                },
                "Color": {
                    "Std": color_std,
                    "RGB": color_rgb,
                },
                "Container": decomposition,
            }

        # further props
        # layer
        all_layer = eleutils.get_layers(ifcfile, elem)
        the_layer = all_layer[0].Name if len(all_layer) > 0 else None
        #if len(all_layer) > 0:
        #    the_layer = all_layer[0].Name
        #else:
        #    the_layer = None
        # type
        type_entry = eleutils.get_type(elem)
        if hasattr(type_entry, "Name"):
            type_name = type_entry.Name
        else:
            type_name = None

        all_props["FurtherElementprops"] = {
            "Layer": the_layer,
            "Type": type_name,
        }

        # main element props
        elem_info = elem.get_info()
        # all_props["MainElementprops"] = str(elem_info)
        # """
        # keep in mind ... https://github.com/IfcOpenShell/IfcOpenShell/issues/2811
        new_elem_info = {}
        for attr, value in elem_info.items():
            if str(type(value)) == "<class 'ifcopenshell.entity_instance.entity_instance'>":
                value = value.to_string()
            elif isinstance(value, tuple):
                new_values = []
                for val in value:
                    if str(type(val)) == "<class 'ifcopenshell.entity_instance.entity_instance'>":
                        new_values.append(val.to_string())
                value = new_values
            # print(type(value))
            new_elem_info[attr] = value
        #print(new_elem_info)
        all_props["MainElementprops"] = new_elem_info
        # """

        # output
        import json
        print(json.dumps(all_props, indent = 4))


def ifc2fcstdAllInPath():
    # define the importer Do not forget to change the importer.open(inDoc) line as well!!!
    # import importIFCbhb
    # import importIFCmin
    # import importIFCprops
    # reload(importIFCprops)

    import importIFC          # use yoriks standard importer
    import os
    ifcinfilepath = "/tmp/"
    ifcoutfilepath = ifcinfilepath
    # Mit Slash am Ende

    # ifcinfilepath  = "/media/sf_vmaustausch/Modelle/ifcs/"
    # ifcoutfilepath  = "/media/sf_vmaustausch/Modelle/ifcs-fc/"

    # ifcinfilepath = "/media/hugo/443E-0875/ifc1/"
    # ifcoutfilepath = "/media/hugo/443E-0875/ifc1-fc/"

    # ifcinfilepath = "/home/hugo/Desktop/ifcimporttests/ifc1--Durchlauf2/ifc1/"
    # ifcoutfilepath = "/home/hugo/Desktop/ifcimporttests/ifc1--Durchlauf2/ifc1-fc/"

    # ifcinfilepath = "/home/hugo/Desktop/ifcimporttests/ifc1--Durchlauf_makeBox/ifc1/"
    # ifcoutfilepath = "/home/hugo/Desktop/ifcimporttests/ifc1--Durchlauf_makeBox/ifc1-fc/"

    # ifcinfilepath = "/home/hugo/Desktop/ifcimporttests/ifc1--Durchlauf_Yorik/ifc1/"
    # ifcoutfilepath = "/home/hugo/Desktop/ifcimporttests/ifc1--Durchlauf_Yorik/ifc1-fc/"

    ifcfilelist = []
    for f in sorted(os.listdir(ifcinfilepath)):
        if f.endswith(".ifc"):
            ifcfilelist.append(os.path.splitext(f)[0])
    if len(ifcfilelist) == 0:
        print("\n No ifc-file found in Directory")

    print("\n\n" + 93 * "-")
    fcstdfilelist = []
    for f in sorted(os.listdir(ifcoutfilepath)):
        if f.endswith(".fcstd"):
            fcstdfilelist.append(os.path.splitext(f)[0])

    # before we start some output to test which files will be prozessed
    for f in sorted(ifcfilelist):
        if f in fcstdfilelist:
            print("Skip file --> ", f)
            continue
        print("Import file --> ", f)
    print("\n\n" + 93 * "-")

    # here we go !
    exceptionfiles = []
    for fno, f in enumerate(sorted(ifcfilelist)):
        print("\n\n" + 93 * "-")
        print(fno, " of ", len(ifcfilelist), "  -->  ", f)
        if f in fcstdfilelist:
            print("Skip file  -->  ", f)
            continue
        inDoc = ifcinfilepath + f + ".ifc"
        outDoc = ifcoutfilepath + f + ".fcstd"
        print(inDoc)
        print(outDoc)

        try:
            pass
            # fcdoc = importIFCprops.open(inDoc)
            # fcdoc = importIFCbhb.open(inDoc)
            # fcdoc = importIFCmin.open(inDoc)
            fcdoc = importIFC.open(inDoc)
        except:
            print("Exception in file  -->  ", f)
            exceptionfiles.append(f)

        # irgendwie macht er die beiden nicht !!! siehe mein FEM mesh vesuche mit python
        # import FreeCADGui
        # FreeCADGui.SendMsgToActiveView("ViewFit")
        # FreeCADGui.activeDocument().activeView().viewAxometric()

        import time
        time.sleep(0.2)
        if f not in exceptionfiles:
            # pass
            fcdoc.saveAs(outDoc)
            FreeCAD.closeDocument(fcdoc.Name)
        # output exceptionfiles after every file
        for e in exceptionfiles:
            print(e, ".ifc")

    print("\n\nENDE")
    print("\n\n" + 93 * "-")

    for e in exceptionfiles:
        print(e, ".ifc")


def attributes_standard_tool(o):
    """prints for the selected object the properties imported from ifc-file
    """
    # test, ob auch nur ein object uebergeben wurde, fuer aufruf per python
    print("\n\n" + 93 * "-")
    print("Output IfcImportedProperties for Object: ", o.Name)
    """
    if hasattr(o,"IfcAttributes"):
        print("IfcAttributes")
        for e in sorted(o.IfcAttributes):
            print("    ", e, " --> ", o.IfcAttributes[e])
    if hasattr(o,"IfcImportedProperties"):
        print("IfcImportedProperties")
        for e in sorted(o.IfcImportedProperties):
            print("    ", e, " --> ", o.IfcImportedProperties[e])
    """
    psetnames = []
    for p in o.PropertiesList:
        if p.startswith("IfcImportedPSet"):
            psetnames.append(p)
    if len(psetnames) > 0:
        print("IfcImportedPSets")
        for psn in psetnames:
            if "IfcPropertySetName" in getattr(o, psn):
                print(getattr(o, psn)["IfcPropertySetName"])
            else:
                print(psn)
            if hasattr(o, psn):
                for e in sorted(getattr(o, psn)):
                    print("    ", e, " --> ", getattr(o, psn)[e])
    if hasattr(o, "IfcImportedQuantities"):
        print("IfcImportedQuantities")
        if "IfcElementQuantityName" in o.IfcImportedQuantities:
            print(o.IfcImportedQuantities["IfcElementQuantityName"])
        else:
            print("no Name")
        for e in sorted(o.IfcImportedQuantities):
            print("    ", e, " --> ", o.IfcImportedQuantities[e])
    if hasattr(o, "IfcImportedMaterials"):
        print("IfcImportedMaterials")
        for e in sorted(o.IfcImportedMaterials):
            print("    ", e, " --> ", o.IfcImportedMaterials[e])
    if hasattr(o, "IfcImportedAttributes"):
        print("IfcImportedAttributes")
        for e in sorted(o.IfcImportedAttributes):
            print("    ", e, " --> ", o.IfcImportedAttributes[e])


def compareVolumes():
    """compares Volumes calculated by FreeCAD and imported of the IfcElementQuantity of ifc-file

    """
    print("\n\n" + 93 * "-")
    print("Compare FreeCAD Volumes and imported Volume of IfcElementQuantity")
    counter = 0
    for o in FreeCAD.ActiveDocument.Objects:
        if hasattr(o, "Shape"):
            o.ViewObject.Visibility = False  # no shape is visible --> no ___body is visible
            if "___body" not in o.Name:
                o.ViewObject.ShapeColor = (0.0, 0.0, 1.0)   # all objects are blue and transparent
                o.ViewObject.Transparency = 75
                o.ViewObject.Visibility = True
                try:
                    volumeFreeCAD = o.Shape.Volume  # if shape is not a solid --> exception --> diese fangen wir ab!!!
                    volumeFreeCAD = volumeFreeCAD * 1E-9  # volume in ifc is in m and volume in FreeCAD is in mm
                except:
                    print("  error calculating volume of " + o.Name + " Shape seams invalid")
                    o.ViewObject.ShapeColor = (1.0, 1.0, 0.0)  # yellow if invalid shape
                    o.ViewObject.Transparency = 0
                if hasattr(o, "IfcImportedQuantities"):
                    volumeImported = 0
                    if not hasattr(o, "VolumeFreeCAD") or not hasattr(o, "VolumeImported"):
                        o.addProperty("App::PropertyString", "VolumeImported", "AaaIfcTools", "Volume imported from IFC")
                        o.addProperty("App::PropertyString", "VolumeFreeCAD", "AaaIfcTools", "Volume calculated by FreeCAD")
                    o.VolumeImported = str(volumeImported)
                    o.VolumeFreeCAD = str(volumeFreeCAD)
                    if "NetVolume" in o.IfcImportedQuantities:
                        o.ViewObject.Transparency = 0  # test
                        volumeImported = float(o.IfcImportedQuantities["NetVolume"])
                    elif "Volume" in o.IfcImportedQuantities:
                        volumeImported = float(o.IfcImportedQuantities["Volume"])
                    else:
                        if hasattr(o, "IfcObjectType") and o.IfcObjectType == "IfcBuildingElementProxy":
                            print(
                                "  Object: {} Allplan does not export VolumeQuantities "
                                "for IfcBuildingElementProxy --> stays blue --> {}"
                                .format(o.Name, o.IfcObjectType)
                            )
                        else:
                            print(
                                "  Object: {} has IfcImportedQuantities but "
                                "neither NetVolume nor Volume --> stays blue --> {}"
                                .format(o.Name, o.IfcObjectType)
                            )
                    if volumeImported != 0:  # es kann nur !=0 wenn Var geaendert wurde, da Var = 0 gesetzt wurde
                        counter += 1  # was ist bei mesh, mhh das hat kein importiertes volumen ungleich 0
                        o.VolumeImported = str(volumeImported)
                        o.ViewObject.ShapeColor = (0.3, 0.4, 0.4)  # grey if IfcImportedQuantities exist
                        o.ViewObject.Transparency = 0
                        # print(o.Name)
                        # print(o.Shape.Volume*1E-9)
                        q = volumeFreeCAD / volumeImported
                        # if q < 0.990 or q > 1.010:
                        if q < 0.90 or q > 1.10:
                            o.ViewObject.ShapeColor = (1.0, 0.0, 0.0)     # red if volume not equal
                            print("  Object: " + o.Name + " --> Volumes not equal: volumeFreeCAD / volumeImported = " + str(q))
                        else:
                            # print("  Object: " + o.Name + " --> Volumes equal: volumeFreeCAD / volumeImported = " + str(q))
                            o.ViewObject.Transparency = 90  # 80 % Transparency if volume is equal
                else:
                    print("  Object: " + o.Name + " has no IfcImportedQuantities --> green")
                    o.ViewObject.ShapeColor = (0.0, 1.0, 0.0)  # green if no IfcImportedQuantities exist
                    o.ViewObject.Transparency = 0
                    # o.ViewObject.Visibility = False
    print("Anzahl verglichener Volumen: ", counter)


# *******************************************************************************************************************************
# *******************************************************************************************************************************
# ifcImportAddOns

def ifcImportAddOns(doc):
    setIfcAttributes(doc)
    logImportedObject(doc)
    # deleteBodyObjects(doc)  # not in use problems !!!
    doc.recompute()

    # return doc is not needed


def setIfcAttributes(doc):
    """reads Ifc Attributes Dictionarys and add Set of AaaIfcTools Properties
    """
    print("\n\n" + 93 * "-")
    print("Set AaaIfcTools Properties")
    for o in doc.Objects:
        if o is not None:
            feature_string = "<class 'FeaturePython'>"  # Py3
            if str(type(o)) == feature_string:
                if hasattr(o, "IfcName") and hasattr(o, "IfcGUID") and hasattr(o, "IfcMaterial1"):
                    continue
                if hasattr(o, "IfcImportedAttributes"):
                    if "ID" in o.IfcImportedAttributes:
                        o.addProperty("App::PropertyString", "IfcID", "AaaIfcTools", "string of imported IfcID")
                        o.IfcID = o.IfcImportedAttributes["ID"]
                    if "Type" in o.IfcImportedAttributes:
                        o.addProperty("App::PropertyString", "IfcObjectType", "AaaIfcTools", "string of imported IfcObjectType")
                        o.IfcObjectType = o.IfcImportedAttributes["Type"]
                    if "PredefinedType" in o.IfcImportedAttributes:
                        o.addProperty("App::PropertyString", "IfcPredefinedType", "AaaIfcTools", "string of imported IfcPredefinedType")
                        o.IfcPredefinedType = o.IfcImportedAttributes["PredefinedType"]
                    if "Name" in o.IfcImportedAttributes:
                        o.addProperty("App::PropertyString", "IfcName", "AaaIfcTools", "string of imported IfcName")
                        o.IfcName = o.IfcImportedAttributes["Name"]
                    if "GUID" in o.IfcImportedAttributes:
                        o.addProperty("App::PropertyString", "IfcGUID", "AaaIfcTools", "string of imported GUID")
                        o.IfcGUID = o.IfcImportedAttributes["GUID"]
                if hasattr(o, "IfcImportedMaterials"):
                    if "layer0material" in o.IfcImportedMaterials:     # IfcImportedMaterials could be empty
                        o.addProperty("App::PropertyString", "IfcMaterial1", "AaaIfcTools", "imported Material of layer 0")
                        o.IfcMaterial1 = o.IfcImportedMaterials["layer0material"]
                if hasattr(o, "IfcImportedProperties"):
                    # TODO update auf neue propertysetstruktur
                    if hasattr(o, "IfcName") and o.IfcName == "":  # Allplanexport hat space hab ich eher bei import auf nichts gesetzt
                        if "Objektname" in o.IfcImportedProperties:
                            o.IfcName = o.IfcImportedProperties["Objektname"]
    doc.recompute()


def logImportedObject(doc):
    ifcobjecttypes = {}
    for o in doc.Objects:
        if "___body" not in o.Name:
            if hasattr(o, "IfcObjectType"):
                if o.IfcObjectType not in ifcobjecttypes:
                    ifcobjecttypes[o.IfcObjectType] = 1
                else:
                    ifcobjecttypes[o.IfcObjectType] += 1
    print("\n\n" + 93 * "-")
    print("Log after the method insert has finished, just before the new Document is returned to FreeCAD\n")
    counter = 0
    for ot in sorted(ifcobjecttypes):
        print(ot, " --> ", ifcobjecttypes[ot])
        counter += int(ifcobjecttypes[ot])
    print("")
    print("Anzahl Objekte inklusive BWS: ", counter)


def deleteBodyObjects(doc):
    for o in doc.Objects:
        if hasattr(o, "Shape"):
            # o.ViewObject.Visibility = False    # no shape is visible --> no ___body is visible
            if "___body" in o.Name and str(type(o)) == "<type 'Part.Feature'>":
                o.ViewObject.Visibility = True
                doc.removeObject(o.Name)
    # FreeCAD.ActiveDocument.recompute()
    doc.recompute()
    # upps das loeschen der body objecte macht gar keinen sinn !!!
    # obwohl nach speichern und laden ist es ok !!!!
    # Vorteil der body objekte. Es ist denkbar, diese auszutauschen, das heisst die Geometrie
    # aendert sich und die attribute bleiben erhalten. So die Geometrie nicht von den Attributen abhaengt.

# -*- coding: utf-8 -*-
# FreeCAD tools of the IFCToolBox
# (c) 2014 Bernd Hahnebach

# ***************************************************************************
# *   (c) Bernd Hahnebach (bernd@bimstatik.org) 2014                        *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# *   Bernd Hahnebach 2014                                                  *
# ***************************************************************************/

import importlib

# import FreeCAD
import FreeCADGui
import ifctools


class Tool1():
    def Activated(self):
        ifctools.ifc2fcstdAllInPath()

    def GetResources(self):
        return {"Pixmap": "python", "MenuText": "ifc2fcstdAllInPath", "ToolTip": "ifc2fcstdAllInPath"}


class Tool2():
    def Activated(self):
        ifctools.compareVolumes()

    def GetResources(self):
        return {"Pixmap": "python", "MenuText": "Compare Volume Quantities", "ToolTip": "Compare Volume Quantities with FreeCAD Volumes"}


class Tool3():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())  # wozu???

    def Activated(self):
        importlib.reload(ifctools)
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            o = selection[0]
            ifctools.attributes_standard_tool(o)
        else:
            print("Mehr als ein Objekt selektiert!")

    def GetResources(self):
        return {"Pixmap": "python", "MenuText": "AttributesTool", "ToolTip": "AttributesTool to output Attributdicitonarys"}


class Tool4():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())  # wozu???, glaube tool ist nur aktiv, wenn ein obj. selektiert

    def Activated(self):
        importlib.reload(ifctools)
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            o = selection[0]
            ifctools.attributes_native_tool(o)
        else:
            print("Mehr als ein Objekt selektiert!")

    def GetResources(self):
        return {"Pixmap": "python", "MenuText": "AttributesTool native", "ToolTip": "AttributesTool to output Attributs of native IFC opener"}


FreeCADGui.addCommand("Tool1", Tool1())
FreeCADGui.addCommand("Tool2", Tool2())
FreeCADGui.addCommand("Tool3", Tool3())
FreeCADGui.addCommand("Tool4", Tool4())

# ***************************************************************************
# *   Copyright (c) 2020 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

import FreeCAD
if FreeCAD.GuiUp:
    import FreeCADGui


if open.__module__ == '__builtin__':
    pyopen = open  # because we'll redefine open below


def open(filename):
    
    print("The file {} will be opened with ifcopenshell. Access it with the identifier 'ifc'")
    FreeCADGui.addModule("ifcopenshell")
    FreeCADGui.doCommand('ifcfile = ifc = ifcopenshell.open("{}")'.format(filename))
    FreeCADGui.doCommand('print(ifc.by_type("IfcProject")[0])')

    return True

# ***************************************************************************
# *                                                                         *
# *   Copyright (c) 2016                                                    *
# *   Bernd Hahnebach <bernd@bimstatik.org>                                 *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************


from __future__ import print_function  # to be able to use this:  print("null shape ", end="")


__title__ = "FreeCAD min IFC importer based on Yorik van Havres IFC importer"
__author__ = "Bernd Hahnebach"
__url__ = "http://www.freecadweb.org"


import os
import sys
import FreeCAD
import Part
if FreeCAD.GuiUp:
    import FreeCADGui


if open.__module__ == '__builtin__':
    pyopen = open  # because we'll redefine open below


def decode(filename, utf=False):

    "turns unicodes into strings"

    if (sys.version_info.major < 3) and isinstance(filename, unicode):
        # workaround since ifcopenshell currently can't handle unicode filenames
        if utf:
            encoding = "utf8"
        else:
            encoding = sys.getfilesystemencoding()
        filename = filename.encode(encoding)
    return filename


def getPreferences():
    """retrieves IFC preferences"""
    global DEBUG, SKIP, FITVIEW_ONIMPORT, ROOT_ELEMENT
    p = FreeCAD.ParamGet("User parameter:BaseApp/Preferences/Mod/Arch")
    SKIP = p.GetString("ifcSkip", "").split(",")
    ROOT_ELEMENT = p.GetString("ifcRootElement", "IfcProduct")
    FITVIEW_ONIMPORT = p.GetBool("ifcFitViewOnImport", False)
    DEBUG = True


def open(filename, skip=[], only=[], root=None):
    "opens an IFC file in a new document"
    docname = os.path.splitext(os.path.basename(filename))[0]
    docname = decode(docname, utf=True)
    doc = FreeCAD.newDocument(docname)
    doc.Label = docname
    doc = insert(filename, doc.Name, skip, only, root)
    return doc


def insert(filename, docname, skip=[], only=[], root=None):
    """insert(filename,docname,skip=[],only=[],root=None): imports the contents of an IFC file.
    skip can contain a list of ids of objects to be skipped, only can restrict the import to
    certain object ids (will also get their children) and root can be used to
    import only the derivates of a certain element type (default = ifcProduct)."""

    getPreferences()

    # *****************************************************************
    # we gone overwrite skip and only, needs to be after getPreferences
    # skip = [1030, 1922, 9813, 13030, 28999, 30631, 34909, 39120]
    # skip = [196, 235, 264, 301, 17154, 17283, 17320, ]
    #
    # only = [679482, 679567]
    # only = [301]
    #
    # add this modules to the modules to reload on my reload tool

    try:
        import ifcopenshell
    except:
        FreeCAD.Console.PrintError("IfcOpenShell was not found on this system. IFC support is disabled\n")
        return

    if DEBUG:
        print("Opening ", filename, "...", end="")
    try:
        doc = FreeCAD.getDocument(docname)
    except:
        doc = FreeCAD.newDocument(docname)
    FreeCAD.ActiveDocument = doc

    if DEBUG:
        print("done.")

    global ROOT_ELEMENT
    if root:
        ROOT_ELEMENT = root

    # global ifcfile # keeping global for debugging purposes
    filename = decode(filename, utf=True)
    ifcfile = ifcopenshell.open(filename)
    from ifcopenshell import geom
    settings = ifcopenshell.geom.settings()
    settings.set(settings.USE_BREP_DATA, True)
    settings.set(settings.SEW_SHELLS, True)
    settings.set(settings.USE_WORLD_COORDS, True)
    settings.set(settings.DISABLE_OPENING_SUBTRACTIONS, False)
    settings.set(settings.INCLUDE_CURVES, True)  # for stuct, if activated arch walls have no hight!!!

    products = ifcfile.by_type(ROOT_ELEMENT)
    objects = {}  # { id:object, ... }

    if FITVIEW_ONIMPORT and FreeCAD.GuiUp:
        overallboundbox = None
        FreeCADGui.ActiveDocument.activeView().viewAxonometric()

    all_entities_group = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup", docname)

    # products
    for pno, product in enumerate(products):
        # print(product)
        pid = product.id()
        ptype = product.is_a()
        print("Product {} of {} is Entity #{}: {}, ".format(
            pno + 1,
            len(products),
            pid,
            ptype,
        ), end="", flush=True)
        name = "ID" + str(pid)
        obj = None
        baseobj = None
        cr = None
        brep = None
        shape = None

        if (ptype == "IfcOpeningElement"):
            print(" --> is computed with parent", end="\n")
            continue
        if pid in skip:
            print(" --> skipped (pid in skip list)", end="\n")
            continue
        if only and pid not in only:
            print(" --> skipped (pid not in only list)", end="\n")
            continue

        # get the shape
        try:
            pass
            # here is where the problems are, in most cases
            cr = ifcopenshell.geom.create_shape(settings, product)
        except:
            print(' no shape from IFCOS,', end="")
            if cr is not None:
                print(cr)
        if not cr:
            # print(' --> product has no shape data', end="")
            pass  # IfcOpenShell will yield an error if a given product has no shape, but we don't care
        else:
            try:
                brep = cr.geometry.brep_data
            except:
                print(' no brep data from IFCOS,', end="")
                if brep is not None:
                    print(brep)
        if not brep:
            print(" no brep", end="")
        else:
            shape = Part.Shape()
            shape.importBrepFromString(brep)
            # a Compound will be created whatever is in the brep
            shape.scale(1000.0)  # IfcOpenShell always outputs in meters

            """
            print("")
            print(cr)
            print("")
            print(brep)
            print("")
            print(shape.Vertexes)
            print("")
            print(shape.Solids)
            print("")
            print("Test: should all return False for a Good Shape.")
            print(shape.isNull())
            print(not shape.isValid())
            print(not shape.Solids)
            print("")
            Part.show(shape)
            print(FreeCAD.ActiveDocument.Shape.Shape.isValid())
            """

            # if not shape.isNull() and shape.isValid() and shape.Vertexes:  # an endless line has no Vertexes, but an Edge!
            if shape.isNull() or (not shape.isValid()) or (not shape.Solids):  # I only need Solids ATM
                print("null Shape or not valid Shape or Shape without Vertexes ", end="")
            else:
                if FITVIEW_ONIMPORT and FreeCAD.GuiUp:
                    try:
                        bb = shape.BoundBox
                        # if DEBUG: print(' ' + str(bb),end="")
                    except:
                        bb = None
                        if DEBUG:
                            print(' BB could not be computed', end="")
                    if bb.isValid():
                        if not overallboundbox:
                            overallboundbox = bb
                        if not overallboundbox.isInside(bb):
                            FreeCADGui.SendMsgToActiveView("ViewFit")
                        overallboundbox.add(bb)
                    FreeCADGui.updateGui()
                if len(shape.Solids) < 1:
                     print("no Solid ", end="")
                else:
                    # zwei varianten, bei mehr als ein solid
                    # ## fuer jeden solid eine docobj erstellen
                    # ## die solids fusionieren
                    for i, s in enumerate(shape.Solids):
                        baseobj = FreeCAD.ActiveDocument.addObject("Part::Feature", name + "_body_" + str(i))
                        baseobj.Shape = s
                        objects[pid] = baseobj
                        all_entities_group.addObject(baseobj)
                        sht = baseobj.Shape.ShapeType if hasattr(baseobj, "Shape") else "None"
                        sols = str(baseobj.Shape.Solids) if hasattr(baseobj, "Shape") else ""
                        if DEBUG:
                            print("creating object: ", baseobj.Name, ", with shape type: ", sht, ", ", str(len(brep) / 1000), "k, ", sols, end="")
                # Was wenn Edge or Shell?
        print("")

    FreeCAD.ActiveDocument.recompute()
    # End products loop

    # cleaning bad shapes
    for obj in objects.values():
        if obj.isDerivedFrom("Part::Feature"):
            if obj.Shape.isNull():
                print("cleaning bad shape of object: #", pid)
                import Arch
                Arch.rebuildArchShape(obj)
    FreeCAD.ActiveDocument.recompute()

    if FreeCAD.GuiUp:
        FreeCADGui.activeDocument().activeView().viewAxometric()
        FreeCADGui.SendMsgToActiveView("ViewFit")

    print("Done.")

    return doc

# Infos
### importIFCbhbstruct.py
+ keep for history reasons :-) and for breaking an ifc import


### importIFCbhbold.py
+ keep for history reasons :-)


### importIFCmin.py
+ minimized ifc import
+ maintain separated for FreeCAD master, but stay similar


### importIFCorg.py
+ the importIFC my importIFCprops is based on
+ means only update if imporIFCprops is updated !

+ compare with Arch importIFC --> only removes defs
+ compare with Props importIFCprops --> see my own extensions to it
+ try to put as much as possible in extra module !!!
+ may be make another module

+ has some more additions like: view fit during import, log file, ...


### fit view on import
+ https://github.com/berndhahnebach/FreeCAD_User_Mod/commit/b74f6ea1594c37e819fd41a6ee9f11ba52fb7bb3
+ https://github.com/berndhahnebach/FreeCAD_bhb/commit/840cb1f9c2a773116d69b04037b581432f2947f3


# Workflow (husch_husch)
+ daher vor ersetzen org und props diese nach old kopieren vergleichen
+ dann weiss ich was kopiert werden muss
+ dann von props_old nach props kopieren
+ old loeschen

+ Workflow smart 
+ habe keinen cherry-pick ATM und aber anpassungen am code gemacht
+ org ersetzt nichts geloescht, props geloescht und org nach props kopiert
+ commit ...
 git commit -m "IFCToolBox: update org and props modules to FC upstream head"

+ cherry-pick last working props commit
+ diesen commit cherry-picken, aber vorher checken ...
 git cherry-pick 6239907addcbc87e1f93ad20a1c9bdb74a1d8abf

+ letzten commit hier zwei zeilen weiter oben reinkopieren, readme commiten
 git commit -m "IFCToolBox: update readme"

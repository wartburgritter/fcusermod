# ***************************************************************************
# *   (c) Bernd Hahnebach (bernd@bimstatik.org) 2014                        *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# *   Bernd Hahnebach 2014                                                  *
# ***************************************************************************/

# to get the AttributeName of the Entity -->
# print(m.attribute_name(1))
# Arch --> Utility IfcBrowser oeffnen '''

# import FreeCAD


def getObjectAttributes(product):
    """write entity attributes to a Dictionary objectAttributes

    objectAttributes = {Key : Value}
    """
    if product.Name is None or product.Name is ' ':   # if Name == oneSpace (Nemetschek Allplan)
        objname = ''
    else:
        objname = product.Name
    if hasattr(product, 'PredefinedType'):
        if product.PredefinedType is None:
            objpredefinedtype = ''
        else:
            objpredefinedtype = product.PredefinedType
    else:
        objpredefinedtype = ''
    objectAttributes = {
        "ID": str(product.id()),
        "Type": product.is_a(),
        "GUID": product.GlobalId,
        "Name": objname,
        "PredefinedType": objpredefinedtype
    }
    return objectAttributes


def getMaterials(materialrelations):
    """ write the ifc-file material data in a dictionary materials

    materials = {id : {'layerXmaterial' : 'matertial', 'layerXthickness' : 'thickness', ... } ... }
    """
    materials = {}
    if len(materialrelations) == 0:
        print("    Keine Materialrelationen in der IFC-Datei vorhanden!")
        return materials  # return empty Dictionary
    for materialrelation in materialrelations:
        # get the material data from the materialrelation
        materiallist = []
        # print(materialrelation)
        # print('   ', materialrelation.RelatingMaterial)
        if materialrelation.RelatingMaterial is not None:  # could be none: Allplan 2012 and no Material is definend (Dachhaut)
            if materialrelation.RelatingMaterial.is_a("IfcMaterialLayerSetUsage"):
                pass
                # std. Allplan: Waende, Decken
                # print('   ', materialrelation.RelatingMaterial.ForLayerSet                  # IfcMaterialLayerSetUsage(0)=Entity IfcMaterialLayerSet)
                # print('   ', materialrelation.RelatingMaterial.ForLayerSet.MaterialLayers   # IfcMaterialLayerSet(0)=EntityList of IfcMaterialLayer)
                for materiallayer in materialrelation.RelatingMaterial.ForLayerSet.MaterialLayers:
                    # print('   ', materiallayer)
                    # print('   ', materiallayer.LayerThickness)
                    # print('   ', materiallayer.Material)
                    # print('   ', materiallayer.Material.Name)
                    # print('   ', materiallayer.Material.Name, ' --> ', materiallayer.LayerThickness)
                    materiallist.append([materiallayer.Material.Name, materiallayer.LayerThickness])

            elif materialrelation.RelatingMaterial.is_a("IfcMaterialLayerSet"):
                # print('   ' ,materialrelation.RelatingMaterial.MaterialLayers)                # IfcMaterialLayerSet(0)=EntityList of IfcMaterialLayer
                for materiallayer in materialrelation.RelatingMaterial.MaterialLayers:
                    print('   ', materiallayer.Material.Name, ' --> ', materiallayer.LayerThickness)
                # materiallayerlist = materialrelation.RelatingMaterial.MaterialLayers
                print('ToDo1 material')
            elif materialrelation.RelatingMaterial.is_a("IfcMaterialList"):
                print('   ', materialrelation.RelatingMaterial.Materials)                    # IfcMaterialList(0)=EntityList of IfcMaterial
                for material in materialrelation.RelatingMaterial.Materials:
                    # print('   ', material)
                    print('   ', material.Name)
                # materiallist = materialrelation.RelatingMaterial.Materials
                print('ToDo2 material')
            elif materialrelation.RelatingMaterial.is_a("IfcMaterial"):
                # std Allplan: Suetzen, Fundamente, Beam
                # print('   ', materialrelation.RelatingMaterial)                              # Entity IfcMaterial
                # print('   ', materialrelation.RelatingMaterial.Name)                         # Entity IfcMaterial
                materiallist.append([materialrelation.RelatingMaterial.Name, None])
            else:
                print('   ', materialrelation.RelatingMaterial.is_a(), " is not implemented")
                # print(materialrelation.RelatingMaterial)
        # print('  ', materiallist)
        # write the list in a dictionary: layerXmaterial --> [matertial], layerXthickness --> [thickness], ...
        # warum nicht sofort dict, warum ueberhaupt dict, Datenmodell ueberdencken
        objectMaterial = {}
        for lno, layer in enumerate(materiallist):
            key = 'layer' + str(lno) + 'material'
            objectMaterial[key] = layer[0]
            key = 'layer' + str(lno) + 'thickness'
            if layer[1]:                        # ist wie "is not None", da None is not allowed as type
                objectMaterial[key] = str(layer[1])
            else:
                objectMaterial[key] = ''

        # for l in sorted(objectMaterial):
        #     print(l, ' --> ', objectMaterial[l])

        # get the relatedObjectID
        # all ifcfiles I have seen: each materialrelation has only one RelatedObject --> each product has its own materialrelation!
        roid = materialrelation.RelatedObjects[0].id()
        # print('Object #', roid)

        materials[roid] = objectMaterial
    return materials


def getPropertiesBhb(propertyrelations):
    """ write ifc-file propertyset data in a tupel of twe dictionaries (propertiesbhb, quantities)

    propertiesbhb = {id : [ ( 'propertysetname' : {property:value} ) ... () ] }   (tupel)
    quantities    = {id : [ ( 'quantityname   ' : {property:value} ) ... () ] }   (tupel)
    """
    propertiesbhb = {}
    quantities = {}
    if len(propertyrelations) == 0:
        print("    Keine Propertyrelationen in der IFC-Datei vorhanden!")
        return (propertiesbhb, quantities)  # return empty Tupel
    # IfcElementQuantity and IfcPropertySet
    for p in propertyrelations:  # ein object kann mehr wie eine relation haben !!!
        # get the relatedObjectID for this relation
        roid = p.RelatedObjects[0].id()

        # since one object could have more than one probertyrelation we need to "append" not to "="
        if p.RelatingPropertyDefinition.is_a('IfcPropertySet'):
            if roid not in propertiesbhb:
                propertiesbhb[roid] = []
            propertiesbhb[roid].append(getPropertryset(p.RelatingPropertyDefinition))

        if p.RelatingPropertyDefinition.is_a("IfcElementQuantity"):
            if roid not in quantities:
                quantities[roid] = []
            quantities[roid].append(getQuantities(p.RelatingPropertyDefinition))

    # print(propertiesbhb)
    # print(quantities)

    return (propertiesbhb, quantities)


def plotPropertiesBhb(propertiesbhb):
    """outut dictionary propertiesbhb

    """
    for pid in sorted(propertiesbhb):
        print('------------------------------------------------------------------------')
        print('Object: #', pid)
        for tupel in propertiesbhb[pid]:  # propertiesbhb[pid] == objectProperties
            print('  ' + tupel[0])    # Tupel[0] ist der Name des Sets
            for k in tupel[1]:                # ueber keys in Tupel[1] welches ein Dictionary er Properties ist
                try:
                    print('      ' + k + '  -->  ' + tupel[1][k].decode("utf8"))
                except:
                    print('      ' + k + '  -->  ' + tupel[1][k])


def getQuantities(qe):
    """Liste von ElementQuantityEntries wird uebergeben.

    Tupel welches string mit quantityname und dictionary mit quantityentries (key, value) wird zurueckuebergeben
    IfcOS gibt unicode strings(u''), aber FreeCAD PorpertyMap Keys muessen sting ('') sein, alles konvertieren!
    """
    gp = {}
    gp['IfcElementQuantityName'] = str(qe.Name)
    # print(qe)                             # IfcElementQuantity
    for q in qe.Quantities:  # IfcElementQuantity(5)=EntityList
        '''
        print(q)
        print(q.Name)                       # IfcQuantityXXX(0)
        print(str(q.Name.encode("utf8")))   # ArchiCAD uses non ascii Fl\X2\00E4\X0\che, thus we encode it
        print(q.Unit)
        print(q.__getitem__(3))             # the name changes !! VolumeValue, AreaValue, LengthValue ...
        if q.Unit is not None:
            print(q.Unit.Name)              # IfcQuantityXXX(2).IfcSIUnit(3)
            print(q.Name, '=', str(q.__getitem__(3)),' ', q.Unit.Name)
        else:
            print(q.Name + '=' + str(q.__getitem__(3)))
        '''
        # eintraege in dictionary schreiben (Values of Add::PropertyMap must be Strings not unicode string !)
        gp[str(q.Name.encode("utf8"))] = str(q.__getitem__(3))
    # print(gp)
    return (str(qe.Name), gp)


def getPropertryset(pse):
    """Liste von PropertySetEntries wird uebergeben.

    Tupel welches string mit setname und dictionary mit setentries (key, value) wird zurueckuebergeben
    IfcOS gibt unicode strings(u''), aber FreeCAD PorpertyMap Keys muessen sting ('') sein, alles konvertieren!
    """
    gp = {}
    gp['IfcPropertySetName'] = str(pse.Name.encode("utf8"))
    # print(pse)                       # IfcPropertySet
    for q in pse.HasProperties:        # IfcPropertySet(4)=EntityList
        if q.is_a("IfcComplexProperty"):
            # print("    Found IfcComplexProperty, alle der Gesamtwand zugeordnet")
            # every wall layer has own props, I only have models with one layer walls
            # TODO walls with more than one wall layer (I have some in Allplan Assistent)
            # print(q)                         # IfcComplexProperty
            # print(q.Name)                    # IfcComplexProperty(0)
            # print(q.Description)             # IfcComplexProperty(1)
            # print(q.UsageName)               # IfcComplexProperty(2)
            # print(q.HasProperties)           # IfcComplexProperty(3)=EntityList
            for e in q.HasProperties:
                if e.is_a("IfcPropertySingleValue"):
                    gp = getPropertryEntry(e, gp)
                else:
                    print("Not implemented yet!")
                    print(e)
        elif q.is_a("IfcPropertySingleValue"):
            gp = getPropertryEntry(q, gp)
        else:
            print("Not implemented yet! Only IfcPropertySingleValue and IfcComplexProperty are is implemented")
            print(q)
    # print(gp)
    return (str(pse.Name.encode("utf8")), gp)


def getPropertryEntry(pe, gp):
    """extract keys and values from IfcPropertySingleValue

    IfcOS gibt unicode strings(u''), aber FreeCAD PorpertyMap Keys muessen sting ('') sein, alles konvertieren!
    """
    # print(pe)                                   # IfcPropertySingleValue
    # print(pe.Name)                              # IfcPropertySingleValue(0)
    # print(pe.NominalValue)                      # IfcPropertySingleValue(2)=IfcXXX see if
    # print('    ', pe.NominalValue.wrappedValue  # IfcXXX(0))

    # print(str(pe.Name.encode("utf8")))
    # testdict = {}
    # testdict[str(pe.Name.encode("utf8"))] = ''
    # print(testdict)
    # revit nutzt als namen fuer properties auch umlaute und leerzeichen, dies ist nicht als dictionary key erlaubt
    #   #18356= IFCPROPERTYSINGLEVALUE('Geb\X2\00E4\X0\udegeschoss',$,IFCBOOLEAN(.T.),$);
    #   #18354= IFCPROPERTYSINGLEVALUE('H\X2\00F6\X0\he f\X2\00FC\X0\r Berechnung',$,IFCLENGTHMEASURE(1.),$);
    # key waere dann Gebaeudegeschoss aber mit richtigem ae mit punkten ueber dem a, das geht nicht
    # auch nutzt revit properies mit leerzeichen Hoehe fuer Berechung
    key = str(pe.Name.encode("utf8"))

    # value (Values of Add::PropertyMap must be Strings)
    if pe.NominalValue is None:
        gp[key] = ""
    elif pe.NominalValue.is_a("IfcDescriptiveMeasure"):
        gp[key] = str(pe.NominalValue.wrappedValue.encode("utf8"))
    elif pe.NominalValue.is_a("IfcLabel"):
        gp[key] = str(pe.NominalValue.wrappedValue.encode("utf8"))
    elif pe.NominalValue.is_a("IfcText"):
        gp[key] = str(pe.NominalValue.wrappedValue.encode("utf8"))
    elif pe.NominalValue.is_a("IfcPositiveLengthMeasure"):
        gp[key] = str(pe.NominalValue.wrappedValue)
    elif pe.NominalValue.is_a("IfcInteger"):
        gp[key] = str(pe.NominalValue.wrappedValue)
    elif pe.NominalValue.is_a("IfcBoolean"):
        gp[key] = str(pe.NominalValue.wrappedValue)
    elif pe.NominalValue.is_a("IfcLengthMeasure"):
        gp[key] = str(pe.NominalValue.wrappedValue)
    elif pe.NominalValue.is_a("IfcAreaMeasure"):
        gp[key] = str(pe.NominalValue.wrappedValue)
    elif pe.NominalValue.is_a("IfcVolumeMeasure"):
        gp[key] = str(pe.NominalValue.wrappedValue)
    elif pe.NominalValue.is_a("IfcPlaneAngleMeasure"):
        gp[key] = str(pe.NominalValue.wrappedValue)
    elif pe.NominalValue.is_a("IfcThermalTransmittanceMeasure"):
        gp[key] = str(pe.NominalValue.wrappedValue)
    elif pe.NominalValue.is_a("IfcCountMeasure"):
        gp[key] = str(pe.NominalValue.wrappedValue)
    elif pe.NominalValue.is_a("IfcMassMeasure"):
        gp[key] = str(pe.NominalValue.wrappedValue)
    elif pe.NominalValue.is_a("IfcLogical"):
        gp[key] = str(pe.NominalValue.wrappedValue)
    elif pe.NominalValue.is_a("IfcIdentifier"):
        gp[key] = str(pe.NominalValue.wrappedValue.encode("utf8"))
    elif pe.NominalValue.is_a("IfcReal"):
        gp[key] = str(pe.NominalValue.wrappedValue)
    else:
        # print(pe.NominalValue)
        print("Not implemented!")
    return (gp)


#################################################################################################################################
#################################################################################################################################
def addIfcImportedAttributes(obj, objectAttributes, objectMaterials, objectPropertySets, objectQuantities):
    """obj: Attribute erzeugen und zuweisen

    """
    # print('Why?  -->  obj  -->  ', obj, '  -->  type(obj)  -->  ', type(obj))  # ForumThread

    obj.addProperty("App::PropertyMap", "IfcImportedAttributes", "Ifc", "dictionary of imported standard object attributes")
    if objectAttributes:
        # print(objectAttributes)
        obj.IfcImportedAttributes = objectAttributes
    else:
        obj.IfcImportedAttributes = {}
    if len(objectMaterials) > 0:
        obj.addProperty("App::PropertyMap", "IfcImportedMaterials", "Ifc", "dictionary of imported materials")
        obj.IfcImportedMaterials = objectMaterials
    if len(objectQuantities) == 1:  # we asume there is onle one IfcElementQuantity !
        obj.addProperty("App::PropertyMap", "IfcImportedQuantities", "Ifc", "dictionary of imported object quantities")
        try:
            obj.IfcImportedQuantities = objectQuantities[0][1]  # its a list containing one tupel (quantityname, qantities)
        except:
            print('Problem with unicode: ', objectQuantities[0][1])
            print(objectQuantities[0][1])
    elif len(objectQuantities) > 1:
        print('more than one IfcElementQuantity per Element are not implemented')

    # All Properties in one Dictionary ohne PropertySetNamen
    obj.addProperty("App::PropertyMap", "IfcImportedProperties", "Ifc", "dictionary of imported object properties")
    AllProperties = {}
    for o in objectPropertySets:
        for k in o[1]:
            AllProperties[k] = o[1][k]
    for k in AllProperties:
        pass
        # print( '            ' + k + '  -->  ' + AllProperties[k] )
    try:
        obj.IfcImportedProperties = AllProperties
    except:
        print('Problem with unicode: ')
        print(AllProperties)

    for tno, tupel in enumerate(objectPropertySets):  # objectProperties
        obj.addProperty("App::PropertyMap", ('IfcImportedPSet' + str(tno)), "Ifc", "dictionary of imported object properties")
        for k in tupel[1]:  # ueber keys in Tupel[1] welches ein Dictionary er Properties ist
            try:
                setattr(obj, ('IfcImportedPSet' + str(tno)), tupel[1])
            except:
                print('Problem with unicode: ')
                print('hoffentlich das letzte')

    return obj


#################################################################################################################################
#################################################################################################################################
# use a template instead of writing each line directly to the file --> see SwissNPK or Yoriks ifc-exporter
def write2log1(oflog, filename, logfile):
    print('logfile ', logfile)
    oflog.write('importIFC logfile\n')
    oflog.write(filename + '\n')
    oflog.write('Was ist denn alles im IfcFile, heisst nicht das es auch importiert wurde!\n')
    oflog.write('muesste logfile in alle defs mit uebergeben, hat fabian aber auch gemacht!\n')
    oflog.write('FreeCAD > fclog.log hilft ein wenig weiter\n')


def write2log2(oflog, prtype):
    # siehe wann Dictionary prtype erstellt, VOR skippingzeug
    oflog.write('\n\nListe waehrend Import, aber vor skipping ID oder Type :')
    oflog.write('\n--------------------------------------------------------\n\n')
    # IfcObjectTypeList
    for p in sorted(prtype):
        oflog.write(p + '  -->  ' + str(prtype[p]) + '\n')

    # AllplanBauteilList
    raum = prtype.get('IfcSpace', 0)
    wand = prtype.get('IfcWall', 0) + prtype.get('IfcWallStandardCase', 0)
    stuetze = prtype.get('IfcColumn', 0)
    decke = prtype.get('IfcSlab', 0)
    unterzug = prtype.get('IfcBeam', 0)
    fundament = prtype.get('IfcFooting', 0)
    proxy = prtype.get('IfcBuildingElementProxy', 0)
    elemete2d = prtype.get('IfcAnnotation', 0)
    oeffnung = prtype.get('IfcOpeningElement', 0)
    treppe = prtype.get('IfcStair', 0)
    sonstige = prtype.get('IfcMember', 0)

    mynumbertmp = 0
    oflog.write('\n\nListe waehrend Import, aber vor skipping ID oder Type :')
    oflog.write('\nSind die in der Datei gefundenen "IfcProducts". Diese werden versucht zu importieren!')
    oflog.write('\nWas letztendlich importiert wurde muss noch geprueft werden!')
    oflog.write('\nund natuerlich auch, ob diese objecte auch eine Shape haben und das gleiche Volumen haben!')
    oflog.write('\n\nIFC Elemente : ')
    oflog.write('\n--------------------------------------------------------\n')
    # BITTE MIT TEMPLATE ERSTELLEN !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    oflog.write('\nRaum                                     : ' + str(raum))
    oflog.write('\nAusbau (P)                               : ' + str(mynumbertmp))
    oflog.write('\nWand                                     : ' + str(wand))
    oflog.write('\nStuetze                                  : ' + str(stuetze))
    oflog.write('\nBoden- und Deckenplatte                  : ' + str(decke))
    oflog.write('\nDachhaut (P)                             : ' + str(mynumbertmp))
    oflog.write('\nDachplatte (P) (sind bei Decken dabei)   : ' + str(mynumbertmp))
    oflog.write('\nUnterzug                                 : ' + str(unterzug))
    oflog.write('\nFundament                                : ' + str(fundament))
    oflog.write('\nIFC Proxy                                : ' + str(proxy))
    oflog.write('\nMoebel und sonst. Ausstattungen (P)      : ' + str(mynumbertmp))
    oflog.write('\n2D-Elemente                              : ' + str(elemete2d))
    oflog.write('\nOeffnung                                 : ' + str(oeffnung))
    oflog.write('\nTreppe                                   : ' + str(treppe))
    oflog.write('\nTuer (P)                                 : ' + str(mynumbertmp))
    oflog.write('\nFenster (P)                              : ' + str(mynumbertmp))
    oflog.write('\nDGM (P)                                  : ' + str(mynumbertmp))
    oflog.write('\nBewehrung (P)                            : ' + str(mynumbertmp))
    oflog.write('\nSonstige Elemente (P)                    : ' + str(sonstige))
    oflog.close()

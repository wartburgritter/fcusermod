import ifcopenshell
import ifcopenshell.api
import ifcopenshell.geom
import ifcopenshell.template

# create base ifcfile
# help(ifcopenshell.template.create)
ifcfile=ifcopenshell.template.create()
# TODO ... add names


# spatial structure
project = ifcfile.by_type("IfcProject")[0]
site = ifcopenshell.api.run("root.create_entity", ifcfile, ifc_class="IfcSite", name="My Site")
building = ifcopenshell.api.run("root.create_entity", ifcfile, ifc_class="IfcBuilding", name="My Building")
storey = ifcopenshell.api.run("root.create_entity", ifcfile, ifc_class="IfcBuildingStorey", name="My Storey")
ifcopenshell.api.run("aggregate.assign_object", ifcfile, product=site, relating_object=project)
ifcopenshell.api.run("aggregate.assign_object", ifcfile, product=building, relating_object=site)
ifcopenshell.api.run("aggregate.assign_object", ifcfile, product=storey, relating_object=building)
ifcopenshell.api.run("geometry.edit_object_placement", ifcfile, product=site)
ifcopenshell.api.run("geometry.edit_object_placement", ifcfile, product=building)
ifcopenshell.api.run("geometry.edit_object_placement", ifcfile, product=storey)


# miscellaneous
origin = ifcfile.createIfcAxis2Placement3D(
    ifcfile.createIfcCartesianPoint((0.0, 0.0, 0.0)),
    ifcfile.createIfcDirection((0.0, 0.0, 1.0)),
    ifcfile.createIfcDirection((1.0, 0.0, 0.0)),
)
placement = ifcfile.createIfcLocalPlacement(storey.ObjectPlacement, origin)
history = ifcopenshell.api.run("owner.create_owner_history", ifcfile)


# get the geometry
abrep_file = open("C:/Users/BHA/Desktop/some_geom_scaled.brep", "r")
brep_data = abrep_file.read()
abrep_file.close()
proddefshape = ifcopenshell.geom.serialise("IFC4", brep_data)


# add product
product = ifcfile.create_entity(
    "IfcBuildingElementProxy",
    GlobalId=ifcopenshell.guid.new(),
    Name="My Building Element",
    Representation=proddefshape,
)
ifcopenshell.api.run(
    "spatial.assign_container",
    ifcfile,
    product=product,
    relating_structure=ifcfile.by_type("IfcBuildingStorey")[0]
)
product.ObjectPlacement = placement


# write
ifcfile.write("C:/Users/BHA/Desktop/mycyl2.ifc")

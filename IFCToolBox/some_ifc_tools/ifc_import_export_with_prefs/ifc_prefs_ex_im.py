# ********************************************************************************
# export set a pref
import exportIFC
prefs = exportIFC.getPreferences()
prefs["ADD_DEFAULT_SITE"] = True
prefs["ADD_DEFAULT_BUILDING"] = True
prefs["ADD_DEFAULT_STOREY"] = True
prefs["SCHEMA"] = "IFC2X3"
exportIFC.export([App.ActiveDocument.Wall], "C:/Users/BHA/Desktop/ifc_in_out_with_prefs/wall.ifc", preferences=prefs)


# export set all pref
import exportIFC
prefs = {
    "DEBUG": True,
    "CREATE_CLONES": False,
    "FORCE_BREP": True,
    "STORE_UID": True,
    "SERIALIZE": False,
    "EXPORT_2D": True,
    "FULL_PARAMETRIC": False,
    "ADD_DEFAULT_SITE": True,
    "ADD_DEFAULT_BUILDING": True,
    "ADD_DEFAULT_STOREY": True,
    "IFC_UNIT": "metre",
    "SCALE_FACTOR": 0.001,
    "GET_STANDARD": False,
    "EXPORT_MODEL": "arch",
    "SCHEMA": "IFC2X3",
}
exportIFC.export([doc.Project], "C:/Users/BHA/Desktop/ifc_in_out_with_prefs/wall.ifc", preferences=prefs)


# ********************************************************************************
# import
import importIFC
prefs = {
    "DEBUG": True,
    "PREFIX_NUMBERS": False,
    "SKIP": ["IfcOpeningElement"],
    "SEPARATE_OPENINGS": False,
    "ROOT_ELEMENT": "IfcProduct",
    "GET_EXTRUSIONS": False,
    "MERGE_MATERIALS": True,
    "MERGE_MODE_ARCH": 1,
    "MERGE_MODE_STRUCT": 0,
    "CREATE_CLONES": False,
    "IMPORT_PROPERTIES": False,
    "SPLIT_LAYERS": False,
    "FITVIEW_ONIMPORT": True,
    "ALLOW_INVALID": True,
    "REPLACE_PROJECT": True,
    "MULTICORE": 0,
    "IMPORT_LAYER": True,
}
doc = importIFC.insert("C:/Users/BHA/Desktop/ifc_in_out_with_prefs/wall.ifc", "", preferences=prefs)

# def insert(srcfile, docname, skip=[], only=[], root=None, preferences=None, cutobj=None):



# ********************************************************************************
prefs = importIFC.getPreferences()
for k,v in prefs.items():
    print('    "{}": {},'.format(k,v))

prefs = exportIFC.getPreferences()
for k,v in prefs.items():
    print('    "{}": {},'.format(k,v))

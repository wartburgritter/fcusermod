import ifcopenshell

# from importIFC
from ifcopenshell import geom
settings = ifcopenshell.geom.settings()
settings.set(settings.USE_BREP_DATA,True)
settings.set(settings.SEW_SHELLS,True)
settings.set(settings.USE_WORLD_COORDS,True)

ifcfile = '/home/hugo/.FreeCAD/Mod/IFCToolBox/More_IfcAdvancedBrep.ifc'

f = ifcopenshell.open(ifcfile)
proxies = f.by_type('IfcBuildingElementProxy')

for p in proxies:
    try:
        cr = ifcopenshell.geom.create_shape(settings, p)
        print(' GOOD shape from IFCOS for {}'.format(p.id()))
    except:
        print(' NO shape from IFCOS for {}'.format(p.id()))



"""
# make a new FreeCAD document
cr = ifcopenshell.geom.create_shape(settings, p)
brep = cr.geometry.brep_data
shape = Part.Shape()
shape.importBrepFromString(brep)
Part.show(shape)
"""


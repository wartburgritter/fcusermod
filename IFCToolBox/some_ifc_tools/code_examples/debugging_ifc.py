import ifcopenshell, os
import Part

# from importIFC
from ifcopenshell import geom
settings = ifcopenshell.geom.settings()
settings.set(settings.USE_BREP_DATA,True)
settings.set(settings.SEW_SHELLS,True)
settings.set(settings.USE_WORLD_COORDS,True)

#ifcfile = '/home/hugo/Desktop/210_King_Merged.ifc'
ifcfile = '/home/hugo/Desktop/nurbs/buildingsmart/cube-advanced-brep.ifc'

f = ifcopenshell.open(ifcfile)
f.by_type('IfcProject')
f.by_type('IfcPerson')
f.by_type('IfcBuildingElementProxy')

p = f.by_id(181)
r = p.Representation
print(p, '\n', r)
# to find the attribute names ...
print(p.attribute_name(0))

# or better print all names and the values
print(p)
for i, e in enumerate(p):
    print("{} --> {}".format(p.attribute_name(i), e))


# make a new FreeCAD document
cr = ifcopenshell.geom.create_shape(settings, p)
brep = cr.geometry.brep_data
shape = Part.Shape()
shape.importBrepFromString(brep)
Part.show(shape)


try:
    cr = ifcopenshell.geom.create_shape(settings, product)
except:
    print(' no shape from IFCOS')


# https://gitlab.com/wartburgritter/BIM--opendev/tree/master/freecad--ifc

# ************************************************************************************************
# write ifc with error objs
# https://github.com/IfcOpenShell/IfcOpenShell/issues/564

# open if in FreeCAD with parsing module
# the opened file is in identifier ifc
error_ifc = ifcopenshell.file(schema=ifc.schema)
error_ifc.add(ifc.by_type("IfcProject")[0])
error_ifc.add(ifc[59398])
error_ifc.write("C:/Users/BHA/Desktop/error.ifc")


# ************************************************************************************************
ifcfile = '/home/hugo/Desktop/nurbs/buildingsmart/basin-advanced-brep.ifc'

f = ifcopenshell.open(ifcfile)
f.by_type('IfcProject')
f.by_type('IfcPerson')

p = f.by_id(615)
cr = ifcopenshell.geom.create_shape(settings, p)



# ************************************************************************************************
ifcfile = '/home/hugo/Desktop/nurbs/allplan/Object ALL.ifc'

f = ifcopenshell.open(ifcfile)
f.by_type('IfcProject')
f.by_type('IfcPerson')

p1 = f.by_id(490)
cr = ifcopenshell.geom.create_shape(settings, p1)

p2 = f.by_id(3662)
cr = ifcopenshell.geom.create_shape(settings, p2)

p3 = f.by_id(3779)
cr = ifcopenshell.geom.create_shape(settings, p3)

p4 = f.by_id(3850)
cr = ifcopenshell.geom.create_shape(settings, p4)

p5 = f.by_id(3989)
cr = ifcopenshell.geom.create_shape(settings, p5)

p6 = f.by_id(4144)
cr = ifcopenshell.geom.create_shape(settings, p6)

p7 = f.by_id(4297)
cr = ifcopenshell.geom.create_shape(settings, p7)

p8 = f.by_id(4488)
cr = ifcopenshell.geom.create_shape(settings, p8)


brep = cr.geometry.brep_data
shape = Part.Shape()
shape.importBrepFromString(brep)
Part.show(shape)

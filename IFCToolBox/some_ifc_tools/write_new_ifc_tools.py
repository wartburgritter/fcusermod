"""
# test if a entity is in ifcfile allready
the_guid = some_obj.GlobalId
is_in = False
try:
    the_found_p = anew_ifc.by_guid(the_guid)
    print("Found: {}".format(the_found_p))
except:
    print("Not found: {}".format(the_guid))

"""


"""
# ************************************************************************************************
# Informationen, Hinweise
#
# add_generic_spatial_structure(ifcfile, elements) nutzt objecte aus ifcfile
# wenn die IfcSite von ifcfile kommt ist das gelaendeobjekt dabei ... :-(
# wunderbar testbar an IfcOpenHouse und aggregate 187
# TODO create new project, site and building, create a method, see create_base_ifc()
#
# wenn die objekte im container nicht BuildingElemnetPart sind
# bsp IfcRoof besteht aus IfcSlab_1, IfcSlab_2, ...
# sind sie zweimal im gebaeude, selber und im container
# sollte auch mit IfcOpenHouse sehr nachvollziehbar sein
# eigentlich koennte man auch ifcpatch verwenden, aber dann hat es noch die orginal spatial structure
# so ist fast besser, da kann man das sofort irgendwo posten.
#
import ifcopenshell
from ifcopenshell import file as init_newifcfile
anew_ifc = init_newifcfile(schema=ifcfile.schema)
#
# add products (building elements) or aggregate when no geometry but Aggregation to BuildingElementParts
anew_ifc.add(ifcfile[12345])
# anew_ifc.add(ifcfile.by_type("IfcBuildingElement")[0])
#
anew_ifc = add_min_spatial_structure(ifcfile, anew_ifc)
anew_ifc.write("C:/Users/BHA/Desktop/problem_obj.ifc")

"""
# ************************************************************************************************
def add_min_spatial_structure(ifcfile, anew_ifc):
    import ifcopenshell
    anew_ifc.add(ifcfile.by_type("IfcProject")[0])
    anew_ifc.add(ifcfile.by_type("IfcBuilding")[0])
    anew_ifc.createIfcRelAggregates(
        ifcopenshell.guid.new(),
        anew_ifc.by_type("IfcOwnerHistory")[0],
        'project container',
        '',
        anew_ifc.by_type("IfcProject")[0],
        [anew_ifc.by_type("IfcBuilding")[0]]
    )
    anew_ifc.createIfcRelAggregates(
        ifcopenshell.guid.new(),
        anew_ifc.by_type("IfcOwnerHistory")[0],
        'building container',
        '',
        anew_ifc.by_type("IfcBuilding")[0],
        # anew_ifc.by_type("IfcBuildingElement")
        list(set(anew_ifc.by_type("IfcBuildingElement")) - set(anew_ifc.by_type("IfcBuildingElementPart")))
    )
    return anew_ifc


# ************************************************************************************************
"""
from some_ifc_tools import write_new_ifc_tools as crazyifctools
import importlib
importlib.reload(crazyifctools)

ifcfile = crazyifctools.create_ifc_from_proddefshape(proddefshape)

"""

"""
# ALL
from ifcopenshell import geom
abrep_file = open("C:/Users/BHA/Desktop/some_geom_scaled.brep", "r")
brep_geom = abrep_file.read()
abrep_file.close()
proddefshape = geom.serialise("IFC4", brep_geom)
proddefshape

from some_ifc_tools import write_new_ifc_tools as crazyifctools
import importlib
importlib.reload(crazyifctools)
ifcfile = crazyifctools.add_prod_to_ifc(proddefshape)

ifcfile.write("C:/Users/BHA/Desktop/some_geom_scaled_ifcnew.ifc")

"""


"""
# cool stuff
import Part
from ifcopenshell import geom
from some_ifc_tools import write_new_ifc_tools as crazyifctools
import importlib
importlib.reload(crazyifctools)

brep_geom = Part.makeCylinder(200,2000).exportBrepToString()
proddefshape = geom.serialise("IFC4", brep_geom)
ifcfile = crazyifctools.add_prod_to_ifc(proddefshape)

ifcfile.write("C:/Users/BHA/Desktop/mycyl.ifc")

"""


def create_base_ifc():

    # based on https://github.com/IfcOpenShell/IfcOpenShell/blob/v0.7.0/src/blenderbim/scripts/obj2ifc.py
    # im selben verzeichnis ist ein scipt dxf2ifc, dort ist create_ifc_file() einfacher
    # noch einfache ... https://community.osarch.org/discussion/1437/add-some-general-method-create-ifc-file
    # ideen https://github.com/EstebanDugueperoux2/ifc-test/blob/main/test.py
    # https://github.com/IfcOpenShell/academy/blob/main/posts/creating-a-simple-wall-with-property-set-and-quantity-information.md
    # TODO!!!!!!!! replace code by ifcopenshell.template, pass manes to template

    # *******************************************************************
    # the base ifc file for adding building elements later on
    import ifcopenshell
    import ifcopenshell.api
    import ifcopenshell.api.owner.settings

    ifcfile = ifcopenshell.api.run("project.create_file", version="IFC4")
    person = ifcopenshell.api.run("owner.add_person", ifcfile)
    person[0] = person.GivenName = None
    person.FamilyName = "user"
    org = ifcopenshell.api.run("owner.add_organisation", ifcfile)
    org[0] = None
    org.Name = "template"
    user = ifcopenshell.api.run("owner.add_person_and_organisation", ifcfile, person=person, organisation=org)
    application = ifcopenshell.api.run("owner.add_application", ifcfile)
    ifcopenshell.api.owner.settings.get_user = lambda ifc: user
    ifcopenshell.api.owner.settings.get_application = lambda ifc: application

    project = ifcopenshell.api.run("root.create_entity", ifcfile, ifc_class="IfcProject", name="My Project")
    lengthunit = ifcopenshell.api.run("unit.add_si_unit", ifcfile, unit_type="LENGTHUNIT")
    ifcopenshell.api.run("unit.assign_unit", ifcfile, units=[lengthunit])
    model = ifcopenshell.api.run("context.add_context", ifcfile, context_type="Model")
    context = ifcopenshell.api.run(
        "context.add_context",
        ifcfile,
        context_type="Model",
        context_identifier="Body",
        target_view="MODEL_VIEW",
        parent=model,
    )
    site = ifcopenshell.api.run("root.create_entity", ifcfile, ifc_class="IfcSite", name="My Site")
    building = ifcopenshell.api.run("root.create_entity", ifcfile, ifc_class="IfcBuilding", name="My Building")
    storey = ifcopenshell.api.run(
        "root.create_entity", ifcfile, ifc_class="IfcBuildingStorey", name="My Storey"
    )
    ifcopenshell.api.run("aggregate.assign_object", ifcfile, product=site, relating_object=project)
    ifcopenshell.api.run("aggregate.assign_object", ifcfile, product=building, relating_object=site)
    ifcopenshell.api.run("aggregate.assign_object", ifcfile, product=storey, relating_object=building)

    ifcopenshell.api.run("geometry.edit_object_placement", ifcfile, product=site)
    ifcopenshell.api.run("geometry.edit_object_placement", ifcfile, product=building)
    ifcopenshell.api.run("geometry.edit_object_placement", ifcfile, product=storey)

    origin = ifcfile.createIfcAxis2Placement3D(
        ifcfile.createIfcCartesianPoint((0.0, 0.0, 0.0)),
        ifcfile.createIfcDirection((0.0, 0.0, 1.0)),
        ifcfile.createIfcDirection((1.0, 0.0, 0.0)),
    )
    placement = ifcfile.createIfcLocalPlacement(storey.ObjectPlacement, origin)
    history = ifcopenshell.api.run("owner.create_owner_history", ifcfile)

    return ifcfile, placement


def add_prod_to_ifc(proddefshape):

    ifcfile, placement = create_base_ifc()

    # *******************************************************************
    # add the geom object as building element to the ifcfile
    product = ifcfile.create_entity(
        "IfcBuildingElementProxy",
        GlobalId=ifcopenshell.guid.new(),
        Name="My Building Element",
        Representation=proddefshape,
    )
    ifcopenshell.api.run(
        "spatial.assign_container",
        ifcfile,
        product=product,
        relating_structure=ifcfile.by_type("IfcBuildingStorey")[0]
    )
    product.ObjectPlacement = placement
    # if the placement is before the container, the placement is at last in ifc


    return ifcfile


"""
import Part
from some_ifc_tools import write_new_ifc_tools as crazyifctools
crazyifctools.shape2nurbsifc(Part.makeCylinder(200,2000))

"""
def shape2nurbsifc(sh, out_p = "C:/Users/BHA/Desktop/myfancygeom.ifc"):

    from ifcopenshell import geom
    # from some_ifc_tools import write_new_ifc_tools as crazyifctools

    proddefshape = geom.serialise("IFC4", sh.exportBrepToString())
    ifcfile = create_ifc_from_proddefshape(proddefshape)
    ifcfile.write(out_p)

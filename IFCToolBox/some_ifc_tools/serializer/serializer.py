# ***************************************************************************
# *   (c) Bernd Hahnebach (bernd@bimstatik.org) 2019                        *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# *   Bernd Hahnebach 2017                                                  *
# ***************************************************************************/

import os
from os.path import join

import FreeCAD
import importIFC


# *****************************************************************************
# See module ser_projects.py for projects code


# *****************************************************************************
def import_ifc_and_save_fcstd(ifc_file=None, doc=None):
    
    if ifc_file is None:
        return
    if doc is None:
        doc_name = ""
    else:
        doc_name = doc.Name

    ifc_dir = os.path.split(ifc_file)[0]
    ifc_name = os.path.splitext(os.path.basename(ifc_file))[0]

    import importIFC
    prefs = {
        "DEBUG": True,
        "PREFIX_NUMBERS": False,
        "SKIP": ["IfcOpeningElement"],
        "SEPARATE_OPENINGS": False,
        "ROOT_ELEMENT": "IfcProduct",
        "GET_EXTRUSIONS": False,
        "MERGE_MATERIALS": True,
        "MERGE_MODE_ARCH": 1,
        "MERGE_MODE_STRUCT": 0,
        "CREATE_CLONES": False,
        "IMPORT_PROPERTIES": False,
        "SPLIT_LAYERS": False,
        "FITVIEW_ONIMPORT": True,
        "ALLOW_INVALID": True,
        "REPLACE_PROJECT": True,
        "MULTICORE": 0,
        "IMPORT_LAYER": True,
    }
    doc = importIFC.insert(ifc_file, doc_name, preferences=prefs)
    doc.recompute()
    doc.saveAs(join(ifc_dir, ifc_name + "FCStd"))
    doc.recompute()
    return doc

    """
    for o in doc.Objects:
        if hasattr(o, "IfcType") and o.IfcType == "Building Storey":
            export_datei = join(pfad, datei_base_name + "_" + o.Label + "_serialize.ifc")
            importIFC.export([o], export_datei)
    doc.recompute()
    FreeCAD.closeDocument(doc.Name)
    """


# *****************************************************************************
def add_bigspace(doc, length, width, height, originx, originy, originz):
    # big BoundBox
    import Part, Arch
    partname = "Raum_part"
    spacename = "Space"
    if doc.getObject(partname) is not None:
        doc.removeObject(partname)
    if doc.getObject(spacename) is not None:
        doc.removeObject(spacename)
    raum_partobj = doc.addObject("Part::Box", partname)
    raum_partobj.Length = length
    raum_partobj.Width = width
    raum_partobj.Height = height
    raum_partobj.Placement.Base = (originx, originy, originz)
    raum_obj = Arch.makeSpace(raum_partobj, name=spacename)
    doc.recompute()
    return raum_obj


# *****************************************************************************
def remove_by_outside_bigspace(doc, raum_obj):
    bigspace = raum_obj.Shape.BoundBox
    for o in doc.Objects:
        if hasattr(o, "Shape") and hasattr(o.Shape, "BoundBox"):
            try:
                bb = o.Shape.BoundBox
            except:
                bb = None
            if bb and bb.isValid():
                # check if bb intersects with bigspace
                if bb.intersect(bigspace) is False:
                    doc.removeObject(o.Name)
    doc.recompute()
    # TODO: gar kein space erzeugen, sondern nur die BoundBox, aber dann nicht sichtbar
    # Geschosse haben neu auch eine Shape
    # isInside ist nur True wenn vollkommen in der BIGSpace
    # heisst alle die nur teilweise drin sind sind False werden gelöscht
    # Ich will aber nur vollständig inside loeschen

# *****************************************************************************
def remove_by_buildingelementtype(doc, ifctype):
    for o in doc.Objects:
        if hasattr(o, "IfcType") and o.IfcType == ifctype:
            doc.removeObject(o.Name)
    doc.recompute()

    # neue schleife, weil sonst error, da window deleted
    # aber noch in doc.Objects
    for o in doc.Objects:
        if hasattr(o, "IfcType") and o.IfcType == "Door":
            doc.removeObject(o.Name)
    doc.recompute()


# *****************************************************************************
def remove_by_attrib_value_contains(doc, in_value):
    # no matter which attribute, in which pset
    for o in doc.Objects:
        if hasattr(o, "IfcProperties"):
            for key, value in o.IfcProperties.items():
                if in_value in value:
                    doc.removeObject(o.Name)
    doc.recompute()


# *****************************************************************************
def remove_by_attrib_key_value(doc, key_to_remove, value_to_remove):
    # exact attribute in exact pset
    for o in doc.Objects:
        if hasattr(o, "IfcProperties"):
            for key, value in o.IfcProperties.items():
                if key == key_to_remove:
                    if value == value_to_remove:
                        doc.removeObject(o.Name)
    doc.recompute()


# *****************************************************************************
def remove_by_typeattr(doc, target_type):
    # target_type must match actual_type
    # print("{}\n".format(target_type))
    for o in doc.Objects:
        actual_type = get_object_type(o)
        if actual_type is not None and target_type == actual_type:
            # print(actual_type)
            doc.removeObject(o.Name)
    doc.recompute()


def remove_by_subtypeattr(doc, target_type):
    # target_type must be in actual_type
    # print("{}\n".format(target_type))
    for o in doc.Objects:
        actual_type = get_object_type(o)
        if actual_type is not None and target_type in actual_type:
            # print(actual_type)
            doc.removeObject(o.Name)
    doc.recompute()


def get_object_type(obj):
    # this is not the Bauteilklasse this is the type in BIMCollabZoom Summary tab
    # ArchiCAD and VectorWorks does it totally different

    # VectorWorks
    # GUID 1Qit2VxgT3BfECV96Gt$kF
    # PSet Pset_SlabCommon
    # Attribut Reference
    # Value IFCIDENTIFIER("C1_BP_Beton_0.43")
    # https://standards.buildingsmart.org/IFC/RELEASE/IFC2x3/TC1/HTML/psd/IfcSharedBldgElements/Pset_SlabCommon.xml

    # it seams Autodesk Revit does it the same way as VectorWorks

    # ArchiCAD
    # separate entity IfcSlabType, IfcWallType, etc
    # https://standards.buildingsmart.org/IFC/RELEASE/IFC2x3/FINAL/HTML/ifcsharedbldgelements/lexical/ifcslab.htm
    # dort suche nach "IfcSlabType", can be found by inverse "IsDefinedBy"

    # IfcOpenShell
    # see forum topic https://community.osarch.org/discussion/507/list-property-set-property-names-and-element-types-that-have-this

    actual_type = None
    if (
        # ArchiCAD
        hasattr(obj, "ObjectType")
        and isinstance(obj.ObjectType, str)
        and obj.ObjectType != ""
    ):
        actual_type = obj.ObjectType
    elif (
        # VectorWorks
        hasattr(obj, "IfcProperties")
        and isinstance(obj.IfcProperties, dict)
        and obj.IfcProperties != {}
    ):
        # to extract props from FreeCAD IFC props dict
        from exportIFC import getPropertyData
        # print(obj.IfcProperties)
        for key, value in obj.IfcProperties.items():
            pset, pname, ptype, pvalue = getPropertyData(key, value, {})
            if (
                pname == "Reference"
                # and ptype == IfcIdentifier
                and "Pset_" in pset and "Common" in pset  # startswith and endswith
                and pvalue is not None
            ):
                # print(pvalue)
                actual_type = pvalue
    print(actual_type)
    return actual_type


# *****************************************************************************
def remove_by_material_label(doc, matlabel):
    for o in doc.Objects:
        if hasattr(o, "Material") and hasattr(o.Material, "Label"):
            if o.Material.Label == matlabel:
                doc.removeObject(o.Name)
    doc.recompute()


# *****************************************************************************
def remove_by_label(doc, objlabel):
    for o in doc.Objects:
        if hasattr(o, "Label") and o.Label == objlabel:
            doc.removeObject(o.Name)
    doc.recompute()


# *****************************************************************************
def remove_axes(doc):
    for o in doc.Objects:
        if hasattr(o, "Proxy") and hasattr(o.Proxy, "Type"):
            if o.Proxy.Type == "Axis":
                doc.removeObject(o.Name)
    doc.recompute()
    for o in doc.Objects:
        if hasattr(o, "Proxy") and hasattr(o.Proxy, "Type"):
            if o.Proxy.Type == "AxisSystem":
                doc.removeObject(o.Name)
    doc.recompute()


# *****************************************************************************
def remove_empty_buildingparts(doc):
    for o in doc.Objects:
        if hasattr(o, "Proxy") and hasattr(o.Proxy, "Type"):
            if o.Proxy.Type == "BuildingPart":
                if len(o.Group) == 0:
                    doc.removeObject(o.Name)
    doc.recompute()

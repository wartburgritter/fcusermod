"""
import importlib
import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_ing_trw as runspzutrw

importlib.reload(runspzutrw)

runspzutrw.serialise_spzu_ing_trw_ortbeton_ug_platvert()
runspzutrw.serialise_spzu_ing_trw_ortbeton_alles_fuer_rohvergleich()
runspzutrw.serialise_spzu_ing_trw_elementstuetzen()

"""


import importlib
from os.path import join
import ifcopenshell
from FreeCAD import Vector
import Part
import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)


std_workpath = "C:/0_BHA_privat/BIMGeomSerializer"
infile_ifc_trw = "infiles/4130_ING_N_TRW.ifc"


# ************************************************************************************************
def serialise_spzu_ing_trw_ortbeton_ug_platvert(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_trw,
        outfile_name = "UG_Platten_Vertikalbauteile_ING_TRW_cut",
        def_remelements = get_remelements_spzu_ing_trw_ortbeton_ug_platvert,
        def_shcutobj = spzu_ing_trw_ortbeton_ug_platvert_get_shcutobj,
        create_ifc = create_ifc
    )
    return True


def serialise_spzu_ing_trw_ortbeton_alles_fuer_rohvergleich(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_trw,
        outfile_name = "Alle_Ortbetonbauteile_fuer_rohvergleich_ING_TRW",
        def_remelements = get_remelements_spzu_ing_trw_ortbeton_fuer_rohvergleich,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


def serialise_spzu_ing_trw_elementstuetzen(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_trw,
        outfile_name = "Alle_Elementstuetzen",
        def_remelements = get_remelements_spzu_ing_trw_elementstuetzen,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


# ************************************************************************************************
def spzu_ing_trw_ortbeton_ug_platvert_get_shcutobj():

    # boxcut fuer ca. 50 cm hohe waende
    print("Shape Cutobj")
    sh_cutobj = Part.makeBox(150000, 60000, 10000)
    sh_cutobj.Placement.move(Vector(-15000, -13000, -2000))
    return sh_cutobj


def get_remelements_spzu_ing_trw_ortbeton_ug_platvert(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Storeys")
    rem_floors = []
    rem_floors += sertools.get_by_storey_name(all_elements, "01.UG_FBOK", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_floors)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcFooting")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Ausgleichsschicht")
    rem_mats += sertools.get_by_material_name(all_elements, "DaemmungHart")
    rem_mats += sertools.get_by_material_name(all_elements, "FertigelementBeton")
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Object Types")
    rem_objtypes = []
    rem_objtypes += sertools.get_by_objtype(all_elements, "Landing")
    sertools.add_elems_to_remelems(rem_elements, rem_objtypes)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def get_remelementsspzu_ing_trw_ortbeton_fuer_rohvergleich(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcFooting")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Ortbeton", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def get_remelements_spzu_ing_trw_elementstuetzen(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Ausgleichsschicht")
    rem_mats += sertools.get_by_material_name(all_elements, "Backstein")
    rem_mats += sertools.get_by_material_name(all_elements, "Daemmung_Hart")
    rem_mats += sertools.get_by_material_name(all_elements, "Daemmung_Weich")
    rem_mats += sertools.get_by_material_name(all_elements, "Fertigelement Beton")
    rem_mats += sertools.get_by_material_name(all_elements, "Ortbeton")
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements
    
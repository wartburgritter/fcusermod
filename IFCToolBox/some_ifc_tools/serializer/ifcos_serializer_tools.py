# ***************************************************************************
# *   (c) Bernd Hahnebach (bernd@bimstatik.org) 2022                        *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************/

import importlib
import os
from os.path import join

import ifcopenshell
import ifcopenshell.util.element as eleutils


# ************************************************************************************************
def run_patch_generic_elems(
    def__get_concrete_elems,
    def__get_concrete_ignore_elems,
    def__get_plattenbauteile_concrete,
    outpath,
    ifc_infile_p,
    ifcfile,
    geschosse,
    plat_vert,
    model_type,
    write_ifc
):

    print("Start run_patch_generic_elems")

    # plat_vert ... "plat", "vert", "" == plat+vert

    # print(ifcfile.by_type("IfcProject")[0].Name)
    # print(ifcfile.by_type("IfcSite")[0].Name)
    # print(ifcfile.by_type("IfcBuilding")[0].Name)

    all_elements = ifcfile.by_type("IfcBuildingElement")

    # the base of all are the ortbeton bauteile
    ortbet_elems = def__get_concrete_elems(all_elements)
    no_ob_elems = list(set(all_elements) - set(ortbet_elems))

    # get and save all plattenbauteile ortbeton
    pla_ob_elems = def__get_plattenbauteile_concrete(ortbet_elems)

    # get ortbetonignorebauteile
    ob_ignore_elems = def__get_concrete_ignore_elems(ortbet_elems)

    # get and save the vertikalbauteile ortbeton
    # diff vertbauteile = orbeton - platten - ortbetonignorebauteile
    ver_ob_elems = list(set(ortbet_elems) - set(pla_ob_elems) - set(ob_ignore_elems))

    # some output
    print("Ele all elem: {}".format(len(all_elements)))
    print("Ele ob elem: {}".format(len(ortbet_elems)))
    print("Ele no elem: {}".format(len(no_ob_elems)))
    print("")
    print("Ele ob p+v+i: {}".format(len(pla_ob_elems + ver_ob_elems + ob_ignore_elems)))
    print("Ele ob igno: {}".format(len(ob_ignore_elems)))
    print("Ele ob plat: {}".format(len(pla_ob_elems)))
    print("Ele ob vert: {}".format(len(ver_ob_elems)))
    print("Sum ob  p+v: {}".format(len(pla_ob_elems + ver_ob_elems)))
    print("")

    # HAAACK, wenn innerhalb BIMTester, dann nicht die generischen dateien erstellen
    # die methode muss ich aber laufen lassen wegen elems_data
    if "_bimtester" in outpath:
        # set write_ifc to False, thus generic ifc are not written :-)
        # passt ... spart einiges an zeit :-)
        write_ifc = False

    # write generic files
    # dreimal if is richtig :-)
    if write_ifc is True and (plat_vert == "" or plat_vert == "vert"):
        # vert needs all plat
        write_patched(outpath, ifc_infile_p, ifcfile, "Alle_Plattenbauteile_{}".format(model_type), pla_ob_elems)
    if write_ifc is True and (plat_vert == "" or plat_vert == "plat"):
        # plat needs all vert
        write_patched(outpath, ifc_infile_p, ifcfile, "Alle_Vertikalbauteile_{}".format(model_type), ver_ob_elems)

    # wann brauche ich diese?
    # Fuer das festlegen der geschossbauteile und der plat_vert zuordung
    write_all_generic = True
    # write_all_generic = False
    if write_ifc is True and write_all_generic is True:
        write_patched(outpath, ifc_infile_p, ifcfile, "Alle_Ortbetonbauteile_{}".format(model_type), ortbet_elems)
        write_patched(outpath, ifc_infile_p, ifcfile, "Alle_NichtOrtbetonbauteile_{}".format(model_type), no_ob_elems)
        write_patched(outpath, ifc_infile_p, ifcfile, "Alle_Ignorebauteile_{}".format(model_type), ob_ignore_elems)

    print("End run_patch_generic_elems")

    return all_elements, ortbet_elems, no_ob_elems, ob_ignore_elems, pla_ob_elems, ver_ob_elems


# ************************************************************************************************
def run_patch_geschoss_elems(
        def__get_geschoss_ob_bauteile,
        elems_data,
        outpath,
        ifc_infile_p,
        ifcfile,
        geschosse,
        plat_vert,
        model_type,
        write_ifc,
        spatial_data,
    ):

    print("Start run_patch_geschoss_elems for plat/vert elems")

    elems_floors_ob = {}
    sum_plat = 0
    sum_vert = 0
    for geschoss in geschosse:
        print(geschoss)
        data = spatial_data[geschoss]
        gesch_name = data["name"]
        # print(gesch_name)
        pla_gesch_ob_elems, ver_gesch_ob_elems = def__get_geschoss_ob_bauteile(
            elems_data,
            geschoss,
            gesch_name
        )
        # print(len(pla_gesch_ob_elems))
        # print(len(ver_gesch_ob_elems))
        sum_plat += len(pla_gesch_ob_elems)
        sum_vert += len(ver_gesch_ob_elems)
        elems_floors_ob[geschoss] = {}
        elems_floors_ob[geschoss]["plat"] = {"Name": "Plattenbauteile", "Elemente": pla_gesch_ob_elems}
        elems_floors_ob[geschoss]["vert"] = {"Name": "Vertikalbauteile", "Elemente": ver_gesch_ob_elems}
        # print(list(elems_floors_ob[geschoss].keys()))

    # TODO: add and remove elements from geschoss
    # start with rampe
    # irgendwie immer eine methode uebergeben. diese macht nichts wenn nichts implementiert ist
    # haaack if in obigen

    print("Sum Gesch plat: {}".format(sum_plat))
    print("Sum Gesch vert: {}".format(sum_vert))
    print("Sum Gesch allg: {}".format(sum_plat + sum_vert))
    # print(list(elems_floors_ob.keys()))

    # write geschoss ortbeton elements
    for geschoss, data in elems_floors_ob.items():
        # print(geschoss)
        out_file_plat_n = "{}_{}_{}".format(geschoss, data["plat"]["Name"], model_type)
        out_file_vert_n = "{}_{}_{}".format(geschoss, data["vert"]["Name"], model_type)
        if write_ifc is True and (plat_vert == "" or plat_vert == "plat"):
            write_patched(outpath, ifc_infile_p, ifcfile, out_file_plat_n, data["plat"]["Elemente"])
        if write_ifc is True and (plat_vert == "" or plat_vert == "vert"):
            write_patched(outpath, ifc_infile_p, ifcfile, out_file_vert_n, data["vert"]["Elemente"])

    print("End run_patch_geschoss_elems for plat/vert elems")

    return elems_floors_ob


# ************************************************************************************************
def run_patch_geschoss_std_elems(
        def__get_modell_patch_bauteile,
        def__get_geschoss_patch_bauteile,
        elems_data,
        outpath,
        ifc_infile_p,
        ifcfile,
        geschosse,
        write_ifc,
        spatial_data,
    ):

    print("# ******* Start run_patch_geschoss_std_elems")

    # get alle elements welche auf geschosse aufgeteilt werden sollen
    elems_patch_modell = def__get_modell_patch_bauteile(elems_data[0])

    elems_floors_patch = {}
    sum_patch = 0
    for geschoss in geschosse:
        # print(geschoss)
        data = spatial_data[geschoss]
        gesch_name = data["name"]
        # print(gesch_name)
        elems_geschoss_patch = def__get_geschoss_patch_bauteile(
            elems_patch_modell,
            geschoss,
            gesch_name
        )
        # print(len(elems_geschoss_patch))
        sum_patch += len(elems_geschoss_patch)

        # save elems
        elems_floors_patch[geschoss] = {}
        elems_floors_patch[geschoss] = {"Name": "Elementstuetzen_ING_TRW", "Elemente": elems_geschoss_patch}
        # print(list(elems_floors_patch[geschoss].keys()))
    # print(list(elems_floors_patch.keys()))

    print("Patch Elems: {} Im Modell".format(sum_patch))
    print("Patch Elems: {} Summe der Geschosse".format(len(elems_patch_modell)))
    print("Alle  Elems: {} Alle".format(len(elems_data[0])))

    # write geschoss patch elements
    for geschoss, data in elems_floors_patch.items():
        if len(data["Elemente"]) == 0:
            continue
        # print(geschoss)
        out_file_plat_n = "{}_{}".format(geschoss, data["Name"])
        # print(out_file_plat_n)
        if write_ifc is True:
            write_patched(outpath, ifc_infile_p, ifcfile, out_file_plat_n, data["Elemente"])

    print("# ******* End run_patch_geschoss_std_elems")

    return elems_floors_patch


# ************************************************************************************************
def serialise(
    workpath,
    infile_ifc,
    outfile_name,
    def_remelements,
    def_shcutobj,
    create_ifc=False, # erste nicht stationaer fixierter parameter
    parsed_ifc=None,
):

    # workpath is inpath and outpath

    print("\n{}".format(outfile_name))
    ifc_infile_p = join(workpath, infile_ifc)
    outdir_newbasename_noending = join(workpath, outfile_name)
    if parsed_ifc is None:
        print("No ifc, we need to parse it first.")
        ifc_file_ifcos = ifcopenshell.open(ifc_infile_p)
    else:
        print("Use ifc parsed by method.")
        ifc_file_ifcos = parsed_ifc

    all_elements, rem_elements = def_remelements(ifc_file_ifcos)
    if def_shcutobj is None:
        sh_cutobj = None
    else:
        sh_cutobj = def_shcutobj()

    # get the guids to add to the new ifc
    add_elems_guid = get_guids_to_add(all_elements, rem_elements)

    # smart view
    write_smart_view(add_elems_guid, outdir_newbasename_noending)

    # write serialised ifc
    if create_ifc is True:

        # use FreeCAD
        # write_ifc_serialised(ifc_infile_p, outdir_newbasename_noending, rem_elements, sh_cutobj)

        # use IfcPatch, but in smart data diff we would need the FC file anyway
        # but we could read the ifc into FC over there, it just needs to be implemented
        # cutobj needs to be done in FC, not easy possible in ifcopenshell only
        write_ifc_patched(ifc_infile_p, ifc_file_ifcos, outdir_newbasename_noending, add_elems_guid)

    return True


# ************************************************************************************************
def remove_elems_from_add(the_entities, rem_entities):

    print(len(the_entities))
    print(len(rem_entities))

    # create lists with ids only
    the_ids = []
    rem_ids = []
    for elem in the_entities:
        the_ids.append(elem.id())
    for elem in rem_entities:
        rem_ids.append(elem.id())

    # difference ids
    diff_ids = list(set(the_ids).difference(set(rem_ids)))

    # build a list of difference ids
    diff_entities = []
    for ifcid in diff_ids:
        for ele in the_entities:
            if ifcid == ele.id():
                diff_entities.append(ele)
                break

    print(len(diff_entities))

    return diff_entities


# ************************************************************************************************
def add_elems_to_remelems(rem_elements, new_rem_elems):
    rem_elements += new_rem_elems
    rem_elements = list(set(rem_elements))
    print("New rems (some might be in rem_elements already): {}".format(len(new_rem_elems)))
    print("Elements to remove: {}".format(len(rem_elements)))


# ************************************************************************************************
def get_by_ifctype(elements, target_ifctype, inverse=False):
    elems_match = []
    # print(target_ifctype)
    # print(inverse)
    for elem in elements:
        # print(elem.is_a())
        if (
            (inverse is False and elem.is_a() == target_ifctype)
            or (inverse is True and elem.is_a() != target_ifctype)
        ):
            # print(elem.Name)
            elems_match.append(elem)
    # print(len(elems_match))
    return elems_match


# ************************************************************************************************
def get_by_name(elements, target_name, inverse=False):
    elems_match = []
    for elem in elements:
        if (
            (inverse is False and elem.Name == target_name)
            or(inverse is True and elem.Name != target_name)
        ):
            # print(elem.Name)
            elems_match.append(elem)
    return elems_match


# ************************************************************************************************
def get_by_description(elements, target_name, inverse=False):
    elems_match = []
    for elem in elements:
        if (
            (inverse is False and elem.Description == target_name)
            or(inverse is True and elem.Description != target_name)
        ):
            # print(elem.Description)
            elems_match.append(elem)
    return elems_match


# ************************************************************************************************
def get_by_guid(elements, target_guid, inverse=False):
    elems_match = []
    for elem in elements:
        if (
            (inverse is False and elem.GlobalId == target_guid)
            or(inverse is True and elem.GlobalId != target_guid)
        ):
            # print(elem.GlobalId)
            elems_match.append(elem)
    return elems_match


# ************************************************************************************************
def get_by_realtype_name(elements, target_realtype_name, contains=False):
    # steht in zoom wenn vorhanden in Summary unter Phase und ueber Description
    # es hat type und type name
    # im IFC in separaten entities
    elems_match = []
    for elem in elements:
        actual_type = eleutils.get_type(elem)
        # print(actual_type)
        if (
            actual_type is not None
            and hasattr(actual_type, "Name")
            and actual_type.Name != ""
        ):
            tn = actual_type.Name
            # print(tn)
            if (
            (contains is False and target_realtype_name == tn)
            or (contains is True and target_realtype_name in tn)
            ):
                # print(tn)
                elems_match.append(elem)
    return elems_match


# ************************************************************************************************
def get_by_objtype(elements, target_objtype, inverse=False):
    # steht in zoom wenn vorhanden in Summary in Zeile unter GUID bei Attributen
    # im IFC direct im entity attribut ObjectType
    elems_match = []
    for elem in elements:
        if (
            (inverse is False and elem.ObjectType == target_objtype)
            or (inverse is True and  elem.ObjectType != target_objtype)
        ):
            # print(elem.ObjectType)
            elems_match.append(elem)
    return elems_match


# ************************************************************************************************
def get_by_material_name(elements, material_name, inverse=False):
    elems_match = []
    count_none_material = 0
    count_normal_material = 0
    count_material_layer = 0
    for elem in elements:
        # print(elem.id())
        # bei IfcBuildingElementPart objekte gibt es evtl probleme ...
        if elem.is_a("IfcBuildingElementPart"):
            # print("Continue, due to IfcBuildingElementPart")
            continue
            # bloed bei inverse bleiben die drinen
        actual_material = eleutils.get_material(elem)
        # if material layer are assigned the material does not have a Name attribut than, thus test
        # print(elem.id(), actual_material)
        if actual_material is None:
            count_none_material += 1
            # print(elem.id(), actual_material)
            if inverse is True:
                # if inverse ist True add all elements without material too
                elems_match.append(elem)
        elif hasattr(actual_material, "Name"):
            count_normal_material += 1
            if (
                (inverse is False and actual_material.Name == material_name)
                or (inverse is True and actual_material.Name != material_name)
            ):
                # print(elem.id(), material_name)
                elems_match.append(elem)
        else:
            count_material_layer += 1
            # print(elem.id(), actual_material)

    print("Objs with None material: {}".format(count_none_material))
    print("Objs with normal material: {}".format(count_normal_material))
    print("Objs with material layer material: {}".format(count_material_layer))

    return elems_match


# ************************************************************************************************
def get_by_storey_name(elements, target_storey_name, inverse=False):
    elems_match = []
    for elem in elements:
        # print(elem)
        # print(elem.id())
        actual_storey_name = None
        if (
            len(elem.ContainedInStructure) == 1
            and elem.ContainedInStructure[0].RelatingStructure.is_a() == "IfcBuildingStorey"
        ):
            actual_storey_name = elem.ContainedInStructure[0].RelatingStructure.Name
        # print(actual_storey_name)
        if (
            (inverse is False and actual_storey_name == target_storey_name)
            or (inverse is True and actual_storey_name != target_storey_name)
        ):
            elems_match.append(elem)

    return elems_match


# ************************************************************************************************
def get_by_property_value(elements, target_pset_name, target_property, target_value, inverse=False):
    elems_match = []
    for elem in elements:
        # print(elem)
        # print(elem.id())
        allpsets = eleutils.get_psets(elem, psets_only=True)
        if target_pset_name not in allpsets:
            continue
        actual_pset_data = allpsets[target_pset_name]
        if target_property in actual_pset_data:
            actual_value = actual_pset_data[target_property]
            # the inverse does only match if the attribute exists but does have onother value
            if (
                (inverse is False and actual_value == target_value)
                or (inverse is True and actual_value != target_value)
            ):
                # print(target_pset_name, target_property, actual_value)
                elems_match.append(elem)

    return elems_match


# ************************************************************************************************
def get_guids_to_add(all_elements, rem_elements):

    # stimmen nicht immer, wenn containerojb hinzugefuegt wird, sind alle unterobj. dabei
    # im SM nicht ganz so wichtig, aber beim patchen IFC schon
    # solve later
    add_guids = []
    no_duplicate_rem_elements = list(set(rem_elements))
    for elem in all_elements:
        if elem in no_duplicate_rem_elements:
            continue
        add_guids.append(elem.GlobalId)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(no_duplicate_rem_elements)))
    print("Enities to add: {}".format(len(set(add_guids))))

    return add_guids


# ************************************************************************************************
# Zoom SmartView
# same as BIMTester, add is red all other are grey transparent
# TODO: not transparent, only show the add_elems_guid
def write_smart_view(sm_guids, smview_file_path_noending):

    from code_bimtester.bimtester.features import zoom_smart_view
    importlib.reload(zoom_smart_view)
    smfile = smview_file_path_noending + ".bcsv"
    zoom_smart_view.create_zoom_set_of_smartviews(smfile, "Serialize_made_by_IFCOS")
    zoom_smart_view.add_smartview(smfile, "MyTest", sm_guids)
    return True


# ************************************************************************************************
def write_ifc_serialised(the_ifcfile, outdir_newbasename_noending, rem_elements, sh_cutobj=None):

    import FreeCAD as App

    # cutobj
    if sh_cutobj is None:
        adoco = None
        acutobj = None
    else:
        adocn = "TheNewDoc"
        adoco = App.newDocument(adocn)
        acutobj = adoco.addObject("Part::Feature", "CutObj")
        acutobj.Shape = sh_cutobj
        adoco.recompute()
        App.ActiveDocument = adoco
    # import
    the_doc = import_in_fc_with_skip_and_cut(the_ifcfile, rem_elements, acutobj)
    # save and export
    groupobj_to_export = the_doc.getObjectsByLabel(os.path.basename(the_ifcfile))[0]
    the_doc.saveAs(outdir_newbasename_noending + ".FCStd")
    export_to_ifc(groupobj_to_export, outdir_newbasename_noending + ".ifc")
    App.closeDocument(the_doc.Name)
    if adoco is not None:
        App.closeDocument(adocn)
    return True


# ************************************************************************************************
def import_in_fc_with_skip_and_cut(ifc_filep, rem_elements=[], cutobj=None):

    # import the ifc file into FreeCAD in ActiveDocument
    # skip the rem_elements
    # cut with cutobj

    skipids = []
    for elem in rem_elements:
        skipids.append(elem.id())
    print(len(skipids))

    importprefs = {
        "DEBUG": True,
        "PREFIX_NUMBERS": False,
        "SKIP": ["IfcOpeningElement"],
        "SEPARATE_OPENINGS": False,
        "ROOT_ELEMENT": "IfcProduct",
        "GET_EXTRUSIONS": False,
        "MERGE_MATERIALS": True,
        "MERGE_MODE_ARCH": 1,
        "MERGE_MODE_STRUCT": 0,
        "CREATE_CLONES": False,
        "IMPORT_PROPERTIES": False,  # glaube das ist extra noch in spreadsheet
        "SPLIT_LAYERS": False,
        "FITVIEW_ONIMPORT": True,
        "ALLOW_INVALID": True,
        "REPLACE_PROJECT": True,
        "MULTICORE": 0,
        "IMPORT_LAYER": True,
    }
    import importIFC
    # no document name is given, importer will use the active document
    doc = importIFC.insert(ifc_filep, "", skip=skipids, preferences=importprefs, cutobj=cutobj)
    return doc


# ************************************************************************************************
def export_to_ifc(groupobj_to_export, export_ifcfile):

    exportprefs = {
        'DEBUG': True,
        'CREATE_CLONES': False,
        'FORCE_BREP': True,
        'STORE_UID': True,
        'SERIALIZE': False,
        'EXPORT_2D': True,
        'FULL_PARAMETRIC': False,
        'ADD_DEFAULT_SITE': True,
        'ADD_DEFAULT_BUILDING': True,
        'ADD_DEFAULT_STOREY': True,
        'IFC_UNIT': 'metre',
        'SCALE_FACTOR': 0.001,
        'GET_STANDARD': False,
        'EXPORT_MODEL': 'arch',
        'SCHEMA': 'IFC2X3'
    }
    import exportIFC
    exportIFC.export([groupobj_to_export], export_ifcfile, "", preferences=exportprefs)
    return True


# ************************************************************************************************
def write_ifc_patched(ifc_infile_p, ifc_file_ifcos, outdir_newbasename_noending, patch_guids):

    print("For ifcpatch collect GUIDs, files and other data.")
    # build the ifcpatch argument string
    # print(patch_guids)
    patch_guids_str = ""
    for guid in patch_guids:
        patch_guids_str += "#{} | ".format(guid)
    patch_guids_str = patch_guids_str.rstrip(" | ")
    # neuere versionen von ifcpatch nutzen einfacheren string fuer ids
    # siehe bipatch_run in bimtworker
    # print(patch_guids_str)

    # print(ifc_infile_p)
    # print(ifc_file_ifcos)
    # print(outdir_newbasename_noending + ".ifc")

    print("Run ifcpatch command from IFCOpenShell.")
    # TODO: move to bimworker_locations in bimtester
    # ifcpatch is not in IfcOpenShell on FreeCAD, thus we need the source code
    # PC Server BIMTester, siehe auch bimworker_locations.py in BIMTester code
    ifcpatch_path_bha = "C:/0_BHA_privat/code_ifcopenshell/official/IfcOpenShell/src/ifcpatch"
    ifcpatch_path_job = "C:/BIMTester/code_ifcopenshell/IfcOpenShell/src/ifcpatch"
    if os.path.isdir(ifcpatch_path_job):
        ifcpatch_path_real = ifcpatch_path_job
    else:
        ifcpatch_path_real = ifcpatch_path_bha
    import sys
    sys.path.append(ifcpatch_path_real)
    # sys.path.append("C:/0_BHA_privat/code_ifcopenshell/IfcOpenShell/src/ifcpatch")
    try:
        import ifcpatch
    except:
        print("Could not import ifcpatch.")
        return False

    try:
        out_ifcfile = ifcpatch.execute({
            "input": ifc_infile_p,
            "file": ifc_file_ifcos, 
            "recipe": "ExtractElements",
            "arguments": [patch_guids_str],
        })
        ifcpatch.write(out_ifcfile, outdir_newbasename_noending + ".ifc")
    except Exception:
        print("Error in ifcpatch for: {}".format(ifc_infile_p))
        return False

    return True


# ************************************************************************************************
def write_patched(outpath, ifc_infile_p, ifcfile, out_filef, elements_ifc):

    print("Start to write patched ifc: {}".format(out_filef))

    out_filep = join(outpath, out_filef)

    # TODO use parameter
    # do not write if file exists
    if os.path.isfile(out_filep + ".ifc"):
        print("File exists. We do not overwrite the file.")
        return

    # test
    #    print(out_filef)
    #    if not out_filep.startswith("Alle"):
    #        return

    if len(elements_ifc) == 0:
        print("Len is 0, thus IFC {} not written.".format(out_filef))
        return

    elements_guids = [elem.GlobalId for elem in elements_ifc]
    status = write_ifc_patched(ifc_infile_p, ifcfile, out_filep, elements_guids)
    print("Endstatus: {}".format(status))

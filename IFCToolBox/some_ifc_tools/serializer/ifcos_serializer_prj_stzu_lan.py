"""
import importlib
import some_ifc_tools.serializer.ifcos_serializer_prj_stzu_lan as runstzulan

importlib.reload(runstzulan)
runstzulan.stzu_lan_serialise(create_ifc=True)

# if opened in FC already
runstzulan.stzu_lan_serialise(parsed_ifc=ifc)

# else
runstzulan.stzu_lan_serialise(create_ifc=True)


"""

"""
# copy fuer debugging, und alle imports
ifc_file = ifc
all_elements = ifc_file.by_type("IfcBuildingElement")
rem_elements = []

"""


# Vorlage !!!
# ifcos_serializer_prj_inri_arc_arc


import importlib
from os.path import join
import ifcopenshell
from FreeCAD import Vector
import Part
import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)


workpath = "C:/Users/BHA/Desktop"
infile_ifc_roh = "4180_LAN_UMG01.ifc"


# ************************************************************************************************
def stzu_lan_serialise(parsed_ifc=None, create_ifc=False):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_roh,
        outfile_name = "4180_LAN_UMG01_BHA",
        def_remelements = stzu_lan_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc,  # mit absicht siehe serialise method
        parsed_ifc = parsed_ifc
    )
    return True


# ************************************************************************************************
def stzu_lan_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    #rem_elements = get_rems_classification(ifc_file, all_elements, rem_elements)
    rem_elements = get_rems_name(ifc_file, all_elements, rem_elements)
    rem_elements = get_rems_description(ifc_file, all_elements, rem_elements)
    #rem_elements = get_rems_realtype(ifc_file, all_elements, rem_elements)
    #rem_elements = get_rems_buildingpartremschildreen(ifc_file, all_elements, rem_elements)
    #rem_elements = get_rems_getback(ifc_file, all_elements, rem_elements)

    print("\nFinished get_remelements")
    return all_elements, rem_elements


def get_rems_name(ifc_file, all_elements, rem_elements):
    print("\nNames")
    new_rem_elems = []
    new_rem_elems += sertools.get_by_name(all_elements, "Baum_3D")
    new_rem_elems += sertools.get_by_name(all_elements, "Fahrradbügel")
    new_rem_elems += sertools.get_by_name(all_elements, "post")
    new_rem_elems += sertools.get_by_name(all_elements, "Leuchten")
    new_rem_elems += sertools.get_by_name(all_elements, "Ausstattung")

    sertools.add_elems_to_remelems(rem_elements, new_rem_elems)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    return rem_elements


def get_rems_description(ifc_file, all_elements, rem_elements):
    print("\nDescription")
    new_rem_elems = []
    new_rem_elems += sertools.get_by_description(all_elements, "Ausstattung")

    sertools.add_elems_to_remelems(rem_elements, new_rem_elems)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    return rem_elements

"""
def get_rems_realtype(ifc_file, all_elements, rem_elements):
    print("\nTypes")
    rem_realtypes = []
    # True nicht exakt, sondern beinhaltet
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Geländer")
    #rem_realtypes += sertools.get_by_realtype_name(all_elements, "INZL Brandwand OG", True)

    sertools.add_elems_to_remelems(rem_elements, rem_realtypes)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    return rem_elements


def get_rems_buildingpartremschildreen(ifc_file, all_elements, rem_elements):
    print("\nChildreen of rem building part elements")
    bep_rem_child_eles = []
    # fuege zu rem_elements die kinder IfcBuildingElementParts der zu skippenden IfcBuilgingElements hinzu
    # wieder workaround da ich aktuell nicht weiss wie inverse finden (warum inverse?)
    #
    # das sind einige tausend !!!
    #
    # get relations
    all_rel_aggregates = {}  # { host:[child,...], ... }
    for rel in ifc_file.by_type("IfcRelAggregates"):
        all_rel_aggregates.setdefault(rel.RelatingObject.id(), []).extend([e.id() for e in rel.RelatedObjects])
    #
    # get building element part childreen
    bep_rem_child_ids = []
    for eleo in rem_elements:
        eleid = eleo.id()
        the_child_ids = []
        if eleid in all_rel_aggregates:
            the_child_ids = all_rel_aggregates[eleid]
            bep_rem_child_ids += the_child_ids
    bep_rem_child_ids = sorted(list(set(bep_rem_child_ids)))
    # print(len(bep_rem_child_ids))
    #
    # add new rems to rem_elements
    for id in bep_rem_child_ids:
        bep_rem_child_eles.append(ifc_file[id])
    sertools.add_elems_to_remelems(rem_elements, bep_rem_child_eles)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    return rem_elements


def get_rems_getback(ifc_file, all_elements, rem_elements):
    print("\nGet back some elements")
    get_back_elems = []
    # daecher der einstellhalle
    get_back_elems += sertools.get_by_material_name(all_elements, "Beton, Stahlbeton")
    # vertikale daemmung im erdreich, zweite hausreihe, ids stimmen nicht mehr
    #get_back_elems += [ifc_file.by_guid("21312fpUqErfrESTLoEsqr")]
    #get_back_elems += [ifc_file.by_guid("2qx0Gw_J609H73xbOuPf2v")]
    get_back_elems += [ifc_file.by_guid("05yLbw3qPfkUB2nnDQU12T")]
    get_back_elems += [ifc_file.by_guid("3P3ehkBwzHAT$cSRzlrHjI")]
    get_back_elems += [ifc_file.by_guid("1coMtgIP6FDwpFu5sLsLou")]
    get_back_elems += [ifc_file.by_guid("0G33zLVm8vATwITPbpdwyW")]
    rem_elements = list(set(rem_elements).difference(set(get_back_elems)))
    print("Elements to remove: {}".format(len(rem_elements)))
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    return rem_elements
"""
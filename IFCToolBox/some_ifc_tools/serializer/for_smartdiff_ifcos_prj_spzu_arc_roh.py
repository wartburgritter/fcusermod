"""
import importlib
import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_arc_roh as runspzuroh

importlib.reload(runspzuroh)

# ANPASSEN!!!
runspzuroh.spzu_arc_roh_ortbeton_eg_decke_serialise()
runspzuroh.spzu_arc_roh_ortbeton_ug_decke_serialise()
runspzuroh.spzu_arc_roh_ortbeton_ug_bpl_serialise()
runspzuroh.spzu_arc_roh_ortbeton_eg_waende_serialise()
runspzuroh.spzu_arc_roh_ortbeton_ug_waende_serialise()
runspzuroh.spzu_arc_roh_ortbeton_alle_platten_serialise()
runspzuroh.spzu_arc_roh_ortbeton_alle_vertikale_serialise()

"""


import importlib
from os.path import join
import ifcopenshell
from FreeCAD import Vector
import Part
import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)


std_workpath = "C:/0_BHA_privat/BIMGeomSerializer"
infile_ifc_roh = "infiles/4130_ARC_ROHM01.ifc"


# ************************************************************************************************
def spzu_arc_roh_ortbeton_geschosse_plat_vert_serialise(
    geschoss="UG",
    plat_vert="plat",
    create_ifc=False,
    workpath=std_workpath
):

    shcutobj_def = None

    if geschoss == "UG" and plat_vert == "plat":
        rem_def = spzu_arc_roh_ortbeton_ug_bpl_get_remelements

    elif geschoss == "UG" and plat_vert == "vert":
        rem_def = spzu_arc_roh_ortbeton_ug_vertikale_get_remelements

    elif geschoss == "EG" and plat_vert == "plat":
        rem_def = spzu_arc_roh_ortbeton_ug_decke_get_remelements
        shcutobj_def = spzu_arc_roh_ortbeton_ug_decke_get_shcutobj

    elif geschoss == "EG" and plat_vert == "vert":
        rem_def = spzu_arc_roh_ortbeton_eg_vertikale_get_remelements

    elif geschoss == "OG1" and plat_vert == "plat":
        rem_def = spzu_arc_roh_ortbeton_eg_decke_get_remelements

    if plat_vert == "plat":
        bauteilsorte = "Platten"
    elif plat_vert == "vert":
        bauteilsorte = "Vertikal"

    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_roh,
        outfile_name = "{}_{}bauteile_ARC_ROH".format(geschoss, bauteilsorte),
        def_remelements = rem_def,
        def_shcutobj = shcutobj_def,
        create_ifc = create_ifc
    )

    return True


def spzu_arc_roh_ortbeton_alle_platten_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_roh,
        outfile_name = "Alle_Plattenbauteile_ARC_ROH",
        def_remelements = spzu_arc_roh_ortbeton_alle_platten_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


def spzu_arc_roh_ortbeton_alle_vertikale_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_roh,
        outfile_name = "Alle_Vertikalbauteile_ARC_ROH",
        def_remelements = spzu_arc_roh_ortbeton_alle_vertikale_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


# ************************************************************************************************
def spzu_arc_roh_ortbeton_eg_decke_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Storeys")
    rem_floors = []
    rem_floors += sertools.get_by_storey_name(all_elements, "01.OG_FBOK", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_floors)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcBeam")
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcWall")
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementPart")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_arc_roh_ortbeton_ug_decke_get_shcutobj():

    # loesche bodenplatte im UG mit boxcut
    print("Shape Cutobj")
    sh_cutobj = Part.makeBox(150000, 60000, 10000)
    sh_cutobj.Placement.move(Vector(-15000, -13000, -11000))
    return sh_cutobj


def spzu_arc_roh_ortbeton_ug_decke_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Storeys")
    # im UG sind die Unterzuege, daher dieses nicht loeschen
    rem_floors = []
    rem_floors += sertools.get_by_storey_name(all_elements, "00.EG_FBOK", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_floors)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    # brauche beams wegen unterzuege decke ug
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementPart")
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementProxy")
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcCovering")
    rem_ifc_classification += ifc_file.by_type("IfcRamp")
    rem_ifc_classification += ifc_file.by_type("IfcWall")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Names")
    # daemmung unter liftdecken innerhalb decke UG
    rem_names = []
    rem_names += sertools.get_by_name(all_elements, "Dämmung")
    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Get back some elements")
    # differenzwaende liftdecken innerhalb decke UG
    get_back_elems = []
    get_back_elems += sertools.get_by_name(all_elements, "Aussenwand UG 28cm")
    rem_elements = list(set(rem_elements).difference(set(get_back_elems)))
    print("Elements to remove: {}".format(len(rem_elements)))
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_arc_roh_ortbeton_ug_bpl_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Storeys")
    rem_floors = []
    rem_floors += sertools.get_by_storey_name(all_elements, "01.UG_FBOK", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_floors)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcBeam")
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcWall")
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementPart")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Names")
    rem_names = []
    rem_names += sertools.get_by_name(all_elements, "Dämmung")
    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Default")
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Object Types")
    rem_objtypes = []
    rem_objtypes += sertools.get_by_objtype(all_elements, "Bodenplatte UG 30cm")
    sertools.add_elems_to_remelems(rem_elements, rem_objtypes)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Get back some elements")
    get_back_elems = []
    get_back_elems += [ifc_file.by_guid("144qY9JR17SQzdf8t_389T")]  # treppe
    #get_back_elems += [ifc_file.by_guid("2aaMyb4Qn0VBweeQ1kXIJt")]  # rampe
    get_back_elems += sertools.get_by_name(all_elements, "Aussenwand UG 35cm")
    rem_elements = list(set(rem_elements).difference(set(get_back_elems)))
    print("Elements to remove: {}".format(len(rem_elements)))
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_arc_roh_ortbeton_eg_vertikale_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Storeys")
    rem_floors = []
    rem_floors += sertools.get_by_storey_name(all_elements, "00.EG_FBOK", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_floors)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcBeam")
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcCovering")
    rem_ifc_classification += ifc_file.by_type("IfcRamp")
    rem_ifc_classification += ifc_file.by_type("IfcSlab")
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementPart")
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementProxy")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_arc_roh_ortbeton_ug_vertikale_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Storeys")
    rem_floors = []
    rem_floors += sertools.get_by_storey_name(all_elements, "01.UG_FBOK", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_floors)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcBeam")
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcCovering")
    rem_ifc_classification += ifc_file.by_type("IfcRamp")
    rem_ifc_classification += ifc_file.by_type("IfcSlab")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Object Types")
    rem_objtypes = []
    rem_objtypes += sertools.get_by_objtype(all_elements, "Aussenwand UG 35cm")
    sertools.add_elems_to_remelems(rem_elements, rem_objtypes)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_arc_roh_ortbeton_ug_platvert_get_shcutobj():

    print("Shape Cutobj")
    sh_cutobj = Part.makeBox(150000,60000,10000)
    sh_cutobj.Placement.move(Vector(-15000,-13000,-2000))



# ************************************************************************************************
def spzu_arc_roh_ortbeton_alle_platten_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcCovering")
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementProxy")
    rem_ifc_classification += ifc_file.by_type("IfcWall")
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementPart")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Names")
    rem_names = []
    rem_names += sertools.get_by_name(all_elements, "Dämmung")
    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Default")
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Get back some elements")
    get_back_elems = []
    get_back_elems += [ifc_file.by_guid("144qY9JR17SQzdf8t_389T")]  # treppe
    rem_elements = list(set(rem_elements).difference(set(get_back_elems)))
    print("Elements to remove: {}".format(len(rem_elements)))
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_arc_roh_ortbeton_alle_vertikale_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcBeam")
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementProxy")
    rem_ifc_classification += ifc_file.by_type("IfcRamp")
    rem_ifc_classification += ifc_file.by_type("IfcSlab")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    # waende sind mit isMaterialLayer (Nicht complex attributes)
    # daher nach Allreal Materialattribut
    print("Properties")
    rem_props = []
    rem_props += sertools.get_by_property_value(all_elements, "Allreal_Material", "Material", "Beton", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_props)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements

"""
import importlib
import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_arc_roh as runspzuroh

importlib.reload(runspzuroh)

runspzuroh.spzu_arc_roh_ortbeton_ug_platvert_serialise()
runspzuroh.spzu_arc_roh_elemente_vorgefertigt_serialise()
runspzuroh.spzu_arc_roh_ortbeton_alles_fuer_rohvergleich_serialise()

"""


import importlib
from os.path import join
import ifcopenshell
from FreeCAD import Vector
import Part
import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)


std_workpath = "C:/0_BHA_privat/BIMGeomSerializer"
infile_ifc_roh = "infiles/4130_ARC_ROHM01.ifc"


# ************************************************************************************************
def spzu_arc_roh_ortbeton_ug_platvert_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_roh,
        outfile_name = "UG_Platten_Vertikalbauteile_ARC_ROH_cut",
        def_remelements = spzu_arc_roh_ortbeton_ug_platvert_get_remelements,
        def_shcutobj = spzu_arc_roh_ortbeton_ug_platvert_get_shcutobj,
        create_ifc = create_ifc
    )
    return True


def spzu_arc_roh_ortbeton_alles_fuer_rohvergleich_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_roh,
        outfile_name = "Alle_Ortbetonbauteile_fuer_trwvergleich_ARC_ROH",
        def_remelements = spzu_arc_roh_ortbeton_fuer_rohvergleich_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


def spzu_arc_roh_elemente_vorgefertigt_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_roh,
        outfile_name = "4130_ARC_ROHM01_Vorgefertigte_Elemente",
        def_remelements = spzu_arc_roh_elemente_vorgefertigt_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


# ************************************************************************************************
def spzu_arc_roh_ortbeton_ug_platvert_get_shcutobj():

    print("Shape Cutobj")
    sh_cutobj = Part.makeBox(150000,60000,10000)
    sh_cutobj.Placement.move(Vector(-15000,-13000,-2000))


def spzu_arc_roh_ortbeton_ug_platvert_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Storeys")
    rem_floors = []
    rem_floors += sertools.get_by_storey_name(all_elements, "01.UG_FBOK", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_floors)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcBeam")
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Names")
    rem_names = []
    rem_names += sertools.get_by_name(all_elements, "Dämmung")
    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    
    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Default")
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Get back some elements")
    get_back_elems = []
    get_back_elems += [ifc_file.by_guid("144qY9JR17SQzdf8t_389T")]  # treppe
    # get_back_elems += [ifc_file.by_guid("2aaMyb4Qn0VBweeQ1kXIJt")]  # rampe
    rem_elements = list(set(rem_elements).difference(set(get_back_elems)))
    print("Elements to remove: {}".format(len(rem_elements)))
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements



# ************************************************************************************************
def spzu_arc_roh_ortbeton_fuer_rohvergleich_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    # TODO siehe vertikalbauteile mit inverse allreal materialattribut

    # nur die Elementstuetzen mit material Beton sind IfcColumn :-)
    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Properties")
    rem_props = []
    rem_props += sertools.get_by_property_value(all_elements, "Allreal_Material", "Material", "Default")
    rem_props += sertools.get_by_property_value(all_elements, "Allreal_Material", "Material", "Isokorb")
    rem_props += sertools.get_by_property_value(all_elements, "Allreal_Material", "Material", "Mauerwerk")
    sertools.add_elems_to_remelems(rem_elements, rem_props)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Names")
    rem_names = []
    rem_names += sertools.get_by_name(all_elements, "Dämmung")
    rem_names += sertools.get_by_name(all_elements, "Projektnullpunkt")
    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Object Types")
    rem_objtypes = []
    rem_objtypes += sertools.get_by_objtype(all_elements, "Unstyled Wall")
    sertools.add_elems_to_remelems(rem_elements, rem_objtypes)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_arc_roh_elemente_vorgefertigt_get_remelements(ifc_file):

    # generiere Fertigteilelemente ifc aus ROH mit elemente
    # heisst loesche alles was NICHT Namen xyz hat

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Names")
    rem_names = []
    for elem in all_elements:
        if elem.Name != "Beton Fertigteil":
            rem_names.append(elem)

    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements

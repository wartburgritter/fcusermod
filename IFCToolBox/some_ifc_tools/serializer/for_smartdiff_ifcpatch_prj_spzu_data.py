"""

import importlib

import some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_spzu_data as prjmodule
importlib.reload(prjmodule)

import some_ifc_tools.serializer.bimgeomsmartdiffprojectdataclass as classprjobj
importlib.reload(classprjobj)
prjobj = classprjobj.BimGeomSmartDiffProjectData(prjmodule)


import some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_0run as runifcpatchforsmartdiff
importlib.reload(runifcpatchforsmartdiff)


runifcpatchforsmartdiff.run_all(prjobj, "01_UG", "plat", "trw")
runifcpatchforsmartdiff.run_all(prjobj, "01_UG", "vert", "")
runifcpatchforsmartdiff.run_all(prjobj, "01_UG", "", "roh")
runifcpatchforsmartdiff.run_all(prjobj, "01_UG", "", "trw")
runifcpatchforsmartdiff.run_all(prjobj, "01_UG", "vert", "trw")
runifcpatchforsmartdiff.run_all(prjobj, "11_EG", "", "trw")
runifcpatchforsmartdiff.run_all(prjobj, "11_EG", "plat", "roh")
runifcpatchforsmartdiff.run_all(prjobj, "11_EG", "plat", "trw")

runifcpatchforsmartdiff.run_all(prjobj, None, "", "", False) # run all but do not write a IFC, for debug

runifcpatchforsmartdiff.run_all(prjobj, None, "", "trw", True)  # run all TRW and write IFC, ca 15 min


runifcpatchforsmartdiff.run_all(prjobj)

# C:\0_BHA_privat\BIMGeomAllSerializerSmartDataDiff
# files in dem verzeichnis

"""


# TODO:
# Moeglichkeit der unterschiedlichen Geschossnamen in TRW und ROH


"""
# code zum testen der klasse BimGeomSmartDiffProjectData

import importlib

import some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_spzu_data as prjobj
importlib.reload(prjobj)

import some_ifc_tools.serializer.bimgeomsmartdiffprojectdataclass as classprjobj
importlib.reload(classprjobj)

prjobj = classprjobj.BimGeomSmartDiffProjectData(prjobj)

import json
print(json.dumps(prjobj.SpatialData, indent=4))  # :-)
prjobj.IfcRoh.by_type("IfcProject")[0].Name
prjobj.IfcTrw.by_type("IfcProject")[0].Name
prjobj.GetDefsModuleRoh.__dir__()
prjobj.GetDefsModuleTrw.__dir__()

"""


getdefsmodule_str_roh = "some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_spzu_defs_arc_roh"
getdefsmodule_str_trw = "some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_spzu_defs_ing_trw"
# runspzuroh = importlib.import_module(getdefsmodule_path)


# data for class, wenn es laeuft obiges loeschen
infile_roh = "4130_ARC_ROHM01.ifc"
infile_trw = "4130_ING_N_TRW.ifc"


spatial_base_data = {
    "01_UG": {
        "name": "01.UG_FBOK",
        "darueber": "11_EG",
        "darunter": None,
    },
    "11_EG": {
        "name": "00.EG_FBOK",
        "darueber": "21_OG1",
        "darunter": "01_UG",
    },
    "21_OG1": {
        "name": "01.OG_FBOK",
        "darueber": "22_OG2",
        "darunter": "11_EG",
    },
    "22_OG2": {
        "name": "02.OG_FBOK",
        "darueber": "23_OG3",
        "darunter": "21_OG1",
    },
    "23_OG3": {
        "name": "03.OG_FBOK",
        "darueber": "24_OG4",
        "darunter": "22_OG2",
    },
    "24_OG4": {
        "name": "04.OG_FBOK",
        "darueber": "25_OG5",
        "darunter": "23_OG3",
    },
    "25_OG5": {
        "name": "05.OG_FBOK",
        "darueber": "26_AG6",
        "darunter": "24_OG4",
    },
    "26_AG6": {
        "name": "06.AG_FBOK",
        "darueber": "27_DG7",
        "darunter": "25_OG5",
    },
    "27_DG7": {
        "name": "07.DA_FBOK",
        "darueber": None,
        "darunter": "26_AG6",
    },
    None: {
        "name": None,
        "darueber": None,
        "darunter": None,
    },
}
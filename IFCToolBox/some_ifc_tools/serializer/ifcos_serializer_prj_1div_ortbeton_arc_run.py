"""
import importlib
import some_ifc_tools.serializer.ifcos_serializer_prj_1div_ortbeton_arc_run as runser

importlib.reload(runser)

runser.rohbau_serialise(True)
runser.ortbeton_all_serialise(True)
runser.ortbeton_slabs_serialise(True)
runser.ortbeton_walls_serialise(True)
runser.ortbeton_rest_serialise(True)

"""

# kopie from module ifcos_serializer_prj_spzu_ing_trw

import importlib
from os.path import join
import ifcopenshell
from FreeCAD import Vector
import Part
import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)


std_workpath = "C:/0_BHA_privat/BIMGeomSerializer"
infile_ifc = "infiles/4115_ARC_ROHM01_20230608.ifc"


# ************************************************************************************************
def rohbau_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc,
        outfile_name = "4115_ARC_ROHM01_20230608_ROH",
        def_remelements = rohbau_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


def ortbeton_all_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc,
        outfile_name = "4115_ARC_ROHM01_20230608_ortbeton_all",
        def_remelements = ortbeton_all_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


def ortbeton_slabs_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc,
        outfile_name = "4115_ARC_ROHM01_20230608_ortbeton_slabs",
        def_remelements = ortbeton_slabs_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


def ortbeton_walls_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc,
        outfile_name = "4115_ARC_ROHM01_20230608_ortbeton_walls",
        def_remelements = ortbeton_walls_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


def ortbeton_rest_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc,
        outfile_name = "4115_ARC_ROHM01_20230608_ortbeton_rest",
        def_remelements = ortbeton_rest_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


# ************************************************************************************************
def rohbau_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Materials")
    # aufpassen buildingelemntparts werden ignoriert
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Allgemein, Unterkonstruktion")
    rem_mats += sertools.get_by_material_name(all_elements, "Beton, unbewehrt")
    rem_mats += sertools.get_by_material_name(all_elements, "Dämmung, Perimeter horizontal")
    rem_mats += sertools.get_by_material_name(all_elements, "Metall, Stahl")
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcDoor")
    rem_ifc_classification += ifc_file.by_type("IfcWindow")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    # fertigteiltreppen, einzelteile, die sind building element part, gehen nicht ueber material oder klasse
    print("Names")
    rem_names = []
    rem_names += sertools.get_by_name(all_elements, "498_Andeer Granit")
    rem_names += sertools.get_by_name(all_elements, "Beton, Stahlbeton")  # zum glueck nur die haben den namen
    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def ortbeton_all_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Beton, Stahlbeton", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementProxy")
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcDoor")
    rem_ifc_classification += ifc_file.by_type("IfcWindow")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    # fertigteiltreppen, einzelteile
    print("Names")
    rem_names = []
    rem_names += sertools.get_by_name(all_elements, "498_Andeer Granit")
    rem_names += sertools.get_by_name(all_elements, "Beton, Stahlbeton")
    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def ortbeton_slabs_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Beton, Stahlbeton", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementProxy")
    rem_ifc_classification += ifc_file.by_type("IfcBeam")
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcDoor")
    rem_ifc_classification += ifc_file.by_type("IfcWall")
    rem_ifc_classification += ifc_file.by_type("IfcWindow")
    rem_ifc_classification += ifc_file.by_type("IfcStair")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    # fertigteiltreppen, einzelteile
    print("Names")
    rem_names = []
    rem_names += sertools.get_by_name(all_elements, "498_Andeer Granit")
    rem_names += sertools.get_by_name(all_elements, "Beton, Stahlbeton")
    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def ortbeton_walls_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Beton, Stahlbeton", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementProxy")
    rem_ifc_classification += ifc_file.by_type("IfcBeam")
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcDoor")
    rem_ifc_classification += ifc_file.by_type("IfcSlab")
    rem_ifc_classification += ifc_file.by_type("IfcStair")
    rem_ifc_classification += ifc_file.by_type("IfcWindow")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    # fertigteiltreppen, einzelteile
    print("Names")
    rem_names = []
    rem_names += sertools.get_by_name(all_elements, "498_Andeer Granit")
    rem_names += sertools.get_by_name(all_elements, "Beton, Stahlbeton")
    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def ortbeton_rest_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Beton, Stahlbeton", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcBuildingElementProxy")
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcSlab")
    rem_ifc_classification += ifc_file.by_type("IfcDoor")
    rem_ifc_classification += ifc_file.by_type("IfcWall")
    rem_ifc_classification += ifc_file.by_type("IfcWindow")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    # fertigteiltreppen, einzelteile
    print("Names")
    rem_names = []
    rem_names += sertools.get_by_name(all_elements, "498_Andeer Granit")
    rem_names += sertools.get_by_name(all_elements, "Beton, Stahlbeton")
    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements

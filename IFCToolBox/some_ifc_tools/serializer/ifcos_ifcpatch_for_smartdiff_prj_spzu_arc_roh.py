"""
import importlib
import some_ifc_tools.serializer.ifcos_ifcpatch_for_smartdiff_prj_spzu_arc_roh as patchforsmartdiffroh

importlib.reload(patchforsmartdiffroh)
patchforsmartdiffroh.run_patch_spzu_arc_roh()

"""


import os
import importlib
from os.path import join

import ifcopenshell

import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)

import some_ifc_tools.serializer.ifcos_ifcpatch_for_smartdiff_prj_spzu_0run as runspzupatch
importlib.reload(runspzupatch)


# ************************************************************************************************
def run_patch_spzu_arc_roh(
    workpath,
    geschosse,
    plat_vert,
    model_type,
    write_ifc
):

    infile_ifc = "infiles/4130_ARC_ROHM01.ifc"
    
    print("\n" + model_type)
    ifc_file_p = join(workpath, infile_ifc)
    # print(ifc_file_p)
    ifcfile = ifcopenshell.open(ifc_file_p)

    # get and write generic elems
    elems_data = sertools.run_patch_generic_elems(
        get_concrete_elems,
        get_concrete_ignore_elems,
        get_plattenbauteile_concrete,
        workpath,
        ifc_file_p,
        ifcfile,
        geschosse,
        plat_vert,
        model_type,
        write_ifc
    )
    all_elements = elems_data[0]
    ortbet_elems = elems_data[1]
    no_ob_elems = elems_data[2]
    ob_ignore_elems = elems_data[3]
    pla_ob_elems = elems_data[4]
    ver_ob_elems = elems_data[5]

    # get and write ortbeton geschosse elems
    elems_floors_ob = sertools.run_patch_geschoss_elems(
        get_geschoss_ob_bauteile,
        elems_data,
        workpath,
        ifc_file_p,
        ifcfile,
        geschosse,
        plat_vert,
        model_type,
        write_ifc
    )

    return True


# ************************************************************************************************
def get_concrete_elems(elements):

    print(len(elements))
    ortbet_elems = sertools.get_by_property_value(elements, "Allreal_Material", "Material", "Beton")
    print(len(ortbet_elems))

    # add building element parts (some concrete walls are ... )
    ortbet_elems += sertools.get_by_ifctype(elements, "IfcBuildingElementPart")

    # remove columns
    ortbet_elems = list(set(ortbet_elems) - set(sertools.get_by_ifctype(ortbet_elems, "IfcColumn")))

    return ortbet_elems


# ************************************************************************************************
def get_concrete_ignore_elems(elements):

    ortbet_ignore_elems = []

    return ortbet_ignore_elems


# ************************************************************************************************
def get_plattenbauteile_concrete(ortbet_elems):

    pla_ob_elems = []

    # get all slabs out of the ortbeton bauteile
    pla_ob_elems += sertools.get_by_ifctype(ortbet_elems, "IfcSlab")
    pla_ob_elems += sertools.get_by_ifctype(ortbet_elems, "IfcRamp")

    # add spezielle ortbeton typen und klassen gemaess attributen
    
    # liftschachtwaende unter BPL UG
    # nur hier, daher keine methode
    pla_ob_elems += sertools.get_by_objtype(ortbet_elems, "Aussenwand UG 35cm")

    # massive unterzuege in Decke UG
    pla_ob_elems += get_massive_unterzuege_ug(ortbet_elems)
    
    # miniliftschachtwaende der townhaeuser grad unter Decke UG
    pla_ob_elems += get_miniliftwaend_town_ug(ortbet_elems)

    # delete duplicates
    pla_ob_elems = list(set(pla_ob_elems))

    return pla_ob_elems


# ************************************************************************************************
def get_miniliftwaend_town_ug(ortbet_elems):
    # miniliftschachtwaende der townhaeuser grad unter Decke UG
    # sind einzig im gesamten modell
    elems = sertools.get_by_objtype(ortbet_elems, "Aussenwand UG 28cm")

    return elems


# ************************************************************************************************
def get_massive_unterzuege_ug(ortbet_elems):
    # massive unterzuege in Decke UG
    # sind einzig im gesamten modell
    elems = []
    elems += sertools.get_by_property_value(ortbet_elems, "Allreal", "Typenname", "C4_UZ_Beton_0.8_0.31")
    elems += sertools.get_by_property_value(ortbet_elems, "Allreal", "Typenname", "C4_UZ_Beton_0.8_0.35")
    elems += sertools.get_by_property_value(ortbet_elems, "Allreal", "Typenname", "C4_UZ_Beton_0.8_0.61")
    elems += sertools.get_by_property_value(ortbet_elems, "Allreal", "Typenname", "C4_UZ_Beton_0.75_0.61")
    elems += sertools.get_by_property_value(ortbet_elems, "Allreal", "Typenname", "C4_UZ_Beton_0.75_0.7")
    return elems


# ************************************************************************************************
def get_geschoss_ob_bauteile(elems_data, geschoss, gesch_name):

    all_elements = elems_data[0]
    ortbet_elems = elems_data[1]
    no_ob_elems = elems_data[2]
    ob_ignore_elems = elems_data[3]
    pla_ob_elems = elems_data[4]
    ver_ob_elems = elems_data[5]


    pla_gesch_ob_elems = sertools.get_by_storey_name(pla_ob_elems, gesch_name)
    ver_gesch_ob_elems = sertools.get_by_storey_name(ver_ob_elems, gesch_name)

    # Ausnahmen: TODO evtl. separte method
    tmp_minibpl_lift_town = sertools.get_by_objtype(ortbet_elems, "Bodenplatte UG 30cm")  # nur hier und einzig im modell
    tmp_miniwaende_lift_town = get_miniliftwaend_town_ug(ortbet_elems)
    tmp_massive_unterzueg_ug = get_massive_unterzuege_ug(ortbet_elems)

    # aufpassen was vertikal und was platten,  nicht hier hin- und her, das ganz vorher ...
    if geschoss == "UG":

        # handle IfcBuildingElementPart, they do not have allreal attribut geschoss
        ver_gesch_ob_elems += sertools.get_by_ifctype(all_elements, "IfcBuildingElementPart")
        # eins ist im EG
        ver_gesch_ob_elems = list(set(ver_gesch_ob_elems) - set(sertools.get_by_guid(all_elements, "31QHSrBTbBxw9E5t1UG90d")))

        # miniwaende Liftschaechte townhaeuser und massive Unterzuege aus UG entfernen
        pla_gesch_ob_elems = list(set(pla_gesch_ob_elems) - set(tmp_minibpl_lift_town))
        pla_gesch_ob_elems = list(set(pla_gesch_ob_elems) - set(tmp_miniwaende_lift_town))
        pla_gesch_ob_elems = list(set(pla_gesch_ob_elems) - set(tmp_massive_unterzueg_ug))

    elif geschoss == "EG":

        # IfcBuildingElementPart im EG, they do not have allreal attribut geschoss
        ver_gesch_ob_elems += sertools.get_by_guid(all_elements, "31QHSrBTbBxw9E5t1UG90d")

        # miniwaende Liftschaechte townhaeuser und massive unterzuege UG in EG hinzufuegen
        pla_gesch_ob_elems += tmp_minibpl_lift_town
        pla_gesch_ob_elems += tmp_miniwaende_lift_town
        pla_gesch_ob_elems += tmp_massive_unterzueg_ug

    return pla_gesch_ob_elems, ver_gesch_ob_elems

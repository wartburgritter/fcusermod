"""
siehe for_smartdiff_ifcpatch_prj_spzu_data fuer code to copy fuer SPZU

module ist komplett projektunabhaengig, alle prj parameter werden uebergeben

"""


import importlib
import os
from os.path import join

import ifcopenshell

import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)


# ************************************************************************************************
# einfach nie geschoss nach geschoss laufen lassen
# folgendes ... geschoss = ["01_UG", "11_EG"] dann keine dopplungen
def run_all(
    prjdata,
    geschosse=None,
    plat_vert="",
    model_type="",
    write_ifc=True,
    outpath=None,  # to set different from inpath, see prjdata for inpath
):

    inpath = prjdata.InPath
    if outpath is None:
        outpath = inpath

    # reload def modules
    prjdata.reload_def_modules()

    spatial_data = prjdata.SpatialData
    ifcfile_roh = prjdata.IfcRoh
    ifcfile_trw = prjdata.IfcTrw
    infile_ifc_name_roh = join(prjdata.InSubDirectory , prjdata.InfileNameRoh)
    infile_ifc_name_trw = join(prjdata.InSubDirectory , prjdata.InfileNameTrw)
    getdef_roh = prjdata.GetDefsModuleRoh
    getdef_trw = prjdata.GetDefsModuleTrw

    # print("roh")
    # print(ifcfile_roh.by_type("IfcProject")[0].Name)
    # print(ifcfile_roh.by_type("IfcSite")[0].Name)
    # print(ifcfile_roh.by_type("IfcBuilding")[0].Name)
    # print("trw")
    # print(ifcfile_trw.by_type("IfcProject")[0].Name)
    # print(ifcfile_trw.by_type("IfcSite")[0].Name)
    # print(ifcfile_trw.by_type("IfcBuilding")[0].Name)

    if geschosse is None:
        geschosse = [geschoss for geschoss in spatial_data.keys() if geschoss is not None]
    elif isinstance(geschosse, str):
        geschosse = [geschosse]
    print(geschosse)

    print(model_type)
    if model_type == "" or model_type == "roh":
        run_patch_a_model(
            inpath,
            outpath,
            geschosse,
            plat_vert,
            "ARC_ROH",
            write_ifc,
            spatial_data,
            infile_ifc_name_roh,
            getdef_roh,
            ifcfile_roh,
        )

    if model_type == "" or model_type == "trw":
        run_patch_a_model(
            inpath,
            outpath,
            geschosse,
            plat_vert,
            "ING_TRW",
            write_ifc,
            spatial_data,
            infile_ifc_name_trw,
            getdef_trw,
            ifcfile_trw,
        )

    return True


# ************************************************************************************************
def run_patch_a_model(
    inpath,
    outpath,
    geschosse,
    plat_vert,
    model_type,
    write_ifc,
    spatial_data,
    infile_ifc_name,
    getdef,
    ifcfile,
):

    print(ifcfile.by_type("IfcProject")[0].Name)
    print(ifcfile.by_type("IfcSite")[0].Name)
    print(ifcfile.by_type("IfcBuilding")[0].Name)

    print("\n" + model_type)
    ifc_infile_p = join(inpath, infile_ifc_name)
    # print(ifc_infile_p)

    # get and write generic elems
    elems_data = sertools.run_patch_generic_elems(
        getdef.get_concrete_elems,
        getdef.get_concrete_ignore_elems,
        getdef.get_plattenbauteile_concrete,
        outpath,
        ifc_infile_p,
        ifcfile,
        geschosse,
        plat_vert,
        model_type,
        write_ifc
    )
    # all_elements = elems_data[0]
    # ortbet_elems = elems_data[1]
    # no_ob_elems = elems_data[2]
    # ob_ignore_elems = elems_data[3]
    # pla_ob_elems = elems_data[4]
    # ver_ob_elems = elems_data[5]

    # get and write ortbeton geschosse elems
    elems_floors_ob = sertools.run_patch_geschoss_elems(
        getdef.get_geschoss_ob_bauteile,
        elems_data,
        outpath,
        ifc_infile_p,
        ifcfile,
        geschosse,
        plat_vert,
        model_type,
        write_ifc,
        spatial_data,
    )

    # hack fuer BIMTester fuer elementstuetzen
    if model_type == "ING_TRW":
        elems_floors_patch = sertools.run_patch_geschoss_std_elems(
            getdef.get_modell_elementstuetzen,
            getdef.get_geschoss_elementstuetzen,
            elems_data,
            outpath,
            ifc_infile_p,
            ifcfile,
            geschosse,
            write_ifc,
            spatial_data,
        )
    
    return True

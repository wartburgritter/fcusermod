"""
import importlib
import some_ifc_tools.serializer.ifcos_serializer_prj_0div_ortbeton_run as runser

importlib.reload(runser)

runser.trw1_ortbeton_serialise(True)
runser.trw2_ortbeton_serialise(True)

"""

# kopie from module ifcos_serializer_prj_spzu_ing_trw

import importlib
from os.path import join
import ifcopenshell
from FreeCAD import Vector
import Part
import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)


std_workpath = "C:/0_BHA_privat/BIMGeomSerializer"
infile_ifc_trw1 = "infiles/VOZU_ING_N_TRW_fbo_20230606_allplan.ifc"
infile_ifc_trw2 = "infiles/VOZU_ING_N_TRW_ORG_20230606_bimtester.ifc"


# ************************************************************************************************
def trw1_ortbeton_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_trw1,
        outfile_name = "VOZU_ING_N_TRW_fbo_20230606_allplan_ortbeton",
        def_remelements = trw1_ortbeton_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


def trw2_ortbeton_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_trw2,
        outfile_name = "VOZU_ING_N_TRW_ORG_20230606_bimtester_ortbeton",
        def_remelements = trw2_ortbeton_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


# ************************************************************************************************
def trw1_ortbeton_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Ortbeton", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def trw2_ortbeton_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Ortbeton", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("GUID")
    rem_guid = []
    rem_guid += [ifc_file.by_id("2_wUN8Gt9068RbMpGSpWBg")]
    rem_guid += [ifc_file.by_id("1aejmxKEvDxRhgOBYa_Tek")]
    rem_guid += [ifc_file.by_id("0mMS_Jbob6DRYsbAnzpDq2")]
    rem_guid += [ifc_file.by_id("1RYrOeRyX3Hufv_H6qsXs_")]
    rem_guid += [ifc_file.by_id("35cAKcgTrAtexKseZC2ZZp")]
    rem_guid += [ifc_file.by_id("2d2aLIcZX0UwIVvJnYS9v5")]
    rem_guid += [ifc_file.by_id("22NEXN_mP4Efw_n1GhNf9d")]
    rem_guid += [ifc_file.by_id("0Pr9iz2lHCkQeTc7Qq2Sy2")]
    rem_guid += [ifc_file.by_id("341XyreCHBOg_Jfmxe8Y80")]
    rem_guid += [ifc_file.by_id("3u4P$Tx5D5EhriQHGbyqhT")]
    rem_guid += [ifc_file.by_id("1k9Qnpoo97URD7BVVv_xcj")]
    rem_guid += [ifc_file.by_id("26208r4EHDwwNXi7QdWINq")]
    sertools.add_elems_to_remelems(rem_elements, rem_guid)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


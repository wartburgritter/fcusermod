"""

import importlib

import some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_rezu_data as prjmodule
importlib.reload(prjmodule)

import some_ifc_tools.serializer.bimgeomsmartdiffprojectdataclass as classprjdata
importlib.reload(classprjdata)
prjobj = classprjdata.BimGeomSmartDiffProjectData(prjmodule)


import some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_0run as runifcpatchforsmartdiff
importlib.reload(runifcpatchforsmartdiff)


runifcpatchforsmartdiff.run_all(prjobj, "01_UG2", "plat", "trw")
runifcpatchforsmartdiff.run_all(prjobj, "01_UG2", "vert", "")
runifcpatchforsmartdiff.run_all(prjobj, "01_UG2", "", "roh")
runifcpatchforsmartdiff.run_all(prjobj, "01_UG2", "", "trw")
runifcpatchforsmartdiff.run_all(prjobj, "01_UG2", "vert", "trw")
runifcpatchforsmartdiff.run_all(prjobj, "11_EG", "", "trw")
runifcpatchforsmartdiff.run_all(prjobj, "11_EG", "plat", "roh")
runifcpatchforsmartdiff.run_all(prjobj, "11_EG", "plat", "trw")

runifcpatchforsmartdiff.run_all(prjobj, None, "", "", False) # run all but do not write a IFC, for debug

runifcpatchforsmartdiff.run_all(prjobj)


# temp
runifcpatchforsmartdiff.run_all(prjobj, "01_UG2", "", "roh")

"""


# TODO:
# Moeglichkeit der unterschiedlichen Geschossnamen in TRW und ROH


"""
# code zum testen der klasse BimGeomSmartDiffProjectData

import importlib

import some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_rezu_data as prjmodule
importlib.reload(prjmodule)

import some_ifc_tools.serializer.bimgeomsmartdiffprojectdataclass as classprjdata
importlib.reload(classprjdata)

prjobj = classprjdata.BimGeomSmartDiffProjectData(prjobj)

import json
print(json.dumps(prjobj.SpatialData, indent=4))  # :-)
prjobj.IfcRoh.by_type("IfcProject")[0].Name
prjobj.IfcTrw.by_type("IfcProject")[0].Name
prjobj.GetDefsModuleRoh.__dir__()
prjobj.GetDefsModuleTrw.__dir__()

"""

getdefsmodule_str_roh = "some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_rezu_defs_arc_roh"
getdefsmodule_str_trw = "some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_rezu_defs_ing_trw"


# data for class, wenn es laeuft obiges loeschen
infile_roh = "8830_ARC_ROH.ifc"
infile_trw = "8830_ING_N_TRW.ifc"

spatial_base_data = {
    "01_UG2": {
        "name": "02.UG_FBOK",
        "darueber": "02_UG1",
        "darunter": None,
    },
    "02_UG1": {
        "name": "01.UG_FBOK",
        "darueber": "11_EG",
        "darunter": "01_UG2",
    },
    "11_EG": {
        "name": "00.EG_FBOK",
        "darueber": "21_OG1",
        "darunter": "02_UG1",
    },
    "21_OG1": {
        "name": "01.OG_FBOK",
        "darueber": "22_OG2",
        "darunter": "11_EG",
    },
    "22_OG2": {
        "name": "02.OG_FBOK",
        "darueber": "23_AG3",
        "darunter": "21_OG1",
    },
    "23_AG3": {
        "name": "03.AG_FBOK",
        "darueber": "24_DA4",
        "darunter": "22_OG2",
    },
    "24_DA4": {
        "name": "04.DA_FBOK",
        "darueber": None,
        "darunter": "23_AG3",
    },
    None: {
        "name": None,
        "darueber": None,
        "darunter": None,
    },
}
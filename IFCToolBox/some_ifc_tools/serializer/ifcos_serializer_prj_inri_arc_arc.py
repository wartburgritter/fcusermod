"""
import importlib
import some_ifc_tools.serializer.ifcos_serializer_prj_inri_arc_arc as runinriarc

importlib.reload(runinriarc)

# if opened in FC already
runinriarc.inri_arc_arc_getroh_serialise(parsed_ifc=ifc)

# else
runinriarc.inri_arc_arc_getroh_serialise()


"""

"""
# copy fuer debugging, und alle imports
ifc_file = ifc
all_elements = ifc_file.by_type("IfcBuildingElement")
rem_elements = []

"""

import importlib
from os.path import join
import ifcopenshell
from FreeCAD import Vector
import Part
import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)


workpath = "C:/Users/BHA/Desktop"
infile_ifc_roh = "55226_ARC_GM01.ifc"


# ************************************************************************************************
def inri_arc_arc_getroh_serialise(parsed_ifc=None, create_ifc=False):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_roh,
        outfile_name = "INRI_ARC_ROH",
        def_remelements = inri_arc_arc_getroh_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc,  # mit absicht siehe serialise method
        parsed_ifc = parsed_ifc
    )
    return True


# ************************************************************************************************
def inri_arc_arc_getroh_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    rem_elements = get_rems_classification(ifc_file, all_elements, rem_elements)
    rem_elements = get_rems_name(ifc_file, all_elements, rem_elements)
    rem_elements = get_rems_realtype(ifc_file, all_elements, rem_elements)
    rem_elements = get_rems_buildingpartremschildreen(ifc_file, all_elements, rem_elements)
    rem_elements = get_rems_getback(ifc_file, all_elements, rem_elements)

    print("\nFinished get_remelements")
    return all_elements, rem_elements


def get_rems_classification(ifc_file, all_elements, rem_elements):
    print("\nIFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcCovering")
    rem_ifc_classification += ifc_file.by_type("IfcDoor")
    rem_ifc_classification += ifc_file.by_type("IfcRoof")
    rem_ifc_classification += ifc_file.by_type("IfcRailing")
    rem_ifc_classification += ifc_file.by_type("IfcStair")
    rem_ifc_classification += ifc_file.by_type("IfcWindow")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    return rem_elements


# aufpassen manche Betonbauteile haben namen mit holz und gipskarton ...
# am besten passt noch der typ, oder das material, wenn es vergeben ist
def get_rems_name(ifc_file, all_elements, rem_elements):
    print("\nNames")
    rem_names = []
    rem_names += sertools.get_by_name(all_elements, "Gips")
    # rem_names += sertools.get_by_name(all_elements, "C2_IW_Gipskarton/XXX_0.15")
    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    return rem_elements
    # die gipsplatten sind teil eines buildingelementpart
    # die ganze wand ist im SmartView dabei, die gipswand aber nicht explizit geloescht
    # test am ende des SmartView manuell zeile: summary name ist Gips entfernen


def get_rems_realtype(ifc_file, all_elements, rem_elements):
    print("\nTypes")
    rem_realtypes = []
    # True nicht exakt, sondern beinhaltet
    # betonlichschaechte gehoeren nicht ins TRW, daher raus
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Beton, Stahlbeton 100")
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "INZL Brandwand OG", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Zwischendecke Technik", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Dämmung, Perimeter vertikal", True)
    #
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "enster", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "tür", True)
    #
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Bekleidung", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Blockfutter", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Blockrahmen", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Brettsperrholz", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Brettschicht", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Bodenbelag", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Fliesen", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Hohlkastendecke", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Holz", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Lehm", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Leichtbau", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Metall", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Parkett", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Terracotta", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Verkleidung", True)
    rem_realtypes += sertools.get_by_realtype_name(all_elements, "Zwischenpodest", True)
    sertools.add_elems_to_remelems(rem_elements, rem_realtypes)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    return rem_elements


def get_rems_buildingpartremschildreen(ifc_file, all_elements, rem_elements):
    print("\nChildreen of rem building part elements")
    bep_rem_child_eles = []
    # fuege zu rem_elements die kinder IfcBuildingElementParts der zu skippenden IfcBuilgingElements hinzu
    # wieder workaround da ich aktuell nicht weiss wie inverse finden (warum inverse?)
    #
    # das sind einige tausend !!!
    #
    # get relations
    all_rel_aggregates = {}  # { host:[child,...], ... }
    for rel in ifc_file.by_type("IfcRelAggregates"):
        all_rel_aggregates.setdefault(rel.RelatingObject.id(), []).extend([e.id() for e in rel.RelatedObjects])
    #
    # get building element part childreen
    bep_rem_child_ids = []
    for eleo in rem_elements:
        eleid = eleo.id()
        the_child_ids = []
        if eleid in all_rel_aggregates:
            the_child_ids = all_rel_aggregates[eleid]
            bep_rem_child_ids += the_child_ids
    bep_rem_child_ids = sorted(list(set(bep_rem_child_ids)))
    # print(len(bep_rem_child_ids))
    #
    # add new rems to rem_elements
    for id in bep_rem_child_ids:
        bep_rem_child_eles.append(ifc_file[id])
    sertools.add_elems_to_remelems(rem_elements, bep_rem_child_eles)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    return rem_elements


def get_rems_getback(ifc_file, all_elements, rem_elements):
    print("\nGet back some elements")
    get_back_elems = []
    # daecher der einstellhalle
    get_back_elems += sertools.get_by_material_name(all_elements, "Beton, Stahlbeton")
    """
    # vertikale daemmung im erdreich, zweite hausreihe, ids stimmen nicht mehr
    get_back_elems += [ifc_file.by_guid("21312fpUqErfrESTLoEsqr")]
    get_back_elems += [ifc_file.by_guid("2qx0Gw_J609H73xbOuPf2v")]
    get_back_elems += [ifc_file.by_guid("05yLbw3qPfkUB2nnDQU12T")]
    get_back_elems += [ifc_file.by_guid("3P3ehkBwzHAT$cSRzlrHjI")]
    get_back_elems += [ifc_file.by_guid("1coMtgIP6FDwpFu5sLsLou")]
    get_back_elems += [ifc_file.by_guid("0G33zLVm8vATwITPbpdwyW")]
    """
    rem_elements = list(set(rem_elements).difference(set(get_back_elems)))
    print("Elements to remove: {}".format(len(rem_elements)))
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    return rem_elements

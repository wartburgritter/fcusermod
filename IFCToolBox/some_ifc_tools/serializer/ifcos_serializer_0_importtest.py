import importlib
from os.path import join
import ifcopenshell
import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)
import importIFC


# ************************************************************************************************
# comment if opened with FC ifc parse opener, TODO get these data from there somehow
# ifc_name = "FCimporttest_merg_material"
# onlyids = [175, 272, 354, 436]
#ifc_name = "4130_ING_N_TRW"
#onlyids = [212, 355]
ifc_name = "4130_ARC_ROHM01"
onlyids = [2524, 2575]
the_ifcfile = join("C:/Users/BHA/Desktop", ifc_name + ".ifc")
ifc_file = ifcopenshell.open(the_ifcfile)


importprefs = {
    "DEBUG": True,
    "PREFIX_NUMBERS": False,
    "SKIP": ["IfcOpeningElement"],
    "SEPARATE_OPENINGS": False,
    "ROOT_ELEMENT": "IfcProduct",
    "GET_EXTRUSIONS": False,
    "MERGE_MATERIALS": True,
    "MERGE_MODE_ARCH": 1,
    "MERGE_MODE_STRUCT": 0,
    "CREATE_CLONES": False,
    "IMPORT_PROPERTIES": False,  # glaube das ist extra noch in spreadsheet
    "SPLIT_LAYERS": False,
    "FITVIEW_ONIMPORT": True,
    "ALLOW_INVALID": True,
    "REPLACE_PROJECT": True,
    "MULTICORE": 0,
    "IMPORT_LAYER": True,
}


importlib.reload(importIFC)
# onlyids = [2524, 2575]
importIFC.insert(the_ifcfile, "", only=onlyids, preferences=importprefs)

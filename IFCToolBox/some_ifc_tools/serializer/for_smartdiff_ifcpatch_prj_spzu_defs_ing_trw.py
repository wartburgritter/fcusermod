import importlib

import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)


# ************************************************************************************************
def get_concrete_elems(elements):

    ortbet_elems = sertools.get_by_material_name(elements, "Ortbeton")
    ortbet_elems += sertools.get_by_material_name(elements, "Fertigelement Beton")  # peter elemente
    # aufpassen mit leerzeichen ist eigentlich falsch, oder ohne ist falsch
    # stuetzen sind ohne, daher so einfach, aber global nicht gut ...
    ortbet_elems += sertools.get_by_name(elements, "Dämmung eingelegt")

    return ortbet_elems


# ************************************************************************************************
def get_concrete_ignore_elems(elements):

    ortbet_ignore_elems = sertools.get_by_ifctype(elements, "IfcFooting")

    return ortbet_ignore_elems


# ************************************************************************************************
def get_plattenbauteile_concrete(elements):

    pla_ob_elems = []

    # get all slabs out of the ortbeton bauteile
    pla_ob_elems += sertools.get_by_ifctype(elements, "IfcSlab")

    # add spezielle ortbeton typen und klassen gemaess attributen
    pla_ob_elems += sertools.get_by_property_value(elements, "Allreal", "Typenname", "C2_AW_Ortbeton_0.35")
    pla_ob_elems += sertools.get_by_property_value(elements, "AllplanAttributes", "HB_eBKPH_Klasse", "C04.01_Unterzug")
    pla_ob_elems += sertools.get_by_guid(elements, "3kkq4wEjb0gPPLUVQLsxpK")
    pla_ob_elems += sertools.get_by_guid(elements, "3_kWvn$jLCs9tgjf4pcmvS")

    # entfernen zwischenpodeste, gehoeren zu waende
    elems_to_rem = sertools.get_by_name(elements, "Podest")
    pla_ob_elems = sertools.remove_elems_from_add(pla_ob_elems, elems_to_rem)


    # delete duplicates
    pla_ob_elems = list(set(pla_ob_elems))

    return pla_ob_elems


# ************************************************************************************************
def get_geschoss_ob_bauteile(elems_data, geschoss, gesch_name):

    all_elements = elems_data[0]
    ortbet_elems = elems_data[1]
    no_ob_elems = elems_data[2]
    ob_ignore_elems = elems_data[3]
    pla_ob_elems = elems_data[4]
    ver_ob_elems = elems_data[5]

    pla_gesch_ob_elems = sertools.get_by_property_value(pla_ob_elems, "Allreal", "Geschoss", gesch_name)
    ver_gesch_ob_elems = sertools.get_by_property_value(ver_ob_elems, "Allreal", "Geschoss", gesch_name)
    
    return pla_gesch_ob_elems, ver_gesch_ob_elems


# ************************************************************************************************
# nicht for smart diff, aber fuer BIM2Field und dies nutzt das system hier
def get_modell_elementstuetzen(all_elements):

    fertigteil_stuetzen_elems = []
    fertigteil_stuetzen_elems += sertools.get_by_material_name(all_elements, "FertigelementBeton")
    fertigteil_stuetzen_elems += sertools.get_by_material_name(all_elements, "Stahl")

    return fertigteil_stuetzen_elems


# ************************************************************************************************
# nicht for smart diff, aber fuer BIM2Field und dies nutzt das system hier
def get_geschoss_elementstuetzen(all_fertigteil_stuetzen_elems, geschoss, gesch_name):

    geschoss_elems_fertigteilstuetzen = sertools.get_by_property_value(
        all_fertigteil_stuetzen_elems,
        "Allreal",
        "Geschoss",
        gesch_name
    )

    return geschoss_elems_fertigteilstuetzen

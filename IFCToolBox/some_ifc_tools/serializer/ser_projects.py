from importlib import reload
from os.path import join

import FreeCAD
import FreeCAD as App
import importIFC

import some_ifc_tools.serializer.serializer as sertools
reload(sertools)


# *****************************************************************************
# HOWTO
# serializer projects
# copy vorlage code behind this  few lines
# see module end for more explanations
#
# here copy a method from another Project (there is a Vorlage at top of module)
# best first import and save as FreeCAD document, than load FreeCAD document and
# serialize inside open document. When happy save as ifc and run
# alltogether, load ifc, save FC, serialize, save FC, save ifc
# load and save ifc by Python and use own prefs


# *****************************************************************************
# 2xxxx, prjname
"""
from importlib import reload
import ser_projects
ser_projects.prjname_serialize()

reload(ser_projects)
ser_projects.prjname_serialize()

"""
def prjname_serialize(doc=App.ActiveDocument):

    print(len(doc.Objects))

    """
    # bound box
    # doc, length, width, height, originx, originy, originz):
    raum_obj = add_bigspace(doc, 75000, 80000, 40000, -25000, -80000, -10000)
    sertools.remove_by_outside_bigspace(doc, raum_obj)
    doc.recompute()
    # return
    """

    sertools.remove_by_attrib_value_contains(doc, "BE_Fensterlaibung")  # TODO kann doch gar nicht so funktionieren
    sertools.remove_by_attrib_key_value(doc, "LoadBearing;;Pset_WallCommon", "IfcBoolean;;False")
    sertools.remove_by_buildingelementtype(doc, "Door")
    sertools.remove_by_label(doc, "Stahl")
    sertools.remove_by_material_label(doc, "Aussenputz")
    sertools.remove_by_subtypeattr(doc, "Sockelkranz")
    sertools.remove_by_typeattr(doc, "Metallgitterrost")

    doc.recompute()
    return


# *****************************************************************************
# 21122, INRI
"""
from importlib import reload
import some_ifc_tools.serializer.ser_projects as ser_projects
ser_projects.inri_serialize()

reload(ser_projects)
ser_projects.inri_serialize()

"""
def inri_serialize(doc=App.ActiveDocument):

    """
    ifc_name = "55226_ARC_GM01"
    # ifc_name = "exporter"

    ifc_file = join("C:/Users/BHA/Desktop", ifc_name + ".ifc")
    doc = sertools.import_ifc_and_save_fcstd(ifc_file)
    doc.recompute()
    return
    """

    print(len(doc.Objects))

    sertools.remove_by_buildingelementtype(doc, "Covering")
    sertools.remove_by_buildingelementtype(doc, "Door")
    sertools.remove_by_buildingelementtype(doc, "Railing")
    sertools.remove_by_buildingelementtype(doc, "Stair")
    sertools.remove_by_buildingelementtype(doc, "Window")

    sertools.remove_by_typeattr(doc, "Beton, Stahlbeton 100")
    sertools.remove_by_typeattr(doc, "INZL Bodenbelag UG  155")
    sertools.remove_by_typeattr(doc, "INZL Bodenbelag OG  155")
    sertools.remove_by_material_label(doc, "Allgemein, Konstruktion Aussen")


    """
    sertools.remove_by_attrib_value_contains(doc, "BE_Fensterlaibung")  # TODO kann doch gar nicht so funktionieren
    sertools.remove_by_attrib_key_value(doc, "LoadBearing;;Pset_WallCommon", "IfcBoolean;;False")
    sertools.remove_by_label(doc, "Stahl")
    sertools.remove_by_material_label(doc, "Aussenputz")
    sertools.remove_by_subtypeattr(doc, "Sockelkranz")
    """

    doc.recompute()
    return


# *****************************************************************************
# 2013_Z\1347 UZI 5\98 NEUES HDL, Hochdrucklabor
"""
from importlib import reload
import ser_projects
ser_projects.hdl_serialize()

reload(ser_projects)
ser_projects.hdl_serialize()

"""
def hdl_serialize(doc=App.ActiveDocument):

    print(len(doc.Objects))
    sertools.remove_by_buildingelementtype(doc, "Door")
    sertools.remove_by_typeattr(doc, "Fassadenelement 4cm")
    sertools.remove_by_typeattr(doc, "Dämmung16.0")
    sertools.remove_by_typeattr(doc, "WD weich 160")
    sertools.remove_by_typeattr(doc, "Metallgitterrost-Schutzzone")
    sertools.remove_by_typeattr(doc, "UB 4 cm")
    sertools.remove_by_typeattr(doc, "UB 8 cm")
    sertools.remove_by_typeattr(doc, "UB 30 cm")

    doc.recompute()
    return


# *****************************************************************************
# 20106, UNAF
"""
from importlib import reload
import ser_projects
ser_projects.unaf_serialize()

reload(ser_projects)
ser_projects.unaf_serialize()

"""
def unaf_serialize(doc=App.ActiveDocument):

    print(len(doc.Objects))
    sertools.remove_by_label(doc, "Geländer-002")
    sertools.remove_by_label(doc, "Stahl")
    sertools.remove_by_label(doc, "Fehlt")

    sertools.remove_by_label(doc, "Fenster-007")
    sertools.remove_by_label(doc, "Fenster-008")
    sertools.remove_by_label(doc, "Tür-008")

    sertools.remove_by_label(doc, "FlowEq-001")
    sertools.remove_by_label(doc, "FlowEq-002")
    sertools.remove_by_label(doc, "Objekt-0013")

    sertools.remove_by_material_label(doc, "Aussenputz 300")
    sertools.remove_by_material_label(doc, "Erdreich 250")
    sertools.remove_by_material_label(doc, "Dämmung 400 (UB)")
    sertools.remove_by_material_label(doc, "Dämmung 430")
    sertools.remove_by_material_label(doc, "Dämmung 780")
    sertools.remove_by_material_label(doc, "Dämmung horizontal 430")
    sertools.remove_by_material_label(doc, "Gips 450")
    sertools.remove_by_material_label(doc, "Holz 440")
    sertools.remove_by_material_label(doc, "Holz 820")
    sertools.remove_by_material_label(doc, "Luft 300")
    sertools.remove_by_material_label(doc, "Naturstein 810")
    sertools.remove_by_material_label(doc, "Metall 200")
    # sertools.remove_by_material_label(doc, "Stahl") # problem zwei stahlstuetzen
    sertools.remove_by_material_label(doc, "Unterlagsboden 420")

    doc.recompute()
    return


    """
    if doc is None:
        doc =  FreeCAD.openDocument('C:/Users/BHA/Desktop/20106_UNAF_ARC_ARC.FCStd')

    # Material, wenn in mehrschichtigen Bauteilen, dann ist
    # es beim globalen material filter evtl. nicht dabei
    # jeweils viele elemente
    sertools.remove_by_material_label(doc, "Kalksandstein 550")
    sertools.remove_by_material_label(doc, "Operator")

    # object type
    sertools.remove_by_subtypeattr(doc, "Leichtbau")
    sertools.remove_by_subtypeattr(doc, "Dämmung")
    # einige Betonbodenplatten haben im object type "Bodenaufbau" oder "Boden"
    doc.recompute()
    """

    doc.save()

    # export to ifc

    FreeCAD.closeDocument(doc.Name)

# *****************************************************************************
# 21048, Allmend Zuerich
"""
from importlib import reload
import ser_projects
ser_projects.allmend_serialize()

reload(ser_projects)
ser_projects.allmend_serialize()

"""
def allmend_serialize(doc=App.ActiveDocument):

    """
    sertools.remove_by_material_label(doc, "Holz, Konstruktion")
    doc.recompute()
    return
    """

    if doc is None:
        doc =  FreeCAD.openDocument('C:/Users/BHA/Desktop/Allmend serialize/G909_ARC_ROH_serialisiert.FCStd')

    # bound box
    # doc, length, width, height, originx, originy, originz):
    raum_obj = add_bigspace(doc, 75000, 80000, 40000, -25000, -80000, -10000)
    sertools.remove_by_outside_bigspace(doc, raum_obj)
    doc.removeObject(raum_obj.Base.Name)
    doc.removeObject(raum_obj.Name)
    doc.recompute()
    # return

    # Bauteilklassen, element class
    sertools.remove_by_buildingelementtype(doc, "Covering")
    sertools.remove_by_buildingelementtype(doc, "Door")
    sertools.remove_by_buildingelementtype(doc, "Railing")
    sertools.remove_by_buildingelementtype(doc, "Window")
    doc.recompute()

    # Material, wenn in mehrschichtigen Bauteilen, dann ist
    # es beim globalen material filter evtl. nicht dabei
    # jeweils viele elemente
    sertools.remove_by_material_label(doc, "Allgemein, Bekleidung")
    sertools.remove_by_material_label(doc, "Allgemein, Bekleidung Aussen")
    sertools.remove_by_material_label(doc, "Belag, Pflaster")
    sertools.remove_by_material_label(doc, "Dämmung, Aussen")
    sertools.remove_by_material_label(doc, "Dämmung, Innen")
    sertools.remove_by_material_label(doc, "Dämmung, Leichtbauwand")
    sertools.remove_by_material_label(doc, "Dämmung, Perimeter vertikal")
    sertools.remove_by_material_label(doc, "Dämmung, Schattenfuge")
    sertools.remove_by_material_label(doc, "Dämmung, Schalldämmung")
    sertools.remove_by_material_label(doc, "Dämmung, Trittschall")
    sertools.remove_by_material_label(doc, "Gips")
    sertools.remove_by_material_label(doc, "Gipsplatte horizontal")
    sertools.remove_by_material_label(doc, "Gipsplatte vertikal")
    sertools.remove_by_material_label(doc, "Gips")
    sertools.remove_by_material_label(doc, "Holz")
    sertools.remove_by_material_label(doc, "Metall, Aluminium")
    sertools.remove_by_material_label(doc, "SBU 020 - Stahl einbrennlackiert")
    sertools.remove_by_material_label(doc, "Beton, Magerbeton")
    sertools.remove_by_material_label(doc, "Unterlagsboden")
    sertools.remove_by_material_label(doc, "Verputz, Aussen")

    # nur vereinzelte elemente, pruefen
    sertools.remove_by_material_label(doc, "Allgemein, Bekleidung Innen")
    sertools.remove_by_material_label(doc, "Allgemein, Bekleidung, Allgemein, Bekleidung")
    sertools.remove_by_material_label(doc, "Allgemein, Unterkonstruktion")
    sertools.remove_by_material_label(doc, "Holzwerkstoff")
    sertools.remove_by_material_label(doc, "Holz, Konstruktion")
    doc.recompute()

    # object type
    sertools.remove_by_subtypeattr(doc, "Leichtbau")
    sertools.remove_by_subtypeattr(doc, "Oblicht")
    sertools.remove_by_subtypeattr(doc, "Dachfenster")
    sertools.remove_by_subtypeattr(doc, "Sockelkranz")
    sertools.remove_by_subtypeattr(doc, "Dachrand")
    sertools.remove_by_subtypeattr(doc, "Badewanne")
    sertools.remove_by_subtypeattr(doc, "Waschtisch")
    sertools.remove_by_subtypeattr(doc, "WC_Laufen")
    sertools.remove_by_subtypeattr(doc, "Gipsplatte")
    sertools.remove_by_subtypeattr(doc, "Metall")
    sertools.remove_by_subtypeattr(doc, "Dämmung")
    # einige Betonbodenplatten haben im object type "Bodenaufbau" oder "Boden"
    doc.recompute()

    doc.save()

    # export to ifc

    FreeCAD.closeDocument(doc.Name)

# *****************************************************************************
# 15001, FZAG, W1 bis W5
"""
import ser_projects
from importlib import reload
reload(ser_projects)
ser_projects.w1bisw5_serialize()

"""
def w1bisw5_serialize(doc=App.ActiveDocument):

    sertools.remove_by_buildingelementtype(doc, "Window")
    sertools.remove_by_buildingelementtype(doc, "Door")
    sertools.remove_by_buildingelementtype(doc, "Distribution Element")
    sertools.remove_by_buildingelementtype(doc, "Building Element Proxy")

    sertools.remove_by_material_label(doc, "Dachbelag")
    sertools.remove_by_material_label(doc, "Zementueberzug")
    sertools.remove_by_material_label(doc, "Geländer Treppe")

    sertools.remove_axes(doc)
    
    # sertools.remove_empty_buildingparts(doc)  # wenn im ersten run FreeCAD crash
    doc.recompute()


# *****************************************************************************
# 16094 BAZU, badener strasse
"""
import ser_projects
from importlib import reload
reload(ser_projects)
ser_projects.badenerstr_serialize()

"""
def badenerstr_serialize(doc=App.ActiveDocument):
    # serialize badenerstrasse ifc
    # """
    # doc, length, width, height, originx, originy, originz):
    raum_obj = add_bigspace(doc, 60000, 60000, 40000, -25000, -10000, -7000)
    sertools.remove_by_outside_bigspace(doc, raum_obj)

    sertools.remove_by_attrib_value_contains(doc, "BE_Fensterlaibung")

    sertools.remove_by_attrib_key_value(doc, "LoadBearing;;Pset_WallCommon", "IfcBoolean;;False")

    sertools.remove_by_buildingelementtype(doc, "Window")
    sertools.remove_by_buildingelementtype(doc, "Door")

    sertools.remove_by_material_label(doc, "Gips")
    sertools.remove_by_material_label(doc, "Metall, Stahl")
    sertools.remove_by_material_label(doc, "Allgemein, Bekleidung Aussen")
    sertools.remove_by_material_label(doc, "Allgemein, Bekleidung Aussen, stark")

    sertools.remove_by_label(doc, "Axes")
    # """

    """
    # split layer import
    sertools.remove_by_material_label(doc, "Sperrschicht, eng")
    sertools.remove_by_material_label(doc, "Dämmung, Aussen")
    sertools.remove_by_material_label(doc, "Belag, Bodenbelag")
    # sertools.remove_by_material_label(doc, "Verputz, Aussen")  # loescht aussenwaende
    # diese sind compounds der drei einzelobj
    # also alles da nur nicht so wie ich gerade jetzt brauche
    """
    # TODO: Bezug auf Liegenschaft, Gebaeude anstatt document aber bug in liegenschaft
    # dann hilfsobjekte problemlos moeglich
    # objekte muessten auch nicht geloescht werden
    # obwohl ohne loeschen die performance in die knie geht
    # aber zum testen fuer bestimmtes leeschen macht sinn
    # in eine gruppe zu schieben oder farbe zu aendern, oder kopie mit anderer farbe anlegen
    # und kopie des gesamten als compound in wireframe (performance ist fast, da nur ein obj!!!)


# *****************************************************************************
"""
import ser_projects
from importlib import reload
reload(ser_projects)
ser_projects.badenerstr_import_ser_export()

"""
def badenerstr_import_ser_export():
    pfad = "C:/0_BHA_privat/serialize"
    datei_base_name = "15055_2226_Zuerich_Master_170529"
    import_datei = join(pfad, datei_base_name + ".ifc")
    save_imp_datei = join(pfad, datei_base_name + ".FCStd")
    save_ser_datei = join(pfad, datei_base_name + "_serialize.FCStd")
    export_datei = join(pfad, datei_base_name + "_serialize.ifc")

    doc = importIFC.insert(import_datei, "mydoc")
    doc.saveAs(save_imp_datei)

    # serialize
    badenerstr_serialize(doc)

    doc.saveAs(save_ser_datei)
    objs = []  # selecting objs to export to ifc
    for o in doc.Objects:
        objs.append(o)
    doc.recompute()
    importIFC.export(objs, export_datei)
    FreeCAD.closeDocument(doc.Name)
    # TODO: delete raum, raum part before export all objects to ifc, or fix and export site


# *****************************************************************************
# gsa sanitaer
"""
import ser_projects
from importlib import reload
reload(ser_projects)
ser_projects.gsa_san_import_ser_export()

"""
def gsa_san_import_ser_export(
):
    pfad = "C:/0_BHA_privat/serialize"
    datei_base_name = "FZAG_GSA--Tech_SA_190507"
    import_datei = join(pfad, datei_base_name + ".ifc")
    save_imp_datei = join(pfad, datei_base_name + ".FCStd")
    save_ser_datei = join(pfad, datei_base_name + "_serialize.FCStd")
    export_datei = join(pfad, datei_base_name + "_serialize.ifc")

    doc = importIFC.insert(import_datei, "mydoc")
    doc.saveAs(save_imp_datei)
    # export an ifc forech building storey (fuer jedes Geschoss)
    for o in doc.Objects:
        if hasattr(o, "IfcType") and o.IfcType == "Building Storey":
            export_datei = join(pfad, datei_base_name + "_" + o.Label + "_serialize.ifc")
            importIFC.export([o], export_datei)
    doc.recompute()
    FreeCAD.closeDocument(doc.Name)


# *****************************************************************************
# w10 grube, mein manueller unit test
"""
import ser_projects
from importlib import reload
reload(ser_projects)
ser_projects.grubew10_import_ser_export()

"""
def grubew10_import_ser_export(
):
    pfad = "C:/0_BHA_privat/serialize"
    datei_base_name = "FZAG_W10_ING_TRW"
    import_datei = join(pfad, datei_base_name + ".ifc")
    save_imp_datei = join(pfad, datei_base_name + ".FCStd")
    save_ser_datei = join(pfad, datei_base_name + "_serialize.FCStd")
    export_datei = join(pfad, datei_base_name + "_serialize.ifc")

    # import importIFCorg
    # doc = importIFCorg.insert(import_datei, "mydoc", prefs={"MERGE_MODE_ARCH":3})
    doc = importIFC.insert(import_datei, "mydoc")
    doc.saveAs(save_imp_datei)

    sertools.remove_by_attrib_key_value(doc, "Umbaukategorie;;Allplan Attributes", "IfcText;;Bestand")
    doc.saveAs(save_ser_datei)
    """
    objs = []  # selecting objs to export to ifc
    for o in doc.Objects:
        objs.append(o)
    doc.recompute()
    importIFC.export(objs, export_datei)
    """
    for o in doc.Objects:
        if hasattr(o, "IfcType") and o.IfcType == "Building Storey":
            export_datei = join(pfad, datei_base_name + "_" + o.Label + "_serialize.ifc")
            importIFC.export([o], export_datei)
    doc.recompute()
    FreeCAD.closeDocument(doc.Name)


# *****************************************************************************
# code to copy and other help
"""
for o in doc.Objects:
    if hasattr(o, "IfcProperties"):
        print(o.Name)
        for key, value in o.IfcProperties.items():
            print("  ", key, " --> ", value)


for key, value in obj.IfcProperties.items():
    print("  ", key, " --> ", value)

"""

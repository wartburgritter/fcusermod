"""
# zum testen

import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)

ifcfile = prjobj.IfcRoh
elements = ifcfile.by_type("IfcBuildingElement")

sertools.get_by_ifctype(elements,"IfcBuildingElementPart")
sertools.get_by_material_name(elements,"Betonfertigteil")
sertools.get_by_guid(elements, "1THQ$DfDNfGg$atPzL85V5")

"""

import importlib

import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)


# ************************************************************************************************
def get_concrete_elems(elements):

    print(len(elements))
    ortbet_elems = sertools.get_by_property_value(elements, "Allreal_Material", "Material", "Beton")
    ortbet_elems += sertools.get_by_material_name(elements, "Beton")
    print(len(ortbet_elems))

    # obwohl material beton nimmts die ben nicht, HAACK add by guid
    ortbet_elems += sertools.get_by_guid(elements, "1THQ$DfDNfGg$atPzL85V5")


    # remove columns
    ortbet_elems = list(set(ortbet_elems) - set(sertools.get_by_ifctype(ortbet_elems, "IfcColumn")))

    # remove Fluchtroehre
    ortbet_elems = list(set(ortbet_elems) - set(sertools.get_by_material_name(ortbet_elems,"Betonfertigteil")))

    # remove Stützenköpfe
    # funktioniert nicht, weil die BuildingElementPart sind
    # ortbet_elems = list(set(ortbet_elems) - set(sertools.get_by_material_name(ortbet_elems,"Beton - Ortbeton")))

    # remove alle BuildingelementPart
    # elementstutzen und kragplattenanschluesse, aktuell falsch klassifiziert
    ortbet_elems = list(set(ortbet_elems) - set(sertools.get_by_ifctype(ortbet_elems,"IfcBuildingElementPart")))

    # remove bestand, neben rampe
    ortbet_elems = list(set(ortbet_elems) - set(sertools.get_by_guid(elements, "3YgAR9pPHzHvTxJNEFgJKV")))
    ortbet_elems = list(set(ortbet_elems) - set(sertools.get_by_guid(elements, "2r6E3T3h9SJxs3VTSV1eRe")))

    return ortbet_elems


# ************************************************************************************************
def get_concrete_ignore_elems(elements):

    ortbet_ignore_elems = []

    return ortbet_ignore_elems


# ************************************************************************************************
def get_plattenbauteile_concrete(ortbet_elems):

    pla_ob_elems = []

    # get all slabs out of the ortbeton bauteile
    pla_ob_elems += sertools.get_by_ifctype(ortbet_elems, "IfcSlab")
    pla_ob_elems += sertools.get_by_ifctype(ortbet_elems, "IfcRamp")

    # delete duplicates
    pla_ob_elems = list(set(pla_ob_elems))

    return pla_ob_elems


# ************************************************************************************************
def get_geschoss_ob_bauteile(elems_data, geschoss, gesch_name):

    all_elements = elems_data[0]
    ortbet_elems = elems_data[1]
    no_ob_elems = elems_data[2]
    ob_ignore_elems = elems_data[3]
    pla_ob_elems = elems_data[4]
    ver_ob_elems = elems_data[5]

    pla_gesch_ob_elems = sertools.get_by_storey_name(pla_ob_elems, gesch_name)
    ver_gesch_ob_elems = sertools.get_by_storey_name(ver_ob_elems, gesch_name)

    # aufpassen was vertikal und was platten, nicht hier hin- und her, das ganz vorher ...
    if geschoss == "01_UG":
        pass
    elif geschoss == "11_EG":
        pass

    return pla_gesch_ob_elems, ver_gesch_ob_elems

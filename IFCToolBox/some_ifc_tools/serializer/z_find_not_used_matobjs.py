# find in FreeCAD document material objects which have not been used
doc = App.ActiveDocument
used_materials = []
mat = None
for obj in doc.Objects:
    if hasattr(obj, "Proxy") and hasattr(obj.Proxy, "Type") and obj.Proxy.Type == "Material":
        continue # materials itself do have a Material property too
    elif hasattr(obj, "Material"):
        mat = obj.Material
        if hasattr(mat, "Proxy") and hasattr(mat.Proxy, "Type") and mat.Proxy.Type == "Material":
            if mat not in used_materials:
                used_materials.append(mat)
        else:
            print("error")
            # should not happen


print(used_materials)
not_used_materials = set(doc.getObject("MaterialContainer").Group) - set(used_materials)
for m in not_used_materials:
    print(m.Label)
    doc.removeObject(m.Name)

doc.recompute()

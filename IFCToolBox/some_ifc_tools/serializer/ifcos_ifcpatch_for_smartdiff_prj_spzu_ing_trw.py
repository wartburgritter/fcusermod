import os
import importlib
from os.path import join

import ifcopenshell

import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)

import some_ifc_tools.serializer.ifcos_ifcpatch_for_smartdiff_prj_spzu_0run as runspzupatch
importlib.reload(runspzupatch)


# ************************************************************************************************
def run_patch_spzu_ing_trw(
    workpath,
    geschosse,
    plat_vert,
    model_type,
    write_ifc
):

    infile_ifc = "infiles/4130_ING_N_TRW.ifc"
    
    print("\n" + model_type)
    ifc_file_p = join(workpath, infile_ifc)
    # print(ifc_file_p)
    ifcfile = ifcopenshell.open(ifc_file_p)

    # get and write generic elems
    elems_data = sertools.run_patch_generic_elems(
        get_concrete_elems,
        get_concrete_ignore_elems,
        get_plattenbauteile_concrete,
        workpath,
        ifc_file_p,
        ifcfile,
        geschosse,
        plat_vert,
        model_type,
        write_ifc
    )
    all_elements = elems_data[0]
    ortbet_elems = elems_data[1]
    no_ob_elems = elems_data[2]
    ob_ignore_elems = elems_data[3]
    pla_ob_elems = elems_data[4]
    ver_ob_elems = elems_data[5]

    # get and write ortbeton geschosse elems
    elems_floors_ob = sertools.run_patch_geschoss_elems(
        get_geschoss_ob_bauteile,
        elems_data,
        workpath,
        ifc_file_p,
        ifcfile,
        geschosse,
        plat_vert,
        model_type,
        write_ifc
    )

    return True


# ************************************************************************************************
def get_concrete_elems(elements):

    ortbet_elems = sertools.get_by_material_name(elements, "Ortbeton")

    return ortbet_elems


# ************************************************************************************************
def get_concrete_ignore_elems(elements):

    ortbet_ignore_elems = sertools.get_by_ifctype(elements, "IfcFooting")

    return ortbet_ignore_elems


# ************************************************************************************************
def get_plattenbauteile_concrete(elements):

    pla_ob_elems = []

    # get all slabs out of the ortbeton bauteile
    pla_ob_elems += sertools.get_by_ifctype(elements, "IfcSlab")

    # add spezielle ortbeton typen und klassen gemaess attributen
    pla_ob_elems += sertools.get_by_property_value(elements, "Allreal", "Typenname", "C2_AW_Ortbeton_0.35")
    pla_ob_elems += sertools.get_by_property_value(elements, "AllplanAttributes", "HB_eBKPH_Klasse", "C04.01_Unterzug")
    pla_ob_elems += sertools.get_by_guid(elements, "3kkq4wEjb0gPPLUVQLsxpK")
    pla_ob_elems += sertools.get_by_guid(elements, "3_kWvn$jLCs9tgjf4pcmvS")

    # delete duplicates
    pla_ob_elems = list(set(pla_ob_elems))

    return pla_ob_elems


# ************************************************************************************************
def get_geschoss_ob_bauteile(elems_data, geschoss, gesch_name):

    all_elements = elems_data[0]
    ortbet_elems = elems_data[1]
    no_ob_elems = elems_data[2]
    ob_ignore_elems = elems_data[3]
    pla_ob_elems = elems_data[4]
    ver_ob_elems = elems_data[5]

    pla_gesch_ob_elems = sertools.get_by_property_value(pla_ob_elems, "Allreal", "Geschoss", gesch_name)
    ver_gesch_ob_elems = sertools.get_by_property_value(ver_ob_elems, "Allreal", "Geschoss", gesch_name)
    
    return pla_gesch_ob_elems, ver_gesch_ob_elems

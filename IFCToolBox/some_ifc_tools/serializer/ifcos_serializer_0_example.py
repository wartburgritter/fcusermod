import importlib
from os.path import join
import ifcopenshell
import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)


# ************************************************************************************************
# comment if opened with FC ifc parse opener, TODO get these data from there somehow
ifc_name = "4130_ING_N_TRW"
the_ifcfile = join("C:/Users/BHA/Desktop", ifc_name + ".ifc")
ifc_file = ifcopenshell.open(the_ifcfile)


outdir_newbasename_noending = join("C:/Users/BHA/Desktop", ifc_name + "_BHA_serialisiert")
rem_elements = []
all_elements = ifc_file.by_type("IfcBuildingElement")
print("All IfcBuildingall_elements: {}".format(len(all_elements)))
print("Enities to remove: {}".format(len(set(rem_elements))))


# ************************************************************************************************
print("Storeys")
rem_floors = sertools.get_by_storey_name(all_elements, "00.EG_FBOK")  # zweite zeile ist anders!
rem_floors += sertools.get_by_storey_name(all_elements, "01.OG_FBOK")
sertools.add_elems_to_remelems(rem_elements, rem_floors)
print("All IfcBuildingall_elements: {}".format(len(all_elements)))


print("IFC-classification")
rem_ifc_classification = ifc_file.by_type("IfcFooting")  # zweite zeile ist anders!
rem_ifc_classification += ifc_file.by_type("IfcColumn")
sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
print("All IfcBuildingall_elements: {}".format(len(all_elements)))


print("GUID")
rem_guid = [ifc_file.by_id("3_kWvn$jLCs9tgjf4pcmvS")]  # zweite zeile ist anders!
rem_guid += [ifc_file.by_id("3kkq4wEjb0gPPLUVQLsxpK")]
sertools.add_elems_to_remelems(rem_elements, rem_guid)
print("All IfcBuildingall_elements: {}".format(len(all_elements)))


print("Names")
rem_names = sertools.get_by_name(all_elements, "Bruestung")  # zweite zeile ist anders!
rem_names += sertools.get_by_name(all_elements, "Podest")
sertools.add_elems_to_remelems(rem_elements, rem_names)
print("All IfcBuildingall_elements: {}".format(len(all_elements)))


print("Materials")
rem_mats = sertools.get_by_material_name(all_elements, "DaemmungHart")  # zweite zeile ist anders!
rem_mats += sertools.get_by_material_name(all_elements, "FertigelementBeton")
sertools.add_elems_to_remelems(rem_elements, rem_mats)
print("All IfcBuildingall_elements: {}".format(len(all_elements)))


print("Object Types")
# ist nicht der type, sondern object type!
rem_objtypes = sertools.get_by_objtype(all_elements, "C1_BP_Beton_0.30")  # zweite zeile ist anders!
sertools.add_elems_to_remelems(rem_elements, rem_objtypes)
print("All IfcBuildingall_elements: {}".format(len(all_elements)))


print("Properties")
rem_props = sertools.get_by_property_value(all_elements, "Allreal", "Typenname", "C2_AW_Ortbeton_0.35")  # zweite zeile ist anders!
sertools.add_elems_to_remelems(rem_elements, rem_props)
print("All IfcBuildingall_elements: {}".format(len(all_elements)))


print("Get back some elements")
get_back_elems = [ifc_file.by_guid("144qY9JR17SQzdf8t_389T")]  # zweite zeile ist anders! # treppe
get_back_elems += sertools.get_by_name(all_elements, "Aussenwand UG 35cm")
rem_elements = list(set(rem_elements).difference(set(get_back_elems)))
print("Elements to remove: {}".format(len(rem_elements)))
print("All IfcBuildingall_elements: {}".format(len(all_elements)))


print("Shape Cutobj")
import Part
sh_cutobj = Part.makeBox(150000, 60000, 10000)
sh_cutobj.Placement.move(FreeCAD.Vector(-15000, -13000, -2000))


# ************************************************************************************************
# smart view and write serialised ifc
sertools.write_smart_view(all_elements, rem_elements, outdir_newbasename_noending)
# sertools.write_ifc_serialised(the_ifcfile, outdir_newbasename_noending, rem_elements, sh_cutobj)

"""
import importlib
import some_ifc_tools.serializer.ifcos_ifcpatch_for_smartdiff_prj_spzu_0run as runspzupatch

importlib.reload(runspzupatch)


runspzupatch.run_all("UG", "vert", "")
runspzupatch.run_all("UG", "", "roh")
runspzupatch.run_all("UG", "vert", "trw")
runspzupatch.run_all("EG", "plat", "roh")

runspzupatch.run_all(None, "", "", False) # run all but do not write a IFC, for debug

runspzupatch.run_all()

"""


import importlib
import os
from os.path import join

import some_ifc_tools.serializer.ifcos_ifcpatch_for_smartdiff_prj_spzu_arc_roh as runspzuroh
import some_ifc_tools.serializer.ifcos_ifcpatch_for_smartdiff_prj_spzu_ing_trw as runspzutrw
importlib.reload(runspzuroh)
importlib.reload(runspzutrw)


std_workpath = join("C:", os.sep, "0_BHA_privat", "BIMGeomSerializer")


spzu_data = {  # generieren aus Alle ifc
    "UG": {
        "name": "01.UG_FBOK",
        "darueber": "EG",
        "darunter": None,
        "guid_roh": "1_TR1TZoT3QBHIsb54G28K",
        "guid_trw": "2CWAFTfujFHv9FKlsI16Ai",
    },
    "EG": {
        "name": "00.EG_FBOK",
        "darueber": "OG1",
        "darunter": "UG",
        "guid_roh": "2l596A1ir1Lwr9_mkzKt3L",
        "guid_trw": "1w$rR5slHBxemKKajA19Pa",
    },
    "OG1": {
        "name": "01.OG_FBOK",
        "darueber": "OG2",
        "darunter": "EG",
        "guid_roh": "0KjHTBCVv9jxQFqvochneA",
        "guid_trw": "1vzK2J_tf4BQaPRexb8JQI",
    },
    "OG2": {
        "name": "02.OG_FBOK",
        "darueber": "OG3",
        "darunter": "OG1",
        "guid_roh": "29z$2u0X91wwX3Qzf6EQgp",
        "guid_trw": "1yUTR96efAueyLyF7PogYo",
    },
    "OG3": {
        "name": "03.OG_FBOK",
        "darueber": "OG4",
        "darunter": "OG2",
        "guid_roh": "1IOW2lC_z0_g0st8vaUuz3",
        "guid_trw": "3WTyeREb983RmW1XpALg2$",
    },
    "OG4": {
        "name": "04.OG_FBOK",
        "darueber": "OG5",
        "darunter": "OG3",
        "guid_roh": "2KQ5uq$LfAUOkFlvU_D6zk",
        "guid_trw": "1mDD5QQkn22eASUS6QP66S",
    },
    "OG5": {
        "name": "05.OG_FBOK",
        "darueber": "AG6",
        "darunter": "OG4",
        "guid_roh": "2NXzn9QgbA0uDMrXOo6tJI",
        "guid_trw": "3uP2Dgbxz6puU630KXX1jc",
    },
    "AG6": {
        "name": "06.AG_FBOK",
        "darueber": "DG7",
        "darunter": "OG5",
        "guid_roh": "2VzFDL$XrDbhE$$KDZjL7H",
        "guid_trw": "2x091RmBTAXO2Sp5ebU$OY",
    },
    "DG7": {
        "name": "07.DG_FBOK",
        "darueber": None,
        "darunter": "AG6",
        "guid_roh": "3mJg6OYg1BEgBY_7bwgkDk",
        "guid_trw": "2$pm07Dqz0XATFvaqtk2Ox",
    },
    None: {
        "guid_roh": None,
        "guid_trw": None,
    },
}


def run_all(geschoss=None, plat_vert="", model_type="", write_ifc=True, workpath=std_workpath):

    if geschoss is None:
        geschosse = [geschoss for geschoss in spzu_data.keys() if geschoss is not None]
    else:
        geschosse = [geschoss]
    print(geschosse)

    if model_type == "" or model_type == "roh":
        runspzuroh.run_patch_spzu_arc_roh(
            workpath,
            geschosse,
            plat_vert,
            "ARC_ROH",
            write_ifc
        )
    if model_type == "" or model_type == "trw":
        runspzutrw.run_patch_spzu_ing_trw(
            workpath,
            geschosse,
            plat_vert,
            "ING_TRW",
            write_ifc
        )

    return True

# warum keine separate vert und plat methode
# parsen ifc und suche der element nicht duplizieren

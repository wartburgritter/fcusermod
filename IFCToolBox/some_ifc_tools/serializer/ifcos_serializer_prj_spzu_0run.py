"""
# siehe ifcos_ifcpatch... !!!

import importlib
import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_0run as runspzu

importlib.reload(runspzu)

# True einfuegen fuer wirklich ifc erstellen, BITTE erst ohne True ausfuehren
runspzu.run_fuer_smart_diff()

runspzu.run_all()

"""


# ************************************************************************************************
# TODO
# den SmartView ohne transparente objekte und nicht rot faerben


# ************************************************************************************************
# in zoom cool
# contains anstatt is (==)
# contains "Lehm", nimmt alle "Lehmdecke", "Lehmboden", "Lehmwand", ...
# oder contains "Holz" nimmt alle "Holzwand 100". "Holzwand 120", ...
# fuer properties umgesetzt, TODO fuer die anderen


# ************************************************************************************************
# das untere passt nicht mehr hier her, da alles mit ifcpatch laeuft, ist aber doch von interesse
#
# IfcBuildingElementPart objekte und probleme import in FreeCAD
#
# hier nicht mehr wichtig, da jetzt ja IfcPatch verwendet wird
#
# gesschossstruktur und auch die buildingelementpart struktur wird bei only nicht in FC eingelesen
# ich muss doch skip nutzen
# sollte kein problem sein aus only skip zu erstellen aber aufpassen Verhalten von BuildingElementPart ???
# erstes ziel alle IfcBuildingElement die nicht IfcBuildingElementPart sind die ich nicht will in eine skip list
# import in FC mit dieser SKIP list ...
# in dieser skiplist duerfen nicht nur die nicht IfcBuildingElementPart sein, sondern es müssen auch alle zu
# skippenden IfcBuildingElementPart sein, bloed das ist irgendwie doppelt ... 
# die zu skippenden IfcBuildingElementPart sollten sich doch anhand des zu skippenden IfcBuildingElement finden lassen
# schleife ueber alle zu sippenden IfcBuildingElemente
#   wenn es IfcBuildingElementParts hat diese zur skiplist hinzufuegen
#
# suche IfcBuildingElement, dann nehme entity und suche, finde ein IfcRelAggregates, dieses ordnet die IfcBuildingElementParts zu
# heisst obige schleife sollte einfach sein, bestimmt ueber inverse has... findbar ... 
# ich finde es nicht ... in FC the old was alle relagg und dann darin suchern
#
# IfcBuildingElementPart objekte und probleme in Zoom
# 
# IfcWall hat kein material und besteht aus vielen IfcBuildingElementPart die haben Material
# Wenn in Zoom das IfcWall hinzugefuegt wird werden auch wieder alle IfcBuildingElementPart hinzugefuegt
# heisst ich muss das IfcWall loeschen
# mit dem Material kann ich aber das IfcWall nicht loeschen, da es kein Material hat.
# ueber den Typ wuerde es funkionieren


import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_arc_roh as runspzuroh
import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_ing_trw as runspzutrw
import importlib
importlib.reload(runspzuroh)
importlib.reload(runspzutrw)


# ************************************************************************************************
def run_all(create_ifc=False):

    runspzuroh.spzu_arc_roh_ortbeton_ug_platvert_serialise(create_ifc)
    runspzuroh.spzu_arc_roh_elemente_vorgefertigt_serialise(create_ifc)
    runspzuroh.spzu_arc_roh_ortbeton_alles_fuer_rohvergleich_serialise()


    runspzutrw.serialise_spzu_ing_trw_ortbeton_ug_platvert(create_ifc)
    runspzutrw.serialise_spzu_ing_trw_ortbeton_alles_fuer_rohvergleich(create_ifc)
    runspzutrw.serialise_spzu_ing_trw_elementstuetzen(create_ifc)

    return True

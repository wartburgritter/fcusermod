# ***************************************************************************
# *   Copyright (c) 2023 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

# class for bimgeomsmartdiff project data

# start bimgeomdiff mit uebergabe projektdatenmodul wie bisher,
# dann started sofort die instanzierung der bimgeomsmartdiffprojectdata class

# project_base_data wird uebergeben
# pfad der infiles
# geruest SpatialData
#
# init methode
# parsen ifc
# zuweisen ifcos instanz
# auslesen guids, generieren spatial data, und zuweisen

# ist auch spaeter mgl
# modulenamen fuer ifcpatch methoden

# ich belasse roh und trw, obwohl es eher modell1 und modell2 sein sollte
# roh kann auch mit roh verglichen werden und arc mit arc
# aber so ist verwechslungsgefahr kleiner


import importlib
import json
import os
from os.path import join

import ifcopenshell


std_inpath = join("C:", os.sep, "0_BHA_privat", "BIMGeomAllSerializerSmartDataDiff")


# macht es sinn zwei klassen zu erstellen ??? je eine fuer jede datei
# mhh dann bin ich ja fast bei IfcStore von BIMTester ... ???


class BimGeomSmartDiffProjectData():

    # ************************************************************************************************
    def __init__(
        self,
        project_base_data_module,
        inpath=std_inpath,
        in_subdir="infiles",
        in_file_name_roh=None,
        in_file_name_trw=None,
    ):

        print("Start init!")

        # all the paramete above is not smart IMHO

        self.InPath = inpath
        self.InSubDirectory = in_subdir

        # file path
        # will be assigned in this init
        self.InfilePathRoh = None
        self.InfilePathTrw = None

        # ifcos instanz
        # will be assigned in this init
        self.IfcRoh = None
        self.IfcTrw = None

        # file name with ending
        if in_file_name_roh is None:
            self.InfileNameRoh = project_base_data_module.infile_roh
        else:
            self.InfileNameRoh = in_file_name_roh
        if in_file_name_trw is None:
            self.InfileNameTrw = project_base_data_module.infile_trw
        else:
            self.InfileNameTrw = in_file_name_trw

        # spatial base data
        self.SpatialData = project_base_data_module.spatial_base_data

        # module string
        self.GetDefsStrRoh = project_base_data_module.getdefsmodule_str_roh
        self.GetDefsStrTrw = project_base_data_module.getdefsmodule_str_trw

        # imported modul instance
        self.GetDefsModuleRoh = importlib.import_module(self.GetDefsStrRoh)
        self.GetDefsModuleTrw = importlib.import_module(self.GetDefsStrTrw)

        # parse roh, get storey guids roh
        self.InfilePathRoh = join(self.InPath, self.InSubDirectory , self.InfileNameRoh)
        print(self.InfilePathRoh)
        if os.path.isfile(self.InfilePathRoh):
            self.IfcRoh = ifcopenshell.open(self.InfilePathRoh)
            self.add_storey_guids(self.IfcRoh, "roh")
        else:
            print("No ARC_ROH file.")

        # parse trw, get storey guids trw
        self.InfilePathTrw = join(self.InPath, self.InSubDirectory , self.InfileNameTrw)
        print(self.InfilePathTrw)
        if os.path.isfile(self.InfilePathTrw):
            self.IfcTrw = ifcopenshell.open(self.InfilePathTrw)
            self.add_storey_guids(self.IfcTrw, "trw")
        else:
            print("No ING_TRW file.")

        # print(json.dumps(self.SpatialData, indent=4))  # Fehler ?
        print("End init!")


    # ************************************************************************************************
    def add_storey_guids(self, ifcfile, model_type):

        if model_type != "roh" and model_type != "trw":
            print("Error, wrong Modell Type")
            return False

        storeys = ifcfile.by_type("IfcBuildingStorey")
        guidcontainer = {}
        for storey in storeys:
            # print(storey.Name)
            # print(storey.GlobalId)
            if storey.Name not in guidcontainer:
                guidcontainer[storey.Name] = storey.GlobalId
            else:
                print("Error, storey names are not unique! ... {}".format(storey.Name))
                # rasise exception

        print(json.dumps(guidcontainer, indent=4))

        for geschoss in self.SpatialData:
            if geschoss is None:
                continue
            geschoss_name = self.SpatialData[geschoss]["name"]
            if geschoss_name in guidcontainer:
                self.SpatialData[geschoss]["guid_{}".format(model_type)] = guidcontainer[geschoss_name]
            else:
                print("Error, geschoss {} aus geruest not found in ifcfile.".format(geschoss_name))
                # rasise exception

        # print(self.SpatialData)
        # print(json.dumps(self.SpatialData, indent=4))

        return True

    # ************************************************************************************************
    def reload_def_modules(self):
        # reload, nach anpassung in den def modulen ausfuehren
        importlib.reload(self.GetDefsModuleRoh)
        importlib.reload(self.GetDefsModuleTrw)
        print("Defmodules reloaded")


"""
import importlib
import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_ing_trw as runspzutrw

importlib.reload(runspzutrw)

# ANPASSEN!!!
runspzutrw.spzu_ing_trw_ortbeton_eg_decke_serialise()
runspzutrw.spzu_ing_trw_ortbeton_ug_decke_serialise()
runspzutrw.spzu_ing_trw_ortbeton_ug_bpl_serialise()
runspzutrw.spzu_ing_trw_ortbeton_eg_waende_serialise()
runspzutrw.spzu_ing_trw_ortbeton_ug_waende_serialise()
runspzutrw.spzu_ing_trw_ortbeton_ug_platvert_serialise()
runspzutrw.spzu_ing_trw_ortbeton_alle_platten_serialise()
runspzutrw.spzu_ing_trw_ortbeton_alle_vertikale_serialise()

"""


import importlib
from os.path import join
import ifcopenshell
from FreeCAD import Vector
import Part
import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)


std_workpath = "C:/0_BHA_privat/BIMGeomSerializer"
infile_ifc_trw = "infiles/4130_ING_N_TRW.ifc"


# ************************************************************************************************
def spzu_ing_trw_ortbeton_geschosse_plat_vert_serialise(
    geschoss="UG",
    plat_vert="plat",
    create_ifc=False,
    workpath=std_workpath
):

    shcutobj_def = None

    if geschoss == "UG" and plat_vert == "plat":
        rem_def = spzu_ing_trw_ortbeton_ug_bpl_get_remelements

    elif geschoss == "UG" and plat_vert == "vert":
        rem_def = spzu_ing_trw_ortbeton_ug_waende_get_remelements

    elif geschoss == "EG" and plat_vert == "plat":
        rem_def = spzu_ing_trw_ortbeton_ug_decke_get_remelements

    elif geschoss == "EG" and plat_vert == "vert":
        rem_def = spzu_ing_trw_ortbeton_eg_waende_get_remelements

    elif geschoss == "OG1" and plat_vert == "plat":
        rem_def = spzu_ing_trw_ortbeton_eg_decke_get_remelements

    if plat_vert == "plat":
        bauteilsorte = "Platten"
    elif plat_vert == "vert":
        bauteilsorte = "Vertikal"

    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_trw,
        outfile_name = "{}_{}bauteile_ING_TRW".format(geschoss, bauteilsorte),
        def_remelements = rem_def,
        def_shcutobj = shcutobj_def,
        create_ifc = create_ifc
    )

    return True


def spzu_ing_trw_ortbeton_alle_platten_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_trw,
        outfile_name = "Alle_Plattenbauteile_ING_TRW",
        def_remelements = spzu_ing_trw_ortbeton_alle_platten_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


def spzu_ing_trw_ortbeton_alle_vertikale_serialise(create_ifc=False, workpath=std_workpath):
    sertools.serialise(
        workpath = workpath,
        infile_ifc = infile_ifc_trw,
        outfile_name = "Alle_Vertikalbauteile_ING_TRW",
        def_remelements = spzu_ing_trw_ortbeton_alle_vertikale_get_remelements,
        def_shcutobj = None,
        create_ifc = create_ifc
    )
    return True


# ************************************************************************************************
def spzu_ing_trw_ortbeton_eg_decke_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Storeys")
    rem_floors = []
    rem_floors += sertools.get_by_storey_name(all_elements, "01.OG_FBOK", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_floors)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcBeam")
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcWall")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Ausgleichsschicht")
    rem_mats += sertools.get_by_material_name(all_elements, "DaemmungHart")
    rem_mats += sertools.get_by_material_name(all_elements, "FertigelementBeton")
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_ing_trw_ortbeton_ug_decke_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    # super einfach ... aktuell ist ausschliesslich komplette decke UG in NPK-C Beton
    print("Properties")
    rem_props = []
    rem_props += sertools.get_by_property_value(all_elements, "AllplanAttributes", "HB_Ortbetontyp", "NPK-C", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_props)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Get back some elements")
    get_back_elems = []
    # decke auf entrauchung oder entlueftung oder was auch immer ...
    get_back_elems += [ifc_file.by_guid("06BLfiAkP4ZQHwQw626EEd")]
    rem_elements = list(set(rem_elements).difference(set(get_back_elems)))
    print("Elements to remove: {}".format(len(rem_elements)))
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_ing_trw_ortbeton_ug_bpl_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Storeys")
    rem_floors = []
    rem_floors += sertools.get_by_storey_name(all_elements, "01.UG_FBOK", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_floors)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcFooting")
    rem_ifc_classification += ifc_file.by_type("IfcWall")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Ausgleichsschicht")
    rem_mats += sertools.get_by_material_name(all_elements, "DaemmungHart")
    rem_mats += sertools.get_by_material_name(all_elements, "FertigelementBeton")
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Names")
    rem_names = []
    rem_names += sertools.get_by_name(all_elements, "Rampe")
    sertools.add_elems_to_remelems(rem_elements, rem_names)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    # Noch besser ... 1 m ueber boden schneiden ... dann ist rampe in arc und trw
    # am gleichen org geschnitten

    print("Properties")
    rem_props = []
    rem_props += sertools.get_by_property_value(all_elements, "Allreal", "Typenname", "C4_DE_Ortbeton_0.30")
    rem_props += sertools.get_by_property_value(all_elements, "Allreal", "Typenname", "C4_DE_Ortbeton_0.18")
    sertools.add_elems_to_remelems(rem_elements, rem_props)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Get back some elements")
    get_back_elems = []
    # liftwaende
    get_back_elems += sertools.get_by_property_value(all_elements, "Allreal", "Typenname", "C2_AW_Ortbeton_0.35")
    # falsch modellierte liftwaende
    get_back_elems += [ifc_file.by_guid("3_kWvn$jLCs9tgjf4pcmvS")]
    get_back_elems += [ifc_file.by_guid("3kkq4wEjb0gPPLUVQLsxpK")]
    get_back_elems += sertools.get_by_name(all_elements, "Aussenwand UG 35cm")
    # Rampenbauteile welche auch BPL sind
    get_back_elems += [ifc_file.by_guid("1pqJoV9hL2I9cvOj_EnQdx")]
    get_back_elems += [ifc_file.by_guid("2mG$RgwkHBBQ88QBcyr834")]
    get_back_elems += [ifc_file.by_guid("1lWAyXqL532ROzmUt4K1wJ")]
    get_back_elems += [ifc_file.by_guid("37mzVMCIf7KwtEMmINy24e")]
    rem_elements = list(set(rem_elements).difference(set(get_back_elems)))
    print("Elements to remove: {}".format(len(rem_elements)))
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_ing_trw_ortbeton_eg_waende_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Storeys")
    rem_floors = []
    rem_floors += sertools.get_by_storey_name(all_elements, "00.EG_FBOK", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_floors)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcSlab")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Backstein")
    rem_mats += sertools.get_by_material_name(all_elements, "DaemmungHart")
    rem_mats += sertools.get_by_material_name(all_elements, "FertigelementBeton")
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))


    print("Properties")
    rem_props = []
    rem_props += sertools.get_by_property_value(all_elements, "AllplanAttributes", "HB_Ortbetontyp", "N/A")
    rem_props += sertools.get_by_property_value(all_elements, "AllplanAttributes", "HB_Ortbetontyp", "NPK-C")
    sertools.add_elems_to_remelems(rem_elements, rem_props)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("GUID")
    rem_guid = []
    #rem_guid += [ifc_file.by_id("3_kWvn$jLCs9tgjf4pcmvS")]
    #rem_guid += [ifc_file.by_id("3kkq4wEjb0gPPLUVQLsxpK")]
    sertools.add_elems_to_remelems(rem_elements, rem_guid)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_ing_trw_ortbeton_ug_waende_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("Storeys")
    rem_floors = []
    rem_floors += sertools.get_by_storey_name(all_elements, "01.UG_FBOK", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_floors)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcFooting")
    rem_ifc_classification += ifc_file.by_type("IfcSlab")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "DaemmungHart")
    rem_mats += sertools.get_by_material_name(all_elements, "FertigelementBeton")
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Properties")
    rem_props = []
    rem_props += sertools.get_by_property_value(all_elements, "Allreal", "Typenname", "C2_AW_Ortbeton_0.35")
    sertools.add_elems_to_remelems(rem_elements, rem_props)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("GUID")
    rem_guid = []
    rem_guid += [ifc_file.by_id("3_kWvn$jLCs9tgjf4pcmvS")]
    rem_guid += [ifc_file.by_id("3kkq4wEjb0gPPLUVQLsxpK")]
    sertools.add_elems_to_remelems(rem_elements, rem_guid)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_ing_trw_ortbeton_alle_platten_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcBeam")
    rem_ifc_classification += ifc_file.by_type("IfcColumn")
    rem_ifc_classification += ifc_file.by_type("IfcFooting")
    rem_ifc_classification += ifc_file.by_type("IfcWall")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Ausgleichsschicht")
    rem_mats += sertools.get_by_material_name(all_elements, "DaemmungHart")
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Get back some elements")
    get_back_elems = []
    # rampe
    get_back_elems += [ifc_file.by_guid("3rboTsimfBGfLp6y_5utWa")]
    get_back_elems += [ifc_file.by_guid("38GW8TEUz4PBweBR4sFWsf")]
    #get_back_elems += [ifc_file.by_guid("2PzyZTZ7r4fAJUn0Xx$qPu")]  # pruefen sollte rampenbauteil sein
    rem_elements = list(set(rem_elements).difference(set(get_back_elems)))
    print("Elements to remove: {}".format(len(rem_elements)))
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements


# ************************************************************************************************
def spzu_ing_trw_ortbeton_alle_vertikale_get_remelements(ifc_file):

    rem_elements = []
    all_elements = ifc_file.by_type("IfcBuildingElement")
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))
    print("Enities to remove: {}".format(len(set(rem_elements))))

    print("IFC-classification")
    rem_ifc_classification = []
    rem_ifc_classification += ifc_file.by_type("IfcFooting")
    rem_ifc_classification += ifc_file.by_type("IfcSlab")
    sertools.add_elems_to_remelems(rem_elements, rem_ifc_classification)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Materials")
    rem_mats = []
    rem_mats += sertools.get_by_material_name(all_elements, "Ortbeton", inverse=True)
    sertools.add_elems_to_remelems(rem_elements, rem_mats)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("Properties")
    rem_props = []
    rem_props += sertools.get_by_property_value(all_elements, "Allreal", "Typenname", "C2_AW_Ortbeton_0.35")
    rem_props += sertools.get_by_property_value(all_elements, "AllplanAttributes", "HB_eBKPH_Klasse", "C04.01_Unterzug")
    sertools.add_elems_to_remelems(rem_elements, rem_props)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    print("GUID")
    rem_guid = []
    rem_guid += [ifc_file.by_id("3_kWvn$jLCs9tgjf4pcmvS")]
    rem_guid += [ifc_file.by_id("3kkq4wEjb0gPPLUVQLsxpK")]
    sertools.add_elems_to_remelems(rem_elements, rem_guid)
    print("All IfcBuildingall_elements: {}".format(len(all_elements)))

    return all_elements, rem_elements

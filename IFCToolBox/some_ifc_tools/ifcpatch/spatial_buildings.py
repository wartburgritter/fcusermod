import ifcopenshell
import ifcpatch  # in BlenderBIM pfad nicht hinzufuegen !!!

def get_building_ids_for_ifcpatch(ifcfile, building):
    # funktioniert nicht gut bei identischen gebaeudenamen
    print(building)
    building_ids_str = ""
    query = "IfcBuildingElement, IFC_Attributes_as_Properties.Gebaeudename=/^{}/".format(building.Name)
    elems = ifcopenshell.util.selector.filter_elements(ifcfile, query)
    print(len(elems))
    if len(elems) == 0:
        print("No elems found. Building guid will be returned.")
        return building.GlobalId
    for elem in elems:
        building_ids_str += "{}, ".format(elem.GlobalId)
    return building_ids_str.rstrip(", ")


# infilename = "22092_K4LZ_ING_HB_N_TRW_org"
infilename = "22092_K4LZ_ING_HB_N_TRW_20240822yyy_bimtester"
# infilename = "4115_HLKS_GM"

in_ifcfile = ifcopenshell.open("C:/Users/BHA/Desktop/" + infilename + ".ifc")
buildings = in_ifcfile.by_type("IfcBuilding")


# *************************************************************************************************
# *************************************************************************************************
# *************************************************************************************************
# kopiere alle obigen zeilen in BlenderBIM
# kopiere einen der unteren blocks in BlenderBIM

# ************** alle
for i, building in enumerate(buildings):
    building_ids = get_building_ids_for_ifcpatch(in_ifcfile, building)
    out_ifcfile = ifcpatch.execute({
        "input": in_ifcfile,
        "file": in_ifcfile,
        "recipe": "ExtractElements",
        "arguments": [building_ids],
    })
    ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/{}_{}_{}.ifc".format(infilename, i, building.Name))
# to get the enter
# to get the enter


# ************** einzeln
abuilding = buildings[-1]
print(abuilding)
building_ids = get_building_ids_for_ifcpatch(in_ifcfile, abuilding)
out_ifcfile = ifcpatch.execute({
    "input": in_ifcfile,
    "file": in_ifcfile,
    "recipe": "ExtractElements",
    "arguments": [building_ids],
})
ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/{}_{}.ifc".format(infilename, abuilding.Name))

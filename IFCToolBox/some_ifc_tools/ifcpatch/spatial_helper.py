# ***************************************************************************************
# ***************************************************************************************
# ***************************************************************************************
# buidlings
#
# ***************************************************************************************
# building test code
s=ifcfile.by_type("IfcSlab")[0]
b=ifcfile.by_type("IfcBuilding")[1]

s.ContainedInStructure
s.ContainedInStructure[0].RelatingStructure.is_a("IfcBuildingStorey")
storey = s.ContainedInStructure[0].RelatingStructure
storey.Decomposes
storey.Decomposes[0].RelatingObject.is_a("IfcBuilding")

s.ContainedInStructure
s.ContainedInStructure[0].RelatingStructure.is_a("IfcBuildingStorey")
s.ContainedInStructure[0].RelatingStructure.Decomposes[0].RelatingObject.is_a("IfcBuilding")
s.ContainedInStructure[0].RelatingStructure.Decomposes[0].RelatingObject.GlobalId == b.GlobalId



def is_in_building(element, building):
    return (
        element.ContainedInStructure
        and element.ContainedInStructure[0].RelatingStructure.is_a("IfcBuildingStorey")
        and element.ContainedInStructure[0].RelatingStructure.Decomposes[0].RelatingObject.is_a("IfcBuilding")
        and element.ContainedInStructure[0].RelatingStructure.Decomposes[0].RelatingObject.GlobalId == building.GlobalId
    )

is_in_building(s,b)


# ***************************************************************************************
# more save idea for building, but not really tested
def is_in_building(element, building):
    try:
        element.ContainedInStructure
        element.ContainedInStructure[0].RelatingStructure.is_a("IfcBuildingStorey")
        element.ContainedInStructure[0].RelatingStructure.Decomposes[0].RelatingObject.is_a("IfcBuilding")
        return element.ContainedInStructure[0].RelatingStructure.Decomposes[0].RelatingObject.GlobalId == building.GlobalId
    except Exception:
        return False  # happens if building has not Storey


# ***************************************************************************************
# old building code
elements = in_ifcfile.by_type("IfcElement")
def is_in_building(element, building):
    return (
        element.ContainedInStructure
        and element.ContainedInStructure[0].RelatingStructure.is_a("IfcBuildingStorey")
        and element.ContainedInStructure[0].RelatingStructure.Decomposes[0].RelatingObject.is_a("IfcBuilding")
        and element.ContainedInStructure[0].RelatingStructure.Decomposes[0].RelatingObject.GlobalId == building.GlobalId
    )
def get_building_ids_for_ifcpatch(elements, building):
    print(building)
    building_ids_str = ""
    for elem in elements:
        if is_in_building(elem, building):
            # print(elem.GlobalId)
            building_ids_str += "{}, ".format(elem.GlobalId)
    return building_ids_str.rstrip(", ")


# ***************************************************************************************
# use ifcos filter
from ifcopenshell.util import selector
query = "IfcBuildingElement, IFC_Attributes_as_Properties.Gebaeudename=/^Gebaeude_TG/"
len(ifcopenshell.util.selector.filter_elements(ifcfile, query))

import ifcopenshell
import ifcpatch  # in BlenderBIM pfad nicht hinzufuegen !!!

def is_in_storey(element, storey):
    return (
        element.ContainedInStructure
        and element.ContainedInStructure[0].RelatingStructure.is_a("IfcBuildingStorey")
        and element.ContainedInStructure[0].RelatingStructure.GlobalId == storey.GlobalId
    )


# implementation mit filter auf geschossnamen nicht moeglich, da geschossnamen gleich sein koenten


def get_storey_ids_for_ifcpatch(elements, storey):
    print(storey)
    storey_ids_str = ""
    for elem in elements:
        if is_in_storey(elem, storey):
            # print(elem.GlobalId)
            storey_ids_str += "{}, ".format(elem.GlobalId)
    return storey_ids_str.rstrip(", ")


infilename = "22092_K4LZ_ING_HB_N_TRW_org"
# infilename = "4115_HLKS_GM"

in_ifcfile = ifcopenshell.open("C:/Users/BHA/Desktop/" + infilename + ".ifc")
storeys = in_ifcfile.by_type("IfcBuildingStorey")
elements = in_ifcfile.by_type("IfcElement")


# *************************************************************************************************
# *************************************************************************************************
# *************************************************************************************************
# kopiere alle obigen zeilen in BlenderBIM
# kopiere einen der unteren blocks in BlenderBIM

# ************** alle
for i, storey in enumerate(storeys):
    storey_ids = get_storey_ids_for_ifcpatch(elements, storey)
    out_ifcfile = ifcpatch.execute({
        "input": in_ifcfile,
        "file": in_ifcfile,
        "recipe": "ExtractElements",
        "arguments": [storey_ids],
    })
    ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/{}_storey_{}_{}.ifc".format(infilename, i, storey.Name))
# to get the enter
# to get the enter


# ************** einzeln
storey_ids = get_storey_ids_for_ifcpatch(elements, storeys[-1])
out_ifcfile = ifcpatch.execute({
    "input": in_ifcfile,
    "file": in_ifcfile,
    "recipe": "ExtractElements",
    "arguments": [storey_ids],
})
ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/{}_patched3.ifc".format(infilename))


# ************** alle bis "OG"
all_ids = None
for i, storey in enumerate(storeys):
    if storey.Name.startswith("OG"):
        stop = True
    else:
        stop = False
    storey_ids = get_storey_ids_for_ifcpatch(elements, storey)
    if i == 0:
        all_ids = storey_ids
    else:
        all_ids += ", {}".format(storey_ids)
    if stop is True:
        break

all_ids = all_ids.rstrip(", ")
out_ifcfile = ifcpatch.execute({
    "input": in_ifcfile,
    "file": in_ifcfile,
    "recipe": "ExtractElements",
    "arguments": [all_ids],
})
ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/{}_patched_alle_bis_benanntes.ifc".format(infilename))


# ************** test
ids = []
for elem in elements:
    if is_in_storey(elem, storeys[-1]):
        ids.append(elem.GlobalId)

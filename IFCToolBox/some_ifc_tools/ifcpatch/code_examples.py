import ifcopenshell

import sys
sys.path.append("C:/0_BHA_privat/code_ifcopenshell/official/IfcOpenShell/src/ifcpatch")
import ifcpatch


# lark muss vorhanden sein, in std FreeCAD nicht installiert


# frueher mit logger?
# https://community.osarch.org/discussion/10/ifcpatch-tool-now-available
# farben, material, psets, ... alles wird mitgeschrieben wie es aussieht


# problem auf Windows ... source of ifcpatch passt nicht zu ifcopenshell in FreeCAD, dann problem
# in BlenderBIM ausführen

# es gibt neue version fuer arguments und alte siehe auch bimpatch_tools.py in BIMWorker

def patch_andwrite(in_ifcfile):
    out_ifcfile = ifcpatch.execute({
        "input": in_ifcfile,
        "file": in_ifcfile,
        "recipe": "ExtractElements",
        #"arguments": ["IfcColumn, IfcSlab"],
        "arguments": [".IfcColumn | .IfcMember"],
        #"arguments": ["#2M522ywzb9t8uspmghvkUb"],
    })
    ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/example_patched1.ifc")

# import importlib
# importlib.reload(ifcpatch)
patch_andwrite(in_ifcfile)


# *******************************************************************************************
in_ifcfile = ifcopenshell.open("C:/Users/BHA/Desktop/example_org.ifc")
out_ifcfile = ifcpatch.execute({
    "input": in_ifcfile,
    "file": in_ifcfile,
    "recipe": "ExtractElements",
    #"arguments": ["IfcColumn, IfcSlab"],
    "arguments": [".IfcColumn | .IfcMember"],
    #"arguments": ["#2M522ywzb9t8uspmghvkUb"],
})
ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/example_patched1.ifc")


in_ifcfile = ifcopenshell.open("C:/Users/BHA/Desktop/example_org.ifc")
out_ifcfile = ifcpatch.execute({
    "input": in_ifcfile,
    "file": in_ifcfile,
    "recipe": "ExtractElements",
    "arguments": ["1$5SfmKH13qgg$IzimnApS, 2xEy9_cSr8FB7jG63VKcQk"],
})
ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/example_patched2.ifc")


in_ifcfile = ifcopenshell.open("C:/Users/BHA/Desktop/4115_HLKS_GM.ifc")
out_ifcfile = ifcpatch.execute({
    "input": in_ifcfile,
    "file": in_ifcfile,
    "recipe": "ExtractElements",
    "arguments": ["IfcFlowTerminal"],
})
ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/4115_HLKS_GM_patched1.ifc")


# *******************************************************************************************
# *******************************************************************************************
# *******************************************************************************************
in_ifcfile = ifcopenshell.open("C:/0_BHA_privat/BIMzIFCs/4130_ARC_ROHM01_20230224.ifc")


out_ifcfile = ifcpatch.execute({
    "input": "C:/0_BHA_privat/BIMzIFCs/4130_ARC_ROHM01_20230224.ifc",
    "file": in_ifcfile,
    "recipe": "ExtractElements",
    "arguments": [".IfcWall"],
})  # braucht schon etwas zeit ...
ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/problem_obj.ifc")


out_ifcfile = ifcpatch.execute({
    "input": "C:/0_BHA_privat/BIMzIFCs/4130_ARC_ROHM01_20230224.ifc",
    "file": in_ifcfile,
    "recipe": "ExtractElements",
    "arguments": [".IfcSlab | .IfcBeam"],
})  # viel schneller ...
ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/problem_obj.ifc")


out_ifcfile = ifcpatch.execute({
    "input": "C:/0_BHA_privat/BIMzIFCs/4130_ARC_ROHM01_20230224.ifc",
    "file": in_ifcfile,
    "recipe": "ExtractElements",
    "arguments": ["#1ibPztTXv6nxQi4njr7ag7"],
})  # viel schneller < 1 sek
ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/problem_obj.ifc")


out_ifcfile = ifcpatch.execute({
    "input": "C:/0_BHA_privat/BIMzIFCs/4130_ARC_ROHM01_20230224.ifc",
    "file": in_ifcfile,
    "recipe": "ExtractElements",
    "arguments": ["#1ibPztTXv6nxQi4njr7ag7 | .IfcColumn"],
})  # viel schneller < 1 sek
ifcpatch.write(out_ifcfile, "C:/Users/BHA/Desktop/problem_obj.ifc")





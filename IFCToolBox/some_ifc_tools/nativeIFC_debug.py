# ************************************************************************************************
# move the boxes to the boundbox of the project without the boxes


# ************************************************************************************************
import importlib
import ifc_tools

importlib.reload(ifc_tools)
p=ifcfile[354]
p
elements = ifc_tools.filter_elements([p], ifcfile)
elements
ifc_tools.get_geom_iterator(ifcfile, elements)

# wenn iterator object und keine geometry ... 
iterator = ifc_tools.get_geom_iterator(ifcfile, elements)
i = 0
while True:
    item = iterator.get()
    if item:
        print("Item: {}".format(i))
        brep = item.geometry.brep_data
        print("BREP, len:  {}".format(len(brep)))
        shape = Part.Shape()
        shape.importBrepFromString(brep, False)
        print("Part Shape:  {}, {}, {}, {}".format(shape, shape.ShapeType, len(shape.Solids), shape.Volume))
        # Part.show(shape)
        mat = ifc_tools.get_matrix(item.transformation.matrix.data)
        print("Transform Matrix:  {}".format(mat))
        shape.scale(1000)
        shape.transformShape(mat)
        Part.show(shape)
    i += 1
    if not iterator.next():
        break

# ************************************************************************************************
# pures IfcOpenShell
import ifcopenshell
from ifcopenshell import geom
ifcfile = ifcopenshell.open("C:/Users/BHA/Desktop/two_boxes.ifc")
settings = ifcopenshell.geom.settings()
settings.set(settings.DISABLE_TRIANGULATION, True)
settings.set(settings.USE_BREP_DATA,True)
settings.set(settings.SEW_SHELLS,True)
aproduct = ifcfile.by_type("IfcBuildingElement")[0]
print(aproduct)
iterator = ifcopenshell.geom.iterator(settings, ifcfile, 1, include=[aproduct])
iterator.initialize()


# akutelle version ohne mein PR
import ifcopenshell
import ifc_tools
from ifcopenshell import geom
settings = ifc_tools.get_settings(ifcfile)
iterator = ifcopenshell.geom.iterator(settings, ifcfile, 1, include=[ifcfile[83]])
iterator.initialize()

# ************************************************************************************************
import ifcopenshell
from ifcopenshell import file as init_newifcfile
from some_ifc_tools.write_new_ifc_tools import add_min_spatial_structure
anew_ifc = init_newifcfile(schema=ifcfile.schema)
#
# building elements or aggregate when no geometry but Aggregation to BuildingElementParts
# anew_ifc.add(ifcfile[339147])
anew_ifc.add(ifcfile.by_type("IfcBuildingElementProxy")[0])
#
anew_ifc = add_min_spatial_structure(ifcfile, anew_ifc)
anew_ifc.write("C:/Users/BHA/Desktop/serializer_test.ifc")


# ************************************************************************************************
# load brep into strin and create a representation out of it
from ifcopenshell import geom
abrep_file = open("C:/Users/BHA/Desktop/some_geom_scaled.brep", "r")
brep_data = abrep_file.read()
abrep_file.close()
try:
    proddefshape = geom.serialise(brep_data)
except TypeError:
    # IfcOpenShell v0.6.0
    proddefshape = geom.serialise("IFC4", brep_data)

if proddefshape:
    print(proddefshape)
else:
    print("Serializer did not return a shape definition")


# ************************************************************************************************
# create a new ifc from a loaded ifcfile and add the proddefshape from brep geometry
import ifcopenshell
from ifcopenshell import file as init_newifcfile
from some_ifc_tools.write_new_ifc_tools import add_min_spatial_structure
anew_ifc = init_newifcfile(schema=ifcfile.schema)

# add the geom object as building element to the ifcfile
product = anew_ifc.create_entity(
    "IfcBuildingElementProxy",
    GlobalId=ifcopenshell.guid.new(),
    Name="My Buialding Element",
    Representation=proddefshape,
)

anew_ifc = add_min_spatial_structure(ifcfile, anew_ifc)
anew_ifc.write("C:/Users/BHA/Desktop/some_geom_scaled_ifccopy.ifc")

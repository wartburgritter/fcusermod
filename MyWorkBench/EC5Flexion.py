# -*- coding: utf-8 -*-
# FreeCAD init script of the Eurocodes module
# (c) 2013 Jonathan Wiedemann

# ***************************************************************************
# *   (c) Jonathan Wiedemann (jonatan@wiedemann.fr) 2013                    *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# *   Jonathan Wiedemann 2013                                               *
# ***************************************************************************/

import FreeCAD
import FreeCADGui


class EC5_CALCULATION1():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def GetResources(self):
        return {'Pixmap': 'python', 'MenuText': 'EC5 Poutre', 'ToolTip': 'EC5 Poutre'}

    def Activated(self):
        print('Command 1')
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1 :
            o = selection[0]
            print('Activated Object: ',o.Name)
            if hasattr(o,"Shape"):
              print('Volume = ',o.Shape.Volume)
        else:
            print("bin in der anderen schleife")


class EC5_CALCULATION2():
    def IsActive(self):
                return bool(FreeCADGui.Selection.getSelectionEx())

    def GetResources(self):
                return {'Pixmap': 'python', 'MenuText': 'EC5 Poutre', 'ToolTip': 'EC5 Poutre'}

    def Activated(self):
        print('Command 2')
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1 :
            o = selection[0]
            print('Activated Object: ',o.Name)
            if hasattr(o,"Shape"):
              print('Area = ',o.Shape.Area)
        else:
            print("bin in der anderen schleife")


class MYIFCIMPORT():
    def GetResources(self):
       return {'Pixmap': 'python', 'MenuText': 'EC5 Poutre', 'ToolTip': 'EC5 Poutre'}

    def Activated(self):
       import  importIFC
       importIFC.open("/home/hugo/Desktop/test/box.ifc")
       # um file zu importieren braucht es dateiname
       # mach doch erstma icons fuer die ganzen einfaerbungen und volumenvergleich


FreeCADGui.addCommand('EC5_Calculation1', EC5_CALCULATION1())
FreeCADGui.addCommand('EC5_Calculation2', EC5_CALCULATION2())
FreeCADGui.addCommand('MyIfcImport', MYIFCIMPORT())

# ***************************************************************************
# *                                                                         *
# *   Copyright (c) 2016 - Bernd Hahnebach <bernd@bimstatik.org>            *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************


import FreeCAD
import os
import Part


__title__ = "FreeCAD Cedrus slab geometry reader"
__author__ = "Bernd Hahnebach "
__url__ = "http://www.freecadweb.org"


Debug = False

if open.__module__ == '__builtin__':
    pyopen = open  # because we'll redefine open below


def open(filename):
    "called when freecad opens a file"
    docname = os.path.splitext(os.path.basename(filename))[0]
    insert(filename, docname)


def insert(filename, docname):
    "called when freecad wants to import a file"
    try:
        doc = FreeCAD.getDocument(docname)
    except NameError:
        doc = FreeCAD.newDocument(docname)
    FreeCAD.ActiveDocument = doc
    import_cedrus_sin(filename)
    if FreeCAD.GuiUp:
        import FreeCADGui
        FreeCADGui.SendMsgToActiveView("ViewFit")
        FreeCADGui.activeDocument().activeView().viewAxonometric()


def import_cedrus_sin(filename, level_data=None):
    '''insert Cedrus slab sin geometry in the ActiveDocument
    '''
    sf = 1000  # scale factor --> Cedrus uses Meter, FreeCAD uses mm

    if level_data is None:
        coord = (0.0, 0.0, 0.0)
        #angle = 0.0
        descr = 'cedrus_slab'
    else:
        coord = (level_data[2][0] * sf, level_data[2][1] * sf, level_data[2][2] * sf)
        #angle = level_data[3]
        descr = level_data[4]

    sin_slab_data = read_cedrus_sin(filename)
    print(filename)
    print(descr)
    # print(sin_slab_data)

    # slab outline
    slab_outline = None
    if sin_slab_data['U']:
        print('  Found slab outlines.')
        outline_lines = []
        for sin_line in sin_slab_data['U']:
            outline_lines.append(makeCedrusEdge(sin_line, sf))
        w = Part.Wire(outline_lines)
        f = Part.Face(w)
        slab_outline = f
    else:
        print('  No slab outlines found.')

    # slab openings
    slab_openings = []
    if sin_slab_data['A']:
        print('  Found slab openings.')
        start_point = True
        vstart = FreeCAD.Vector(0, 0, 0)
        for no, sin_line in enumerate(sin_slab_data['A']):
            l = makeCedrusEdge(sin_line, sf)
            if start_point is True:
                vstart = l.Vertexes[0].Point
                start_point = False
                opening_lines = []
                opening_lines.append(l)
            else:
                opening_lines.append(l)
                if l.Vertexes[1].Point == vstart:  # closed opening
                    start_point = True
                    w = Part.Wire(opening_lines)
                    f = Part.Face(w)
                    slab_openings.append(f)
    if slab_outline:
        for o in slab_openings:
            slab_outline = slab_outline.cut(o)
    else:
        print('  No slab openings found.')
    if slab_outline:
        for o in slab_openings:
            slab_outline = slab_outline.cut(o)

    # slab boarders material
    slab_material_borders = []
    if sin_slab_data['GM']:
        print('  Found slab boarders for materials.')
        slab_material_borders = slab_outline.Edges
        for sin_line in sin_slab_data['GM']:
            slab_material_borders.append(makeCedrusEdge(sin_line, sf))
        if slab_outline:
            w = slab_material_borders[0]
            for no, e in enumerate(slab_material_borders):
                no += 1
                if no > 1:
                    w = w.fuse(e)
            ex = w.extrude(FreeCAD.Vector(0, 0, 5))
            slab_materials = slab_outline.cut(ex)
            if len(slab_materials.Shells) == 1:
                slab_outline = slab_materials.Shells[0]
            else:
                print('Could not make one shell out of all material faces!')
                slab_outline = slab_materials
    else:
        print('  No slab boarders for materials found.')

    # slab boarders area supports
    slab_area_supports = []
    if sin_slab_data['GF']:
        if slab_outline:
            # slab has at least one area support
            # it is not allowed to have area supports outside the slab geometry
            # better implementation needed to get the faces of the slab geometry
            # like the material boarder
            print('  Found slab area supports.')
            for sin_line in sin_slab_data['GF']:
                obj = FreeCAD.ActiveDocument.addObject('Part::Feature', 'slab_area_support')
                obj.Shape = makeCedrusEdge(sin_line, sf)
                obj.Placement.Base = coord
        else:
            # no slab geometry, we could have mutliple areas with area supports in this slab
            # make a face for each area support
            print('  Found area supports without slab geometry.')
            start_point = True
            vstart = FreeCAD.Vector(0, 0, 0)
            for sin_line in sin_slab_data['GF']:
                l = makeCedrusEdge(sin_line, sf)
                if start_point is True:
                    vstart = l.Vertexes[0].Point
                    start_point = False
                    area_support_lines = []
                    area_support_lines.append(l)
                else:
                    area_support_lines.append(l)
                    if l.Vertexes[1].Point == vstart:  # closed area support
                        start_point = True
                        w = Part.Wire(area_support_lines)
                        f = Part.Face(w)
                        slab_area_supports.append(f)
    else:
        print('  No slab boarders for area supports found.')

    # slab_material_boxes
    if sin_slab_data['D']:
        print('  Found slab material boxes.')
        for mbox in sin_slab_data['D']:
            print(mbox)
            p = FreeCAD.Vector(mbox[0][0] * sf, mbox[0][1] * sf, 0)
            v = Part.Vertex(p)
            Part.show(v)
            # http://forum.freecadweb.org/viewtopic.php?f=22&t=16079&start=10#p127536
    else:
        print('  No slab material boxes found.')

    # walls
    walls = []
    if sin_slab_data['W']:
        print('  Found walls.')
        for wall in sin_slab_data['W']:
            # print(wall)
            wall_points = []
            wall_lines = []
            for wall_point in wall[0]:
                wall_points.append(FreeCAD.Vector(wall_point[0] * sf, wall_point[1] * sf, 0))
            for no, wall_point in enumerate(wall_points):
                if no > 0:
                    l = Part.makeLine(wall_points[no - 1], wall_points[no])
                    wall_lines.append(l)
                no += 1
            # make a wire from the lines by fuse them, Part.Wire koente hier auch gehen
            # ist eine non-manifold wall moeglich?!
            # test eine wand zues viereck und dann weiter?
            w = wall_lines[0]
            for no, e in enumerate(wall_lines):
                no += 1
                if no > 1:
                    w = w.fuse(e)
            walls.append(w)
        wall_faces = []
        for w in walls:
            wall_faces.append(w.extrude(FreeCAD.Vector(0, 0, -2000)))
        walls = wall_faces
        # cut slab_outline mit jedem extrudierten wire
        # FEM_fix mit alles wire
        # mesh, material, thickness (vorerst eine fuer alle) frequenzy analysis :-)
    else:
        print('  No walls found.')

    #######################
    # create freecad shapes
    if walls:
        obj = FreeCAD.ActiveDocument.addObject('Part::Feature', 'Walls')
        obj.Shape = Part.makeCompound(walls)
        obj.Placement.Base = coord

    if slab_area_supports:
        for f in slab_area_supports:
            obj_name = descr + '_area_support'
            obj = FreeCAD.ActiveDocument.addObject('Part::Feature', obj_name)
            obj.Shape = f
            obj.Placement.Base = coord

    if slab_outline:
        obj = FreeCAD.ActiveDocument.addObject('Part::Feature', descr)
        obj.Shape = slab_outline
        obj.Placement.Base = coord

    FreeCAD.ActiveDocument.recompute()


def read_cedrus_sin(cedrus_sin_filepath):
    ''' reads a Cedrus slab geometry file STRUKTUR_Recovery1.SIN
        and extracts the geometry
    '''

    outline_lines = []
    opening_lines = []
    boarder_lines_materials = []
    boarder_lines_area_supports = []
    mat_box = []
    material_boxes = []
    wall_coordinates = []
    walls = []
    read_wall_coordination_line = False

    cedrus_sin_file = pyopen(cedrus_sin_filepath, "r")
    # mesh_info = cedrus_sin_file.readline().strip().split()

    start_reading_geometry = False

    for line in cedrus_sin_file:
        if line.startswith('MS_END'):
            start_reading_geometry = True

        if start_reading_geometry:
            linecolumns = line.split()
            if line.startswith('U'):
                # outline line
                px1 = float(linecolumns[1])
                py1 = float(linecolumns[2])
                px2 = float(linecolumns[3])
                py2 = float(linecolumns[4])
                outline_lines.append((px1, py1, px2, py2))
            elif line.startswith('A'):
                # opening line
                px1 = float(linecolumns[1])
                py1 = float(linecolumns[2])
                px2 = float(linecolumns[3])
                py2 = float(linecolumns[4])
                opening_lines.append((px1, py1, px2, py2))
            elif line.startswith('G'):
                # boarder line
                px1 = float(linecolumns[1])
                py1 = float(linecolumns[2])
                px2 = float(linecolumns[3])
                py2 = float(linecolumns[4])
                if linecolumns[5] == 'M':  # material boarder line
                    boarder_lines_materials.append((px1, py1, px2, py2))
                elif linecolumns[5] == 'F':  # area support boarder line
                    boarder_lines_area_supports.append((px1, py1, px2, py2))
            elif line.startswith('D'):
                # material box, first line
                px1 = float(linecolumns[4])
                py1 = float(linecolumns[5])
                mat_box.append((px1, py1))
            elif mat_box:
                # material box, second line
                mat_box.append(float(linecolumns[1]))  # thickness
                material_boxes.append(mat_box)
                mat_box = []
            elif line.startswith('W'):
                # wall, first line
                px1 = float(linecolumns[4])
                py1 = float(linecolumns[5])
                wall_coordinates.append((px1, py1))
            elif wall_coordinates and line.startswith('  L'):
                read_wall_coordination_line = True
            elif wall_coordinates and read_wall_coordination_line:
                if not line.startswith('   B'):
                    # wall continues
                    px1 = float(linecolumns[0])
                    py1 = float(linecolumns[1])
                    wall_coordinates.append((px1, py1))
                read_wall_coordination_line = False
            elif wall_coordinates and line.startswith('  A'):
                # wall, line with thickness and axis position
                wall_coordinates = tuple(wall_coordinates)
                ax = linecolumns[1][0]  # axis_position
                th = float(linecolumns[2])  # thickness
                wall = (wall_coordinates, ax, th)
                walls.append(wall)
                wall_coordinates = []

    cedrus_sin_file.close()

    sin_slab_data = {
        # [(x1,y1,x2,y2), (x1,y1,x2,y2), ...]
        'U': outline_lines,
        # [(x1,y1,x2,y2), (x1,y1,x2,y2), ...]
        'A': opening_lines,
        # [(x1,y1,x2,y2), (x1,y1,x2,y2), ...]
        'GM': boarder_lines_materials,
        # [(x1,y1,x2,y2), (x1,y1,x2,y2), ...]
        'GF': boarder_lines_area_supports,
        # [[(x1,y1), thickness], [(x1,y1), thickness], ...]
        'D': material_boxes,
        # [(((x1,y1), (x2,y2), ...), axis_position, thickness), (((x1,y1), (x2,y2), ...), axis_position, thickness), ...]
        'W': walls}

    return sin_slab_data


# helpers
def makeCedrusEdge(sin_line, sf):
    v1 = FreeCAD.Vector(sin_line[0] * sf, sin_line[1] * sf, 0)
    v2 = FreeCAD.Vector(sin_line[2] * sf, sin_line[3] * sf, 0)
    l = Part.makeLine(v1, v2)
    return l

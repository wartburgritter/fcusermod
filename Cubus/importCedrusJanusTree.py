# ***************************************************************************
# *                                                                         *
# *   Copyright (c) 2016 - Bernd Hahnebach <bernd@bimstatik.org>            *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************


import FreeCAD
import os
import importCedrusSin


__title__ = "FreeCAD Cedrus building geometry reader"
__author__ = "Bernd Hahnebach "
__url__ = "http://www.freecadweb.org"


Debug = False

if open.__module__ == '__builtin__':
    pyopen = open  # because we'll redefine open below


def open(filename):
    "called when freecad opens a file"
    docname = os.path.splitext(os.path.basename(filename))[0]
    insert(filename, docname)


def insert(filename, docname):
    "called when freecad wants to import a file"
    try:
        doc = FreeCAD.getDocument(docname)
    except NameError:
        doc = FreeCAD.newDocument(docname)
    FreeCAD.ActiveDocument = doc
    import_cedrus_building_tree(filename)
    if FreeCAD.GuiUp:
        import FreeCADGui
        FreeCADGui.SendMsgToActiveView("ViewFit")
        FreeCADGui.activeDocument().activeView().viewAxonometric()


def import_cedrus_building_tree(filename):
    '''insert Cedrus builing geometry recovery data in the ActiveDocument
    '''
    building_path = filename.rstrip('/INP/janusTree.recov') + '/'

    building_tree = read_cedrus_building_tree(filename)

    for level in building_tree:
        level_data = building_tree[level]
        # print(level, ' --> ', level_data)

        sin_path = building_path + level_data[1] + '/INP/STRUKTUR_Recovery1.SIN'
        # print(sin_path)
        # print(level_data)
        print('Start reading' + sin_path)
        importCedrusSin.import_cedrus_sin(sin_path, level_data)
        print('Finished reading' + sin_path)


def read_cedrus_building_tree(cedrus_building_tree_filepath):
    ''' reads a Cedrus building geometry file janusTree.recov
        and extracts the geometry
    '''
    builting_tree_file = pyopen(cedrus_building_tree_filepath, "r")

    building_tree_data = {}
    storey_data = {}
    for line in builting_tree_file:
        line = line.strip()  # get rid of newline
        storey_string = '| +-Story<50>'
        if line.startswith(storey_string):
            folder_in_brackets = line.lstrip(storey_string).rstrip(']')
            folder = folder_in_brackets.split('[')[1] + '.C6F'
            storey_data[folder] = folder

        level_string = '| |   Name<37>='
        coord_string = '| |   CoordD3<146>='
        angle_string = '| |   Angle<166>='
        descr_string = '| |   Description<189>='
        if storey_data and line.startswith(level_string):
            level = line.lstrip(level_string)
            storey_data[level] = level
        elif storey_data and line.startswith(coord_string):
            coord_with_slash = line.lstrip(coord_string)
            coord_list = coord_with_slash.split('/')
            coord = (float(coord_list[0]), float(coord_list[1]), float(coord_list[2]))
            storey_data[coord] = coord
        elif storey_data and line.startswith(angle_string):
            angle = float(line.lstrip(angle_string))
            storey_data[angle] = angle
        elif storey_data and line.startswith(descr_string):
            descr = line.lstrip(descr_string)
            storey_data[descr] = descr

            building_tree_data[storey_data[level]] = (storey_data[level],
                                                      storey_data[folder],
                                                      storey_data[coord],
                                                      storey_data[angle],
                                                      storey_data[descr])
            storey_data = {}

    builting_tree_file.close()
    return building_tree_data

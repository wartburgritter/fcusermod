# ***************************************************************************
# *   Copyright (c) 2020 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

import importlib
import os

# import FreeCAD

import raumbuchtools


if open.__module__ == "__builtin__":
    # because we'll redefine open below (Python2)
    pyopen = open
elif open.__module__ == "io":
    # because we'll redefine open below (Python3)
    pyopen = open


def open(filename):

    # rds ... room data sheets ... Raumbuch
    importlib.reload(raumbuchtools)

    print("Start: run quality gate for room data sheets.")
    # run quality gate for rds
    qgate_rds = raumbuchtools.ifcfilepath2qgrsd(filename)
    file_name_qgate_rds = os.path.join(
        os.path.dirname(filename),
        os.path.splitext(os.path.basename(filename))[0] + "_qg-rb.yml"
    )
    raumbuchtools.qgaterds2yaml(file_name_qgate_rds, qgate_rds)
    raumbuchtools.qgate_summary(qgate_rds)
    print("Finished: run quality gate for room data sheets.")

    # get room data sheets
    print("Start: creating room data sheets.")
    rds = raumbuchtools.ifcfilepath2rds(filename)
    print("Finished: creating room data sheets.")

    # export yaml
    print("Start: exporting room data sheets to json/yaml file.")
    file_name_rds = os.path.join(
        os.path.dirname(filename),
        os.path.splitext(os.path.basename(filename))[0] + "_rb.yml"
    )
    raumbuchtools.rds2yaml(file_name_rds, rds)
    print("Finished: exporting room data sheets to json/yaml file.")

    # export excel
    print("Start: exporting room data sheets to excel file.")
    file_name_rds = os.path.join(
        os.path.dirname(filename),
        os.path.splitext(os.path.basename(filename))[0] + "_rb.xlsx"
    )
    raumbuchtools.rds2excel(file_name_rds, rds)
    print("Finished: exporting room data sheets to excel file.")

    return rds

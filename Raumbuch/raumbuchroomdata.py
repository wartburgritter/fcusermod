# ***************************************************************************
# *   Copyright (c) 2020 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************


class RoomData():
    def __init__(self, pid):

        # no mapping
        self.Pid = pid  # the ifc id, unique in one file
        self.GlobalId = ""  # worldwide unique

        # entity mapping
        self.Number = ""  # id given by the Architect, should be uniqe in one building
        self.Name = ""
        self.Description = ""
        self.LongName = ""
        self.InteriorOrExterior = ""  # None/Interior/Exterior, What is an exterior room?

        # pset mapping
        self.Category = ""
        self.CoveringFloor = ""
        self.CoveringWall = ""
        self.CoveringCeiling = ""
        self.RoomUse = ""

        # extended1
        self.Area = ""
        self.Perimeter = ""
        self.Category = ""
        self.Level = ""
        self.Occupancy = ""

        # extended2
        self.CostCenter = ""


# Site
# Gebaeude
# Geschoss
# Grundstueck
# Standort
# Fluchtweg
"""
Zone ObjectType Submeter;;Constraints

Traceback (most recent call last):
  File "<string>", line 1, in <module>
  File "/home/hugo/.FreeCAD/Mod/Raumbuch/raumbuch_create_fromifc.py", line 45, in open
    qgate_rds = raumbuchtools.ifcfilepath2qgrsd(filename)
  File "/home/hugo/.FreeCAD/Mod/Raumbuch/raumbuchtools.py", line 55, in ifcfilepath2qgrsd
    qualitygate_rds = ifcos2qgrds(ifcfile)
  File "/home/hugo/.FreeCAD/Mod/Raumbuch/raumbuchtools.py", line 110, in ifcos2qgrds
    elif prop_value != prop_value.rstrip() or prop_value != prop_value.lstrip():
<class 'AttributeError'>: 'float' object has no attribute 'rstrip'
hugo@Ahorn:~$ 
"""

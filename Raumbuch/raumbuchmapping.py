# ***************************************************************************
# *   Copyright (c) 2020 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

"""
see module raumbuchpropoutput
print the properties of a IfcSpace from a ifc file and set the mappings
"""


def get_entity_mapping():
    """
    attributes of IfcSpace entity
    attrib class RoomData, identifier is room : attrib entity IfcSpace, identifier is ifcspace
    """

    emapping1 = {
        "Number": "Name",
        "Name": "LongName",
        "Description": "Description",
        "InteriorOrExterior": "InteriorOrExteriorSpace",
    }

    emapping2 = emapping1

    emapping3 = emapping1

    return emapping1


def get_pset_mapping():
    """
    attributes of IfcSpace psets
    attrib psets IfcSpace, to be sure attrib identifier and pset is used
    the FreeCAD notation is used directly in the mapping for simplicity
    attrib class RoomData: identifier attrib;;pset name
    """

    pmapping1 = {
        "Category": "Category;;Pset_SpaceCommon",
        "CoveringFloor": "FloorCovering;;Pset_SpaceCoveringRequirements",
        "CoveringWall": "WallCovering;;Pset_SpaceCoveringRequirements",
        "CoveringCeiling": "CeilingCovering;;Pset_SpaceCoveringRequirements",
        "RoomUse": "Raumnutzung_nach_SIA_2024;;Zusaetzliche_Element_Eigenschaften_Schweiz",
    }

    pmapping2 = {
        "Area": "Area;;Dimensions",
        "Perimeter": "Perimeter;;Dimensions",
        "Level": "Level;;Constraints",
        "Occupancy": "Occupancy;;Identity Data",
    }

    pmapping3 = {
        "CostCenter": "Kosten;;Allplan Attributes",
    }

    return pmapping1


# ***************************************************************************
# *   Copyright (c) 2020 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

"""
import raumbuch_propoutput_fromifc
ifcfile = "01fGZyzsrjwbpisV5dsb$3__46.ifc"
path = "/home/hugo/Desktop/"
#path = "C:/Users/BHA/Desktop/"
props = raumbuch_propoutput_fromifc.open(path + ifcfile)
raumbuchtools.output_room_properties(props)


"""

import importlib

# import FreeCAD

import raumbuchtools


if open.__module__ == "__builtin__":
    # because we'll redefine open below (Python2)
    pyopen = open
elif open.__module__ == "io":
    # because we'll redefine open below (Python3)
    pyopen = open


def open(filename):

    importlib.reload(raumbuchtools)
    room_properties = raumbuchtools.get_room_properties_from_ifcfilepath(filename)
    raumbuchtools.output_room_properties(room_properties)

    return room_properties

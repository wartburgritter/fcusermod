### Infos
+ class RoomData hat immer alle Attribute 
+ SIA Name in dt. dazuschreiben und kurze erlaeuterung evtl. dt. und englisch


### Explizites Mapping
+ Mappingdaten vorhanden
+ diese koennen fuer das QGate verwendet werden
+ QGate prueft explizit nur auf diese Attribute und Attributwerte
+ genau diese werden exporiert (auch die leeren)
+ hinzu kommen noch UUID und PID


### Implizites Mapping
+ keine Mappingdaten vorhanden
+ gehe ueber alle spaces --> erstelle lise der psets und Attribute
+ nur pprops, die eprops sind immer gleich
+ gehe ueber alle spaces --> fuer jeden Attributwert speichere
    + True, wenn belegt und Wert ok ist
    + False, wenn nicht belegt (None, "", nd)
    + den Wert, wenn der Wert falsch ist
+ QGate ouput: ueber obige daten auswertung erstellen
    + count ... prop key vorhanden (normal sind alle keys in allen spaces vorhanden ... pruefen!)
    + count ... props alle werte belegt
    + count ... props werte fehlen
    + count ... props vereinzelt belegt
    + count ... props nicht belegt
+ Raumbuch export, nicht die unbelegten Werte exportieren


### QGate was finden?
+ fehlende Eintraege
+ angehaengte und vorangestellte Leerzeichen
+ falsche Eintraege, BSP irgendwelcher Text, wenn boolscher Wert

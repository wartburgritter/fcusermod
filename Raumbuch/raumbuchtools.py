# ***************************************************************************
# *   Copyright (c) 2020 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

"""
rds[0].__dict__
list(rds[0].__dict__.keys())


import raumbuch_create_fromifc
ifcfile = "01fGZyzsrjwbpisV5dsb$3__46.ifc"
path = "/home/hugo/Desktop/"
path = "C:/Users/BHA/Desktop/"
rds = raumbuch_create_fromifc.open(path + ifcfile)

"""

import importlib
import yaml

import ifcopenshell

# import FreeCAD

import importIFCHelper
import exportIFC

import raumbuchmapping
import raumbuchroomdata


# ****************************************************************************
def ifcfilepath2qgrsd(filename):

    # filename with path to a ifc file

    ifcfile = ifcopenshell.open(filename)
    qualitygate_rds = ifcos2qgrds(ifcfile)

    return qualitygate_rds


def ifcos2qgrds(ifcfile):

    # ifcopenshell ifc file instance
    ifc_spaces = ifcfile.by_type("IfcSpace")

    # get all attrib keys
    # e_attrib, they are fixed with IfcSpace definition
    e_attrib_keys = {
        "GlobalId",
        # OwnerHistory is entity, ignored
        "Name",
        "Description",
        "ObjectType",
        # ObjectPlacement is entity, ignored
        # Representation is entity, ignored
        "LongName",
        "CompositionType",
        "InteriorOrExteriorSpace",
        "ElevationWithFlooring"
    }

    # p_attrib, from property sets
    p_attrib_keys = set()
    for ifcspace in ifc_spaces:
        pid = ifcspace.id()
        pprops = importIFCHelper.getIfcPsetPoperties(ifcfile, pid)
        for k, v in pprops.items():
            if k not in p_attrib_keys:
                p_attrib_keys.add(k)

    print("")
    for k in sorted(e_attrib_keys):
        print(k)
    print("")
    for k in sorted(p_attrib_keys):
        print(k)
    print("")

    # check property values for each IfcSpace
    qualitygate_rds = {}
    psetpropertytypes = []
    for ifcspace in ifc_spaces:
        pid = ifcspace.id()
        qg_space = {}
        # print(pid)

        # properties from IfcSpace entity
        for k in e_attrib_keys:
            prop_value = str(getattr(ifcspace, k))
            # print(prop_value)
            if prop_value is None or prop_value == "" or prop_value == "nd":
                qg_space[k] = False
            elif prop_value != prop_value.rstrip() or prop_value != prop_value.lstrip():
                qg_space[k] = prop_value
                # print("{} --> {}".format(k, prop_value))
            else:
                qg_space[k] = True

        # for k, v in qg_space.items():
        #     print("{} --> {}".format(k, v))

        # properties from psets
        psetproperties = importIFCHelper.getIfcPsetPoperties(ifcfile, pid)
        for k in p_attrib_keys:
            # workaround, do smarter
            try:
                pset, pname, ptype, prop_value = exportIFC.getPropertyData(
                    k, psetproperties[k], {"DEBUG": True}
                )
            except:
                print("propkeyproblem in element: {} for {}".format(ifcspace.id(), k))
                continue
            if ptype not in psetpropertytypes:
                psetpropertytypes.append(ptype)
            if prop_value is None or prop_value == "" or prop_value == "nd":
                qg_space[k] = False
            elif ptype == "IfcLabel" and (
                prop_value != prop_value.rstrip() or prop_value != prop_value.lstrip()
            ):
                qg_space[k] = prop_value
                # print("{} --> {}".format(k, prop_value))
            else:
                qg_space[k] = True

        # for k, v in qg_space.items():
        #     print("{} --> {}".format(k, v))

        qualitygate_rds[pid] = qg_space

    print(sorted(psetpropertytypes))  # TODO QGate for eache ptype values
    return qualitygate_rds


def qgaterds2yaml(file_name_qgate_rds, qualitygate_rds):
    exportfile_qgate_rds = open(file_name_qgate_rds, "w")
    yaml.dump(  # https://dpinte.wordpress.com/2008/10/31/pyaml-dump-option/
        qualitygate_rds,
        exportfile_qgate_rds,
        indent=2,
        default_flow_style=False,
        allow_unicode=True,
        width=1000,
    )
    exportfile_qgate_rds.close()


def qgate_summary(qualitygate_rds_all):
    """
    output summary
    - count keys
    - for each key filled in how many, filled not in how many
    - the above sorted by how many, most at top
    - may be a new line after all filled and before zero filled
    - wrong keys with value for each pid if there is at least one
    """

    # init qg_keys and qg_count
    for k, v in qualitygate_rds_all.items():
        qg_keys = list(v.keys())
        break
    qg_count = {}
    for k in qg_keys:
        qg_count[k] = 0
    # print(qg_count)

    # count ...
    for qg_space in qualitygate_rds_all:
        for k, v in qualitygate_rds_all[qg_space].items():
            # print(k)
            if v is not False and k in qg_count:
                # True or some wrong Value, but filled
                qg_count[k] += 1

    print("")
    print(len(qualitygate_rds_all))
    qg_count_keys = []
    qg_count_values = []
    for k, v in qg_count.items():
        qg_count_keys.append(k)
        qg_count_values.append(v)
    qg_count_for_pd = {
        "Quality Gate Value": qg_count_values,
        "Quality Gate Property": qg_count_keys,
    }
    import pandas as pd
    df = pd.DataFrame(qg_count_for_pd)
    df2 = df.sort_values(by=["Quality Gate Value"], ascending=False)
    # https://stackoverflow.com/a/41475484
    from tabulate import tabulate
    # print(tabulate(df2, showindex=False, headers=df.columns, tablefmt='psql', numalign="left"))
    print(tabulate(df2, showindex=False, headers=df.columns, numalign="left"))

    # TODO improve output somehow
    # for k, v in qg_count.items():
    #     print("{} --> {}".format(k, v))
    # print("")

    # dict sorted by values
    # https://stackoverflow.com/a/3177911
    # for k in sorted(qg_count, key=qg_count.get, reverse=True):
    #     print(k, qg_count[k])
    # print("")

    # pandas
    # df.style.set_properties(**{'text-align': 'right'})
    # pd.set_option("display.colheader_justify","right")
    # print(df.to_string(formatters={'Quality Gate Property':'{{:<{}s}}'.format(df['Quality Gate Property'].str.len().max()).format}, index=False))

    # wrong property values
    qualitygate_rds_wrong = {}
    for qg_space in qualitygate_rds_all:
        qg_wrong = {}
        for k, v in qualitygate_rds_all[qg_space].items():
            if v is not True and v is not False:
                qg_wrong[k] = v
        qualitygate_rds_wrong[qg_space] = qg_wrong
    print("\nWrong entries, for example if there is a Space on string end:")
    for qg_space in qualitygate_rds_wrong:
        for k, v in qualitygate_rds_wrong[qg_space].items():
            print("{}: {} --> '{}'".format(qg_space, k, v))

    # missing property values
    missing_boarder = 0.66

    # TODO Split, see wrong property values !!!!!!!!!!!!!!!!!!!!!!!
    print("\nMissing entries (more than 66 procent of the keys need to be filled in):")
    missing = len(qualitygate_rds_all) * 0.66
    for qg_space in qualitygate_rds_all:
        for k, v in qualitygate_rds_all[qg_space].items():
            if k in qg_count and qg_count[k] >= missing and v is False:
                print("{}: {}".format(qg_space, k))

    # TODO
    # export all these into yml instead of the True, False, Value dict


# ****************************************************************************
def ifcfilepath2rds(filename):

    # filename with path to a ifc file

    ifcfile = ifcopenshell.open(filename)
    room_data_sheets = ifcos2rds(ifcfile)

    return room_data_sheets


def ifcos2rds(ifcfile):

    # ifcopenshell ifc file instance

    ifc_spaces = ifcfile.by_type("IfcSpace")

    # editing data structure without restart of FreeCAD
    importlib.reload(raumbuchmapping)
    importlib.reload(raumbuchroomdata)

    room_data_sheets = []
    for ifcspace in ifc_spaces:
        pid = ifcspace.id()
        room = raumbuchroomdata.RoomData(pid)
        room.GlobalId = ifcspace.GlobalId

        e_mapping = raumbuchmapping.get_entity_mapping()
        for k, v in e_mapping.items():
            prop_value = getattr(ifcspace, v)
            if prop_value is not None:
                prop_value = prop_value.rstrip()  # rstrip() --> QualityGate !!! no white space !!!
            setattr(room, k, prop_value)
        # setattr(room, "Number", getattr(ifcspace, e_mapping["Number"]))
        # print(room.__dict__)
        # print(getattr(ifcspace, "Name"))

        # build list of pset properties
        psetproperties = importIFCHelper.getIfcPsetPoperties(ifcfile, pid)

        p_mapping = raumbuchmapping.get_pset_mapping()
        for k, v in p_mapping.items():
            # should be done in QualityGate
            if v not in psetproperties:
                print("{} not in {}".format(v, pid))
                continue
            # use the value from pset mapping dict and get from psetproperties dict the value for this key
            # the key must exist in the psetproperties dict (QualityGate!!!)
            # the attribute of the class RoomData needs to bo initialised by the class construktor
            mapset, mapname, ptype, pvalue = exportIFC.getPropertyData(
                v, psetproperties[v], {"DEBUG": True}
            )
            # print("{}: {} --> {}: {}".format(mapset, mapname, ptype, pvalue))
            setattr(room, k, pvalue)

        room_data_sheets.append(room)

    if len(room_data_sheets) > 0:
        print("{}".format(list(room_data_sheets[0].__dict__.keys())))

    return room_data_sheets


# ****************************************************************************
def prepare_rds_for_export(room_data_sheets):

    dict_rds = {}
    for room in room_data_sheets:
        dict_rds[room.Pid] = room.__dict__.copy()
        # it seams the dicts in dict_rds are references to the class object
        # means if a key is deleted the class attribute is deleted
        # make a copy

    # do not export the keys if all rooms have empty or not valid values
    # only delete if the value of a key is in all rooms "" OR None OR nd

    # TODO more general, not three time the same.
    # do with some loop or def

    # go into the first room and save the "", None, "nd" keys
    first_rds = room_data_sheets[0].__dict__
    print(sorted(list(first_rds.keys())))
    keys_none = set()
    keys_empty = set()
    keys_nd = set()
    for k, v in first_rds.items():
        if v is None:
            keys_none.add(k)
        if v == "":
            keys_empty.add(k)
        if v == "nd":
            keys_nd.add(k)
    # print(sorted(keys_none))
    # print(sorted(keys_empty))
    # print(sorted(keys_nd))

    # go into all rooms and check for "", None,  "nd"
    for room in dict_rds:
        for k, v in dict_rds[room].items():
            if k in keys_none and v is not None:
                keys_none.remove(k)
            if k in keys_empty and v != "":
                keys_empty.remove(k)
            if k in keys_nd and v != "nd":
                keys_nd.remove(k)
    # print(sorted(keys_none))
    # print(sorted(keys_empty))
    # print(sorted(keys_nd))
    # union of the sets above
    kwev = keys_none | keys_empty | keys_nd  # kwev == keys_with_empty_values
    print(sorted(kwev))

    # delete the keys if all rooms have empty values
    for room in dict_rds:
        for key_to_delete in kwev:
            del dict_rds[room][key_to_delete]
            # https://stackoverflow.com/questions/11277432/how-to-remove-a-key-from-a-python-dictionary

    return dict_rds


def rds2yaml(rds_file_name, room_data_sheets):

    dict_rds = prepare_rds_for_export(room_data_sheets)

    exportfile_rds = open(rds_file_name, "w")
    yaml.dump(  # https://dpinte.wordpress.com/2008/10/31/pyaml-dump-option/
        dict_rds,
        exportfile_rds,
        indent=2,
        default_flow_style=False,
        allow_unicode=True,
        width=1000,
    )
    exportfile_rds.close()


# ****************************************************************************
def rds2excel(rds_file_name, room_data_sheets):

    dict_rds = prepare_rds_for_export(room_data_sheets)

    # export to excel
    # print(dict_rds)
    import pandas as pd

    df = pd.DataFrame(dict_rds)
    df = df.transpose()

    writer = pd.ExcelWriter(
        rds_file_name,
        engine="xlsxwriter"
    )
    df.to_excel(
        writer,
        startrow = 1,
        sheet_name="Raumbuch",
    )

    # https://stackoverflow.com/a/36554382
    # Indicate workbook and worksheet for formatting
    workbook = writer.book
    worksheet = writer.sheets["Raumbuch"]

    # Iterate through each column and set the width
    for i, col in enumerate(df.columns):
        worksheet.set_column(i+1, i+1, 25)
    writer.save()
    # writer.close()  # file seams to be closed automaticlly


# ****************************************************************************
def get_room_properties_from_ifcfilepath(filename, output=False, index=0):

    # filename with path to a ifc file

    ifcfile = ifcopenshell.open(filename)
    room_properties = get_room_properties_from_ifcos(ifcfile, output, index)

    return room_properties


def get_room_properties_from_ifcos(ifcfile, output=False, index=0):

    # ifcopenshell ifc file instance

    ifc_spaces = ifcfile.by_type("IfcSpace")
    room_properties = []

    if len(ifc_spaces) <= index:
        print("Only {} IfcSpaces found.".format(len(ifc_spaces)))
    else:
        p = ifc_spaces[index]
        pid = p.id()

        eprops = {}
        for i, e in enumerate(p):
            eprops[p.attribute_name(i)] = str(e)
            # e could be a ifcos entity instance in last line, without str() --> bang crash
            # print("{} --> {}".format(p.attribute_name(i), e))

        pprops = importIFCHelper.getIfcPsetPoperties(ifcfile, pid)

        room_properties = [str(p), eprops, pprops]

    if output is True:
        output_room_properties(room_properties)

    return room_properties


def output_room_properties(room_properties):

    p = room_properties[0]
    eprops = room_properties[1]
    pprops = room_properties[2]

    # prints
    print("")
    print(p)
    print("")

    for k, v in eprops.items():
        print("{} --> {}".format(k, v))

    print("")

    for k, v in pprops.items():
        pset, pname, ptype, pvalue = exportIFC.getPropertyData(k, v, {"DEBUG": True})
        print("{}: {} --> {}: {}".format(pset, pname, ptype, pvalue))
        print("{}: {}".format(k, v))
        # print("")


# ****************************************************************************
"""
import os
import yaml
import ifcopenshell
import raumbuchtools

ifcfiles_dir = (
    "/home/hugo/Documents/projekte--closeddev/0_ifcfilecontainer/by_uuid"
)
ifcfiles_list = sorted(os.listdir(ifcfiles_dir))

for name_ifcfile in ifcfiles_list:
    filepath_ifcfile = os.path.join(ifcfiles_dir, name_ifcfile)
    ifcfile = ifcopenshell.open(filepath_ifcfile)
    es = f.by_type("IfcSpace")
    if es:
        props = raumbuchtools.get_room_properties_from_ifcos(ifcfile, True)
        outfile = open("/home/hugo/Desktop/" + name_ifcfile + ".txt", "w")
        yaml.dump(props, outfile, indent=2, default_flow_style=False, allow_unicode=True)
        outfile.close()

"""

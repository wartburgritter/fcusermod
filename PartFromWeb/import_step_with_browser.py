# ***************************************************************************
# *   Copyright (c) 2018 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

# forum topic
# https://forum.freecadweb.org/viewtopic.php?f=10&t=25189

# very helpful links
# https://stackoverflow.com/questions/51011244/pyside2-and-supporting-addtojavascriptwindowobject
# https://stackoverflow.com/questions/39544089/how-can-i-access-python-code-from-javascript-in-pyqt-5-7/42740287


from PySide2 import QtCore
from PySide2 import QtWebChannel
from PySide2 import QtWebEngineWidgets
from PySide2 import QtWidgets

from import_step_from_web import WebPartImporter


the_html_code = """
<html>
<head>
    <script src="qrc:///qtwebchannel/qwebchannel.js"></script>
</head>
<header><title>title</title></header>
<body>

<p id="output"></p>

<h2><a href="#" onclick="jsWebPartImporter.text('https://forum.freecadweb.org/download/file.php?id=48016')">Click to import a Part from Internet into FreeCAD</a></h2>
<h2><a href="#" onclick="alert('Javascript works!')">Click for Java Script Test</a></h2>

<script type="text/javascript">
    window.onload = function() {
        new QWebChannel(qt.webChannelTransport, function (channel) {
            window.jsWebPartImporter = channel.objects.jsWebPartImporter;
            console.log(jsWebPartImporter);
        });
    }
</script> 
</body>
</html>
"""


class BrowserWidget(QtWidgets.QDialog):
    def __init__(self):
        super(BrowserWidget, self).__init__()
        self.web_view = QtWebEngineWidgets.QWebEngineView()

        channel = QtWebChannel.QWebChannel(self)
        channel.registerObject("jsWebPartImporter", WebPartImporter(self.web_view))
        self.web_view.page().setWebChannel(channel)

        # layout 
        self.web_view.setSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding
        )
        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addLayout(QtWidgets.QHBoxLayout())
        self.layout.addWidget(self.web_view)

    def showEvent(self, event):
        super(BrowserWidget, self).showEvent(event)
        self.web_view.setHtml(the_html_code)

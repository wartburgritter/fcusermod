"""
# https://stackoverflow.com/questions/51011244/pyside2-and-supporting-addtojavascriptwindowobject
# run from FreeCAD

from PySide2 import QtWidgets
import jswebsample
app = QtWidgets.QApplication(sys.argv)
w = jswebsample.AppWindow()
w.show()
#sys.exit(app.exec_())

# should show and update the time, it did it for me on 8.8.2020

"""


import sys
from PySide2 import QtCore, QtGui, QtWidgets, QtWebEngineWidgets, QtWebChannel


class WebEnginePage(QtWebEngineWidgets.QWebEnginePage):
    pass


class AppManager(QtCore.QObject):
    textChanged = QtCore.Signal(str)
    def __init__(self, webview):
        QtCore.QObject.__init__(self)
        self.m_text = ""

        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.on_timeout)
        timer.start(1000)

    def on_timeout(self):
        self.text  = QtCore.QDateTime.currentDateTime().toString()

    @QtCore.Property(str, notify=textChanged)
    def text(self):
        return self.m_text

    @text.setter
    def setText(self, text):
        if self.m_text == text:
            return
        self.m_text = text
        self.textChanged.emit(self.m_text)


class WebView(QtWebEngineWidgets.QWebEngineView):
    def __init__(self, parent=None):
        QtWebEngineWidgets.QWebEngineView.__init__(self, parent)
        self.setPage(WebEnginePage(self))

    def contextMenuEvent(self, event):
        pass


class AppWindow(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.view = WebView(self)
        self.page = self.view.page()
        self.app_manager = AppManager(self.view)
        channel = QtWebChannel.QWebChannel(self)
        self.page.setWebChannel(channel)
        channel.registerObject("app_manager", self.app_manager)
        self.view.load(QtCore.QUrl.fromLocalFile(QtCore.QDir.current().filePath("/home/hugo/.FreeCAD/Mod/PartFromWeb/sampleweb.html")))
        self.setCentralWidget(self.view)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    w = AppWindow()
    w.show()
    sys.exit(app.exec_())

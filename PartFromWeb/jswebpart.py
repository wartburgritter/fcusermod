"""
# https://stackoverflow.com/questions/51011244/pyside2-and-supporting-addtojavascriptwindowobject
# run from FreeCAD

import sys
from PySide2 import QtWidgets
import jswebpart
app = QtWidgets.QApplication(sys.argv)
w = jswebpart.AppWindow()
w.show()
#sys.exit(app.exec_())

# meine part from web seite kommt als Fenster am 8.8.2020

"""

from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2 import QtWebEngineWidgets
from PySide2 import QtWebChannel


from import_step_from_web import WebPartImporter


class WebEnginePage(QtWebEngineWidgets.QWebEnginePage):
    pass


class WebView(QtWebEngineWidgets.QWebEngineView):
    def __init__(self, parent=None):
        QtWebEngineWidgets.QWebEngineView.__init__(self, parent)
        self.setPage(WebEnginePage(self))

    def contextMenuEvent(self, event):
        pass


class AppWindow(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.view = WebView(self)
        self.page = self.view.page()
        self.app_manager = WebPartImporter(self.view)
        channel = QtWebChannel.QWebChannel(self)
        self.page.setWebChannel(channel)
        channel.registerObject("app_manager", self.app_manager)
        self.view.load(
            QtCore.QUrl.fromLocalFile(QtCore.QDir.current().filePath(
                "/home/hugo/.FreeCAD/Mod/PartFromWeb/samplepart.html"
            ))
        )
        self.setCentralWidget(self.view)


class AppWindow2(QtWidgets.QDialog):
    def __init__(self):
        super(AppWindow2, self).__init__()
        self.view = WebView(self)
        self.page = self.view.page()
        self.app_manager = WebPartImporter(self.view)
        channel = QtWebChannel.QWebChannel(self)
        self.page.setWebChannel(channel)
        channel.registerObject("app_manager", self.app_manager)

        self.view.setSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding
        )
        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addLayout(QtWidgets.QHBoxLayout())
        self.layout.addWidget(self.view)

    def showEvent(self, event):
        super(AppWindow2, self).showEvent(event)
        self.view.load(
            QtCore.QUrl.fromLocalFile(QtCore.QDir.current().filePath(
                "/home/hugo/.FreeCAD/Mod/PartFromWeb/samplepart.html"
            ))
        )

# ***************************************************************************
# *   Copyright (c) 2018 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

import FreeCAD
import FreeCADGui


class TOOL1():
    def Activated(self):
        from PySide2 import QtCore
        from PySide2 import QtWidgets
        import import_step_with_browser

        mw = FreeCADGui.getMainWindow()
        d = QtWidgets.QDockWidget()
        d.setWidget(import_step_with_browser.BrowserWidget())
        mw.addDockWidget(QtCore.Qt.RightDockWidgetArea, d)

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": "tool2",
            "ToolTip": "Import a STEP Part from Web, Modul: import_step_with_browser"
        }


class TOOL2():
    def Activated(self):
        from PySide2 import QtCore
        from PySide2 import QtWidgets
        import jswebpart

        mw = FreeCADGui.getMainWindow()
        d = QtWidgets.QDockWidget()
        d.setWidget(jswebpart.AppWindow2())
        mw.addDockWidget(QtCore.Qt.RightDockWidgetArea, d)

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": "tool2",
            "ToolTip": "Import a STEP Part from Web, Module: jswebpart"
        }


FreeCADGui.addCommand("PartFromWeb_1", TOOL1())
FreeCADGui.addCommand("PartFromWeb_2", TOOL2())

# ***************************************************************************
# *   Copyright (c) 2018 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

"""
# code to import a part from web
import tempfile
import FreeCADGui
import Part
from urllib.request import urlretrieve

tmpstepfile_path = tempfile.NamedTemporaryFile(suffix=".step").name
urlretrieve("https://forum.freecadweb.org/download/file.php?id=48016", tmpstepfile_path)
Part.show(Part.read(tmpstepfile_path))
FreeCADGui.ActiveDocument.activeView().viewAxonometric()
FreeCADGui.SendMsgToActiveView("ViewFit")


# code to test the import class
from import_step_from_web import WebPartImporter
WebPartImporter().text("https://forum.freecadweb.org/download/file.php?id=48016")

"""


import os
import tempfile
from urllib.request import urlretrieve
from PySide2 import QtCore

import FreeCAD
import FreeCADGui
import Part


class WebPartImporter(QtCore.QObject):
    def __init__(self, parent=None):
        super(WebPartImporter, self).__init__(parent)

    @QtCore.Slot(str)
    def text(self, url_to_step_file):

        print(url_to_step_file)

        # get a temp file with ending step
        tmpstepfile_path = tempfile.NamedTemporaryFile(suffix=".step").name
        print(tmpstepfile_path)

        # download the step file
        urlreturn = urlretrieve(url_to_step_file, tmpstepfile_path)
        print(urlreturn[0])

        # read the step (the read method needs file ending step)
        # and add it to the active document (create a new if there is no active doc)
        Part.show(Part.read(tmpstepfile_path))
        print(FreeCAD.ActiveDocument.Name)
        os.remove(tmpstepfile_path)

        # set the view
        FreeCADGui.ActiveDocument.activeView().viewAxonometric()
        FreeCADGui.SendMsgToActiveView("ViewFit")

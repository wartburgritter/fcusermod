# ***************************************************************************
# *   Copyright (c) 2018 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

# not needed imports but to get flake8 quired
import FreeCADGui as Gui
from FreeCADGui import Workbench

# needed imports
import PartFromWebGui
False if PartFromWebGui.__name__ else True  # flake8


class PartFromWeb(Workbench):
    "PartFromWeb workbench object"
    MenuText = "PartFromWeb"
    ToolTip = "ToolTip PartFromWeb"
    
    def Initialize(self) :
        self.appendToolbar("PartFromWeb", ["PartFromWeb_1", "PartFromWeb_2"])
        self.appendMenu("PartFromWeb", ["PartFromWeb_1", "PartFromWeb_2"])

    def GetClassName(self):
        return "Gui::PythonWorkbench"


Gui.addWorkbench(PartFromWeb())

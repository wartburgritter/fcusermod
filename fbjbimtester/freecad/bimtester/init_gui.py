# import FreeCAD
import FreeCADGui as Gui

command_to_init_fbjbimworker = """
# changes made here, need to be made in workbench fbjbimtester as well
import importlib
import sys
from platform import node as pcname
if pcname() == "Fabien":
    bwcodepath = "C:/0_ffo_privat/BIMTesterCode/bimworker/scriptBIMTesting"
elif pcname() == "JP-PC14" or pcname() == "BOOKBERND":
    bwcodepath = "C:/0_BHA_privat/BIMTesterCode/bimworker/scriptBIMTesting"
elif pcname() == "Ahorn":
    bwcodepath = "/home/hugo/Documents/dev/bimtester/jobcode/fbjcode/scriptBIMTesting/"
else:
    print("Machine {} not known.".format(pcname()))

sys.path.append(bwcodepath)
import bimworker_locations as bwlocs
import bimworker_run as bwrun
importlib.reload(bwrun)
"""

wbname = "FBJBIMTester"


class BIMTester(Gui.Workbench):
    """
    Class which gets initiated at startup of the FreeCAD GUI.
    """

    #icon_path = ""
    #Icon = icon_path
    MenuText = wbname
    ToolTip = "ToolTip for {}".format(wbname)

    def Initialize(self):
        """
        Called when the workbench is first activated.
        """
        tool_count = 1
        tool_specifier_list = []
        for i in range(tool_count):
            tool_specifier_list.append("{}_{}".format(wbname, i+1))
        self.appendToolbar(wbname, tool_specifier_list)
        self.appendMenu(wbname, tool_specifier_list)

        # run FBJBIMTester code on change to workbench
        # or on Startup FreeCAD if this workbench is activated on startup
        try:
            Gui.doCommand(command_to_init_fbjbimworker)
        except:
            print("Problem on importing scriptBIMTesting code.")


    def GetClassName(self):
        return "Gui::PythonWorkbench"


Gui.addWorkbench(BIMTester())


class TOOL1():

    def Activated(self):

        # reload on every button click
        Gui.doCommand("importlib.reload(bwrun)")
        Gui.doCommand(
            "bwrun.one_run_all_bimworker(bwlocs.get_log_dir_user(), bwlocs.get_log_dir_developer())"
        )


    def GetResources(self):
        return {
            # "Pixmap": icon_path,
            "MenuText": "run FBJBIMTester",
            "ToolTip": "run Start FBJBIMTester"
        }


Gui.addCommand("{}_1".format(wbname), TOOL1())

# ***************************************************************************
# *   (c) Bernd Hahnebach (bernd@bimstatik.org) 2014                        *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# *   Bernd Hahnebach 2014                                                  *
# ***************************************************************************/


import Part


def precision():
    "precision(): returns the precision"
    return 4


def isPtOnEdge(pt, edge):
    '''isPtOnEdge(Vector,edge): Tests if a point is on an edge'''
    v = Part.Vertex(pt)
    d = v.distToShape(edge)
    if d:
        if round(d[0], precision()) == 0:
            return True
    return False


def isPtEqVertex(pt1, vertex):
    '''isPtEqPt(pt1,pt2): Tests if two points are equal'''
    pt2 = vertex.Point
    d = pt1.distanceToPoint(pt2)
    if round(d, precision()) == 0:
        return True
    return False


def getNodeByEdge(MeshObject, Edge):
    '''getNodeByEdge(MeshObject, Edge):  '''
    # print('Edge --> Beginn: ', Edge.Vertexes[0].Point, '  End: ' ,Edge.Vertexes[0].Point)
    nodes = []
    meshnodes = MeshObject.FemMesh.Nodes
    for meshnode in meshnodes:
        if isPtOnEdge(meshnodes[meshnode], Edge) is True:
            # print(meshnode, ' --> ', meshnodes[meshnode])
            nodes.append(meshnode)
    return nodes


def getNodeByVertex(MeshObject, Vertex):
    '''getNodeByVertex(MeshObject, Vertex):  '''
    nodes = []
    meshnodes = MeshObject.FemMesh.Nodes
    for meshnode in meshnodes:
        if isPtEqVertex(meshnodes[meshnode], Vertex) is True:
            # print(meshnode, ' --> ', meshnodes[meshnode])
            nodes.append(meshnode)
    return nodes


def checkIfAllVertexesOfShapeAreInsideFemMesh(ObjectShape, n, MeshObject, ObjectName):
    '''checkIfAllVertexesOfShapeAreInsideFemMesh(ObjectShape, n, MeshObject, ObjectName):
    check if ALL Vertexes of ObjectShape where found in Fem-Mesh '''
    for v in ObjectShape.Vertexes:
        found = False
        for i in n:
            if isPtEqVertex(MeshObject.FemMesh.getNodeById(i), v) is True:
                found = True
        if found is False:
            print('Warning: ', ObjectName, ' --> ', v, ' ', v.Point, ' of ', ObjectShape, ' not found in FEM-Mesh')


def getAbaqusElementTableHack(meshobject):
    '''
    get the element table by python
    use write writeABAQUS() to write femmeshdata to tempfile and read femmeshdata from tempfile
    get the dictionary elementtable { elementid : [ nodeid, nodeid, ... , nodeid ] }
    '''
    import os
    fa = '/tmp/abaquselementtable_' + meshobject.Name + '.tmp'  # tempfile would be OperatingSystem independent
    meshobject.FemMesh.writeABAQUS(fa)
    f = open(fa, 'r')
    isElementLine = False
    elementtable = {}                   # { elementid : [ nodeid, nodeid, ... , nodeid ] }
    for l in f:
        if isElementLine is True:
            # print(l.rstrip(', \n'))
            elementID = int(l.rstrip(', \n').split(',', 1)[0])              # int
            nodeIDsStr = l.rstrip(', \n').split(',', 1)[1].split(',')     # list of strings
            nodeIDs = []
            for noStr in nodeIDsStr:
                nodeIDs.append(int(noStr))
            elementtable[elementID] = nodeIDs
        if l.startswith('*Element, TYPE='):
            # print(l)
            isElementLine = True    # next l will be first l of the elements
    os.remove(fa)               # or use del fa
    return elementtable


def getElementsByNodes(elementtable, n):
    '''
    e: elementlist
    n: nodelist, elementtable: AbaqusElementTable
    Entscheidung ob ein element zu den Knoten der Knotenliste gehoert.
    Nur wenn alle Knoten eines Elementes innerhalb des Solids sind gehoert Element zu Solid
    Fange mit Meshes ohne saubere Trennung gar nicht erst an (fusion mesh)
    '''
    e = []  # elementlist
    for elementID in sorted(elementtable):
        # print(elementID, ' --> ', elementtable[elementID])
        # print(type(elementID))
        # print(type(elementtable[elementID][0]))
        nodecount = 0
        for nodeID in elementtable[elementID]:
            if nodeID in n:
                nodecount = nodecount + 1
        if nodecount == len(elementtable[elementID]):   # alle knoten des elements sind im solid!
            # print(elementID, ' --> ', elementtable[elementID])
            # print(elementID, ' --> ', nodecount)
            e.append(elementID)
    return e


def getNodesByElements(elementtable, elementlist):
    '''
    elementtable ... { elementid : [ nodeid, nodeid, ... , nodeid ] }      the elementtable
    elementlist ...  [ elementid, elementid, ... , elementid]              a list of some elements
    '''
    nodelist = []
    for elementID in sorted(elementtable):
        if elementID in elementlist:
            for nodeID in elementtable[elementID]:
                if nodeID not in nodelist:
                    nodelist.append(nodeID)
    return nodelist


def makeFemMeshS3FromFreecadMesh(freecadmesh):
    '''makes FemMesh from FreeCAD Mesh
    triangle facesets --> S3 FEM Element
    import femutils
    femutils.freecadmesh2femmeshS3(App.ActiveDocument.getObject("Mesh").Mesh)
    '''
    import Fem
    femmesh = Fem.FemMesh()
    for pt in freecadmesh.Points:
        # erster index im freecadmesh is 0 --> FemMesh should start with 1
        # print(pt.Index)
        # print(pt.Vector)
        femmesh.addNode(pt.x, pt.y, pt.z, pt.Index + 1)
    for fts in freecadmesh.Facets:
        # print(fts.PointIndices)
        faceids = []
        faceids.append(fts.PointIndices[0] + 1)
        faceids.append(fts.PointIndices[1] + 1)
        faceids.append(fts.PointIndices[2] + 1)
        femmesh.addFace(faceids)
    Fem.show(femmesh)


def makeFreecadMeshFromCompoundOffFaces(compounds):
    '''list of compoundshapes, see imported analytical ifc meshes'''
    import Mesh
    freecadmeshes = []
    for comp in compounds:
        freecadmesh = []
        for v in comp.Vertexes:
            p = []
            p.append(v.X)
            p.append(v.Y)
            p.append(v.Z)
            freecadmesh.append(p)
        freecadmeshobj = Mesh.Mesh(freecadmesh)
        freecadmeshes.append(freecadmeshobj)
    for fcm in freecadmeshes:
        print(fcm)
        Mesh.show(fcm)


def deleteDuplicatesAndWriteFemMesh(fem_mesh):
    print('Meshinfo for old FEM Mesh')
    print(fem_mesh)
    import sys
    sys.stdout.flush()
    # find the duplicate nodes --> add them to a dictionary duplicate_nodes
    import DraftVecUtils
    duplicate_nodes = {}
    if 0 in fem_mesh.Nodes:
        print('node numbering in FEM Mesh starts with 0 --> not implemented')
        return
    for node in sorted(fem_mesh.Nodes):
        vec1 = fem_mesh.Nodes[node]
        # it is assumed node numbering starts with 1 and has step off 1 and no "holes" in node numbering
        # if this is not true error ausgeben !!!
        for n in range(node + 1, fem_mesh.NodeCount + 1):  # nur die restlichen nodes, hat probleme siehe zeilen darueber
            # mit keys arbeiten, nicht node + 1 sondern key um eins erhoehen (also "holes" ueberspringen)
            # mit unterer ifschleife bricht funktion nicht ab, aber die anzahl der holes nodes fehlen am ende !
            if n in fem_mesh.Nodes:
                vec2 = fem_mesh.Nodes[n]
                if DraftVecUtils.equals(vec1, vec2):  # beachte precision in Draft Preferences!!!
                    # wenn doppelte punkte --> kein zusammenhaengendes mesh --> nur einzelne nicht verbundene mesh elemente
                    print('duplicate nodes:  ', node, ' == ', n, ' --> ', vec1, ' == ', vec2)
                    sys.stdout.flush()
                    # besser obige printen, das zeigt bei grossen meshen es geht was ...
                    duplicate_nodes[n] = node
            else:
                print('can not find node: ', n)  # hole in node numbering!
    for n in duplicate_nodes:
        m = duplicate_nodes[n]
        # print('duplicate nodes:  ', n, ' == ', m, ' --> ', fem_mesh.Nodes[n], ' == ', fem_mesh.Nodes[m])
    print('\nFound ', len(duplicate_nodes), ' duplicat nodes.')

    # find duplicate nodes in the fem_element_table --> replaces them
    fem_element_table = getFemElementTable(fem_mesh)  # get the fem_element_table
    # for e in fem_element_table:
    #     print(e, ' --> ', fem_element_table[e])
    ncount = 0
    for e in fem_element_table:
        nodeslist_old = []
        nodeslist_new = []
        for n in fem_element_table[e]:
            nodeslist_old.append(n)
            if n in duplicate_nodes:
                print(n, ' --> ', duplicate_nodes[n])
                n = duplicate_nodes[n]
                ncount += 1
            nodeslist_new.append(n)
        # print(nodeslist_old)
        # print(nodeslist_new)
        fem_element_table[e] = tuple(nodeslist_new)   # convert nodeslist_new to a tuple before assignment
    print('Made ', ncount, ' node replacements in elementtable.')  # one node could be in more than one element!
    # for e in fem_element_table:
    #     print(e, ' --> ', fem_element_table[e])

    # use the changed fem_element_table to generate a new FemMesh
    nodes = []
    for e in fem_element_table:
        for n in fem_element_table[e]:
            if n not in nodes:
                nodes.append(n)
    import Fem
    m = Fem.FemMesh()
    for n in sorted(nodes):
        v = fem_mesh.Nodes[n]
        m.addNode(v.x, v.y, v.z, n)
    if getFemMeshElementType(fem_mesh) == 'beam':  # siehe shell und beam, warum nicht so, weil wohl be quadratic ein error kommt !!!
        for e in fem_element_table:
            if len(fem_element_table[e]) == 2:
                m.addEdge(fem_element_table[e][0], fem_element_table[e][1])
            elif len(fem_element_table[e]) == 3:
                print('adding quadratic edges by python is not supported')
                # m.addEdge(fem_element_table[e][0], fem_element_table[e][1], fem_element_table[e][2])
    elif getFemMeshElementType(fem_mesh) == 'shell':
        for e in fem_element_table:
            m.addFace(list(fem_element_table[e]))   # convert tuple to list before add
    elif getFemMeshElementType(fem_mesh) == 'volume':
        for e in fem_element_table:
            m.addVolume(list(fem_element_table[e]))   # convert tuple to list before add
        print('Only Volumes written, Faceload will not work anymore!')
    print('\nMeshinfo for new FEM Mesh')
    print(m)
    Fem.show(m)


def getFemElementTable(fem_mesh):
    # print("We'll try to get the fem_element_table!")
    fem_element_table = {}
    if fem_mesh.VolumeCount > 0:  # volume mesh
        print('volume mesh')
        for i in fem_mesh.Volumes:
            fem_element_table[i] = fem_mesh.getElementNodes(i)
    elif fem_mesh.VolumeCount == 0 and fem_mesh.FaceCount > 0:  # shell mesh
        print('shell mesh')
        for i in fem_mesh.Faces:
            fem_element_table[i] = fem_mesh.getElementNodes(i)
    elif fem_mesh.VolumeCount == 0 and fem_mesh.FaceCount == 0 and fem_mesh.EdgeCount > 0:  # beam mesh
        print('beam mesh')
        for i in fem_mesh.Edges:
            fem_element_table[i] = fem_mesh.getElementNodes(i)
    else:
        print('Neither volume nor shell nor beam mesh!')
    print(len(fem_element_table), ' elements in fem_element_table')
    return fem_element_table


def getFemMeshElementType(fem_mesh):
    # print("We'll try to get the fem_mesh_element_type")
    if fem_mesh.VolumeCount > 0:  # volume mesh
        fem_mesh_element_type = 'volume'
    elif fem_mesh.VolumeCount == 0 and fem_mesh.FaceCount > 0:  # shell mesh
        fem_mesh_element_type = 'shell'
    elif fem_mesh.VolumeCount == 0 and fem_mesh.FaceCount == 0 and fem_mesh.EdgeCount > 0:  # beam mesh
        fem_mesh_element_type = 'beam'
    else:
        fem_mesh_element_type = 'other'
    # print(fem_mesh_element_type, ' FEM Mesh')
    return fem_mesh_element_type


##########################################################################################
##########################################################################################
##########################################################################################
# shape2mesh funktionen !!!

'''
ziel dreiecke selectieren mesh wird erstellt
zwei funktionen
- erstelle aus einer dreiecksface (planar testen nicht noetig) ein s3 element

idee:
Part --> advanced utilities to create shapes (shape builder)
nur
Fem --> utilite to create FemMeshElements from shapes FEMMeshBuilder
'''

'''
schreib das mit ner klasse und die klasse hat attribute
wie
FemMesh
node_id

aber dann muss ich zum shape2mesh jedesmal eine klasse erstellen,
kann das die funktion selber machen.

triangelface2tianglelinearshell(femmesh, face, node_id)

cornernodes(femmesh, face, node_id)
'''

'''
T E S T mit auslagern
def shape2femmeshS3(shapelist):
    #     makes FemMesh from Shapes
    #    triangle faces --> S3 FEM Element
    import Fem
    import DraftVecUtils
    faces_triangle = shapelist
    m = Fem.FemMesh()
    node_id = 1
    for f in faces_triangle:
        node_info = add_triangle_corner_nodes(m, f, node_id)
        #{node_id:(node1_id, node2_id, node3_id)}

        print(node_info, ' ', node1_id, ' ', node2_id, ' ', node3_id)
        m.addFace([node1_id,node2_id,node3_id])

        # print(node1_id, ' ', node2_id, ' ', node3_id)
        #m.addFace([node1_id,node2_id,node3_id])

        # mein algorithmus ist nicht gerade clever, es fehlen Sortalgorithmen
        # problem: punkte in addFace in FreeCADs FemMesh muessen in uhrzeigersinn hinzugefuegt werden!!!
        # nur meshvoerderseite ist orange, CalculiX erwartet counterclockwise

    Fem.show(m)
    print('')
    print(len(m.Nodes))
    print(node_id-1)
'''


def less(a, b, center):
    '''
    taken from http://stackoverflow.com/questions/6989100/sort-points-in-clockwise-order
    compute the cross product of vectors (center -> a) x (center -> b)
    '''
    det = (a.x - center.x) * (b.y - center.y) - (b.x - center.x) * (a.y - center.y)
    if det < 0:
        return True
    if det > 0:
        return False


def shape2femmeshS3(shapelist):
    '''makes FemMesh from Shapes
    triangle faces --> S3 FEM Element
    '''
    import Fem
    import DraftVecUtils
    faces_triangle = shapelist
    m = Fem.FemMesh()
    node_id = 1
    for f in faces_triangle:
        # corner nodes
        node1_id = None
        node2_id = None
        node3_id = None

        # folgendes geht wohl auch viel weniger teuer !!!
        # check if second point is counterclockwise, if not switch point 2 and 3
        vert = []
        vert.append(f.Vertexes[0])
        if less(f.Vertexes[1].Point, f.Vertexes[2].Point, f.CenterOfMass) is True:
            vert.append(f.Vertexes[2])
            vert.append(f.Vertexes[1])
        else:
            vert.append(f.Vertexes[1])
            vert.append(f.Vertexes[2])

        for i, v in enumerate(vert):
            if node_id == 1:
                m.addNode(v.X, v.Y, v.Z, node_id)
                print(node_id, ' --> ', m.Nodes[node_id])
                node1_id = node_id
                node_id += 1
            else:
                for meshnode in m.Nodes:
                    vec1 = m.Nodes[meshnode]
                    vec2 = v.Point
                    if DraftVecUtils.equals(vec1, vec2):  # beachte precision in Draft Preferences!!!
                        # wenn doppelte punkte --> kein zusammenhaengendes mesh --> nur einzelne nicht verbundene mesh elemente
                        if i == 0:
                            node1_id = meshnode
                            print(node1_id, ' --> ', m.Nodes[meshnode])
                        elif i == 1:
                            node2_id = meshnode
                            print(node2_id, ' --> ', m.Nodes[meshnode])
                        elif i == 2:
                            node3_id = meshnode
                            print(node3_id, ' --> ', m.Nodes[meshnode])
                        break  # kann nur maximal einmal True ergeben schleife beenden
                else:  # evaluated if for is terminated normaly (not by break)
                    m.addNode(v.X, v.Y, v.Z, node_id)
                    print(node_id, ' --> ', m.Nodes[node_id])
                    if i == 0:
                        node1_id = node_id
                    elif i == 1:
                        node2_id = node_id
                    elif i == 2:
                        node3_id = node_id
                    node_id += 1

        print(node1_id, ' ', node2_id, ' ', node3_id)
        m.addFace([node1_id, node2_id, node3_id])

        # mein algorithmus ist nicht gerade clever, es fehlen Sortalgorithmen
        # problem: punkte in addFace in FreeCADs FemMesh muessen in uhrzeigersinn hinzugefuegt werden!!!
        # nur meshvoerderseite ist orange, CalculiX erwartet counterclockwise

    Fem.show(m)
    print('')
    print(len(m.Nodes))
    print(node_id - 1)


###################################################################################################
def shape2femmeshS6(shapelist):
    # S6 funktion nicht aendern -->  ist wie meine sich copy die sofort da is!
    '''makes FemMesh from Shapes
    triangle faces --> S6 FEM Element
    '''
    import Fem
    import DraftVecUtils
    import Part
    faces_triangle = shapelist
    m = Fem.FemMesh()
    node_id = 1
    for f in faces_triangle:
        # corner nodes
        node1_id = None
        node2_id = None
        node3_id = None
        for i, v in enumerate(f.Shape.Vertexes):
            if node_id == 1:
                m.addNode(v.X, v.Y, v.Z, node_id)
                print(node_id, ' --> ', m.Nodes[node_id])
                node1_id = node_id
                node_id += 1
            else:
                for meshnode in m.Nodes:
                    vec1 = m.Nodes[meshnode]
                    vec2 = v.Point
                    if DraftVecUtils.equals(vec1, vec2):  # beachte precision in Draft Preferences!!!
                        # wenn doppelte punkte --> kein zusammenhaengendes mesh --> nur einzelne nicht verbundene mesh elemente
                        if i == 0:
                            node1_id = meshnode
                            print(node1_id, ' --> ', m.Nodes[meshnode])
                        elif i == 1:
                            node2_id = meshnode
                            print(node2_id, ' --> ', m.Nodes[meshnode])
                        elif i == 2:
                            node3_id = meshnode
                            print(node3_id, ' --> ', m.Nodes[meshnode])
                        break  # kann nur maximal einmal True ergeben schleife beenden
                else:  # evaluated if for is terminated normaly (not by break)
                    m.addNode(v.X, v.Y, v.Z, node_id)
                    print(node_id, ' --> ', m.Nodes[node_id])
                    if i == 0:
                        node1_id = node_id
                    elif i == 1:
                        node2_id = node_id
                    elif i == 2:
                        node3_id = node_id
                    node_id += 1

        # !!! code bis hierher mit S3 identical --> auslagern !!!

        # middle nodes
        node4_id = None  # middle of node1_id and node2_id
        node5_id = None  # middle of node2_id and node3_id
        node6_id = None  # middle of node3_id and node4_id
        node4_vec = Part.makeLine(m.Nodes[node1_id], m.Nodes[node2_id]).CenterOfMass
        node5_vec = Part.makeLine(m.Nodes[node2_id], m.Nodes[node3_id]).CenterOfMass
        node6_vec = Part.makeLine(m.Nodes[node3_id], m.Nodes[node1_id]).CenterOfMass

        for meshnode in m.Nodes:
            vec1 = m.Nodes[meshnode]
            vec2 = node4_vec
            if DraftVecUtils.equals(vec1, vec2):
                node4_id = meshnode
                print(node4_id, ' --> ', m.Nodes[meshnode])
                break
        else:
            m.addNode(node4_vec.x, node4_vec.y, node4_vec.z, node_id)
            print(len(m.Nodes), '   ', node_id, ' --> ', m.Nodes[node_id])
            node4_id = node_id
            node_id += 1

        for meshnode in m.Nodes:
            vec1 = m.Nodes[meshnode]
            vec2 = node5_vec
            if DraftVecUtils.equals(vec1, vec2):
                node5_id = meshnode
                print(node5_id, ' --> ', m.Nodes[meshnode])
                break
        else:
            m.addNode(node5_vec.x, node5_vec.y, node5_vec.z, node_id)
            print(len(m.Nodes), '   ', node_id, ' --> ', m.Nodes[node_id])
            node5_id = node_id
            node_id += 1

        for meshnode in m.Nodes:
            vec1 = m.Nodes[meshnode]
            vec2 = node6_vec
            if DraftVecUtils.equals(vec1, vec2):
                node6_id = meshnode
                print(node6_id, ' --> ', m.Nodes[meshnode])
                break
        else:
            m.addNode(node6_vec.x, node6_vec.y, node6_vec.z, node_id)
            print(node_id, ' --> ', m.Nodes[node_id])
            node6_id = node_id
            node_id += 1

        # addFace
        print(node1_id, ' ', node2_id, ' ', node3_id, ' ', node4_id, ' ', node5_id, ' ', node6_id)
        m.addFace([node1_id, node2_id, node3_id, node4_id, node5_id, node6_id])

    Fem.show(m)


###################################################################################################
def add_triangle_corner_nodes(femmesh, face, node_id):
        import DraftVecUtils
        f = face
        m = femmesh
        node_id = node_id
        # corner nodes
        node1_id = None
        node2_id = None
        node3_id = None
        for i, v in enumerate(f.Shape.Vertexes):
            if node_id == 1:
                m.addNode(v.X, v.Y, v.Z, node_id)
                print(node_id, ' --> ', m.Nodes[node_id])
                node1_id = node_id
                node_id += 1
            else:
                for meshnode in m.Nodes:
                    vec1 = m.Nodes[meshnode]
                    vec2 = v.Point
                    if DraftVecUtils.equals(vec1, vec2):  # beachte precision in Draft Preferences!!!
                        # wenn doppelte punkte --> kein zusammenhaengendes mesh --> nur einzelne nicht verbundene mesh elemente
                        if i == 0:
                            node1_id = meshnode
                            print(node1_id, ' --> ', m.Nodes[meshnode])
                        elif i == 1:
                            node2_id = meshnode
                            print(node2_id, ' --> ', m.Nodes[meshnode])
                        elif i == 2:
                            node3_id = meshnode
                            print(node3_id, ' --> ', m.Nodes[meshnode])
                        break  # kann nur maximal einmal True ergeben schleife beenden
                else:  # evaluated if for is terminated normaly (not by break)
                    m.addNode(v.X, v.Y, v.Z, node_id)
                    print(node_id, ' --> ', m.Nodes[node_id])
                    if i == 0:
                        node1_id = node_id
                    elif i == 1:
                        node2_id = node_id
                    elif i == 2:
                        node3_id = node_id
                    node_id += 1
        return {node_id: (node1_id, node2_id, node3_id)}


###################################################################################################
def shape2femmeshS4(shapelist):
    '''makes FemMesh from Shapes
    quadrangle faces --> S4 FEM Element
    '''
    import Fem
    import DraftVecUtils
    faces_triangle = shapelist
    m = Fem.FemMesh()
    node_id = 1
    for f in faces_triangle:
        # corner nodes
        node1_id = None
        node2_id = None
        node3_id = None
        node4_id = None
        for i, v in enumerate(f.Shape.Vertexes):
            if node_id == 1:
                m.addNode(v.X, v.Y, v.Z, node_id)
                print(node_id, ' --> ', m.Nodes[node_id])
                node1_id = node_id
                node_id += 1
            else:
                for meshnode in m.Nodes:
                    vec1 = m.Nodes[meshnode]
                    vec2 = v.Point
                    if DraftVecUtils.equals(vec1, vec2):  # beachte precision in Draft Preferences!!!
                        # wenn doppelte punkte --> kein zusammenhaengendes mesh --> nur einzelne nicht verbundene mesh elemente
                        if i == 0:
                            node1_id = meshnode
                            print(node1_id, ' --> ', m.Nodes[meshnode])
                        elif i == 1:
                            node2_id = meshnode
                            print(node2_id, ' --> ', m.Nodes[meshnode])
                        elif i == 2:
                            node3_id = meshnode
                            print(node3_id, ' --> ', m.Nodes[meshnode])
                        elif i == 3:
                            node4_id = meshnode
                            print(node4_id, ' --> ', m.Nodes[meshnode])
                        break  # kann nur maximal einmal True ergeben schleife beenden
                else:  # evaluated if for is terminated normaly (not by break)
                    m.addNode(v.X, v.Y, v.Z, node_id)
                    print(node_id, ' --> ', m.Nodes[node_id])
                    if i == 0:
                        node1_id = node_id
                    elif i == 1:
                        node2_id = node_id
                    elif i == 2:
                        node3_id = node_id
                    elif i == 3:
                        node4_id = node_id
                    node_id += 1

        print(node1_id, ' ', node2_id, ' ', node3_id, ' ', node4_id)
        m.addFace([node1_id, node2_id, node3_id, node4_id])

        # punkte in addFace in FreeCADs FemMesh muessen in uhrzeigersinn hinzugefuegt werden --> siehe S6

    Fem.show(m)
    print('')
    print(len(m.Nodes))
    print(node_id - 1)

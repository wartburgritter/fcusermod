# -*- coding: utf-8 -*-
# FreeCAD tools of the FEMToolBox
# (c) 2014 Bernd Hahnebach

# ***************************************************************************
# *   (c) Bernd Hahnebach (bernd@bimstatik.org) 2014                        *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# *   Bernd Hahnebach 2014                                                  *
# ***************************************************************************/


import FreeCAD
import FreeCADGui


# ************************************************************************************************
class TOOL1():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())
        # Icon ist nur activ, wenn etwas selektiert ist !!!

    def Activated(self):
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            sel = selection[0]
            # print('\nSelected Object: ',sel.Name)
            if sel.isDerivedFrom("Fem::FemMeshObject"):
                print(sel.FemMesh)
        else:
            print('Mehr als ein Objekt selektiert!')

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'Print FemMesh Info',
            'ToolTip': 'Print FemMesh Info'
        }


# ************************************************************************************************
class TOOL2():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())
        # Icon ist nur activ, wenn etwas selektiert ist !!!

    def Activated(self):
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            sel = selection[0]
            print('\nSelected Object: ', sel.Name)
            if sel.isDerivedFrom("Fem::FemMeshObject"):
                m = sel.FemMesh
                if sel.FemMesh.Groups:
                    for g in sel.FemMesh.Groups:
                        print('ID: ' + str(g) + ' --> ' + m.getGroupName(g)) + ' --> ' + m.getGroupElementType(g)
                        print(m.getGroupElements(g))
                        print('')
                else:
                    print('No grups in mesh')
            else:
                print('Select a FEM mesh object, not something else!')
        else:
            print('Mehr als ein Objekt selektiert!')

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'Print Detailed FemMesh Group Info',
            'ToolTip': 'Print Detailed FemMesh Group Info'
        }


# ************************************************************************************************
class TOOL3():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        '''opens results of selected analysis in cgx'''
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            sel = selection[0]
            # print('\nSelected Object: ' ,sel.Name)
            if hasattr(sel, "Proxy") and sel.Proxy.Type == 'FemAnalysis':
                # print(sel.Name)
                analysis = sel
                for m in FreeCAD.activeDocument().MechanicalAnalysis.Member:
                    if m.isDerivedFrom("Fem::FemMeshObject"):
                        filename = m.Name
                        break
                else:  # for loop runs through all members but no mesh found
                    print('No Mesh --> No Output')
                    return
                from platform import system
                if system() == "Linux":
                    if analysis.OutputDir == '':
                        ccxoutputdir = '/tmp/'   # std output on Linux if OutputDir is left blank
                    else:
                        ccxoutputdir = analysis.OutputDir
                    filename = ccxoutputdir + filename + '.frd'
                    cgxcommand = 'cgx ' + filename
                    print(cgxcommand)
                    import subprocess
                    try:
                        output = subprocess.check_output(
                            [cgxcommand, '-1'],
                            shell=True,
                            stderr=subprocess.STDOUT,
                        )
                    except:
                        FreeCAD.Console.PrintError('Error!!, sorry..')
                    print(output)
                elif system() == "Windows":
                    if analysis.OutputDir == '':
                        ccxoutputdir = 'C:/tmp/'   # std output on Windows if OutputDir is left blank
                    else:
                        ccxoutputdir = analysis.OutputDir
                        print('The OutputDir of the MechanicalAnalysis was changed. The macro may fail to work!')
                    filename = ccxoutputdir + filename + '.inp'
                    print(filename)
                    # the webbrowser.open() does not work directly on *.frd --> *.inp are used to open SciTE
                    # in windows inp-file extension has to be assigned to open SciTE on double click (works if SciTE is installed)
                    # The OutputDir of the MechanicalAnalysis should left blank --> C:/tmp is used
                    print('In SciTE --> Menue Tools --> Post Process or Shift+F10')
                    import webbrowser
                    webbrowser.open(filename)
                else:
                    print("Your OS is ", system(), ", which is not yet supported !")
            else:
                print('Select one MechanicalAnalysis Object')

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'open results file in cgx',
            'ToolTip': 'open results off selected analysis in cgx'
        }


# ************************************************************************************************
class TOOL4():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            o = selection[0]
            # print('\nSelected Object: ',o.Name)
            if hasattr(o, "FemMesh"):
                # print(o.FemMesh)
                # selection.HighlightedNodes = range(30)

                # command
                # Gui.ActiveDocument.Plane_Mesh.HighlightedNodes = range(App.ActiveDocument.Plane_Mesh.FemMesh.NodeCount)
                print('Copy command  into python konsole!')
                print('Hey für 3D netze sollte es gehen, aber da kann ich es auch direkt im ViewProvider einstellen')
                print('Gui.ActiveDocument.', o.Name, '.HighlightedNodes = range(0)')
                print('Gui.ActiveDocument.', o.Name, '.HighlightedNodes = range(App.ActiveDocument.', o.Name, '.FemMesh.NodeCount)')
            print('Mehr als ein Objekt selektiert!')

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'commands to copy for hightlight all fem mesh nodes',
            'ToolTip': 'commands to copy for hightlight all nodes off selected fem mesh'
        }


# ************************************************************************************************
class TOOL5():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 2:
            o1 = selection[0]
            o2 = selection[1]
            print('\n')
            print('o1 --> Selected Object: ', o1.Name)
            print('o2 --> Selected Object: ', o2.Name)

            if hasattr(o1, "Shape"):  # ein femmesh hat auch eine shape !!!
                if hasattr(o2, "FemMesh"):
                    print(o1.Name, ' has a Shape!')
                    print(o2.Name, ' has a FemMesh!')
                    ShapeObject = o1
                    MeshObject = o2
            if hasattr(o2, "Shape"):
                if hasattr(o1, "FemMesh"):
                    print(o1.Name, ' has a FemMesh!')
                    print(o2.Name, ' has a Shape!')
                    ShapeObject = o2
                    MeshObject = o1

            print('ShapeObject: ', ShapeObject.Name)
            print('MeshObject: ', MeshObject.Name)

            n = []
            if ShapeObject.Shape.ShapeType == 'Compsolid':
                print('FoundCompsolid --> not supported --> downgrade to Solid')
            if ShapeObject.Shape.ShapeType == 'Solid':
                print('Found Solid or Compsolid')
                n = MeshObject.FemMesh.getNodesBySolid(ShapeObject.Shape)
            elif ShapeObject.Shape.ShapeType == 'Shell':
                print('Found Shell --> not supported --> downgrade to Face')
            elif ShapeObject.Shape.ShapeType == 'Face':
                print('Found Face')
                n = MeshObject.FemMesh.getNodesByFace(ShapeObject.Shape)
            elif ShapeObject.Shape.ShapeType == 'Wire':
                print('Found Wire --> not supported --> downgrade to Edge')
            elif ShapeObject.Shape.ShapeType == 'Edge':
                print('Found Edge')
                n = MeshObject.FemMesh.getNodesByEdge(ShapeObject.Shape)
            elif ShapeObject.Shape.ShapeType == 'Vertex':
                print('Vertex')
                n = MeshObject.FemMesh.getNodesByVertex(ShapeObject.Shape)
            elif ShapeObject.Shape.ShapeType == 'Compound':
                print('Compound')
            else:
                print("I don't know what shape the selected shape object has!")

            # print(n)
            print('\n Anzahl gruener Knoten: ', len(n))
            if len(n) > 0:
                # Gui.ActiveDocument.Plane_Mesh.HighlightedNodes = range(App.ActiveDocument.Plane_Mesh.FemMesh.NodeCount)
                print('Gui.ActiveDocument.', MeshObject.Name, '.HighlightedNodes = range(0)')
                # print('Gui.ActiveDocument.', MeshObject.Name, '.HighlightedNodes = ', n)
                MeshObject.ViewObject.HighlightedNodes = n
                # Hinweis: VO geht wie App und Gui nur in der PythonKonsole
        else:
            print('You should select exact two objects, one mesh and one shape!!!')
            return

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'highlight nodes inside shape',
            'ToolTip': 'highlight nodes inside shape'
        }


# ************************************************************************************************
class TOOL6():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 2:
            o1 = selection[0]
            o2 = selection[1]
            print('\n')
            print('o1 --> Selected Object: ', o1.Name)
            print('o2 --> Selected Object: ', o2.Name)

            if hasattr(o1, "Shape"):  # ein femmesh hat auch eine shape !!!
                if hasattr(o2, "FemMesh"):
                    print(o1.Name, ' has a Shape!')
                    print(o2.Name, ' has a FemMesh!')
                    ShapeObject = o1
                    MeshObject = o2
            if hasattr(o2, "Shape"):
                if hasattr(o1, "FemMesh"):
                    print(o1.Name, ' has a FemMesh!')
                    print(o2.Name, ' has a Shape!')
                    ShapeObject = o2
                    MeshObject = o1

            print('ShapeObject: ', ShapeObject.Name)
            print('MeshObject: ', MeshObject.Name)

            import femutils
            # get the element sets for materials
            abaqus_element_table = femutils.getAbaqusElementTableHack(MeshObject)  # { elementid : [ nodeid, nodeid, ... , nodeid ] }

            n = []
            nshell = []
            ninsidesolid = []
            n2 = []
            n3 = []
            e = []

            # !!!!!!!!!!!1warum errot element which cuts materialtrennface, aber higlighted nodes = 0  ?????

            if ShapeObject.Shape.ShapeType == 'Solid':
                print('Solid : ', ShapeObject.Name)
                for face in ShapeObject.Shape.Faces:
                    facenodes = MeshObject.FemMesh.getNodesByFace(face)
                    for nid in facenodes:
                        if nid not in nshell:
                            nshell.append(nid)

                n = MeshObject.FemMesh.getNodesBySolid(ShapeObject.Shape)
                # nodes inside and onshell of the solid
                # in n die knoten der trennflaeche abziehen

                for nid in n:
                    if nid not in nshell:
                        ninsidesolid.append(nid)  # nur knoten nicht auf shell

                e = femutils.getElementsByNodes(abaqus_element_table, ninsidesolid)
                # elements passend zu den nodes of ninsidesolid
                # in e auch elemente die nurknoten auf der trennflaeche

                n2 = femutils.getNodesByElements(abaqus_element_table, e)
                for nid in n2:
                    if nid not in n:
                        n3.append(nid)
            else:
                print("Select a solid !")

            nodes_to_highlight = n3
            # print(nodes_to_highlight)
            print('\n Anzahl gruener Knoten: ', len(nodes_to_highlight))
            if len(nodes_to_highlight) > 0:
                # Gui.ActiveDocument.Plane_Mesh.HighlightedNodes = range(App.ActiveDocument.Plane_Mesh.FemMesh.NodeCount)
                print('Gui.ActiveDocument.', MeshObject.Name, '.HighlightedNodes = range(0)')
                # print('Gui.ActiveDocument.', MeshObject.Name, '.HighlightedNodes = ', nodes_to_highlight)
                MeshObject.ViewObject.HighlightedNodes = nodes_to_highlight
                # Hinweis: VO geht wie App und Gui nur in der PythonKonsole
        else:
            print('You should select exact two objects, one mesh and one shape!!!')
            return

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'highlight nodes outside shape',
            'ToolTip': 'highlight nodes outside shape'
        }


# ************************************************************************************************
class TOOL7():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            obj = selection[0]
            print('Selected Object: ', obj.Name)
            FreeCAD.ActiveDocument.MechanicalAnalysis.Member = FreeCAD.ActiveDocument.MechanicalAnalysis.Member + [obj]
            FreeCAD.ActiveDocument.recompute()
        else:
            print('Mehr als ein Objekt selektiert!')

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'add object to MechanicalAnalysis',
            'ToolTip': 'adds the selected object to the Analysis "MechanicalAnalysis"'
        }


# ************************************************************************************************
class TOOL8():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            obj = selection[0]
            print('Selected Object: ', obj.Name)
            addObject = False
            if obj.isDerivedFrom("App::MaterialObjectPython"):
                addObject = True
            elif obj.isDerivedFrom("Fem::ConstraintFixed"):
                addObject = True
            elif obj.isDerivedFrom("Fem::ConstraintForce"):
                addObject = True
            elif obj.isDerivedFrom("Fem::ConstraintPressure"):
                addObject = True
            elif obj.isDerivedFrom("Fem::FemMeshObject"):
                addObject = True
            elif hasattr(obj, "Proxy") and obj.Proxy.Type == "FemBeamSection":
                addObject = True
            elif hasattr(obj, "Proxy") and obj.Proxy.Type == "FemShellThickness":
                addObject = True
            else:
                print('Not able to add this object to an MechanicalAnalysis')

            if addObject:
                member = FreeCAD.ActiveDocument.MechanicalAnalysis.Member
                member.remove(obj)
                FreeCAD.ActiveDocument.MechanicalAnalysis.Member = member
                FreeCAD.ActiveDocument.recompute()

        else:
            print('Mehr als ein Objekt selektiert!')

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'remove object from MechanicalAnalysis',
            'ToolTip': 'removes the selected object from the Analysis "MechanicalAnalysis"'
        }


# ************************************************************************************************
class TOOL9():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        selection = FreeCADGui.Selection.getSelectionEx()
        if len(selection) == 2:
            sel1 = selection[0]
            sel2 = selection[1]
            print('\n')
            print('Selected Object1: ', sel1.ObjectName)
            print('Selected Object2: ', sel2.ObjectName)
            if sel2.Object.isDerivedFrom("App::MaterialObjectPython"):
                MaterialObject = sel2.Object
                print(MaterialObject.Name, ' is MaterialObject!')
                if hasattr(sel1.Object, "Shape"):
                    print(sel1.ObjectName, ' has a Shape!')
                    if len(sel1.SubElementNames) != 1:
                        if sel1.Object.Shape.ShapeType == 'Solid':
                            print(
                                'Solids are not yet supported. '
                                'I do not know how to add them '
                                'to a App::PropertyLinkSubList'
                            )
                            return
                        print(
                            'Exact one subelement should be selected! '
                            'Not two subs and not the whole shape! '
                        )
                        print(
                            'Did you select a object in tree view? '
                            'You should select the object in the sceen (3Dview)'
                        )
                        return
                else:
                    print('the second selected object has no Shape')
                    return
            else:
                print(
                    'Select the Shape Object first, use ctrl '
                    'for selecting the MaterialObject in TreeView'
                )
                return
        else:
            print('Exact one shape or subshape and one MaterialObject should be selected !')
            return

        if hasattr(MaterialObject, 'References'):
            print(MaterialObject.References)
        else:
            MaterialObject.addProperty(
                "App::PropertyLinkSubList",
                "References", "Material",
                "List of material shapes"
            )

        print('reset MaterialObject.References')
        MaterialObject.References = [(sel1.Object, sel1.SubElementNames[0])]
        print(MaterialObject.References)

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'add Material Reference Objects',
            'ToolTip': 'add Material Reference Objects'
        }


# ************************************************************************************************
class TOOL10():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        import femutils
        selection = FreeCADGui.Selection.getSelection()
        trianglefaces = []
        for s in selection:
            if hasattr(s, "Shape"):
                if s.Shape.ShapeType == 'Face':
                    if len(s.Shape.Vertexes) == 3:
                        trianglefaces.append(s.Shape)
                    else:
                        print('Non triangle face!: ', s)
                    # print('triangle face found')
                elif s.Shape.ShapeType == 'Compound':
                    for f in s.Shape.Faces:
                        if len(f.Vertexes) == 3:
                            trianglefaces.append(f)
                        else:
                            print('Non triangle face!: ', f)
                else:
                    print('Neither Compound nor Face ', s)
                    return
        print(trianglefaces)
        femutils.shape2femmeshS3(trianglefaces)

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'shape2femmeshS3',
            'ToolTip': 'shape2femmeshS3, select triangle faces or compound of triangle faces'
        }


# ************************************************************************************************
class TOOL11():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        import femutils
        selection = FreeCADGui.Selection.getSelection()
        for s in selection:
            if hasattr(s, "Shape"):
                if s.Shape.ShapeType == 'Face' and len(s.Shape.Vertexes) == 3:
                    pass
                    # print('triangle face found')
                else:
                    print('Select only triangle faces!')
                    return
            else:
                print('Select only shapes!')
                return
        femutils.shape2femmeshS6(selection)

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'shape2femmeshS6',
            'ToolTip': 'shape2femmeshS6'
        }


# ************************************************************************************************
class TOOL12():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        import femutils
        selection = FreeCADGui.Selection.getSelection()
        for s in selection:
            if hasattr(s, "Shape"):
                if s.Shape.ShapeType == 'Face' and len(s.Shape.Vertexes) == 4:
                    pass
                    # print('triangle face found')
                else:
                    print('Select only quadrangle faces!')
                    return
            else:
                print('Select only shapes!')
                return
        femutils.shape2femmeshS4(selection)

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'shape2femmeshS4',
            'ToolTip': 'shape2femmeshS4'
        }


# ************************************************************************************************
class TOOL13():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        import femutils
        selection = FreeCADGui.Selection.getSelection()
        compoundsofftrianglefaces = []
        for s in selection:
            if hasattr(s, "Shape"):
                if s.Shape.ShapeType == 'Compound':
                    for f in s.Shape.Faces:
                        if not len(f.Vertexes) == 3:
                            print('Non triangle face!: ', f)
                            compoundsofftrianglefaces = []
                            return
                    compoundsofftrianglefaces.append(s.Shape)
                else:
                    print('Neither Compound nor Face ', s)
                    compoundsofftrianglefaces = []
                    return
        print(compoundsofftrianglefaces)
        femutils.makeFreecadMeshFromCompoundOffFaces(compoundsofftrianglefaces)

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'makeFreecadMeshFromCompoundOffFaces',
            'ToolTip': 'makeFreecadMeshFromCompoundOffFaces, select compound of triangle faces'
        }


# ************************************************************************************************
class TOOL14():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            sel = selection[0]
            # print('\nSelected Object: ',sel.Name)
            if sel.isDerivedFrom("Mesh::Feature"):
                print(sel.Mesh)
                import femutils
                femutils.makeFemMeshS3FromFreecadMesh(sel.Mesh)
                sel.ViewObject.Visibility = False
            else:
                print('Select FreeCAD Mesh --> ObjType --> Mesh::Feature')
        else:
            print('Mehr als ein Objekt selektiert!')

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'makeFemMeshS3FromFreecadMesh',
            'ToolTip': 'makeFemMeshS3FromFreecadMesh'
        }


# ************************************************************************************************
class TOOL15():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            sel = selection[0]
            # print('\nSelected Object: ',sel.Name)
            if hasattr(sel, "FemMesh"):
                # print(sel.FemMesh)
                import femutils
                femutils.deleteDuplicatesAndWriteFemMesh(sel.FemMesh)
                sel.ViewObject.Visibility = False
        else:
            print('Mehr als ein Objekt selektiert!')

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'deleteDuplicatesAndWriteFemMesh',
            'ToolTip': 'deleteDuplicatesAndWriteFemMesh'
        }


# ************************************************************************************************
class TOOL16():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            sel = selection[0]
            # print('\nSelected Object: ',sel.Name)
            if sel.isDerivedFrom("App::MaterialObjectPython"):
                MaterialObject = sel
                print('Selection: ', MaterialObject.Name, ' is a MaterialObject')
                # print(MaterialObject.Material)
                print(MaterialObject.Material['Name'])
                print(MaterialObject.Material['YoungsModulus'])
                print(MaterialObject.Material['Density'])
                print(MaterialObject.Material['PoissonRatio'])
        else:
            print('Mehr als ein Objekt selektiert!')

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'getMechanicalMaterialValues',
            'ToolTip': 'getMechanicalMaterialValues'
        }


# ************************************************************************************************
class TOOL17():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            sel = selection[0]
            # print('\nSelected Object: ',sel.Name)
            if hasattr(sel, "Shape"):
                print('We found a opbjct with a shape! Lets start ...')
                import Part
                if sel.Shape.ShapeType == 'CompSolid':
                    print('Found CompSolid')
                    if len(sel.Shape.Solids) == 2:
                        s1 = sel.Shape.Solids[0]
                        s2 = sel.Shape.Solids[1]
                        # check on identical faces
                        for i in s1.Faces:
                            for j in s2.Faces:
                                print("Is same: %s" % (i.isSame(j)))
                                if i.isSame(j):
                                    # pass
                                    Part.show(i)
                    elif len(sel.Shape.Solids) > 2:
                        print('In development')
                        '''
                        for i in sel.Shape.Faces:
                            for j in sel.Shape.Faces:
                                if j is not i:
                                    #print("Is same: %s" % (i.isSame(j)))
                                    if i.isSame(j):
                                        # pass
                                        Part.show(i)
                        '''
                    else:
                        print('Not possible! An CompSolid has to have at least two solids!')
                elif sel.Shape.ShapeType == 'Compound':
                    print('Kein CompSolid, aber evtl. Compound mit Compsolid child !')
                    if len(sel.Shape.CompSolids) == 1:
                        if len(sel.Shape.Solids) == 2:
                            s1 = sel.Shape.Solids[0]
                            s2 = sel.Shape.Solids[1]
                            # check on identical faces
                            for i in s1.Faces:
                                for j in s2.Faces:
                                    print("Is same: %s" % (i.isSame(j)))
                                    if i.isSame(j):
                                        # pass
                                        Part.show(i)
                    else:
                        print('Compound hat nicht genau einen CompSolid!')
                else:
                    print('weder CompSolid noch Compound!')
            else:
                print('Keine Shape!')
        else:
            print('Mehr als ein Objekt selektiert!')

    def GetResources(self):
        return {
            'Pixmap': 'python',
            'MenuText': 'et the shared faces of a CompSolid',
            'ToolTip': 'get the shared faces of a CompSolid'
        }


# ************************************************************************************************
FreeCADGui.addCommand('FEMToolBox_tool1', TOOL1())    # Print FemMesh Info
FreeCADGui.addCommand('FEMToolBox_tool2', TOOL2())  # Print Group Info
FreeCADGui.addCommand('FEMToolBox_tool3', TOOL3())    # open results file in cgx
FreeCADGui.addCommand('FEMToolBox_tool4', TOOL4())    # commands to copy for hightlight all nodes off selected fem mesh
FreeCADGui.addCommand('FEMToolBox_tool5', TOOL5())    # highlight nodes inside shape
FreeCADGui.addCommand('FEMToolBox_tool6', TOOL6())    # highlight nodes outside shape
FreeCADGui.addCommand('FEMToolBox_tool7', TOOL7())    # add object to MechanicalAnalysis
FreeCADGui.addCommand('FEMToolBox_tool8', TOOL8())    # remove object from MechanicalAnalysis
FreeCADGui.addCommand('FEMToolBox_tool9', TOOL9())    # add Material Reference Objects
FreeCADGui.addCommand('FEMToolBox_tool10', TOOL10())    # shape2femmeshS3, select triangle faces or compound of triangle faces
FreeCADGui.addCommand('FEMToolBox_tool11', TOOL11())  # shape2femmeshS6
FreeCADGui.addCommand('FEMToolBox_tool12', TOOL11())  # shape2femmeshS4
FreeCADGui.addCommand('FEMToolBox_tool13', TOOL13())  # makeFreecadMeshFromCompoundOffFaces
FreeCADGui.addCommand('FEMToolBox_tool14', TOOL14())  # makeFemMeshS3FromFreecadMesh
FreeCADGui.addCommand('FEMToolBox_tool15', TOOL15())  # deleteDuplicatesAndWriteFemMesh
FreeCADGui.addCommand('FEMToolBox_tool16', TOOL16())  # getMechanicalMaterialValues
FreeCADGui.addCommand('FEMToolBox_tool17', TOOL17())  # get the shared faces of a CompSolid

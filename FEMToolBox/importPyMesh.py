# ***************************************************************************
# *                                                                         *
# *   Copyright (c) 2016 - Bernd Hahnebach <bernd@bimstatik.org>            *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************


import FreeCAD
import os
import femmesh.meshtools as FemMeshTools


__title__ = "FreeCAD Python Mesh reader and writer"
__author__ = "Bernd Hahnebach "
__url__ = "http://www.freecadweb.org"


# ************************************************************************************************
# ********* generic FreeCAD import and export methods ********************************************
# names are fix given from FreeCAD, these methods are called from FreeCAD
# they are set in FEM modules Init.py

if open.__module__ in ['__builtin__','io']:
    pyopen = open
    # pyopen is used to open a file


def open(filename):
    "called when freecad opens a file"
    docname = os.path.splitext(os.path.basename(filename))[0]
    insert(filename, docname)


def insert(filename, docname):
    "called when freecad wants to import a file"
    try:
        doc = FreeCAD.getDocument(docname)
    except NameError:
        doc = FreeCAD.newDocument(docname)
    FreeCAD.ActiveDocument = doc
    import_python_mesh(filename)


# export mesh to python
def export(objectslist, filename):
    "called when freecad exports a file"
    if len(objectslist) != 1:
        FreeCAD.Console.PrintError("This exporter can only export one object.\n")
        return
    obj = objectslist[0]
    if not obj.isDerivedFrom("Fem::FemMeshObject"):
        FreeCAD.Console.PrintError("No FEM mesh object selected.\n")
        return
    femnodes_mesh = obj.FemMesh.Nodes
    femelement_table = FemMeshTools.get_femelement_table(obj.FemMesh)
    if FemMeshTools.is_solid_femmesh(obj.FemMesh):
        fem_mesh_type = "Solid"
    elif FemMeshTools.is_face_femmesh(obj.FemMesh):
        fem_mesh_type = "Face"
    elif FemMeshTools.is_edge_femmesh(obj.FemMesh):
        fem_mesh_type = "Edge"
    else:
        FreeCAD.Console.PrintError("Export of this FEM mesh to Python not supported.\n")
        return
    f = pyopen(filename, "w")
    write_python_mesh_to_file(femnodes_mesh, femelement_table, fem_mesh_type, f)
    f.close()


# ************************************************************************************************
# ********* module specific methods **************************************************************

def import_python_mesh(filename, analysis=None):
    '''insert a FreeCAD FEM Mesh object in the ActiveDocument
    '''
    mesh_name = os.path.basename(os.path.splitext(filename)[0])
    femmesh = read_python_mesh(filename)
    if femmesh:
        mesh_object = FreeCAD.ActiveDocument.addObject('Fem::FemMeshObject', mesh_name)
        mesh_object.FemMesh = femmesh


def read_python_mesh(python_mesh_input):
    ''' read a Python mesh file and returns a Fem::FemMesh
    '''
    # FreeCAD.Console.PrintError("Not yet implemented\n")
    python_mesh_file = pyopen(python_mesh_input, "r")

    import FreeCADGui
    for no, line in enumerate(python_mesh_file):
        line = line.strip()
        # print(line)
        FreeCADGui.doCommand(line)

    python_mesh_file.close()

    return None


# ********* module specific methods *********
def write_python_mesh_to_file(femnodes_mesh, femelement_table, fem_mesh_type, f):
    write_makemesh = True  # write import Fem and Fem.show to file if set to True

    mesh_name = "femmesh"
    f.write("# Created by FreeCAD Python FEM mesh exporter\n")
    if write_makemesh:
        f.write("import Fem\n")
    f.write(mesh_name + " = Fem.FemMesh()\n")
    f.write("\n")

    # nodes
    f.write("# nodes\n")
    for node in femnodes_mesh:
        # print(node, ' --> ', femnodes_mesh[node])
        vec = femnodes_mesh[node]
        f.write("{0}.addNode({1}, {2}, {3}, {4})\n".format(mesh_name, vec.x, vec.y, vec.z, node))
    f.write("\n\n")

    # elements
    f.write("# elements\n")
    for element in femelement_table:
        # print(element, ' --> ', femelement_table[element])
        if fem_mesh_type == "Solid":
            f.write("{0}.addVolume({1}, {2})\n".format(mesh_name, list(femelement_table[element]), element))
        elif fem_mesh_type == "Face":
            f.write("{0}.addFace({1}, {2})\n".format(mesh_name, list(femelement_table[element]), element))
        elif fem_mesh_type == "Edge":
            f.write("{0}.addEdge({1}, {2})\n".format(mesh_name, list(femelement_table[element]), element))

    if write_makemesh:
        f.write("\n\n# show FEM mesh\n")
        f.write("Fem.show({0})\n".format(mesh_name))


'''
# objname = "Box_Mesh"
# objname = "hexa8"
# objname = "cal_can_18"
objname = "Plane_mesh"

meshobject = App.ActiveDocument.getObject(objname)
print(meshobject.FemMesh)
filepath = "/home/hugo/Desktop/quads/pythonmeshexporttest.py"
import importPyMesh
# im build path bearbeiten ... /home/hugo/Documents/dev/freecad/freecadbhb_dev/build/Mod/Fem/
reload(importPyMesh)
importPyMesh.export([meshobject] , filepath)


'''

# -*- coding: utf-8 -*-
# FreeCAD init script of the FEMToolBox  Workbench
# (c) 2014 Bernd Hahnebach

# ***************************************************************************
# *   (c) Bernd Hahnebach (bernd@bimstatik.org) 2014                        *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Lesser General Public License for more details.                   *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# *   Bernd Hahnebach 2014                                                  *
# ***************************************************************************/


class FEMToolBox(Workbench):
    "FEMToolBox workbench object"
    Icon = """
/* XPM */
static char * infologo_xpm[] = {
"16 16 33 1",
" 	c None",
".	c #61320B",
"+	c #5D420B",
"@	c #4F4C09",
"#	c #564930",
"$	c #754513",
"%	c #815106",
"&	c #666509",
"*	c #875F55",
"=	c #6E7000",
"-	c #756A53",
";	c #717037",
">	c #946637",
",	c #92710E",
"'	c #797A0A",
")	c #7C7720",
"!	c #8A8603",
"~	c #88886F",
"{	c #AF8181",
"]	c #999908",
"^	c #BB8D8D",
"/	c #AAAA00",
"(	c #A9A880",
"_	c #B5B419",
":	c #C1A9A9",
"<	c #B1B19A",
"[	c #BEBE00",
"}	c #B9B8B4",
"|	c #CACC00",
"1	c #D4D4BC",
"2	c #DBD2D0",
"3	c #EEEEED",
"4	c None",
"4444444444444444",
"4444443113444444",
"4444<;']]!;<^^24",
"444(&@!]]]=&#^{3",
"44<']')@++)!&*{^",
"44)]/[|//[/]'@{{",
"42=/_|||||[]!&*{",
"4(&][|||||[/'@#}",
"3-..,|||||[)&&~4",
"^*$%.!|||[!+/](4",
"^{%%%._[[_&/[_14",
":{>%%.!//])_[_44",
"2{{%%+!]!!)]]344",
"4:{{#@&=&&@#3444",
"44224}~--~}44444",
"4444444444444444"};
"""
    MenuText = "FEMToolBox"
    ToolTip = "ToolTip FEMToolBox"

    def Initialize(self):
        import FEMToolBoxGui
        self.appendToolbar('FEMToolBox', [
            'FEMToolBox_tool1',
            'FEMToolBox_tool2',
            'FEMToolBox_tool3',
            'FEMToolBox_tool4',
            'FEMToolBox_tool5',
            'FEMToolBox_tool6',
            'FEMToolBox_tool7',
            'FEMToolBox_tool8',
            'FEMToolBox_tool9',
            'FEMToolBox_tool10',
            'FEMToolBox_tool11',
            'FEMToolBox_tool12',
            'FEMToolBox_tool13',
            'FEMToolBox_tool14',
            'FEMToolBox_tool15',
            'FEMToolBox_tool16',
            'FEMToolBox_tool17',
        ])

        self.appendMenu('FEMToolBox', [
            'FEMToolBox_tool1',
            'FEMToolBox_tool2',
            'FEMToolBox_tool3',
            'FEMToolBox_tool4',
            'FEMToolBox_tool5',
            'FEMToolBox_tool6',
            'FEMToolBox_tool7',
            'FEMToolBox_tool8',
            'FEMToolBox_tool9',
            'FEMToolBox_tool10',
            'FEMToolBox_tool11',
            'FEMToolBox_tool12',
            'FEMToolBox_tool13',
            'FEMToolBox_tool14',
            'FEMToolBox_tool15',
            'FEMToolBox_tool16',
            'FEMToolBox_tool17',
        ])

    def GetClassName(self):
        return "Gui::PythonWorkbench"


Gui.addWorkbench(FEMToolBox())


FreeCAD.addExportType("FEM mesh Python without defs (*.py)", "importPyMesh")
FreeCAD.addImportType("FEM mesh Python without defs (*.py)", "importPyMesh")

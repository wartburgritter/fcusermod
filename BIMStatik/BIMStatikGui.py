# ***************************************************************************
# *   Copyright (c) 2017 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

import importlib

import FreeCAD
import FreeCADGui

from bimstatiktools import bimgeomdiff_run
from bimstatiktools import commandtools
from bimstatiktools import cut_with_intersections
from bimstatiktools import geomcompare
from bimstatiktools import geomchecks
from bimstatiktools import groupintersectiontools
from bimstatiktools import grouptools
from bimstatiktools import multifusion


class TOOL1():
    def Activated(self):
        importlib.reload(bimgeomdiff_run)
        print("bimgeomdiff_run reloaded")
        importlib.reload(commandtools)
        print("commandtools reloaded")
        importlib.reload(cut_with_intersections)
        print("cut_with_intersections reloaded")
        importlib.reload(geomcompare)
        print("geomcompare reloaded")
        importlib.reload(geomchecks)
        print("geomchecks reloaded")
        importlib.reload(grouptools)
        print("grouptools reloaded")
        importlib.reload(groupintersectiontools)
        print("groupintersectiontools reloaded")
        importlib.reload(multifusion)
        print("multifusion reloaded")

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": "tool1",
            "ToolTip": "reloads all BIMStatik modules"
        }


class TOOL2():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        sel = FreeCADGui.Selection.getSelection()
        if len(sel) != 1:
            print("Mehr als ein Objekt selektiert!")
        else:
            if (
                sel[0].isDerivedFrom("App::DocumentObjectGroupPython")
                or sel[0].isDerivedFrom("App::DocumentObjectGroup")
            ):
                print("\nYou selected a group object :-)")
                fusegroup = sel[0]
                fuselist = fusegroup.Group
                # fusegroup.ViewObject.Visibility = True
                if geomchecks.checkShapes(fuselist):
                    returnstatus = multifusion.make_smart_multifusion(fuselist)
                    if returnstatus:
                        fusegroup.ViewObject.Visibility = False
                else:
                    print("No fusion was made!")

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": "MyMultiFuse",
            "ToolTip": "MyMultiFuse"
        }


class TOOL3():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        sel = FreeCADGui.Selection.getSelection()
        if len(sel) != 1:
            print("Mehr als ein Objekt selektiert!")
        else:
            if (
                sel[0].isDerivedFrom("App::DocumentObjectGroupPython")
                or sel[0].isDerivedFrom("App::DocumentObjectGroup")
            ):
                import time
                print("\nYou selected a group object :-)")
                for o in FreeCAD.ActiveDocument.Objects:
                    o.ViewObject.Visibility = False
                pi = FreeCAD.Base.ProgressIndicator()
                pi.start("Animation", len(sel[0].Group))
                for o in sel[0].Group:
                    FreeCAD.Console.PrintMessage("{}\n".format(o.Name))
                    o.ViewObject.Visibility = True
                    FreeCADGui.ActiveDocument.update()
                    time.sleep(0.25)
                    o.ViewObject.Visibility = False
                    FreeCADGui.ActiveDocument.update()
                    pi.next()

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": "show one by one",
            "ToolTip": "show all group objects one by one"
        }


class TOOL4():
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        sel = FreeCADGui.Selection.getSelection()
        if len(sel) != 1:
            print("Mehr als ein Objekt selektiert!")
        else:
            if (
                sel[0].isDerivedFrom("App::DocumentObjectGroupPython")
                or sel[0].isDerivedFrom("App::DocumentObjectGroup")
            ):
                print("\nYou selected a group object :-)")
                fusegroup = sel[0]
                fuselist = fusegroup.Group
                # fusegroup.ViewObject.Visibility = True
                if geomchecks.checkShapes(fuselist):
                    print("shape list check returned True!")
                else:
                    print("shape list check returned False!")

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": "tool1",
            "ToolTip":
            "check group objects"
        }


class TOOL5():
    """
    select one object and a group with objects to find the intersection objects
    """
    def IsActive(self):
        return bool(FreeCADGui.Selection.getSelectionEx())

    def Activated(self):
        import Part
        sel = FreeCADGui.Selection.getSelection()
        if len(sel) != 2:
            print(
                "Bitte genau zwei Objekte selektieren, "
                "nicht mehr und nicht weniger!"
            )
        else:
            if hasattr(sel[0], "Shape"):
                if (
                    sel[1].isDerivedFrom("App::DocumentObjectGroupPython")
                    or sel[1].isDerivedFrom("App::DocumentObjectGroup")
                ):
                    print(
                        "Object with Shape found and group obj found. "
                        "Tool will start ..."
                    )
                    # Part.show(sel[0].Shape)
                    intersections = geomcompare.find_intersection_objs(sel[0], sel[1])
                    for sh in intersections:
                        Part.show(sh)
            else:
                print(
                    "Bitte erst das Objekt mit Shape und "
                    "dann das Group Objekt selektieren!"
                )

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": "tool5",
            "ToolTip": (
                "Find the intersection object: "
                "select one object and a group with objects."
            )
        }


class TOOL6():
    """
    select two groups, the tool does group1 cut group2
    depending on the selection order
    """
    def IsActive(self):
        sel = FreeCADGui.Selection.getSelection()
        if len(sel) == 2:
            if (
                sel[0].isDerivedFrom("App::DocumentObjectGroupPython")
                or sel[0].isDerivedFrom("App::DocumentObjectGroup")
            ):
                if (
                    sel[1].isDerivedFrom("App::DocumentObjectGroupPython")
                    or sel[0].isDerivedFrom("App::DocumentObjectGroup")
                ):
                    return True

    def Activated(self):
        sel = FreeCADGui.Selection.getSelection()
        geomcompare.group_cut_group(sel[0], sel[1])
        # selection order matters
        # see https://forum.freecadweb.org/viewtopic.php?f=10&t=33304

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": "model1 cut model2",
            "ToolTip": (
                "select two groups, group1 cut group2, "
                "depending on selection order"
            )
        }


class TOOL7():
    """
    select one group, the tool does check all Shapes of the group
    """
    def IsActive(self):
        sel = FreeCADGui.Selection.getSelection()
        if (
            len(sel) == 1
            and (
                sel[0].isDerivedFrom("App::DocumentObjectGroupPython")
                or sel[0].isDerivedFrom("App::DocumentObjectGroup")
            )
        ):
            return True

    def Activated(self):
        importlib.reload(geomchecks)

        sel = FreeCADGui.Selection.getSelection()
        geomchecks.check_group_geometry(sel[0])

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": "check group member",
            "ToolTip": "select one group,  each Shape of the group is checked"
        }


class TOOL8():
    """
    select two groups, the tool does group1 cut group2 and group2 cut group1

    """
    def IsActive(self):
        sel = FreeCADGui.Selection.getSelection()
        if len(sel) == 2:
            if (
                sel[0].isDerivedFrom("App::DocumentObjectGroupPython")
                or sel[0].isDerivedFrom("App::DocumentObjectGroup")
            ):
                if (
                    sel[1].isDerivedFrom("App::DocumentObjectGroupPython")
                    or sel[0].isDerivedFrom("App::DocumentObjectGroup")
                ):
                    return True

    def Activated(self):
        importlib.reload(commandtools)

        sel = FreeCADGui.Selection.getSelection()
        commandtools.find_diff_volumes(sel[0], sel[1])

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": "Find Differenzvolumen von Zwei Gruppen nur Shapes",
            "ToolTip": "Find Differenzvolumen von Zwei Gruppen nur Shapes"
        }


class TOOL9():
    def IsActive(self):
        sel = FreeCADGui.Selection.getSelection()
        if len(sel) == 2:
            if (
                sel[0].isDerivedFrom("App::DocumentObjectGroupPython")
                or sel[0].isDerivedFrom("App::DocumentObjectGroup")
            ):
                if (
                    sel[1].isDerivedFrom("App::DocumentObjectGroupPython")
                    or sel[0].isDerivedFrom("App::DocumentObjectGroup")
                ):
                    return True

    def Activated(self):
        importlib.reload(commandtools)

        sel = FreeCADGui.Selection.getSelection()
        commandtools.find_diff_with_arch_volumes(sel[0], sel[1])

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": "Find Differenzvolumen von Zwei Gruppen auch Archobj",
            "ToolTip": "Find Differenzvolumen von Zwei Gruppen auch Archobj"
        }


class TOOL10():
    def IsActive(self):
        sel = FreeCADGui.Selection.getSelection()
        if len(sel) == 2:
            if (
                sel[0].isDerivedFrom("App::DocumentObjectGroupPython")
                or sel[0].isDerivedFrom("App::DocumentObjectGroup")
            ):
                if (
                    sel[1].isDerivedFrom("App::DocumentObjectGroupPython")
                    or sel[0].isDerivedFrom("App::DocumentObjectGroup")
                ):
                    return True

    def Activated(self):
        importlib.reload(commandtools)

        sel = FreeCADGui.Selection.getSelection()
        commandtools.debug_find_diff_with_arch_volumes(sel[0], sel[1])

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": (
                "Debug: Find Differenzvolumen von "
                "Zwei Gruppen auch Archobj"
            ),
            "ToolTip": (
                "Debug: Find Differenzvolumen "
                "von Zwei Gruppen auch Archobj"
            )
        }


class TOOL11():
    def IsActive(self):
        return True

    def Activated(self):
        importlib.reload(bimgeomdiff_run)
        bimgeomdiff_run.run_bimgeomdiff()

    def GetResources(self):
        return {
            "Pixmap": "python",
            "MenuText": "Run GeomDiff",
            "ToolTip": (
                "Run GeomDiff in "
                "C:/0_BHA_privat/BIMGeomDiff"
            )
        }


FreeCADGui.addCommand("BIMStatik_tool1", TOOL1())
FreeCADGui.addCommand("BIMStatik_tool2", TOOL2())
FreeCADGui.addCommand("BIMStatik_tool3", TOOL3())
FreeCADGui.addCommand("BIMStatik_tool4", TOOL4())
FreeCADGui.addCommand("BIMStatik_tool5", TOOL5())
FreeCADGui.addCommand("BIMStatik_tool6", TOOL6())
FreeCADGui.addCommand("BIMStatik_tool7", TOOL7())
FreeCADGui.addCommand("BIMStatik_tool8", TOOL8())
FreeCADGui.addCommand("BIMStatik_tool9", TOOL9())
FreeCADGui.addCommand("BIMStatik_tool10", TOOL10())
FreeCADGui.addCommand("BIMStatik_tool11", TOOL11())

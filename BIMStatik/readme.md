### TODO wichtig
+ BlenderBIM diff testen
+ mein geometrieprueftool automatisch als script
    + in FreeCAD ifc oeffnen, dann kommt bei importauswahl geometriepruefung IFC ohne import
    + die errorgeometrieen ins selbe verzeichnis in ein ifc schreiben wie das ifc selber
+ auch in der similar suche mit vertexen das volumen und schwerpunkt mit gossen toleranzen als sicherheit einfuegen
+ die einzelnen geometrie beispiele ins repo mit ablegen

### Allgemeines
+ in den codefiles sind noch ganz viele Hinweise
+ besser als wireframe im vergleich
    + eine gruppe orange linien und transparenz auf 90 % die andere wireframe schwarz
+ der aktuelle vergleich beruecksichtigt keine Materialieen
    + bsp 18044 verschiebung der vertiefungen
    + bpl ist gedaemmt
    + bei verschiebung von vertiefung ist dort wo daemmung war neu beton 
    + und dort wo beton war neu daemmung, das wird nicht als aenderung gefunden

### Geometrievergleich Allgemeines
+ collector voranstellen, damit auch mit standard imports (importIFC) gearbeitet werden kann
    + Export des ergebnisses nach ifc, json (Diff-Format von BlenderBIM)


### Geometrievergleich Ablauf
+ Ziel des group1 cut group2
+ Mehrvolumen von group1 zu group2 finden
+ Das Mindervolumen enspricht dem Mehrvolumen von group2 zu group1 --> cut andersherum
+ fuer jedes objekt
    + finde gleiches (similar) objekt in Bezug auf Volumengeometrie
    + es muss nicht das selbe sein
    + es kann auch ein punkt in einer Kante eingefuegt sein
    + durch Neuerstellung sind alle Punkte < 0.5 mm verschoben
    ... 
    + eben ein gleiches Volumen, nicht das selbe Volumen
    + wird ein gliches volumen gefunden ist das aktuelle volumen kein Mehrvolumen
+ fuer jedes objekt
    + finde volumen, welches das aktuelle komplett umschliesst
    + entsteht durch verbindung von objekten
    + wenn ein anderes volumen das aktuelle umschliesst, ist das aktuelle kein Mehrvolumen
+ fuer jedes objekt
    + finde intersection objekte
    + fuer jedes intersection objekte
        + ziehe dieses von dem aktuellen objekt ab
        + nimm das resultat und schaue
            + gibt es ein gleiches (similar) volumen
            + gibt es ein volumen dass dieses komplett umschliesst
            + beide obigen koerper koennen bei den cuts entstehen
            + wenn nicht cut mit naechsten intersection obj

# ***************************************************************************
# *   Copyright (c) 2017 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

# not needed imports but to get flake8 quired
import FreeCADGui as Gui
from FreeCADGui import Workbench

# needed imports
import BIMStatikGui
False if BIMStatikGui.__name__ else True  # flake8


class BIMStatik(Workbench):
    "BIMStatik workbench object"

    MenuText = "BIMStatik"
    ToolTip = "ToolTip BIMStatik"

    def Initialize(self):
        tool_count = 11
        tool_specifier_list = []
        for i in range(tool_count):
            tool_specifier_list.append("BIMStatik_tool{}".format(i+1))
        self.appendToolbar('BIMStatik', tool_specifier_list)
        self.appendMenu('BIMStatik', tool_specifier_list)

    def GetClassName(self):
        return "Gui::PythonWorkbench"


Gui.addWorkbench(BIMStatik())

# ***************************************************************************
# *   Copyright (c) 2017 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

# to be able to use this:  print("null shape ", end="")
from __future__ import print_function

import time

import FreeCAD
import Part

from .cut_with_intersections import cut_with_intersections
from .geomchecks import check_goemetry_for_bool
# from .geomchecks import check_solid_geometry
from .geomchecks import get_duplicate_by_name
from .geomchecks import is_similar_geometry
from .groupintersectiontools import find_intersection_objs
from .groupintersectiontools import get_intersecBB_objs
from .grouptools import copy_group_with_only_parts
from .grouptools import find_obj_in_arch_grp

if FreeCAD.GuiUp:
    import FreeCADGui


"""
search intersection with pivy and coin is very fast but needs an stl as input
https://forum.freecadweb.org/viewtopic.php?f=22&t=28175#p228260
https://forum.freecadweb.org/viewtopic.php?f=22&t=37593&p=320223#p320222

some links unbedingt lesen
https://forum.freecadweb.org/viewtopic.php?t=37593&start=10

weitere links
https://forum.freecadweb.org/viewtopic.php?t=28175&start=20
https://forum.freecadweb.org/viewtopic.php?t=3120
https://forum.freecadweb.org/viewtopic.php?t=764
"""

"""
# BBintersectiontest
box1 = Part.makeBox(2,2,2)
box2 = Part.makeBox(4,4,4)
box2.Placement.Base = (-1, -1, -1)
Part.show(box1)
Part.show(box2)
box1.BoundBox.intersect(box2.BoundBox)
box2.BoundBox.intersect(box1.BoundBox)
"""


# ************************************************************************************************
def group_cut_group(
    model_base,
    model_cut,
    grp_base_intersectionBB_objs=None,
    model_arch_base=None,
    model_spatial_base=None
):
    """
    model_base ... group of Part::Features
    model_cut ... group of Part::Features
    no sub groups
    no ArchComponents or similar objects
    just dozens or hundreds or thousands
    of simple Part::Features with a Shape (best only a Solid)
    """

    from Arch import makeFloor

    time_start_groupcut = time.process_time()
    FreeCAD.Console.PrintMessage("\n")
    FreeCAD.Console.PrintMessage("Begin: Method group_cut_group\n")

    doc = model_base.Document
    guidoc = model_base.ViewObject.Document
    if FreeCAD.GuiUp:
        guidoc.activeView().viewAxonometric()
        import FreeCADGui
        FreeCADGui.SendMsgToActiveView("ViewFit")
        FreeCADGui.updateGui()
        guidoc.update()

    # container for results
    grp_name_base = model_base.Label.rstrip("_Parts")
    grp_name_cut = model_cut.Label.rstrip("_Parts")
    FreeCAD.Console.PrintMessage("{} minus {}\n".format(grp_name_base, grp_name_cut))
    gpr_name_no_intersection = "Mehrbauteile_in {}_und_Minderbauteile_in_{}".format(grp_name_base, grp_name_cut)
    gpr_name_cut_result = "Mehrvolumen_in {}_und_Mindervolumen_in_{}".format(grp_name_base, grp_name_cut)
    # create empty group for objects which are compleatly not in the other group
    # might because they are not there, but could be because of a error too
    FreeCAD.ActiveDocument = doc
    no_intersection_grp = makeFloor(name=gpr_name_no_intersection)
    no_intersection_grp.Label = gpr_name_no_intersection

    # create geometry copy group and fill with objects
    result_model_cut = copy_group_with_only_parts(model_base, gpr_name_cut_result)

    # Ziel:
    # group model_base cut group model_cut
    # --> Ergebnis ist das Mehrvolumen (Plusvolumen) von model_base zu model_cut
    # um das Mindervolumen (Minusvolumen) zu erhalten wird die operation
    # mit vertauschten gruppen nochmals durchgefuehrt

    # heisst wenn ein object in model_base und model_cut identisch ist,
    # dann ist dieses Volumen kein Mehrvolumen, sofort naechstes baseobj bitte
    # aber das objekt braucht es spaeter eventuell nochmals
    # da es ein objekt geben kann welches in model_cut in dieses hineinreicht
    # aber in model_base nicht hineinreicht (interne kollision wurde entfernt),
    # dann wird wenn das dann ander reiche ist sichergestellt,
    # dass dann kein mehrvolumen gefunden wird.

    cutTolerance = 1.e-1  # 0.1 mm = 100 muecrometer
    for count, baseobj in enumerate(model_base.Group):

        # Informationen ueber den Ablauf der diffberechnung***************************************
        # fuer jedes object baseobj in model_base (diese Gruppe)
        # - finde duplicate von baseobj in result_model_cut (die andere Gruppe)
        #   idenitfier: cutobj
        # - finde ob es ein aehnliches objekt in der anderen gruppe gibt
        #   - wenn ja, dann loesche cutobj und continze zum nachsten baseobj
        # - finde fuer baseobj alle intersections in model_cut (der anderen Gruppe)
        #   list identifier: inter_shs
        # - make boolean subtraction:
        #   fuer jedes objekt ish in inter_shs: cutobj minus ish = cut_result_sh
        #   - mache tausend millionen checks mit cut_result_sh
        #     wenn ok fuege es zu result_model_cut hinzu
        #   - da stimmt irgendwas nicht mit der
        #     for ish in inter_shs und den zwei zeilen davor und den zeilen danach, WAS?
        #   - bitte besser dokumentieren was passiert
        #   - loesche cutobj

        # find obj to cut in result_model_cut, exact duplicate of baseobj ************************
        # we should find it in any case
        cutobj = get_duplicate_by_name(baseobj, result_model_cut)
        if not cutobj:
            # this should never happen
            FreeCAD.Console.PrintError(
                "  Trouble, because cutobj was not found in duplicate_group\n"
            )
            continue

        # debugging ******************************************************************************
        debugobj = None
        # set debugobj to "" for NO debugging
        # set debugobj to "objname" for debugging
        # woher bekomme ich den objname
        # lasse tool10 (Debugg finde differenzvolumen) laufen, dann bleiben die Parts gruppen
        # in der partgruppe finde das betreffende objekt und nimm den Namen, nicht das label
        # reload modules nicht vergessen
        # set in cut_with_intersections debug auf True
        # debugobj = "Component029001"  # O45
        # debugobj = "Component010001"  # 20088_ARC_ROH
        # debugobj = "Component135012"  # SPZU
        # debugobj = "Component282003"  # SPZU
        # debugobj = "Component028001"  # SPZU
        if debugobj is None:
            pass
        else:
            if baseobj.Name == debugobj:
                FreeCAD.Console.PrintWarning(
                    "Debug is active for object: {}\n".format(baseobj.Name)
                )
                new_obj = doc.addObject("Part::Feature", "Debug_" + baseobj.Name)
                new_obj.Shape = baseobj.Shape
            else:
                doc.removeObject(cutobj.Name)
                continue

        # obj information print ******************************************************************
        # print object info here to have only one debug code part
        FreeCAD.Console.PrintMessage(
            "Objekt {} of {}, {},\n"
            .format(count + 1, len(model_base.Group), baseobj.Name)
        )
        if FreeCAD.GuiUp:
            import FreeCADGui
            FreeCADGui.updateGui()

        # search for similar object **************************************************************
        # wenn gleiche modelle verglichen werden (zwei ING_TRW oder zwei ARC_ROH)
        # wird die meiste Zeit hier teuer verkauft
        # weil diese Modelle haben ja bis auf die Aenderungen ausschliesslich similar geometry
        # man koennte auch kommplett nur bauteilorientiert vergleichen
        #
        # wenn das finden der BoundBox intersection schnell ist, dann erst das
        # is similar nur an den BoundBox intersection obj durchfuehren
        #
        # pruefe ob es die gleiche (gleiche und selbe) geometrie
        # mit tolerance im model_cut (der anderen modellgruppe) gibt
        # es kann sein es gibt similar geometrie,
        # aber sie wird nicht gefunden das ist ok,
        # aber wenn unterschiedliche geometrieen als similar gefunden werden,
        # wird dieser unterschied komplett ignoriert.
        # wobei dies immernoch besser ist als False positives
        # heisst unterschiede die es gar nicht gibt

        # get the intersection objects by the use of the BoundingBox
        if grp_base_intersectionBB_objs is not None:
            if baseobj.Name in grp_base_intersectionBB_objs:
                intersecBBobjs = grp_base_intersectionBB_objs[baseobj.Name]
            else:
                intersecBBobjs = []  # TODO all keys should be in intersecBBobjs even if it is a empty list
        else:
            FreeCAD.Console.PrintMessage("    Intersection BB objects missing. Get them!\n")
            intersecBBobjs = get_intersecBB_objs(baseobj, model_cut)
        # wg time in report view mehrere console prints
        FreeCAD.Console.PrintMessage(
            "    search in the other group (model_cut):\n"
        )
        FreeCAD.Console.PrintMessage(
            "    found {} intersection obj(s) by BoundingBox, "
            .format(len(intersecBBobjs))
        )
        # search in there to find a similar object
        # wenn similar geometry gefunden dann delete cut_obj und continue,
        # da dieses Objekt (cut_obj) weder Mehrbauteil ist noch Mehrvolumen hat
        found_similar = False
        for mco in intersecBBobjs:
            # print(cutobj.Name)
            # print(mco.Name)
            if is_similar_geometry(cutobj.Shape, mco.Shape) is True:
                FreeCAD.Console.PrintMessage(
                    "found a similar geometry object --> continue.\n"
                )
                doc.removeObject(cutobj.Name)
                found_similar = True
                break
        if found_similar:
            continue

        # no similar object found
        FreeCAD.Console.PrintMessage(
            "found NO similar geometry.\n"
        )

        # find intersection objects from cutobj in model_cut group********************************
        inter_shs = find_intersection_objs(baseobj, model_cut, intersecBBobjs)
        # if there is no intersection, copy to grp _no_interSection and continue******************
        if len(inter_shs) == 0:
            # if baseobj has no intersections just remove cutobj and continue with next baseobj
            FreeCAD.Console.PrintMessage("  No intersections found, continue.\n")
            FreeCAD.Console.PrintMessage(
                "  If there was no error in intersection search, "
                "this is just a totaly new Object.\n"
            )
            no = doc.addObject("Part::Feature", (baseobj.Name + "_no_interSection"))
            no.Shape = baseobj.Shape
            doc.removeObject(cutobj.Name)
            # find baseobj in model_arch_base to create a structure from cut_resuld_obj and move it to no_intersection_grp
            find_obj_in_arch_grp(baseobj, model_arch_base, no, no_intersection_grp)
            continue

        # if there are intersections find the intersection volumes********************************
        # Der eigentliche cut der objekt geometrie mit allen intesections ************************
        # siehe dort fuer checks etc.
        # wenn der cut schief laeuft wird eine empty, invalid Shape zurueckgegeben, Solids ist leer, Volumen gibt error
        cut_result_sh = cut_with_intersections(inter_shs, cutobj.Shape, cutTolerance)
        if cut_result_sh.isNull() is False:
            solidcount = len(cut_result_sh.Solids)
            if solidcount == 1:
                cut_resuld_obj = doc.addObject("Part::Feature", "{}_cut".format(cutobj.Name))
                cut_resuld_obj.Shape = cut_result_sh
                # find baseobj in model_arch_base to create a structure from cut_resuld_obj
                # move cut_resuld_obj to result_model_cut
                find_obj_in_arch_grp(baseobj, model_arch_base, cut_resuld_obj, result_model_cut)
            elif solidcount > 1:
                for count, sol in enumerate(cut_result_sh.Solids):
                    cut_resuld_sol_obj = doc.addObject("Part::Feature", "{}_cut_sol{}".format(cutobj.Name, count + 1))
                    cut_resuld_sol_obj.Shape = cut_result_sh.Solids[count]
                    # find baseobj in model_arch_base to create a structure from cut_resuld_obj
                    # move cut_resuld_obj to result_model_cut
                    find_obj_in_arch_grp(baseobj, model_arch_base, cut_resuld_sol_obj, result_model_cut)
        else:
                FreeCAD.Console.PrintMessage(
                    "  The cuts did not return a new Volume without errors.\n"
                )

        # delete cutobj
        doc.removeObject(cutobj.Name)

        if FreeCAD.GuiUp:
            guidoc.update()
            import FreeCADGui
            FreeCADGui.updateGui()

    if FreeCAD.GuiUp:
        FreeCAD.Console.PrintMessage("Set colors of the result.\n")
        for ovw in no_intersection_grp.Group:
            ovw.ViewObject.ShapeColor = (1.0, 0.75, 0.0)  # orange
        for ovw in result_model_cut.Group:
            ovw.ViewObject.ShapeColor = (1.0, 0.0, 0.0)  # rot
    doc.recompute()
    group_cut_time = round((time.process_time() - time_start_groupcut), 3)
    FreeCAD.Console.PrintMessage("Duration group cut: {} seconds.\n".format(group_cut_time))
    FreeCAD.Console.PrintMessage("Ende: Method group_cut_group\n")

    return no_intersection_grp, result_model_cut

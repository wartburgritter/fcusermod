# ***************************************************************************
# *   Copyright (c) 2017 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

# to be able to use this:  print("null shape ", end="")
from __future__ import print_function

import FreeCAD

import Part

if FreeCAD.GuiUp:
    import FreeCADGui


def check_group_geometry(check_group):
    doc = FreeCAD.ActiveDocument
    FreeCAD.Console.PrintMessage(
        "Find defect Shapes in {}\n"
        .format(check_group.Label)
    )

    # be careful Label could have special character
    grp_name = "Defects_{}".format(check_group.Label)
    # create empty group
    grp_bad = doc.addObject("App::DocumentObjectGroup", grp_name)

    for obj in check_group.Group:
        error = check_solid_geometry(obj.Shape)
        if error == "":
            pass
            # FreeCAD.Console.PrintMessage(":-)\n")
        else:
            FreeCAD.Console.PrintMessage("{}\n".format(error))
            no = doc.addObject("Part::Feature", (obj.Name + "_copy"))
            no.Shape = obj.Shape
            grp_bad.addObject(no)


def check_goemetry_for_bool(commonShape, error=""):
    bop_check = False
    if len(commonShape.Vertexes) == 0:
        error += "No clean commonShape: no Vertexes.\n"
    else:
        simple_sh_test = False
        try:
            simple_sh_test = commonShape.isValid()
        except Exception:
            error += "No clean commonShape: isValid raised an exeception.\n"
        if simple_sh_test is False:
            error += "No clean commonShape: isValid returned False.\n"
        if simple_sh_test is True:
            sh_volume = 0
            try:
                sh_volume = commonShape.Volume
            except Exception:
                error += "No clean commonShape: volume can not be calculated\n"
            if sh_volume < 1:  # ein mm3 ist fuer meine belange ausreichend
                error += "No clean commonShape: volume of commonShape < 1mm3: " + str(sh_volume) + " mm3 \n"
            else:
                try:
                    # check does not return anything
                    # it raises an ValueError if shape has errors
                    commonShape.check(True)
                    bop_check = True
                except ValueError:
                    error += "No clean commonShape: bad bob check : " + str(commonShape) + " \n"
                if bop_check is True:
                    # no = doc.addObject("Part::Feature", (io.Name + "_intersection"))
                    # interGroup.addObject(no)
                    # no.Shape = commonShape
                    # warum nicht immer die io Shape hinzufuegen
                    # dann habe ich nur cleane Shapes im Cut
                    # ganz extrem, einfach hier im intesection gar keine boolschen operationen
                    # dann sind einfach einige ohne intersection im cut
                    # im cut evtl. Shapes vorher sortieren, nur wie erst die haupt Shape, oder zu letzt
                    # na erste die Hauptshape, denn wenn das Restvolumen im cut dann null ist kann ich
                    # continue machen
                    # der teste geht am besten ueber volume und schwerpunkt
                    # die Shape, die Volumen und Schwerpunkt am naechsten and der base shape hat
                    # wird zu erst abgezogen
                    # der Text hier sollte aber zu dem cut kommen
                    # hier nur die infos bezueglich uebergabe io oder common
                    return error, bop_check
    return error, bop_check


def check_solid_geometry(sh, Name="", tol_length=1, tol_volume=1):

    # aufpassen bei anpassen der strings, andere module haengen daran
    # bsp. methode copy_group_with_arch_to_part in module grouptools

    if sh.isNull():
        return "Nullshape"
    if not sh.Vertexes:
        # print(sh.exportBrepToString())
        # print("{}\n\n\n".format(40*"*"))
        return "No Vertexes"
    if len(sh.Solids) == 0:
        return "No Solids"
    if len(sh.Solids) > 1:
        return "More than one solid"
    if not((sh.ShapeType == "Solid") or (sh.ShapeType == "Compound")):
        return "Neither Solid nor Compound"

    # sh should be a Shape with Vertexes
    # ShapeType should be Solid or Compound
    # a Compound should only have one Solid

    # eine nicht null shape ohne elemente entsteht durch:
    # intersection von sich nicht beruehrenden koerpern
    # wand mit oeffnung groesser als die wand
    # (cut und der minuskoerper ist grosser oder gleich dem basiskoerper)

    # evtl gibt ifcos eine brep zurueck, eine null brep,
    # ich teste auf "", aber der string einer Nullshape ist nicht ""
    # daher test ok, dann der erste test hier ist auf vertexes.
    # eigentlich in ifcos test noch isNull() und len(Vertexes) ausfehuren
    # denn nur wenn es Vertexes hat wird auch eine Geomtrie zurueckgegeben

    # habe "cannot calculate inventor representation"
    # obwohl Shape isNull() False zurueckgegeben hat
    # zweiter test sofort auf Anzahl Vertexes

    # isValid() on a Nullshape (sh=Part.Shape()) returns a exception
    # isNull() an dieser Shape returns True
    # eine Intersection von sich nicht beruehrenden Objekten gibt keine Nullshape zurueck,
    # sondern eine Shape ohne geometrie
    # isValid() gibt True zurueck
    # isNull() gibt False zurueck
    # kann alles schoen mit print(sh.exportBrepToString()) getestet werden
    
    error = check_a_geometry_with_elements(sh, Name, tol_length, tol_volume)
    return error


def check_a_geometry_with_elements(sh, Name="", tol_length=1, tol_volume=1):

    # a geometry with at least one Vertex is assumed

    # Volumen of Nullshape returns runtime error
    # Volumen von Shape ohne Geometrie returned 0.0

    # Modellqualitaet
    # Mehr als ein Solid und min Edge length < 1 mm sind perse keine Modellfehler
    # aber sind unguenstig modelliert und nicht gut fuer Massenber aus Geometrie
    # in Architekturmodellen aber nicht selten
    # TODO:
    # den test spliten, in error und qualitaet um beides separat zu machen

    error = ""
    # we check if it has exact one Shell
    # --> ein guter solid kann mehr als eine Shell haben
    # Test deactiviert --> sicher ?!
    if (sh.Shells) == -1:
        error += (
            "  Bad shape: {} Shape has negative Shell count "
            ":-) test is inactive.\n"
            .format(Name)
        )
    else:
        # we have a Solid --> we check if it has a Volume
        volTolerance = tol_volume  # standard 1 mm3
        if sh.Volume < volTolerance:
            error += (
                "  Bad shape: {} Shape has a Volume < {} .\n"
                .format(Name, volTolerance)
            )
        else:
            # we have a Solid with one Shell and a Volume
            # --> we check the Shape geometry
            # check() returns None no matter if it fails or not
            # + a ValueError is raised if it fails.
            try:
                # standard check, means no BOP check, very fast
                sh.check(False)
            except ValueError:
                error += (
                    "  Bad shape: {} Shape failed at standard Shape check\n"
                    .format(Name)
                )
            if not hasEdgeTwoFaces(sh):
                error = (
                    "  Bad shape: {} at least one Edge "
                    "has not exact two faces.\n"
                    .format(Name)
                )
            else:
                maxEdgeLength = tol_length  # standard 1 mm
                if has_short_edges(sh, maxEdgeLength):
                    error += (
                        "  Bad shape: {} at least one Edge "
                        "is smaller than: {} mm.\n"
                        .format(Name, maxEdgeLength)
                    )
                else:
                    try:
                        sh.check(False)  # True is BOP check
                    except ValueError:
                        error += (
                            "  Bad shape: {} Shape failed at BOP Shape check\n"
                            .format(Name)
                        )
                        # Part.show(sh)

    if error:
        error = error.rstrip()  # remove last new line
        # print("  check_solid_geometry failed on obj: " + obj.Name + ".\n")
        # FreeCAD.Console.PrintMessage("  check_solid_geometry failed on obj: " + Name + ".")
        FreeCAD.Console.PrintMessage("{}\n".format(error))
    return error


def checkShapes(list_of_objs):
    """
        check the list_of_objs if all objs have a valid Solid Shape
    """
    failedfusetestgrp = FreeCAD.ActiveDocument.addObject(
        "App::DocumentObjectGroup",
        "FailedFuseTest"
    )
    print("\nTest the list_of_objs ...")
    for obj in list_of_objs:
        sh = obj.Shape
        name = obj.Name
        error = ""
        # print("Check if has Shape")
        if not hasattr(obj, "Shape"):
            error = "  Bad obj: " + name + " has no Shape."
        else:
            if not sh.Vertexes:
                error = "  Bad obj: " + name + " Shape seams empty because there is no Vertex.\n"
            else:
                # we have a shape --> we check fo a Solid
                # print(sh.ShapeType)
                if not (sh.ShapeType == "Solid" or sh.ShapeType == "Compound"):
                    error = "  Bad obj: " + name + " Shape is neither a Solid nor a Compound. \n" + sh.ShapeType
                elif sh.ShapeType == "Solid":
                    checkinfo = check_solid_geometry(sh, name)
                    if checkinfo:
                        error = checkinfo
                        # print("problem on Solid")
                elif sh.ShapeType == "Compound":
                    if len(sh.Solids) != 1:
                        error = "  Bad obj: " + obj.Name + "More than one Solid: " + str(len(sh.Solids))
                    else:
                        # import FemMeshTools
                        # if FemMeshTools.is_same_geometry(sh, sh.Solids[0]):
                        # obige methode checks on CenterOfMass
                        # aber Compound Shape hat das nicht
                        # wir nehmen an der erste solid is der compound
                        checkinfo = check_solid_geometry(sh.Solids[0], name)
                        if not checkinfo:
                            error = checkinfo
                            # print("problem on Compound")

        if error:
            print(error + "\n")
            failedfusetestgrp.addObject(obj)
        if FreeCAD.GuiUp:
            FreeCADGui.updateGui()
            # to get messages printed on screen ...
            # may be PrintMessage should be used
    if failedfusetestgrp.Group:
        print(
            "  Bad list_of_objs --> :-(:-(:-(, "
            "Bad objects where MOVED in separate group.\n"
        )
        return False
    else:
        FreeCAD.ActiveDocument.removeObject(failedfusetestgrp.Name)
        print("  Good list_of_objs --> :-):-):-)\n")
        return True


def has_short_edges(sh, maxEdgeLength):
    for e in sh.Edges:
        if e.Length < maxEdgeLength:
            # Part.show(e)
            return True
    return False


def hasEdgeTwoFaces(sh):
    # https://forum.freecadweb.org/viewtopic.php?f=22&t=28175&p=228666#p228666
    # assumed is a solid which passes Shape checks
    # means no further check is done here
    # jede face eines solids muss exakt zwei faces haben
    # nicht eine face und nicht drei faces

    # print(len(sh.Faces))
    # print(len(sh.Edges))

    for baseEdge in sh.Edges:
        countEdges = 0
        for f in sh.Faces:
            for e in f.Edges:
                if e.isSame(baseEdge):
                    countEdges = countEdges + 1

        if countEdges != 2 and countEdges != 1:
            # print("    countEdges= " + str(countEdges))
            # Part.show(sh)
            # Part.show(baseEdge)
            return False
        elif countEdges == 1:
            # es gibt ausnahmen, wie bps ein zylinder
            # eine kugel usw ... die haben faces mit nur einer edge
            # wenn face planar ist dann return false
            # wenn sie nichtplanar ist dann return true
            # print("    countEdges = " + str(countEdges) + " ... fuer zylinder und kugel ist das moeglich, fuer quader nicht")
            for f in sh.Faces:
                for e in f.Edges:
                    if e.isSame(baseEdge):
                        # Part.show(sh)
                        # Part.show(f)
                        # Part.show(baseEdge)
                        if not f.Surface.isPlanar():
                            print(
                                "    edge with only one NON planar face "
                                "(could be a cylinder shell)"
                            )
                            return True
                        print("    edge with only one planar face")
                        return False
            print("    problem in hasEdgeTwoFaces")
            return False

    return True
    # es gibt mehrere varianten des False
    # entweder nur eine face oder sogar drei oder vier faces anstatt zwei
    # ID12282 eine edge viermal, richtig :-)


def areInSamePlane(face1, face2):
    from FreeCAD import Vector as Vector
    import DraftVecUtils
    import math
    # normalTol = 0.01
    angleTol = 0.01

    # pruefen, ebene face

    # pruefen, sind normalenrichtung colinear
    # https://github.com/berndhahnebach/FreeCAD_bhb/blob/master/src/Mod/Draft/DraftVecUtils.py#L220
    # normalAt see https://forum.freecadweb.org/viewtopic.php?f=22&t=28325
    face1COM = face1.CenterOfMass
    # u,v for normalAt calculation
    face1uvCOM = face1.Surface.parameter(face1COM)
    face2COM = face2.CenterOfMass
    # u,v for normalAt calculation
    face2uvCOM = face1.Surface.parameter(face2COM)
    face1Normal = face1.normalAt(face1uvCOM[0], face1uvCOM[1])
    face2Normal = face2.normalAt(face2uvCOM[0], face2uvCOM[1])

    # how to set the Draft precision for this def only?
    if DraftVecUtils.isColinear([face1Normal, face2Normal]):
        # Part.show(face1)
        # Part.show(face2)
        # pruefen, sind flaechen in der selben ebene
        # --> winkel normale face1 zu schwerpunktabstand face1 zu face2
        # sollte 90 grad sein
        # https://www.freecadweb.org/wiki/Vector_API
        # https://www.freecadweb.org/wiki/FreeCAD_vector_math_library
        # will be in radians, 90 degree = 1,5708 = Pi / 2
        angle = Vector.getAngle(face1Normal, Vector.sub(face1COM, face2COM))
        # print(angle)
        if (math.pi * 0.5 * (1 - angleTol)) < angle < (math.pi * 0.5 * (1 + angleTol)):
            # print(angle)
            Part.show(face1)
            Part.show(face2)
            return True
    return False


def hasSolidLonleyFaces(sh):
    # das mit dem cut ist doch bloed, siehe exact two faces per edge
    tol = 1.e-1
    # 0.1 mm = 100 muecrometer, evtl. noch hoeher setzen bis auf 1 mm ?!?
    for f in sh.Faces:
        for fi in sh.Faces:
            if f.Area >= fi.Area:
                fbig = f
                fsmall = fi
            else:
                fbig = fi
                fsmall = f

            # print(fbig)
            # print(fsmall)
            fbig.fixTolerance(tol)
            fsmall.fixTolerance(tol)
            cutSh = fbig.cut(fsmall)
            if cutSh.isNull() is False and (len(cutSh.Vertexes) > 0):  # and (cutSh.Area < fbig.Area):
                # Part.show(cutSh)
                if (cutSh.Area > 0):
                    pass
                    # problem in cut in genau der massgeben boolschen operation in bsp shape
                    # Part.show(fbig)
                    # Part.show(fsmall)
                    # es wurde auch Querstehende gefunden, eine von vier
                    # alles wurde zweimal gefunden, da ja jede mit jeder
                    # heisst jeder cut zweimal
            # die ganzen geometrie pruefungen und test usow
            # sollten in extra module ausgelagert sein.
            areInSamePlane(fbig, fsmall)
    return False


def getDuplicateFromCopy(base_obj, duplicate_group):
    """
    basiert auf same_geometry from Fem.femmesh.meshtools
    es werden nicht alle duplikate gefunden ...
    da ist was nicht gut mit meiner same_geometry ...
    same_geometry sollte exact identische kopien sicher finden.
    da hat es evtl feherl o und base_obj und cutbbobj sind gemischt
    Workaround: use get_duplicate_by_name
    """
    import femmesh.meshtools as meshtools
    oBB = base_obj.Shape.BoundBox
    cut_BB_objs = []
    found_duplicate_obj = False
    duplicate_obj = None
    print("  Start search for duplicate_obj (copy of o) in duplicate_group")
    for co in duplicate_group.Group:
        FreeCAD.Console.PrintMessage("  " + co.Name + "\n")
        if co.Shape.BoundBox.intersect(oBB):
            cut_BB_objs.append(co)
        for cutBBobj in cut_BB_objs:
            if cutBBobj.Shape.ShapeType != "Solid":
                # geht gar nicht, wenn ich vorher geprueft habe ...
                FreeCAD.Console.PrintError("non Solid: " + cutBBobj.Name)
            if meshtools.is_same_geometry(co.Shape, cutBBobj.Shape):
                found_duplicate_obj = True
                duplicate_obj = co
                break
    if found_duplicate_obj:
        FreeCAD.Console.PrintMessage(
            "  Found cut object in model copy: " + co.Name + "\n"
        )
        return duplicate_obj
    else:
        FreeCAD.Console.PrintError(
            "  Not found duplicate_obj in duplicate_group\n"
        )
        # geht eigentlich gar nicht. Wir suchen nach einem Duplikat.
        # Das muss gefunden werden.
        return None


def get_duplicate_by_name(base_obj, duplicate_group):
    for co in duplicate_group.Group:
        if co.Name == base_obj.Name + "_copy":
            return co
    FreeCAD.Console.PrintError("  Not found cut_obj in duplicate_group\n")
    return None


def is_similar_geometry(sh1, sh2, tol_vol=0.01, tol_com=0.000001):

    # wichtig, ich brauche die gleiche geometrie, nicht die selbe
    # sogar ein Vertex in einer Kante ist nicht schoen,
    # aber die geometrie ist in
    # meinem sinne hier gleich. Das umschlossene Volumen zaehlt

    # gibt es evtl. eine meshmethode die das schnell und gut kann
    # von anderer per Python verfuegbarer bibliothek

    # is_same_geometry findet nur die selbe geometrie,
    # da alle vertexes identisch sein muessen,
    # ohne tolerance bei den punktkoordinaten

    # punkte duerfen aber laut mir um tolerance verschoben sein
    # es kann sogare ein punkt irgendwo eingefuegt sein, dann stimmen anzahl
    # punkte, kanten und flaechen nicht mehr
    # dann ist es auch sicher nicht die selbe geometrie, aber die gleiche
    # und ich suche die gleiche!!!
    # is_same_geometry(App.ActiveDocument.ID114342_body_001.Shape, App.ActiveDocument.ID116057_body_001.Shape)

    from femtools.geomtools import is_same_geometry
    if is_same_geometry(sh1, sh2) is True:
        return True
    else:
        # Vergleiche gleiche Punktanzahl, aber um Toleranz verschoben
        # FreeCAD.Console.PrintMessage(
        #     "  Geometry is not the same we will check if it is similar (gleich)\n"
        # )
        # we need more checks here

        # compare the Vertexes
        # mostly these ones which where mad new but on base of dwg,
        # thus parts of mm moved
        # 15025 has two of them which I would like to see find as similar
        # needs 0.4 mm tolerance
        tol_ver = 0.4  # absolut value in mm
        similar_vertexes = 0
        if len(sh1.Vertexes) == len(sh2.Vertexes) and len(sh1.Vertexes) > 1:
            # FreeCAD.Console.PrintMessage("Check Vertexes with tolerance, Begin.\n")
            for vs1 in sh1.Vertexes:
                for vs2 in sh2.Vertexes:
                    if (
                        (abs(vs1.X - vs2.X) < tol_ver)
                        and (abs(vs1.Y - vs2.Y) < tol_ver)
                        and (abs(vs1.Z - vs2.Z) < tol_ver)
                    ):
                        similar_vertexes += 1
                        continue
            # FreeCAD.Console.PrintMessage("{}\n".(same_Vertexes))
            if similar_vertexes == len(sh1.Vertexes):
                # FreeCAD.Console.PrintMessage("Check Vertexes with tolerance, True.\n")
                return True
            else:
                pass
                # FreeCAD.Console.PrintMessage("Check Vertexes with tolerance, False.\n")
        else:
            # ungleiche punktanzahl
            # gleiches volumen und gleicher schwerpunkt
            # TODO: aeusseren Eckpunkt, an denen edges grosse knicke haben vergleichen
            # two slabs, one with a small aussparung would not
            # or if the small aussparung moved
            # they are not similar. here we could find some more to test ...

            # erst punkte finden die mit tolleranz gleich sind.
            # die restlichen sollten auf einer kante liegen,
            # die zwischen zwei gefundenen gleichen punkten liegt
            # dann similar
            # eigentlich doch auch wenn der Mehrpunkt auf einer flaeche des koerpers mit
            # weniger punkten liegt dann doch auch similar
            # dann ist das volume und schwerpunkt zeug nicht noetig

            # tolerances --> see method parameter
            # volume a tolerance
            # if a aussparung in a slab or wall just moved the volume does not change
            # tol_vol = 0.01
            # needs to be small to find any differences
            # 18044 base slab with just one changed small aussparung
            # if the object is a huge slab but a small aussparung just moved
            # a very small tolerance for CenterOfMass is needed
            # tol_com = 0.000001

            # print(sh1.Volume)
            # print(sh2.Volume)
            if 1 - tol_vol < (sh1.Volume / sh2.Volume) < 1 + tol_vol:
                # FreeCAD.Console.PrintMessage(
                #     "    Volume is similar but not sure yet if geometry.\n"
                # )

                com_sh1 = sh1.CenterOfMass
                com_sh2 = sh2.CenterOfMass
                # differenz damit die absolute lage im raum keine rolle spielt
                longest_edge_length = 0
                for e in sh1.Edges:
                    if e.Length > longest_edge_length:
                        longest_edge_length = e.Length
                for e in sh2.Edges:
                    if e.Length > longest_edge_length:
                        longest_edge_length = e.Length

                if (
                    (abs(com_sh1.x - com_sh2.x) < tol_com * longest_edge_length)
                    and (abs(com_sh1.y - com_sh2.y) < tol_com * longest_edge_length)
                    and (abs(com_sh1.z - com_sh2.z) < tol_com * longest_edge_length)
                    # (1 - tol_com < (com_sh1.x / com_sh2.x) < 1 + tol_com)
                    # and (1 - tol_com < (com_sh1.y / com_sh2.y) < 1 + tol_com)
                    # and (1 - tol_com < (com_sh1.z / com_sh2.z) < 1 + tol_com)
                ):

                    # FreeCAD.Console.PrintMessage(
                    #     "    Volume and CenterOfMass are similar, "
                    #     "we assume geometry is similar.\n"
                    # )
                    return True

                # print("    Only Volume is similar but not the geometry.")

    return False

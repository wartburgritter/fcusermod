# ***************************************************************************
# *   Copyright (c) 2017 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

import FreeCAD


# ************************************************************************************************
def add_wireframe(group):
    # make a wireframe compound out of the input group objects
    doc = group.Document
    c1 = doc.addObject("Part::Compound", group.Label + "_Wire")
    c1.Links = group.Group
    c1.ViewObject.LineWidth = 1.0
    if FreeCAD.GuiUp:
        c1.ViewObject.DisplayMode = "Wireframe"
        group.ViewObject.hide()
    doc.recompute()


# ************************************************************************************************
# the spatial structure elements do have a Shape too
# the building storey is a building part which could be a assembly too
spatial_structure = [
    "Building",
    "Building Storey",
    "Building Element Part"
]


# ************************************************************************************************
def copy_archgroup_spatial(group):
    print(group.Label)
    doc = group.Document
    new_grp = doc.copyObject(group, True)
    new_grp.Label = group.Label + "_Spatial"
    for obj in new_grp.OutListRecursive:
        if hasattr(obj, "IfcType"):
            if obj.IfcType not in spatial_structure:
                doc.removeObject(obj.Name)
        else:
            doc.removeObject(obj.Name)
    doc.recompute()
    return new_grp


# ************************************************************************************************
def copy_group_with_arch_to_part(group):
    from .geomchecks import check_solid_geometry
    print(group.Label)
    doc = group.Document
    name_new_grp = group.Label + "_Parts"
    new_grp = doc.addObject("App::DocumentObjectGroup", "_" + name_new_grp)
    new_grp.Label = name_new_grp
    name_error_grp = group.Label + "_Errors_on_copy_Objekte_sind_nicht_im_Diff"
    error_grp = doc.addObject("App::DocumentObjectGroup", "_" + name_error_grp)
    error_grp.Label = name_error_grp

    # FreeCAD object names can not begin with a digit and can not have a $ inside
    # https://forum.freecadweb.org/viewtopic.php?f=22&t=54520

    for obj in group.OutListRecursive:
        if (
            hasattr(obj, "Shape")
            and hasattr(obj, "IfcType")
            and obj.IfcType not in spatial_structure
            and hasattr(obj, "GlobalId")
            # and obj.GlobalId != ""  # seams only set on import ifc, thus commented because of diff of diffs ?!?
        ):
            # print(obj.Name)
            # tol_length = 0.2  # mm
            tol_length = 0.01  # mm  # test wegen smart diff und minikleine Wandstuecke
            solid_check = check_solid_geometry(obj.Shape, tol_length=tol_length)
            if solid_check == "":
                new_obj = doc.addObject("Part::Feature", obj.Name)
                new_obj.Label = obj.GlobalId
                new_grp.addObject(new_obj)
                new_obj.Shape = obj.Shape.Solids[0]
            # split compounds of solids into solids
            elif solid_check == "More than one solid":  # aufpassen der String ist schnell falsch
                print("Try to splitt multiple solids")
                for index, sol in enumerate(obj.Shape.Solids):
                    new_obj = doc.addObject("Part::Feature", obj.Name)
                    new_obj.Label = obj.GlobalId
                    new_obj.Shape = sol
                    more_solidcheck = check_solid_geometry(sol, tol_length=tol_length)
                    if more_solidcheck == "":
                        new_grp.addObject(new_obj)
                    else:
                        print(more_solidcheck)
                        error_grp.addObject(new_obj)
            else:
                print(solid_check)
                new_obj = doc.addObject("Part::Feature", obj.Name)
                new_obj.Label = obj.GlobalId
                error_grp.addObject(new_obj)
                new_obj.Shape = obj.Shape
    doc.recompute()
    if FreeCAD.GuiUp:
        error_grp.ViewObject.Visibility = False
    # print(new_grp.Group)
    return new_grp


# ************************************************************************************************
def copy_group_with_only_parts(group, name=None):
    from Arch import makeFloor
    doc = group.Document
    FreeCAD.ActiveDocument = doc
    if not name:
        name = group.Name + "_copy"
    else:
        name = name
    # directly adding group.Group in the next line moves all the objects
    # deepcopy and copy do not work here, thus empty list, followed by a for loop
    new_group = makeFloor([], name=name)
    new_group.Label = name
    for o in group.Group:
        no = doc.addObject("Part::Feature", (o.Name + "_copy"))
        new_group.addObject(no)
        no.Shape = o.Shape
        # funktioniert nicht fuer Feature::Python oder group object
    return new_group


# ************************************************************************************************
def find_obj_in_arch_grp(baseobj, model_arch_base, no, grp_to_move_too):

    # creae a Arch Structure foreach solid in no
    #    use the type of the baseobj if found
    #    use Building Element Proxy if not found
    #
    # baseobj
    #    baseobj
    # model_arch_base
    #    basis group Arch Objects, in there we search for the object
    # no
    #    cut_obj or no_intersection_obj, is a Part::Feature
    #    this will be searched in model_arch_base
    #    if found the IfcType (IfcWall etc.) is used
    # grp_to_move_too
    #    the group the no obj is moved too

    from Arch import makeStructure

    doc = baseobj.Document
    FreeCAD.ActiveDocument = doc
    nao = makeStructure(no)

    # find baseobj in model_arch_base
    # print(baseobj.Label[:12])
    # print("")
    found_guid = False
    for arc_obj in model_arch_base.OutListRecursive:
        if hasattr(arc_obj, "GlobalId"):
            # print(arc_obj.GlobalId[:12])
            if arc_obj.GlobalId[:12] == baseobj.Label[:12]:  # see comments after method
                found_guid = True
                # print(arc_obj.GlobalId[:12])
                nao.IfcType = arc_obj.IfcType
                # WICHTIG, nicht die GUID uebernehmen, es kann mehrere gleiche geben
                # weil bei mehreren Solids werden mehrere Part::Features erstellt
                # und hier her geparsed
                # print(arc_obj.InList[0].Name)
                # print(arc_obj.InList[0].Label)
                # suche label in dem spatial tree group
                # TODO move nao obj in den spatial tree
                # aber ich brauche bauteil und teilvolumentrennung
                break
    else:
        # no GUID found
        nao.IfcType = "Building Element Proxy"
    # print("found_guid: {}".format(found_guid))
    grp_to_move_too.addObject(nao)


"""
"3oVnDCDbT6weBwy7o"[:12]
"GUID_3oVnDCDbT6weBwy7oXUTKV_copy_cut"[5:17]

>>>
>>> "3oVnDCDbT6weBwy7o"[:12]
'3oVnDCDbT6we'
>>> "GUID_3oVnDCDbT6weBwy7oXUTKV_copy_cut"[5:17]
'3oVnDCDbT6we'
>>>
"""

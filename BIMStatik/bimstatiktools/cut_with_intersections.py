# ***************************************************************************
# *   Copyright (c) 2023 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

# to be able to use this:  print("null shape ", end="")
from __future__ import print_function

import time

import FreeCAD
import Part

from .geomchecks import is_similar_geometry


# ************************************************************************************************
def cut_with_intersections(inter_shs, cutobj_sh, cutTolerance):

    # cutTolerance ...
    # https://forum.freecadweb.org/viewtopic.php?f=27&t=18391&start=10#p151947
    # https://forum.freecadweb.org/viewtopic.php?style=3&f=3&t=31007&sid=bfc6b58286bcd6a40d24d025a219400a&start=20
    # https://dev.opencascade.org/doc/occt-7.3.0/overview/html/occt_user_guides__boolean_operations.html#occt_algorithms_11a
    # https://forum.freecadweb.org/viewtopic.php?t=6010
    # https://forum.freecadweb.org/viewtopic.php?t=11237
    # fuer genau meine probleme mit kleinen abstaenden gibt es FuzzyOperations

    # debug = True
    debug = False

    # es wird mit cutobj_sh gestarted und dann abgezogen bis rest oder bis nichts
    # rest ist unterschied
    # nichts heisst das obj hat keinen unterschied
    # lieber nichts zurueckgeben als falscher unterschied
    cut_result_sh = cutobj_sh

    FreeCAD.Console.PrintMessage(
        "  Start to cut all found {} intersection objects to get the remaining volume.\n"
        .format(len(inter_shs))
    )
    # FreeCAD.Console.PrintMessage("    {}\n".format(inter_shs))

    # neue idee, die reihenfolge der cuts spiellt sicher eine rolle
    # resort
    # https://www.delftstack.com/de/howto/python/sort-list-of-lists-in-python/
    # nach Volumen, das groesste zuerst
    temp_helper_unsorted = []
    for inter_sh in inter_shs:
        temp_helper_unsorted.append([inter_sh, inter_sh.Volume])
    from operator import itemgetter
    temp_helper_sorted = sorted(temp_helper_unsorted, key=itemgetter(1), reverse=True)
    sort_volumen_inter_shs = []
    for helplist in temp_helper_sorted:
        sort_volumen_inter_shs.append(helplist[0])
    inter_shs = sort_volumen_inter_shs

    # noch neue idee, mit volumen pruefen
    # (vol1 + vol2) minus vol_vereinigung = vol_intersection
    # vol1 - vol_intersection = vol(cut(vol1 mit vol2))

    # Der eigentliche cut des obj mit allen intesections *************************************
    # auslagern mit den informationen hier --> done
    # a) ausschliessen, das eine gleiche geometrie wie cut_result_sh in intersections ist
    # weil ein cut mit einer geometrie die der eigenen gleich ist
    # erzeugt oft eine invalid Shape, aber besser hier als wenn das obj in intersections
    # nicht gefunden wird. Heisst wenn es bei similar nicht gefunden wird muss es unbedingt
    # bei intersections gefunden werden. Nun muessen wir uns hier einen Kopf machen wie
    # wir das handeln ... wenn error, schwerpunkt und volumen pruefen, dann klar es ist
    # evtl. aehnlich, evtl. mit offset dann cut, dann kann man das noch besser pruefen
    # b) ausschliessen, das eine Geometrie cut_result vollkommen umschliesst
    # make a boolean cut: cutobj with each ish from inter_shs
    # use the result of each cut as start for the next cut
    # TODO before next cut teste of aktuelles cut_result_sh gleich mit der naechsten ish ist,
    # oder ob cut_result_sh kommplett innerhalb von ish ist,
    # dann wuerde es wieder eine invalid shape im cut geben
    # if after all cuts there is still some volume left, this volume has been added
    # remove the cutobj in any case, thus we may miss some cuts, but we do not have to many
    # add cut result obj, if it has Vertexes and Bop check passes
    cut_result_sh.fixTolerance(cutTolerance)
    for count, ish in enumerate(inter_shs):
 
        FreeCAD.Console.PrintMessage(
            "    Make the cut for ish {} (index {}) out of {} intersection shapes.\n"
            .format(count + 1, count, len(inter_shs))
        )

        if len(ish.Vertexes) == 0:
            FreeCAD.Console.PrintError(
                "  ish index {} has 0 Vertexes, FIXME (should never happen)!!!".format(count)
            )
            continue
        # Part.show(ish)
 
        # check if cut_result_sh and ish are is_similar_geometry
        # this could happen, a result from a cut before made them similar, does it really happen?
        # this is ok and means no new volume added
        # print("{}, {}".format(len(cut_result_sh.Vertexes), len(ish.Vertexes)))
        # the last cut has returned a compound, a compound does not have a CenterOfMass
        # print("{}, {}".format(cut_result_sh.Solids[0].CenterOfMass, ish.Solids[0].CenterOfMass))
        if is_similar_geometry(cut_result_sh.Solids[0], ish.Solids[0]):
            FreeCAD.Console.PrintMessage(
                "    cut_result_sh and ish index {}, are similar, no new volume.\n".format(count)
            )
            cut_result_sh = Part.Shape()  # empty invalid Shape, isNull() returns True
            break
        # TODO das der erste solid einfach uebergeben wird gefaellt mir nicht so gut
 
        ish.fixTolerance(cutTolerance)
        cut_result_old_sh = cut_result_sh  # temporary save old cut_result_sh for debugging

        # *** debugging
        # set debug am methodenanfang
        if debug is True:
            doc = FreeCAD.ActiveDocument
            cut_result_obj = doc.addObject("Part::Feature", "Cutresult_{}".format(count))  # last result
            cut_result_obj.Shape = cut_result_sh
            i_obj = doc.addObject("Part::Feature", "Intersection_index{}".format(count))  # next one
            i_obj.Shape = ish
            # Part.show(ish)
            # Part.show(cut_result_sh)
            # der letzte cut fehlt, das ist dann das obj im unterschied was die suche ausgeloest hat

        # the cut
        cut_result_sh = cut_result_sh.cut(ish)
        if len(cut_result_sh.Vertexes) == 0:
            cut_result_sh = Part.Shape()  # empty invalid Shape, isNull() returns True
            break
        else:
            try:
                cut_result_sh.check(True)
            except ValueError:
                FreeCAD.Console.PrintMessage(
                    "  Bad BOP check cut_result_sh for ish index {}: cut_result_sh has has bad geometry.\n"
                    .format(count)
                )
                if debug is True:
                    this_cut_result_obj = doc.addObject("Part::Feature", "Cutresult_{}_BAD".format(count + 1))  # this result
                    this_cut_result_obj.Shape = cut_result_sh
                # Result is set to Nullshape. keine Unterschiede
                #
                # ich habe bsp wo der die cut operation in der GUI manuell keine probleme hatte
                # ich habe bsp wo der bop_check kein problem macht, aber der cut falsch ist
                #
                cut_result_sh = Part.Shape()  # empty invalid Shape, isNull() returns True
                break

        # pruefe cut mit volumina, da cut ohne fehler funktioniert hat, annahme fuse funktionier erst recht
        vol_cut_result_sh = cut_result_sh.Volume
        vol_cut_result_old_sh = cut_result_old_sh.Volume
        vol_ish = ish.Volume
        vol_summe = vol_cut_result_old_sh + vol_ish
        shape_fuse = cut_result_old_sh.fuse(ish)
        # Part.show(shape_fuse)
        if shape_fuse.isNull() is False:
            vol_fuse = shape_fuse.Volume
        else:
            vol_fuse = 0
        vol_intersection = vol_summe - vol_fuse
        vol_calculated = vol_cut_result_old_sh - vol_intersection
        vol_cut = cut_result_sh.Volume
        FreeCAD.Console.PrintMessage("      {}\n".format(round(vol_intersection, 0)))
        if debug is True:
            FreeCAD.Console.PrintMessage("{}\n".format(round(vol_cut_result_sh, 0)))
            FreeCAD.Console.PrintMessage("{}\n".format(round(vol_cut_result_old_sh, 0)))
            FreeCAD.Console.PrintMessage("{}\n".format(round(vol_ish, 0)))
            FreeCAD.Console.PrintMessage("{}\n".format(round(vol_summe, 0)))
            FreeCAD.Console.PrintMessage("{}\n".format(round(vol_fuse, 0)))
            FreeCAD.Console.PrintMessage("{}\n".format(round(vol_intersection, 0)))
            FreeCAD.Console.PrintMessage("{}\n".format(round(vol_calculated, 0)))
            FreeCAD.Console.PrintMessage("{}\n".format(round(vol_cut, 0)))
        # Part.show(cut_result_old_sh)
        # Part.show(ish)
        # folgende situation ... cut funktioniert, es ist eine intersection vorhanden
        # heisst volumen sollte kleiner werden, aber es wird falsches ergebnis zurueckgegen
        # cut_result_sh hat gleiches volumen, aber mehr Vertexes, Kanten, FlaechenBOPCheck
        # BOPCheck gibt keinen error zurueck.
        if vol_intersection == 0:
            is_len_vertexes_equal = False
            for sol in shape_fuse.Solids:
                if len(cut_result_sh.Vertexes) == len(cut_result_old_sh.Vertexes):
                    # koennte auch zufall sein
                    is_len_vertexes_equal = True
                    break  # alles ok nachster cut
            else:
                FreeCAD.Console.PrintMessage("  Bad cut obwohl BOPcheck ok.\n")
                cut_result_sh = Part.Shape()  # empty invalid Shape, isNull() returns True
                break  # Not ok, keine unterschiede trotz intersections
        # koennte alles in der intersection suche laufen, volumenzeug etc.

    FreeCAD.Console.PrintMessage("  Finished to get the remaining volume.\n")

    # BEACHTEN, nur wenn gueltige Shape mit volumen da ist, wird diese hinzugefuegt
    # heisst es wird auch bei falscher Shape geloescht, nicht nur bei leerer !
    # zuerst !!!!! Wobei erstmal loesen,
    # dass Shape zuviel hinzugefuegt oder zuwenig geloescht wird
    # Es hat shapes, die sind in beiden, oder in allen drei groups ...

    if (
        cut_result_sh.isNull() is False
        and cut_result_sh.isValid() is True
        and cut_result_sh.Volume > 0
    ):
        try:
            cut_result_sh.check(True)
        except ValueError:
            FreeCAD.Console.PrintMessage(
                "  Bad BOP check after last cut: cut_result_sh has has bad geometry.\n"
            )
            # test noetig wenn der test je ish count deaktiviert ist
            # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            # hier umstellen Shapes mit bad check sind resultat und im check weiter oben fuer weiter laufen lassen
            # wenn es keine unterschiede mehr gibt kann man dass noch laufen lassen
            # TODO mit parameter steuern
            return Part.Shape()  # empty invalid Shape, isNull() returns True
            # return cut_result_sh
        return cut_result_sh
    else:
        return Part.Shape()  # empty invalid Shape, isNull() returns True

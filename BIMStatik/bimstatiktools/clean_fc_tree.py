# ***************************************************************************
# *   Copyright (c) 2022 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

"""
from bimstatiktools.clean_fc_tree import clean_tree
clean_tree()


"""

import FreeCAD as App


def clean_tree():
    # init
    doc = App.ActiveDocument
    doc.recompute()

    # groups
    delete_grp_endings = [
        "_Parts",
        "_Errors_on_group_copy",
        "BIMGeomDiff_Ergebnisse",
    ]
    delete_grp_objs = []
    for o in doc.Objects:
        for delete_grp_ending in delete_grp_endings:
            if o.Label.endswith(delete_grp_ending):
                delete_grp_objs.append(o)
    for grp in delete_grp_objs:
        grp.removeObjectsFromDocument()
        doc.removeObject(grp.Name)
        doc.recompute()

    # compounds
    delete_compou_endings = [
        "_Parts_Wire",
    ]
    delete_compou_objs = []
    for o in doc.Objects:
        for delete_compou_ending in delete_compou_endings:
            if o.Label.endswith(delete_compou_ending):
                delete_compou_objs.append(o)
    for compou in delete_compou_objs:
        doc.removeObject(compou.Name)
    
    doc.recompute()
    
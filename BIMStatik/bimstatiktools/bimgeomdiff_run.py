# ***************************************************************************
# *   Copyright (c) 2022 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

import glob
import importlib
import os
import shutil
import sys
import time
from os.path import join


# TODO
# eine kleine gui wie in BIMTester ist auch denkbar --> dauert aber laenger


# TODO
# damit ifc richtig importiert werden, wird ATM FreeCAD.exe anstatt FreeCADCmd.exe ausgefuehrt
# dann aber alle prints in der Gui und diese wird bei scriptende geschlossen
# log datei erstellen


# script von einem gestarteten FreeCAD ausfuehren, copy the following (oder button clicken ;-))
# ist auch per click aus module BIMStatik abrufbar
# bei beiden bleibt die FreeCAD oberflaeche danach offen
"""
import importlib
import bimstatiktools.bimgeomdiff_run as bimgeomdiffrun

importlib.reload(bimgeomdiffrun)

bimgeomdiffrun.run_bimgeomdiff()
bimgeomdiffrun.run_bimgeomdiff(["Smart_ARC_ROH_Plattenbauteile_UG", "Smart_ING_TRW_Plattenbauteile_UG"])
bimgeomdiffrun.run_bimgeomdiff(["4130_ING_N_TRW_20240417", "4130_ING_N_TRW_20240418b"])

"""

std_workpath = join("C:", os.sep, "0_BHA_privat", "BIMGeomDiff")


def run_bimgeomdiff(ifc_file_names=[], workpath=std_workpath, outname=None):

    time_start = time.process_time()
    print("\n\nStart BIMGeomDiff")

    diffdir = workpath
    if os.path.isdir(diffdir) is False:
        print(" The diffdir: {} does not exist.".format(diffdir))
        return

    if ifc_file_names == []:
        # get ifc files in BIMGeomDiff directory
        ifc_files = glob.glob(join(diffdir, "*.ifc"))
        if len(ifc_files) != 2:
            print("Not exact two ifc files in working dir.")
        # print(ifc_files)
        # print(os.path.splitext(ifc_files[0])[0])
        # print(os.path.basename(ifc_files[0]))
        # print(os.path.basename(os.path.splitext(ifc_files[0])[0]))
        ifc_file_names.append(os.path.basename(os.path.splitext(ifc_files[0])[0]))
        ifc_file_names.append(os.path.basename(os.path.splitext(ifc_files[1])[0]))
    else:
        if len(ifc_file_names) != 2:
            print("Not exact two ifc file names parsed to method.")
            return
        ifc_files = [
            join(diffdir, ifc_file_names[0] + ".ifc"),
            join(diffdir, ifc_file_names[1] + ".ifc"),
        ]
        # print(ifc_files)
        for ifc_file_p in ifc_files:
            if os.path.isfile(ifc_file_p) is False:
                print("File does not exist: {}".format(ifc_file_p))
                return

    # print(len(ifc_files))
    for aifc_file in ifc_files:
        print("    {}".format(aifc_file))

    # get outdir name and outresult file name
    if outname is None:
        diff_result_name = "Unterschiede"
        diff_result_dir = join(diffdir, "Report__{}__diff__{}".format(ifc_file_names[0], ifc_file_names[1]))
    elif outname == "short":
        diff_result_name = "Unterschiede"
        diff_result_dir = join(diffdir, "Diffreport")
    else:
        # Report_EG_Platten wobei EG der Smartdiff Geschossname ist
        diff_result_name = "Diffresult_{}".format(outname)  # glaube probleme wenn mit zahl beginnt
        # print(diff_result_name)
        diff_result_dir = join(diffdir, "Diff_{}".format(outname))

    # import FreeCAD document
    sys.path.append("C:/0_BHA_privat/progr/FreeCAD/FreeCAD_0.20.xxxxx/bin")
    import FreeCAD as fcapp
    import FreeCADGui as fcgui

    # new FreeCAD document and save the empty doc
    # TODO pruefe ob document mit dem namen schon vorhandnen, wenn ja return
    doc = fcapp.newDocument(diff_result_name)
    result_all_path = join(diff_result_dir, diff_result_name + ".FCStd")
    doc.saveAs(result_all_path)
    doc.recompute()
    # doc.addObject("Part::Box","Box")
    # doc.recompute()
    # doc.save()

    # copy ifcs into result dir
    for aifc_file in ifc_files:
        shutil.copy2(aifc_file, diff_result_dir)

    # ifc import prefs
    ifc_import_prefs = {
        "DEBUG": True,
        "PREFIX_NUMBERS": False,
        "SKIP": ["IfcOpeningElement"],
        "SEPARATE_OPENINGS": False,
        "ROOT_ELEMENT": "IfcProduct",
        "GET_EXTRUSIONS": False,
        "MERGE_MATERIALS": True,
        "MERGE_MODE_ARCH": 1,
        "MERGE_MODE_STRUCT": 0,
        "CREATE_CLONES": False,
        "IMPORT_PROPERTIES": False,
        "SPLIT_LAYERS": False,
        "FITVIEW_ONIMPORT": True,
        "ALLOW_INVALID": True,
        "REPLACE_PROJECT": True,
        "MULTICORE": 0,
        "IMPORT_LAYER": True,
    }

    # es gibt:
    # doc recompute
    # gui doc recompute
    # gui updateGui
    

    # import ifcs and save doc
    from importIFC import insert as insert_ifc
    ifc_main_grps = []
    for aifc_file in ifc_files:
        # do not use doc = insert... a other doc would be returned and my doc overwritten
        insert_ifc(aifc_file, diff_result_name, preferences=ifc_import_prefs)
        doc.recompute()
        ifc_main_grp_obj = doc.getObjectsByLabel(os.path.basename(aifc_file))[0]
        ifc_main_grps.append(ifc_main_grp_obj)
        guidoc = ifc_main_grp_obj.ViewObject.Document
        guidoc.update()
        fcgui.updateGui()
        doc.save()
        time.sleep(2)
        for o in doc.Objects:
            o.ViewObject.Visibility = False
        doc.recompute()
        fcgui.updateGui()
        # exit()
    fcgui.updateGui()

    # run diff and save doc
    print("\n\n")
    print("# {}".format(40*"*"))
    print("# Start to run the BIMGeomDiff ...")
    print("# {}".format(40*"*"))
    fcgui.updateGui()
    # exit()
    from bimstatiktools import commandtools
    importlib.reload(commandtools)
    commandtools.find_diff_with_arch_volumes(ifc_main_grps[0], ifc_main_grps[1])
    doc.save()
    fcgui.updateGui()

    # ifc export prefs
    ifc_export_prefs = {
        "DEBUG": True,
        "CREATE_CLONES": False,
        "FORCE_BREP": True,
        "STORE_UID": True,
        "SERIALIZE": False,
        "EXPORT_2D": True,
        "FULL_PARAMETRIC": False,
        "ADD_DEFAULT_SITE": True,
        "ADD_DEFAULT_BUILDING": True,
        "ADD_DEFAULT_STOREY": True,
        "IFC_UNIT": "metre",
        "SCALE_FACTOR": 0.001,
        "GET_STANDARD": False,
        "EXPORT_MODEL": "arch",
        "SCHEMA": "IFC2X3",
    }

    # export ifc
    
    print("\n\n")
    print("# {}".format(40*"*"))
    print("# Export the result to ifc ...")
    print("# {}".format(40*"*"))
    fcgui.updateGui()
    time.sleep(2)
    from exportIFC import export as export_ifc
    the_building_obj = doc.getObjectsByLabel("BIMGeomDiff_Ergebnisse")[0]
    ifc_export_file_path = join(diff_result_dir, diff_result_name + ".ifc")
    export_ifc([the_building_obj], ifc_export_file_path, preferences=ifc_export_prefs)

    all_time = round((time.process_time() - time_start), 3)
    print("\n")
    print("Overall duration: {} seconds.\n".format(all_time))

    # evtl nach export nochmal speichern, sonst sind die guids des diff exports nicht gespeichert

    # close open Documents
    fcapp.closeDocument(doc.Name)

    if os.path.isfile(result_all_path + "1"):
        os.remove(result_all_path + "1")
    if os.path.isfile(result_all_path + "2"):
        os.remove(result_all_path + "2")

# ***************************************************************************
# *   Copyright (c) 2017 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

import FreeCAD

from bimstatiktools import geomcompare
from bimstatiktools import grouptools


# ************************************************************************************************
# ************************************************************************************************
# ************************************************************************************************
def find_diff_with_arch_volumes(grp_arch_1, grp_arch_2):

    import importlib
    importlib.reload(geomcompare)
    # can geloescht werden wenn entwicklung pausiert

    # ********************************************************************************************
    print("\n\n")
    print("# {}".format(40*"*"))
    print("# Copy a parts group out of every Arch group ...")
    print("# {}".format(40*"*"))
    if FreeCAD.GuiUp:
        grp_arch_1.ViewObject.Visibility = False
        grp_arch_2.ViewObject.Visibility = False
    if FreeCAD.GuiUp:
        import FreeCADGui
        FreeCADGui.updateGui()
    grp_parts_1 = grouptools.copy_group_with_arch_to_part(grp_arch_1)
    grp_parts_2 = grouptools.copy_group_with_arch_to_part(grp_arch_2)

    # ********************************************************************************************
    print("\n\n")
    print("# {}".format(40*"*"))
    print("# Make two wireframe compound out of the part group objects ...")
    print("# {}".format(40*"*"))
    if FreeCAD.GuiUp:
        import FreeCADGui
        FreeCADGui.updateGui()
    grouptools.add_wireframe(grp_parts_1)
    grouptools.add_wireframe(grp_parts_2)

    # ********************************************************************************************
    # what is this for? TODO to get the spatial structure in the diff
    # make two group copies of the orgin but only with the spatial structure
    # grp_spatial_1 = grouptools.copy_archgroup_spatial(grp_arch_1)
    # grp_spatial_2 = grouptools.copy_archgroup_spatial(grp_arch_2)

    # ********************************************************************************************
    print("\n\n")
    print("# {}".format(40*"*"))
    print("# Get for every obj of both groups all intersectionBBobjs ...")
    print("# {}".format(40*"*"))
    if FreeCAD.GuiUp:
        import FreeCADGui
        FreeCADGui.updateGui()
    from .groupintersectiontools import get_both_grp_intersectionBB_objs
    grp1_intersectionBB_objs_1, grp1_intersectionBB_objs_2 = get_both_grp_intersectionBB_objs(
        grp_parts_1,
        grp_parts_2
    )
    # print(grp1_intersectionBB_objs_1)
    # print(grp1_intersectionBB_objs_2)
    # print(len(grp1_intersectionBB_objs_1))
    # print(len(grp1_intersectionBB_objs_2))
    # print(list(grp1_intersectionBB_objs_1.keys()))
    # print(list(grp1_intersectionBB_objs_2.keys()))

    # exit()

    # ********************************************************************************************
    if FreeCAD.GuiUp:
        import FreeCADGui
        FreeCADGui.updateGui()
    print("\n\n")
    print("# {}".format(40*"*"))
    print("# Start first model group cut ...")
    print("# {}".format(40*"*"))
    no_intersection_grp_12, model_cut_result_12 = geomcompare.group_cut_group(
        grp_parts_1,
        grp_parts_2,
        grp1_intersectionBB_objs_1,
        grp_arch_1,
        # grp_spatial_1
    )

    # ********************************************************************************************
    print("\n\n")
    print("# {}".format(40*"*"))
    print("# Start second model group cut ...")
    print("# {}".format(40*"*"))
    if FreeCAD.GuiUp:
        import FreeCADGui
        FreeCADGui.updateGui()
    no_intersection_grp_21, model_cut_result_21 = geomcompare.group_cut_group(
        grp_parts_2,
        grp_parts_1,
        grp1_intersectionBB_objs_2,
        grp_arch_2,
        # grp_spatial_2
    )
    print("\n")
    print("# End of both model group cuts ...")
    print("# {}".format(40*"*"))
    if FreeCAD.GuiUp:
        import FreeCADGui
        FreeCADGui.updateGui()

    # ********************************************************************************************
    # add result floors to a building
    from Arch import makeBuilding
    abuilding = makeBuilding([
        no_intersection_grp_12,
        model_cut_result_12,
        no_intersection_grp_21,
        model_cut_result_21
    ])
    abuilding.Label = "BIMGeomDiff_Ergebnisse"
    doc = grp_arch_1.Document
    doc.recompute()

    # ********************************************************************************************
    # clean tree
    # delete parts groups
    grp_parts_1.removeObjectsFromDocument()
    # because off a access violation break
    try:
        doc.removeObject(grp_parts_1.Name)
    except:
        print("Problem on remove object {}".format(grp_parts_2.Name))
    doc.recompute()
    grp_parts_2.removeObjectsFromDocument()
    # because off a access violation break
    try:
        doc.removeObject(grp_parts_2.Name)
    except:
        print("Problem on remove object {}".format(grp_parts_2.Name))
    doc.recompute()

    # delete empty Error copy groups 
    for o in doc.Objects:
        if hasattr(o, "Group") and len(o.Group) == 0 and "Errors_on_" in o.Label:
            o.removeObjectsFromDocument()
            doc.removeObject(o.Name)
            doc.recompute()


# ************************************************************************************************
# ************************************************************************************************
# ************************************************************************************************
def debug_find_diff_with_arch_volumes(grp_arch_1, grp_arch_2):

    import importlib
    importlib.reload(geomcompare)
    importlib.reload(grouptools)

    print("# copy every object of the arch groups to part groups *******************************")
    grp_parts_1 = grouptools.copy_group_with_arch_to_part(grp_arch_1)
    grp_parts_2 = grouptools.copy_group_with_arch_to_part(grp_arch_2)
    if FreeCAD.GuiUp:
        grp_arch_1.ViewObject.Visibility = False
        grp_arch_2.ViewObject.Visibility = False

    # es gibt objekte ohne Shape
    # die geben bei check und dann bei group copy Error die sollte man vollkommen loeschen
    # direkt nach import (oder in groupe schieben)
    # return

    print("# Start first model group cut *******************************************************")
    no_intersection_grp_12, model_cut_result_12 = geomcompare.group_cut_group(
        grp_parts_1,
        grp_parts_2,
        None,
        grp_arch_1,
    )

    """ # ATM  only make one cut in selection order
    print("# Start second model group cut ********************************************************")
    no_intersection_grp_21, model_cut_result_21 = geomcompare.group_cut_group(
        grp_parts_2,
        grp_parts_1,
        None,
        grp_arch_2,
    )
    """

    print("# add result floors to a building ***************************************************")
    from Arch import makeBuilding
    abuilding = makeBuilding([
        no_intersection_grp_12,
        model_cut_result_12,
        # no_intersection_grp_21,
        # model_cut_result_21
    ])
    abuilding.Label = "BIMGeomDiff_Ergebnisse"
    doc = grp_arch_1.Document
    doc.recompute()


# ************************************************************************************************
# ************************************************************************************************
# ************************************************************************************************
def find_diff_volumes(grp_1, grp_2):

    # make two wireframe compound out of the input group objects
    grouptools.add_wireframe(grp_1)
    grouptools.add_wireframe(grp_2)

    # group cut
    geomcompare.group_cut_group(grp_1, grp_2)
    geomcompare.group_cut_group(grp_2, grp_1)
    # TODO: return the group names

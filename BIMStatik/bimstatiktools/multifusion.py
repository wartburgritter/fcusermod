# ***************************************************************************
# *   Copyright (c) 2017 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

from __future__ import print_function  # to be able to use this:  print("null shape ", end="")

import FreeCAD

import Part

if FreeCAD.GuiUp:
    import FreeCADGui


def make_smart_multifusion(fuselist):
    # evtl. noch vor jedem fuse den compound aufsplitten
    # dann solid zu solid fuse durchfuehren
    # die erste wird mit sich selber fusioniert
    # funktioniert fuer komplexe shapes (gesamtes geschoss) nicht fehelerfrei
    import time
    FreeCAD.Console.PrintMessage("We make a fusion of all shapes in the group\n")
    doc = FreeCAD.ActiveDocument
    pi = FreeCAD.Base.ProgressIndicator()
    pi.start("Animation", len(fuselist))
    fusestepgroup = doc.addObject("App::DocumentObjectGroup", "MultiFuseSteps")

    for o in doc.Objects:
        o.ViewObject.Visibility = False
    if FreeCAD.GuiUp:
        FreeCADGui.ActiveDocument.update()

    # find start shape, the one with lowest z-value
    alllenfuselist = len(fuselist)
    startobj = None
    minallz = 1E99
    timestart = time.clock()
    for o in fuselist:
        minobjz = 1E99
        for v in o.Shape.Vertexes:
            if v.Z < minobjz:
                minobjz = v.Z
        if minobjz < minallz:
            minallz = minobjz
            startobj = o
    timeduration = str(time.clock() - timestart)
    del fuselist[fuselist.index(startobj)]
    fusesh = startobj.Shape
    print("  Our startobj is {0}, time: {1}".format(startobj.Name, timeduration))
    fusestepobj = doc.addObject(
        "Part::Feature",
        "Fuse_" + (str(alllenfuselist - len(fuselist)) + "_" + startobj.Name)
    )
    fusestepgroup.addObject(fusestepobj)
    fusestepobj.Shape = fusesh
    doc.recompute()
    if FreeCAD.GuiUp:
        FreeCADGui.ActiveDocument.update()

    # my smart fuse
    bblist = []
    for o in fuselist:
        bblist.append(o.Shape.BoundBox)
    while fuselist:
        if FreeCAD.GuiUp:
            FreeCADGui.ActiveDocument.update()
        bbfuse = fusesh.BoundBox  # in one line we would enlarge bb of fusesh
        bbfuse.enlarge(2)  # make bbfuse 10 mm greater in all direktions

        # find a shape to fuse with fusesh
        fuseobj = None
        minallz = 1E99
        print("  seach fot the next fuse objekt: ", end="")
        timestart = time.clock()
        intersectioncount = 0
        for i, o in enumerate(fuselist):
            if bbfuse.intersect(bblist[i]):
                intersectioncount += 1
                s = o.Shape
                is_a_posible_fuse = False
                # we try isInside and use 2 mm as tolerance,
                # first version was with distToShape which slooow on komokex shapes
                # funktioniert super fuer STA modelle aus axis, aber nicht fuer TWR modelle
                for v in o.Shape.Vertexes:
                    if fusesh.isInside(v.Point, 5, True):
                        is_a_posible_fuse = True
                        break
                # find the obj with lowest max z koord
                if is_a_posible_fuse:
                    maxobjz = -1E99
                    for v in o.Shape.Vertexes:
                        if v.Z > maxobjz:
                            maxobjz = v.Z
                    if maxobjz < minallz:
                        minallz = maxobjz
                        fuseobj = o
        timeduration = str(time.clock() - timestart)

        # make fuse
        print("  fuse", end="")
        if not fuseobj:
            print("  No more object to fuse, we leave the fuselist", end="")
            # we will be here if there is a shape which is not connected to all other shapes
            # may be a balkon
            break
        else:
            print(
                " we found object {0}, time {1}, intersectioncounts {2}"
                .format(fuseobj.Name, timeduration, intersectioncount), end=""
            )
            print("  {} -->".format(fuseobj.Name), end="")
            timestart = time.clock()
            newfusesh = fusesh.fuse(fuseobj.Shape)
            print(" fuse time: {}".format(str(time.clock() - timestart)), end="")
            if not newfusesh:
                print("  problem", end="")
                break
            else:
                print("  refine", end="")
                # remove redundant edges (refine Shape in GUI)
                newfusesh = newfusesh.removeSplitter()
                # sometimes parts of a shape gets lost during a fusion
                print("  volume check", end="")
                if newfusesh.Volume < fusesh.Volume:
                    print("  Volume of newfusesh is smaller Volume fusesh: ", end="")
                    Part.show(newfusesh)
                    break
                else:
                    """
                    print("  BOP-Check", end="")
                    # newfusesh.check(True)  # to get the error on screen
                    try:
                        newfusesh.check(True)
                    except ValueError:
                        print("  BAD BOP-check !!!", end="")
                        break
                    """
                    if fusesh.isValid():  # simple check
                        print("  fusecheck is fine", end="")
                        fusesh = newfusesh
                        # we will have a new fusestep obj in the next line
                        # so make the current one not visible anymor
                        fusestepobj.ViewObject.Visibility = False
                        if FreeCAD.GuiUp:
                            FreeCADGui.ActiveDocument.update()
                        fusestepobj = doc.addObject(
                            "Part::Feature",
                            ("Fuse_" + str(alllenfuselist - len(fuselist) + 1) + "_" + fuseobj.Name)
                        )
                        fusestepgroup.addObject(fusestepobj)
                        fusestepobj.Shape = fusesh
                        doc.recompute()
                        pi.next()

        # the fuseobj needs to be deleted from fuselist and bblist
        index = fuselist.index(fuseobj)
        del fuselist[index]
        del bblist[index]
        print("  deleted {}, still {} objects to fuse".format(fuseobj.Name, len(fuselist)))

        doc.removeObject(fuseobj.Name)  # for debug reason sometimes useful, in conjunction with next lines
        if len(fuselist) == 0:
            break

    # add fusesh solid to the document
    print("\n  Solids in fusesh: {}".format(len(fusesh.Solids)))
    for s in fusesh.Solids:
        Part.show(s)
    return True

"""

# load project data first, see lines to copy in prj data module


import importlib
import bimstatiktools.bimgeomdiff_all as runallbimgeomdiff
importlib.reload(runallbimgeomdiff)


runallbimgeomdiff.run_all(prjdata)


# rezu alle geschosse in liste, auch zum rauskopieren
runallbimgeomdiff.run_all(prjdata, geschosse = ["01_UG2", "02_UG1", "11_EG", "21_OG1", "22_OG2", "23_AG3", "24_DA4"])


# ein gesamtes geschoss
# div geschossnamen, je nach bedarf anpassen
runallbimgeomdiff.run_all(prjdata, geschosse = "01_UG1")
runallbimgeomdiff.run_all(prjdata, geschosse = "23_AG3")
runallbimgeomdiff.run_all(prjdata, geschosse = "24_DA4")
runallbimgeomdiff.run_all(prjdata, geschosse = "22_OG2")


# nur vertikale oder platten fuer ein geschoss
# div geschossnamen, je nach bedarf anpassen
runallbimgeomdiff.run_vertikale(prjdata, "01_UG")
runallbimgeomdiff.run_platten(prjdata, "01_UG")
runallbimgeomdiff.run_platten(prjdata, "11_EG")

"""


import importlib
import os
from os.path import join

import bimstatiktools.bimgeomdiff_run as bimgeomdiffrun
importlib.reload(bimgeomdiffrun)

# std_workpath = join("C:", os.sep, "0_BHA_privat", "BIMGeomDiff")
std_workpath = join("C:", os.sep, "0_BHA_privat", "BIMGeomAllSerializerSmartDataDiff")


def run_all(prjdata, workpath=std_workpath, geschosse=None):

    spatial_data = prjdata.SpatialData

    if geschosse is None:
        geschosse = [geschoss for geschoss in spatial_data.keys() if geschoss is not None]
    elif isinstance(geschosse, str):
        geschosse = [geschosse]
    print(geschosse)

    # see smartdatadiff.smartdatadiff_run
    for geschoss in geschosse:
       if (
            geschoss is not None
            and  geschoss != "DG7"
        ):
            run_platten(geschoss, workpath)
            run_vertikale(geschoss, workpath)

    return True


def run_vertikale(geschoss, workpath=std_workpath):
    bimgeomdiffrun.run_bimgeomdiff(
        [
            "{}_Vertikalbauteile_Smart_ARC_ROH".format(geschoss),
            "{}_Vertikalbauteile_Smart_ING_TRW".format(geschoss),
        ],
        workpath=workpath,
        outname="{}_Vertikalbauteile".format(geschoss),
    )
    return True


def run_platten(geschoss, workpath=std_workpath):
    bimgeomdiffrun.run_bimgeomdiff(
        [
            "{}_Plattenbauteile_Smart_ARC_ROH".format(geschoss),
            "{}_Plattenbauteile_Smart_ING_TRW".format(geschoss),
        ],
        workpath=workpath,
        outname="{}_Plattenbauteile".format(geschoss),
    )
    return True

# ***************************************************************************
# *   Copyright (c) 2023 Bernd Hahnebach <bernd@bimstatik.org>              *
# *                                                                         *
# *   This file is part of the FreeCAD CAx development system.              *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

# to be able to use this:  print("null shape ", end="")
from __future__ import print_function

import FreeCAD
import Part


"""
# dies als GUI tool implementieren in BIMStatik
from bimstatiktools import geomchecks
geomchecks.is_similar_geometry(App.ActiveDocument.Component808002.Shape.Solids[0], App.ActiveDocument.Common002.Shape.Solids[0])
"""


# ************************************************************************************************
def get_both_grp_intersectionBB_objs(grp_parts_1, grp_parts_2):

    grp1_intersectionBB_objs_1 = {}
    grp1_intersectionBB_objs_2 = {}

    for obj in grp_parts_1.Group:
        intersecBB_objs = get_intersecBB_objs(obj, grp_parts_2)

        grp1_intersectionBB_objs_1[obj.Name] = intersecBB_objs

        for inBBobj in intersecBB_objs:
            if inBBobj.Name not in grp1_intersectionBB_objs_2:
                grp1_intersectionBB_objs_2[inBBobj.Name] = [obj]
            else:
                grp1_intersectionBB_objs_2[inBBobj.Name].append(obj)

    return grp1_intersectionBB_objs_1, grp1_intersectionBB_objs_2


# ************************************************************************************************
def get_intersecBB_objs(compare_obj, model2group):
    
    intersecBB_objs = []  # objekte mit intersection BB
    compareBB = compare_obj.Shape.BoundBox
    # FreeCAD.Console.PrintMessage("    Start BoundBoxes intersections calculation\n")
    for obj in model2group.Group:
        if obj.Shape.BoundBox.intersect(compareBB):
            # FreeCAD.Console.PrintMessage("found intersection BB")
            intersecBB_objs.append(obj)
    # FreeCAD.Console.PrintMessage("    " + str(intersecBB_objs) + "\n")
    # FreeCAD.Console.PrintMessage("    End BoundBoxes intersections calculation\n")
    # FreeCAD.Console.PrintMessage("    Start common calculation\n")

    return intersecBB_objs


# ************************************************************************************************
# TODO: beschreiben, uebergabeparameter, returnwerte und was macht methode
def find_intersection_objs(compare_obj, model2group, intersecBB_objs):

    FreeCAD.Console.PrintMessage(
        "  Start search for intersection objects.\n"
    )

    compare_sh = compare_obj.Shape

    debugprints = False
    if debugprints is True:
        FreeCAD.Console.PrintMessage("  We search for the intersection objets for: " + compare_obj.Name + "\n")
        FreeCAD.Console.PrintMessage("  Intersection objets search\n")
        Part.show(compare_sh)

    # intersection BoundBoxes
    if intersecBB_objs == []:
        intersecBB_objs = get_intersecBB_objs(compare_obj, model2group)

    if intersecBB_objs == []:
        FreeCAD.Console.PrintWarning(
            "    No intersection BoundBox found. Totally a new volume.\n"
        )
        # I have had not yet an example which does not return a intersecBB_objs but has a intersection.
        # might happen on wrong BB calculation, would be bad because we need ALL intersections.
        return []
    FreeCAD.Console.PrintMessage("    Found {} intersection BB\n".format(len(intersecBB_objs)))

    intesecBB_shs = []
    for intersecBB_obj in intersecBB_objs:
        intesecBB_shs.append(intersecBB_obj.Shape)


    # intersection objects
    intersec_shs = []
    all_error = ""

    # ueber allem steht, dass ALLE intersections zurueckgegeben werden muessen. ALLE!!!
    #
    # erst muss ich SICHER wirklich SICHER die intersection objects finden,
    # dann kann ich den cut in group_cut_group in angriff nehmen
    #
    # Eine nicht zurueckgegebene intersection die eine wirkliche intersection hat
    # resultiert wegen des spaeter fehlenden cuts in einem unterschied,
    # der gar kein unterschied ist
    # sondern nur ein unterschied weil die intersection nicht gefunden wurde
    #
    # man koennte auch einfach alle boundbox intersections als shape zurueckgeben und dann gibt
    # es einfach viele cuts mit nichts, aber problem sind wieder die nicht funktionierenden
    # intersections, wenn die intersection fehler erzeugt, dann kann der cut dann auch fehler
    # erzeugen. Aber wenn der cut fehler erzeugt, dann wird kein Unterschied hinzugefuegt.
    #
    # Wenn zu wenig intersections zurueggegeben werden ist die nicht gefundene intersection
    # dann ein unterschied.
    #
    # workaround die sicher alle intersection findet ...
    # return all intersecBB_objs als intersec_shs
    # ist in der summe sogar schneller
    # die mehr boolean cuts sind schneller als die ganze common tragoedie

    # TODO: es hat aber vor der eigentlichen common noch viel anderen code
    # ahnlicher code macht Sinn und das volumenzeug

    # siehe Text oben, assign intesecBB_shs to intersc_shs
    intersec_shs = intesecBB_shs
    # siehe text oben, aktuell keine intersection berechnung mit common
    ##intersec_shs = make_common_with_bbintersctions(compare_sh, intesecBB_shs)

    FreeCAD.Console.PrintMessage("  Finshed intersection objets search. Found {} object(s).\n".format(len(intersec_shs)))
    return intersec_shs


# ************************************************************************************************
# irgendwie ist der folgende code komisch. Der sucht auf Messer intersections
# besser waere es die zu suchen, die sicher keine intersections sind
# diese dann nicht zu intersections hinzufuegen. Dann weniger probleme dort, und schneller
# den coden komplett in Ruhe lassen
def make_common_with_bbintersctions(compare_sh, intesecBB_shs):

    from .geomchecks import check_goemetry_for_bool
    from .geomchecks import is_similar_geometry

    commonTolerance = 1.e-1  # 0.1 mm = 100 muecrometer, evtl. noch hoeher setzen, bis auf 1 mm ?!?
    compare_sh.fixTolerance(commonTolerance)

    intersec_shs = []
    all_error = False

    for count, bbinter_sh in enumerate(intesecBB_shs):

        FreeCAD.Console.PrintMessage("      BB {} of {}: ".format(count + 1, len(intesecBB_shs)))

        # Part.show(bbinter_sh)
        common_sh = None
        error = ""
        bop_check = False
        """
        # an Shapes mit vielen faces ist distToShape langsam,
        # aus performance gruenden vorher erst mit isInside filtern
        if compare_sh.distToShape(bbinter_sh)[0] > commonTolerance:
            error += "    this BoundBox intersection distToShape is greater than the tolerance.\n"
        """

        # ganz neue idee boolsche operationen
        # Problemreihenfolge
        # vereinigung, cut, intersection
        # heisst wenn vereinigung nicht geht, geht meist auch cut und intesection nicht
        # aber
        # wenn intersection nicht geht, kann cut und vereinigung noch gehen
        # wenn cut nicht geht geht evtl. noch vereinigung
        # zwei objekte vereinigen, wenn volumen nicht vol1 + vol2, dann intersection
        # :-) das sollte doch sehr robust sein, vor allem
        # wenn der vereinigung nicht geht, dann geht auch spaeter der cut nicht,
        # dann komplett ignorieren, heisst keine intersections ueberhaupt
        # (vol1 + vol2) minus vol_vereinigung = vol_intersection, super test hier
        # siehe auch cut_with_intersections methode

        # es gibt aber auch aehnliche, die bei intersection fehler ausgeben,
        # aber eben doch nicht gleich sind, also nicht frueher gefunden werden koenen
        # weil sie ahnlich sind aber nicht similar
        # die muessen hier gefunden werden. Evl. nach volumen und schwerpunkt suchen
        # aber mit groesserer tolleranz als bei similar, weil hier soll ja die decke
        # mit verschobener aussparung gefunden werden, oder das bauteil, aber mit
        # einem minianhaengsel and einer seite
        # oder einen offset anwenden 1 cm , dann intersection machen
        # dass ist die intersection identisch mit dem intersection object
        # dann intersection objekt hinzufuegen

        # !!!
        # try offset on all shapes of modell2 and check them afterwards in gui,
        # if 99 % work use offset before common or before start to find intersections

        # tolerance fix auch in cut in group_cut_group, hoffentlich hilft das,
        # dort hab ich 70 muecrometer difference shapes das soll unter tolerance fallen ...

        use_inter_shape = False
        for v in bbinter_sh.Vertexes:
            # an intersection volume could have only touching points, thus True
            if compare_sh.isInside(v.Point, 0.1, True):
                use_inter_shape = True
                break
                # Spezialfall: zwei shapes durchdringen sich,
                # ohne dass punkt inside ist, das geht einfach
                # Spezialfall: die compare_sh shape liegt komplett innerhalb der bbinter_sh
                #    --> TODO FIXME gar nicht erst nach intersection suchen
                # Mit distToShape arbeiten
            use_inter_shape = True
        if use_inter_shape is False:
            error += (
                "this BoundBox intersection has no point "
                "inside compare_sh or on a face of compare_sh.\n"
            )
        if use_inter_shape is False and (compare_sh.distToShape(bbinter_sh)[0] > commonTolerance):
            error += "this BoundBox intersection distToShape is greater than the tolerance.\n"
        else:
            # volumen und schwerpunkt mit groesserer toleranz pruefen,
            # wenn ok zu intersection hinzufuegen,
            # BB hatte intersection und Volumen und Schwerpunkt sind aehnlich
            # dann ist es auch eine intersection (kenne kein gegenbeispiel)
            # dies vor der common operation pruefen das eben keine gegenbeispiel
            found_nearlysimilar = False
            if is_similar_geometry(compare_sh, bbinter_sh, tol_vol=0.1, tol_com=0.001) is True:
                FreeCAD.Console.PrintMessage("    Found one intersection with Volume and Center of Mass\n")
                # TODO kontroll parameter.
                # Wenn obj hinzugefuegt werden, die keine intersection sind, waere das nicht gut.
                # Daher nie zwei hinzufuegen, es kann nur eins geben.
                if found_nearlysimilar is True:
                    error += (
                        "Found more than one nearly similar intersections. "
                        "This should never happen. Please debug.\n"
                    )
                    # intersec_shs.append(bbinter_sh)
                    # spaeter
                found_nearlysimilar = True
                bop_check = True  # bbinter_sh is clean shape, we have checked this before
                # Nachdenken !!!!!!!!!!!! hier wird die bbinter_sh hinzugefuegt, nicht die common Shape
                # das ist falsch die bbinter_sh ist evtl groesser oder kleiner
            else:
                # boolsche operation intesection (common)
                bbinter_sh.fixTolerance(commonTolerance)
                # Part.show(compare_sh)
                # Part.show(bbinter_sh)
                common_sh = compare_sh.common(bbinter_sh)
                if not common_sh.isNull():
                    # Part.show(common_sh)
                    # es gibt faelle bei denen es einen Part.OCCError gibt
                    # dieser loest eine Exception aus und bricht programm ab
                    try:
                        common_sh = common_sh.removeSplitter()
                    except Exception:
                        print("Problem on removeSplitter, ignored (in geomcompare.py)")

                # call check_goemetry_for_bool
                error, bop_check = check_goemetry_for_bool(common_sh, error)

        if error != "" and bop_check is False and len(common_sh.Vertexes) != 0:
            # workaround check on Vertexes damit naechsten drei zeilen nicht eintreten
            # Bei allen mit leerer common_sh wird auch dies probiert
            # das will ich gerade nicht, sondern nur wenn es eine common_sh mit volumen etc hat und die errors hat.
            # es wird immer die obere und untere decke fuer jede wand hier als bbinter_sh gefunden!!!
            # try to make offsetsh_compare_sh bigger with 10 mm offset (bigger is more robust than smaller)
            FreeCAD.Console.PrintError(error)
            FreeCAD.Console.PrintError("Try postive offset on compare_sh.\n")
            try:
                offsetsh_compare_sh = compare_sh.makeOffsetShape(offset=10, tolerance=1e-3, offsetMode=0, join=2)
            except Exception:
                FreeCAD.Console.PrintMessage("Error on offset.\n")
                offsetsh_compare_sh = None
            if offsetsh_compare_sh is None or check_goemetry_for_bool(offsetsh_compare_sh) != ("", True):
                    # no change on error an bop_check
                    FreeCAD.Console.PrintMessage("Offset on compare_sh did not return a Shape without errors.\n")
            else:
                # try common
                common_offsetsh_compare_sh = offsetsh_compare_sh.common(bbinter_sh).removeSplitter()
                if check_goemetry_for_bool(common_offsetsh_compare_sh) != ("", True):
                    FreeCAD.Console.PrintMessage("Common of compare_sh with offset and bbinter_sh did NOT return a clean Shape.\n")
                else:
                    # set error and bop_check
                    FreeCAD.Console.PrintMessage("Common of compare_sh with offset and bbinter_sh did return a clean Shape :-).\n")
                    error, bop_check = "", True

        # Nachdenken es wird die offset shape hinzugefuegt. Test mit 20 mm offset. jetzt nicht mehr
        # try positive offset on compare_sh  YES

        if error == "" and bop_check is True:
            # intersec_shs.append(common_sh)
            intersec_shs.append(bbinter_sh)  # HIER WIRD DIE bbinter_sh HINZUGEFUEGT, NICHT DIE COMMON SHAPE
            FreeCAD.Console.PrintMessage("found intersection object.\n")

        else:
            FreeCAD.Console.PrintError(error)
            # Part.show(common_sh)
            all_error = True

            """
            compare_sh and bbinter_sh are orginal Shapes, thus have been checked before
            try:
                # check does not return anything
                # it raises an ValueError if shape has errorscompare_sh.check(True)
                compare_sh.check(True)
            except ValueError:
                FreeCAD.Console.PrintError(
                    "    The compare obj has erros. "
                    "This might result in errors in trying to find intersections.\n"
                )
            try:
                # check does not return anything
                # it raises an ValueError if shape has errorscompare_sh.check(True)
                bbinter_sh.check(True)
            except ValueError:
                FreeCAD.Console.PrintError(
                    "    The object which probably intersects has errors. "
                    "This might result in errors in trying to find intersections.\n"
                )
            """

    if intersec_shs:
        pass
        # FreeCAD.Console.PrintMessage("    " + str(intersec_shs) + "\n")
    else:
        if all_error is True:
            FreeCAD.Console.PrintMessage(
                "    No intersection found, there have been errors. "
                "This might be the reason.\n"
            )
        else:
            FreeCAD.Console.PrintMessage(
                "    No intersection found, there was no error. "
                "But there was a BoundBox intersection.\n"
            )

    if len(intesecBB_shs) != len(intersec_shs) and all_error is True:
        # wg time in report view mehrere prints
        FreeCAD.Console.PrintMessage(
            "    Intersection BB {} != {} Intersection objects:\n"
            .format(len(intesecBB_shs), len(intersec_shs))
        )
        FreeCAD.Console.PrintMessage(
            "      problematisch, wenn ein nicht verwendetes IntersectionBoundBox Objekt\n"
        )
        FreeCAD.Console.PrintMessage(
        "      dem compare_sh stark aehnlich ist, aber nicht similar.\n"
        )  # siehe oben hinweise: das gleiche darf nicht dabei sein

    FreeCAD.Console.PrintMessage("    End common operation\n")
    return intersec_shs


"""
# offset for better robust intersections and cuts
# Offset tool creates parallel copies of a selected shape
# at a certain distance from the base shape, giving a new object
# does not work some robust

sh = App.ActiveDocument.Shape.Shape
new_sh = sh.makeOffsetShape(
    offset=20,  # negative Value will shrink the shape
    tolerance=1e-3,
    inter=False,  # not implemented
    self_inter=False,  # not implemented
    offsetMode=0,  # 0=skin, 1=pipe, 2=recto-verso
    join=2,  # 0=arcs, 1=tangent, 2=intersection
    fill=True   # offsetting a shell will yield a solid
)
Part.show(new_sh)

Part.show(aShape.makeOffsetShape(offset=-1, tolerance=1e-3, offsetMode=0, join=2))

"""

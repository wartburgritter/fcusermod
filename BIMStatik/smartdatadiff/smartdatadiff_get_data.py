import importlib
import os
from os.path import join

import FreeCAD as App
from Arch import makeBuilding

from smartdatadiff import smartdatadiff_tools
importlib.reload(smartdatadiff_tools)


def get_smart_intersection_data_platten(
    workpath,
    diff_floor,
    roh_geschoss_guid=None,
    roh_ge_zweit_guid=None,
    trw_geschoss_guid=None,
    trw_ge_zweit_guid=None,
):

    print("workpath: {}".format(workpath))
    print("diff_floor: {}".format(diff_floor))
    # print("roh_geschoss_guid: {}".format(roh_geschoss_guid))
    # print("roh_ge_zweit_guid: {}".format(roh_ge_zweit_guid))
    # print("trw_geschoss_guid: {}".format(trw_geschoss_guid))
    # print("trw_ge_zweit_guid: {}".format(trw_ge_zweit_guid))

    # inits
    roh_name = "ARC_ROH"
    trw_name = "ING_TRW"

    # file names
    docn_roh_platten_floor = diff_floor + "_Plattenbauteile_" + roh_name
    docn_roh_vertikale_alle = "Alle_Vertikalbauteile_" + roh_name
    docn_trw_platten_floor = diff_floor + "_Plattenbauteile_" + trw_name
    docn_trw_vertikale_alle = "Alle_Vertikalbauteile_" + trw_name
    # file paths
    docp_roh_platten_floor = join(workpath, docn_roh_platten_floor + ".FCStd")
    docp_roh_vertikale_alle = join(workpath, docn_roh_vertikale_alle + ".FCStd")
    docp_trw_platten_floor = join(workpath, docn_trw_platten_floor + ".FCStd")
    docp_trw_vertikale_alle = join(workpath, docn_trw_vertikale_alle + ".FCStd")
    print(docp_roh_platten_floor)
    print(docp_roh_vertikale_alle)
    print(docp_trw_platten_floor)
    print(docp_trw_vertikale_alle)
    if (
        os.path.isfile(docp_roh_platten_floor) is False
        or os.path.isfile(docp_roh_vertikale_alle) is False
        or os.path.isfile(docp_trw_platten_floor) is False
        or os.path.isfile(docp_trw_vertikale_alle) is False
    ):
        print("FCStd File missing in workingdir: {}".format(workpath))
        return


    create_smart_intersection_objects_model_for_platten(
        workpath=workpath,
        diff_floor=diff_floor,
        # geom1 = roh
        geom1_name=roh_name,
        geom1_geschoss_guid=roh_geschoss_guid,
        geom1_ge_zweit_guid=roh_ge_zweit_guid,
        docn_geom1_vertikale_alle=docn_roh_vertikale_alle,
        docp_geom1_platten_floor=docp_roh_platten_floor,
        docp_geom1_vertikale_alle=docp_roh_vertikale_alle,
        # geom2 = trw
        geom2_geschoss_guid=trw_geschoss_guid,
        docn_geom2_platten_floor=docn_trw_platten_floor,
        docp_geom2_platten_floor=docp_trw_platten_floor,
    )

    create_smart_intersection_objects_model_for_platten(
        workpath=workpath,
        diff_floor=diff_floor,
        # geom1 = trw
        geom1_name=trw_name,
        geom1_geschoss_guid=trw_geschoss_guid,
        geom1_ge_zweit_guid=trw_ge_zweit_guid,
        docn_geom1_vertikale_alle=docn_trw_vertikale_alle,
        docp_geom1_platten_floor=docp_trw_platten_floor,
        docp_geom1_vertikale_alle=docp_trw_vertikale_alle,
        # geom2 = roh
        geom2_geschoss_guid=roh_geschoss_guid,
        docn_geom2_platten_floor=docn_roh_platten_floor,
        docp_geom2_platten_floor=docp_roh_platten_floor,
    )


def get_smart_intersection_data_vertikale(
    workpath,
    diff_floor,
    roh_geschoss_guid=None,
    roh_ge_zweit_guid=None,
    trw_geschoss_guid=None,
    trw_ge_zweit_guid=None,
):

    print("workpath: {}".format(workpath))
    print("diff_floor: {}".format(diff_floor))
    # print("roh_geschoss_guid: {}".format(roh_geschoss_guid))
    # print("roh_ge_zweit_guid: {}".format(roh_ge_zweit_guid))
    # print("trw_geschoss_guid: {}".format(trw_geschoss_guid))
    # print("trw_ge_zweit_guid: {}".format(trw_ge_zweit_guid))

    # inits
    roh_name = "ARC_ROH"
    trw_name = "ING_TRW"

    # ********************************************************************************************
    # smart walls
    # file names
    docn_roh_platten_alle = "Alle_Plattenbauteile_" + roh_name
    docn_roh_vertikale_floor = diff_floor + "_Vertikalbauteile_" + roh_name
    docn_trw_platten_alle = "Alle_Plattenbauteile_" + trw_name
    docn_trw_vertikale_floor = diff_floor + "_Vertikalbauteile_" + trw_name
    # file paths
    docp_roh_platten_alle = join(workpath, docn_roh_platten_alle + ".FCStd")
    docp_roh_vertikale_floor = join(workpath, docn_roh_vertikale_floor + ".FCStd")
    docp_trw_platten_alle = join(workpath, docn_trw_platten_alle + ".FCStd")
    docp_trw_vertikale_floor = join(workpath, docn_trw_vertikale_floor + ".FCStd")
    print(docp_roh_platten_alle)
    print(docp_roh_vertikale_floor)
    print(docp_trw_platten_alle)
    print(docp_trw_vertikale_floor)
    if (
        os.path.isfile(docp_roh_platten_alle) is False
        or os.path.isfile(docp_roh_vertikale_floor) is False
        or os.path.isfile(docp_trw_platten_alle) is False
        or os.path.isfile(docp_trw_vertikale_floor) is False
    ):
        print("FCStd File missing in workingdir: {}".format(workpath))
        return

    create_smart_intersection_objects_model_for_vertikale(
        workpath=workpath,
        diff_floor=diff_floor,
        # geom1 = roh
        geom1_name=roh_name,
        geom1_geschoss_guid=roh_geschoss_guid,
        geom1_ge_zweit_guid=roh_ge_zweit_guid,
        docn_geom1_platten_alle=docn_roh_platten_alle,
        docp_geom1_vertikale_floor=docp_roh_vertikale_floor,
        docp_geom1_platten_alle=docp_roh_platten_alle,
        # geom2 = trw
        geom2_geschoss_guid=trw_geschoss_guid,
        docn_geom2_vertikale_floor=docn_trw_vertikale_floor,
        docp_geom2_vertikale_floor=docp_trw_vertikale_floor,
    )

    create_smart_intersection_objects_model_for_vertikale(
        workpath=workpath,
        diff_floor=diff_floor,
        # geom1 = trw
        geom1_name=trw_name,
        geom1_geschoss_guid=trw_geschoss_guid,
        geom1_ge_zweit_guid=trw_ge_zweit_guid,
        docn_geom1_platten_alle=docn_trw_platten_alle,
        docp_geom1_vertikale_floor=docp_trw_vertikale_floor,
        docp_geom1_platten_alle=docp_trw_platten_alle,
        # geom2 = roh
        geom2_geschoss_guid=roh_geschoss_guid,
        docn_geom2_vertikale_floor=docn_roh_vertikale_floor,
        docp_geom2_vertikale_floor=docp_roh_vertikale_floor,
    )


# ************************************************************************************************
# ************************************************************************************************
# ************************************************************************************************
def create_smart_intersection_objects_model_for_platten(
    workpath,
    diff_floor,
    #
    geom1_name,
    geom1_geschoss_guid,
    geom1_ge_zweit_guid,
    docn_geom1_vertikale_alle,
    docp_geom1_platten_floor,
    docp_geom1_vertikale_alle,
    #
    geom2_geschoss_guid,
    docn_geom2_platten_floor,
    docp_geom2_platten_floor,
):

    # ************************************************************************************************
    # GEOM1 Vertikalbauteile mit GEOM2 Plattenbauteile xyz fuer Smart GEOM1 Platten im Geschoss xyz
    # ************************************************************************************************
    docn_inter_geom1vt_geom2pl = diff_floor + "_Plattenbauteile_Intersections_fuer_" + geom1_name
    # print(docn_inter_geom1vt_geom2pl)
    docp_inter_geom1vt_geom2pl = join(workpath, docn_inter_geom1vt_geom2pl + ".FCStd")
    doco_inter_geom1vt_geom2pl = App.open(docp_geom1_vertikale_alle)
    doco_inter_geom1vt_geom2pl.saveAs(docp_inter_geom1vt_geom2pl)

    # merge GEOM1 Plattenbauteile xyz
    doco_inter_geom1vt_geom2pl.mergeProject(docp_geom2_platten_floor)
    # import ifc ware wohl schneller als der merge
    doco_inter_geom1vt_geom2pl.recompute()
    # remove leere geschosse
    smartdatadiff_tools.remove_empty_grps(doco_inter_geom1vt_geom2pl)
    doco_inter_geom1vt_geom2pl.save()

    # get the geschoss objects
    grp_geom1_vt_geschoss = smartdatadiff_tools.get_archobj_by_guid(doco_inter_geom1vt_geom2pl, geom1_geschoss_guid)
    grp_geom1_vt_zweit = smartdatadiff_tools.get_archobj_by_guid(doco_inter_geom1vt_geom2pl, geom1_ge_zweit_guid)
    grp_geom2_platten = smartdatadiff_tools.get_archobj_by_guid(doco_inter_geom1vt_geom2pl, geom2_geschoss_guid)

    # calcultate the intersections
    grpinter_geom1_geschoss = smartdatadiff_tools.calculate_group_intersection(
        doco_inter_geom1vt_geom2pl,
        grp_geom2_platten,
        grp_geom1_vt_geschoss,
        "Intersections_Geschoss"
    )
    grpinter_geom1_zweit = smartdatadiff_tools.calculate_group_intersection(
        doco_inter_geom1vt_geom2pl,
        grp_geom2_platten,
        grp_geom1_vt_zweit,
        "Intersections_Zweit"
    )

    # export groups to ifc
    building = makeBuilding([grpinter_geom1_geschoss, grpinter_geom1_zweit])
    ifcfilep_inter_geom1vt_geom2pl = join(workpath, docn_inter_geom1vt_geom2pl + ".ifc")
    smartdatadiff_tools.export_to_ifc(building, ifcfilep_inter_geom1vt_geom2pl)
    doco_inter_geom1vt_geom2pl.save()
    App.closeDocument(doco_inter_geom1vt_geom2pl.Name)

    # merge ifcfiles
    docn_geom1_smart_platten = diff_floor + "_Plattenbauteile_Smart_" + geom1_name
    smartdatadiff_tools.merge_intersection_storeys_into_base_data(
        ifcfile_p=os.path.splitext(docp_geom1_platten_floor)[0] + ".ifc",
        mergefile_p=ifcfilep_inter_geom1vt_geom2pl,
        outfile_p=join(workpath, docn_geom1_smart_platten + ".ifc")
    )

    """
    # create smart GEOM1 Plattenbauteile Geschoss xyz from GEOM1 Platten im Geschoss xyz
    docn_geom1_smart_platten = diff_floor + "_Plattenbauteile_Smart_" + geom1_name
    docp_geom1_smart_platten = join(workpath, docn_geom1_smart_platten + ".FCStd")
    doco_geom1_smart_platten = App.open(docp_geom1_platten_floor)
    doco_geom1_smart_platten.saveAs(docp_geom1_smart_platten)
    # remove leere geschosse
    smartdatadiff_tools.remove_empty_grps(doco_geom1_smart_platten)
    doco_geom1_smart_platten.save()

    # copy intersections
    # https://forum.freecadweb.org/viewtopic.php?style=4&p=608750#p608750
    # copy = target_doc.copyObject(source_doc.getObject('aBox'), True)
    # TODO copiere ueber identifier der returned wurde anstatt oder mit doc
    inter_copy_geom1_geschoss = doco_geom1_smart_platten.copyObject(grpinter_geom1_geschoss, True)
    inter_copy_geom1_zweit = doco_geom1_smart_platten.copyObject(grpinter_geom1_zweit, True)
    main_site_grp_geom1 = doco_geom1_smart_platten.getObject("Group")  # haaaack
    if inter_copy_geom1_geschoss:
        main_site_grp_geom1.addObject(inter_copy_geom1_geschoss)
    if inter_copy_geom1_zweit:
        main_site_grp_geom1.addObject(inter_copy_geom1_zweit)
    # TODO move materials too
    doco_geom1_smart_platten.recompute()
    doco_geom1_smart_platten.save()
    # export ifc
    export_geom1_ifcfile = join(workpath, docn_geom1_smart_platten + ".ifc")
    smartdatadiff_tools.export_to_ifc(main_site_grp_geom1, export_geom1_ifcfile)

    # close open Documents
    App.closeDocument(doco_inter_geom1vt_geom2pl.Name)
    App.closeDocument(doco_geom1_smart_platten.Name)
    """

# ************************************************************************************************
# ************************************************************************************************
# ************************************************************************************************
def create_smart_intersection_objects_model_for_vertikale(
    workpath,
    diff_floor,
    #
    geom1_name,
    geom1_geschoss_guid,
    geom1_ge_zweit_guid,
    docn_geom1_platten_alle,
    docp_geom1_vertikale_floor,
    docp_geom1_platten_alle,
    #
    geom2_geschoss_guid,
    docn_geom2_vertikale_floor,
    docp_geom2_vertikale_floor,
):

    # ************************************************************************************************
    # GEOM1 Plattenbauteile mit GEOM2 Vertikalbauteile xyz fuer Smart GEOM1 Vertikale im Geschoss xyz
    # ************************************************************************************************
    docn_inter_geom1pl_geom2vt = diff_floor + "_Vertikalbauteile_Intersections_fuer_" + geom1_name
    # print(docn_inter_geom1pl_geom2vt)
    docp_inter_geom1pl_geom2vt = join(workpath, docn_inter_geom1pl_geom2vt + ".FCStd")
    doco_inter_geom1pl_geom2vt = App.open(docp_geom1_platten_alle)
    doco_inter_geom1pl_geom2vt.saveAs(docp_inter_geom1pl_geom2vt)

    # merge GEOM1 Vertikalbauteile xyz
    doco_inter_geom1pl_geom2vt.mergeProject(docp_geom2_vertikale_floor)
    # import ifc ware wohl schneller als der merge
    doco_inter_geom1pl_geom2vt.recompute()
    # remove leere geschosse
    smartdatadiff_tools.remove_empty_grps(doco_inter_geom1pl_geom2vt)
    doco_inter_geom1pl_geom2vt.save()

    # get the geschoss objects
    grp_geom1_pl_geschoss = smartdatadiff_tools.get_archobj_by_guid(doco_inter_geom1pl_geom2vt, geom1_geschoss_guid)
    grp_geom1_pl_zweit = smartdatadiff_tools.get_archobj_by_guid(doco_inter_geom1pl_geom2vt, geom1_ge_zweit_guid)
    grp_geom2_vertikale = smartdatadiff_tools.get_archobj_by_guid(doco_inter_geom1pl_geom2vt, geom2_geschoss_guid)

    # calcultate the intersections
    grpinter_geom1_geschoss = smartdatadiff_tools.calculate_group_intersection(
        doco_inter_geom1pl_geom2vt,
        grp_geom2_vertikale,
        grp_geom1_pl_geschoss,
        "Intersections_Geschoss"
    )
    grpinter_geom1_zweit = smartdatadiff_tools.calculate_group_intersection(
        doco_inter_geom1pl_geom2vt,
        grp_geom2_vertikale,
        grp_geom1_pl_zweit,
        "Intersections_Zweit"
    )

    # export groups to ifc
    building = makeBuilding([grpinter_geom1_geschoss, grpinter_geom1_zweit])
    ifcfilep_inter_geom1pl_geom2vt = join(workpath, docn_inter_geom1pl_geom2vt + ".ifc")
    smartdatadiff_tools.export_to_ifc(building, ifcfilep_inter_geom1pl_geom2vt)
    doco_inter_geom1pl_geom2vt.save()
    App.closeDocument(doco_inter_geom1pl_geom2vt.Name)

    # merge ifcfiles
    docn_geom1_smart_vertikale = diff_floor + "_Vertikalbauteile_Smart_" + geom1_name
    smartdatadiff_tools.merge_intersection_storeys_into_base_data(
        ifcfile_p=os.path.splitext(docp_geom1_vertikale_floor)[0] + ".ifc",
        mergefile_p=ifcfilep_inter_geom1pl_geom2vt,
        outfile_p=join(workpath, docn_geom1_smart_vertikale + ".ifc")
    )

    """
    # create smart GEOM1 Vertikalbauteile Geschoss xyz from GEOM1 Platten im Geschoss xyz
    docn_geom1_smart_vertikale = diff_floor + "_Vertikalbauteile_Smart_" + geom1_name
    docp_geom1_smart_vertikale = join(workpath, docn_geom1_smart_vertikale + ".FCStd")
    doco_geom1_smart_vertikale = App.open(docp_geom1_vertikale_floor)
    doco_geom1_smart_vertikale.saveAs(docp_geom1_smart_vertikale)
    # remove leere geschosse
    smartdatadiff_tools.remove_empty_grps(doco_geom1_smart_vertikale)
    doco_geom1_smart_vertikale.save()

    # copy intersections
    # https://forum.freecadweb.org/viewtopic.php?style=4&p=608750#p608750
    # copy = target_doc.copyObject(source_doc.getObject('aBox'), True)
    # TODO copiere ueber identifier der returned wurde anstatt oder mit doc
    inter_copy_geom1_geschoss = doco_geom1_smart_vertikale.copyObject(grpinter_geom1_geschoss, True)
    inter_copy_geom1_zweit = doco_geom1_smart_vertikale.copyObject(grpinter_geom1_zweit, True)
    main_site_grp_geom1 = doco_geom1_smart_vertikale.getObject("Group")  # haaaack
    if inter_copy_geom1_geschoss:
        main_site_grp_geom1.addObject(inter_copy_geom1_geschoss)
    if inter_copy_geom1_zweit:
        main_site_grp_geom1.addObject(inter_copy_geom1_zweit)
    # TODO move materials too
    doco_geom1_smart_vertikale.recompute()
    doco_geom1_smart_vertikale.save()
    # export ifc
    export_geom1_ifcfile = join(workpath, docn_geom1_smart_vertikale + ".ifc")
    smartdatadiff_tools.export_to_ifc(main_site_grp_geom1, export_geom1_ifcfile)

    # close open Documents
    App.closeDocument(doco_inter_geom1pl_geom2vt.Name)
    App.closeDocument(doco_geom1_smart_vertikale.Name)
    """

"""
    # zuordnungen, spaeter parameteraufruf
    geom1_geschoss_guid = roh_geschoss_guid
    geom1_ge_zweit_guid = roh_ge_zweit_guid
    geom2_geschoss_guid = trw_geschoss_guid
    geom2_ge_zweit_guid = trw_ge_zweit_guid
    geom1_name = roh_name
    geom2_name = trw_name
    docn_geom1_platten = docn_roh_platten_floor
    docn_geom1_vertikale_alle = docn_roh_vertikale_alle
    docn_geom2_platten_floor = docn_trw_platten_floor
    docn_geom2_vertikale = docn_trw_vertikale_alle
    docp_geom1_platten_floor = docp_roh_platten_floor
    docp_geom1_vertikale_alle = docp_roh_vertikale_alle
    docp_geom2_platten_floor = docp_trw_platten_floor
    docp_geom2_vertikale = docp_trw_vertikale_alle


    # ************************************************************************************************
    # GEOM1 Vertikalbauteile mit GEOM2 Plattenbauteile xyz fuer Smart GEOM1 Platten im Geschoss xyz
    # ************************************************************************************************
    docn_inter_geom1vt_geom2pl = diff_floor + "_Intersections__" + docn_geom1_vertikale_alle + "__" + docn_geom2_platten_floor
    # print(docn_inter_geom1vt_geom2pl)
    docp_inter_geom1vt_geom2pl = join(workpath, docn_inter_geom1vt_geom2pl + ".FCStd")
    doco_inter_geom1vt_geom2pl = App.open(docp_geom1_vertikale_alle)
    doco_inter_geom1vt_geom2pl.saveAs(docp_inter_geom1vt_geom2pl)

    # merge GEOM1 Plattenbauteile xyz
    doco_inter_geom1vt_geom2pl.mergeProject(docp_geom2_platten_floor)
    # import ifc ware wohl schneller als der merge
    doco_inter_geom1vt_geom2pl.recompute()
    # remove leere geschosse
    smartdatadiff_tools.remove_empty_grps(doco_inter_geom1vt_geom2pl)
    doco_inter_geom1vt_geom2pl.save()

    # get the geschoss objects
    grp_geom1_vt_geschoss = smartdatadiff_tools.get_archobj_by_guid(doco_inter_geom1vt_geom2pl, geom1_geschoss_guid)
    grp_geom1_vt_zweit = smartdatadiff_tools.get_archobj_by_guid(doco_inter_geom1vt_geom2pl, geom1_ge_zweit_guid)
    grp_geom2_platten = smartdatadiff_tools.get_archobj_by_guid(doco_inter_geom1vt_geom2pl, geom2_geschoss_guid)

    # calcultate the intersections
    grpinter_geom1_geschoss = smartdatadiff_tools.calculate_group_intersection(doco_inter_geom1vt_geom2pl, grp_geom2_platten, grp_geom1_vt_geschoss, "Intersections_Oben")
    grpinter_geom1_zweit = smartdatadiff_tools.calculate_group_intersection(doco_inter_geom1vt_geom2pl, grp_geom2_platten, grp_geom1_vt_zweit, "Intersections_Unten")
    # doc nicht schliessen, wir wollen noch objekte kopieren
    doco_inter_geom1vt_geom2pl.save()  # nochmal, ich weiss

    # create smart GEOM1 Plattenbauteile Geschoss xyz from GEOM1 Platten im Geschoss xyz
    docn_geom1_smart_platten = diff_floor + "_Smart_Plattenbauteile_" + geom1_name
    docp_geom1_smart_platten = join(workpath, docn_geom1_smart_platten + ".FCStd")
    doco_geom1_smart_platten = App.open(docp_geom1_platten_floor)
    doco_geom1_smart_platten.saveAs(docp_geom1_smart_platten)
    # remove leere geschosse
    smartdatadiff_tools.remove_empty_grps(doco_geom1_smart_platten)
    doco_geom1_smart_platten.save()

    # copy intersections
    # https://forum.freecadweb.org/viewtopic.php?style=4&p=608750#p608750
    # copy = target_doc.copyObject(source_doc.getObject('aBox'), True)
    # TODO copiere ueber identifier der returned wurde anstatt oder mit doc
    inter_copy_geom1_geschoss = doco_geom1_smart_platten.copyObject(grpinter_geom1_geschoss, True)
    inter_copy_geom1_zweit = doco_geom1_smart_platten.copyObject(grpinter_geom1_zweit, True)
    main_site_grp_geom1 = doco_geom1_smart_platten.getObject("Group")  # haaaack
    if inter_copy_geom1_geschoss:
        main_site_grp_geom1.addObject(inter_copy_geom1_geschoss)
    if inter_copy_geom1_zweit:
        main_site_grp_geom1.addObject(inter_copy_geom1_zweit)
    # TODO move materials too
    doco_geom1_smart_platten.recompute()
    doco_geom1_smart_platten.save()
    # export ifc
    export_geom1_ifcfile = join(workpath, docn_geom1_smart_platten + ".ifc")
    smartdatadiff_tools.export_to_ifc(main_site_grp_geom1, export_geom1_ifcfile)

    # close open Documents
    App.closeDocument(doco_inter_geom1vt_geom2pl.Name)
    App.closeDocument(doco_geom1_smart_platten.Name)

    # ************************************************************************************************
    # GEOM2 Vertikalbauteile mit GEOM1 Plattenbauteile xyz fuer Smart GEOM2 Platten im Geschoss xyz
    # ************************************************************************************************
    docn_inter_geom2vt_geom1pl = diff_floor + "_Intersections__" + docn_geom2_vertikale + "__" + docn_geom1_platten
    # print(docn_inter_geom2vt_geom1pl)
    docp_inter_geom2vt_geom1pl = join(workpath, docn_inter_geom2vt_geom1pl + ".FCStd")
    doco_inter_geom2vt_geom1pl = App.open(docp_geom2_vertikale)
    doco_inter_geom2vt_geom1pl.saveAs(docp_inter_geom2vt_geom1pl)

    # merge GEOM1 Plattenbauteile xyz
    doco_inter_geom2vt_geom1pl.mergeProject(docp_geom1_platten_floor)
    # import ifc ware wohl schneller als der merge
    doco_inter_geom2vt_geom1pl.recompute()
    # remove leere geschosse
    smartdatadiff_tools.remove_empty_grps(doco_inter_geom2vt_geom1pl)
    doco_inter_geom2vt_geom1pl.save()

    # get the geschoss objects
    grp_geom2_vt_geschoss = smartdatadiff_tools.get_archobj_by_guid(doco_inter_geom2vt_geom1pl, geom2_geschoss_guid)
    grp_geom2_vt_zweit = smartdatadiff_tools.get_archobj_by_guid(doco_inter_geom2vt_geom1pl, geom2_ge_zweit_guid)
    grp_geom1_platten = smartdatadiff_tools.get_archobj_by_guid(doco_inter_geom2vt_geom1pl, geom1_geschoss_guid)

    # calcultate the intersections
    grpinter_geom2_geschoss = smartdatadiff_tools.calculate_group_intersection(doco_inter_geom2vt_geom1pl, grp_geom1_platten, grp_geom2_vt_geschoss, "Intersections_Oben")
    grpinter_geom2_zweit = smartdatadiff_tools.calculate_group_intersection(doco_inter_geom2vt_geom1pl, grp_geom1_platten, grp_geom2_vt_zweit, "Intersections_Oben")
    # doc nicht schliessen, wir wollen noch objekte kopieren
    doco_inter_geom2vt_geom1pl.save()  # nochmal, ich weiss

    # create smart GEOM2 Plattenbauteile Geschoss xyz from GEOM2 Platten im Geschoss xyz
    docn_geom2_smart_platten = diff_floor + "_Smart_Plattenbauteile_" + geom2_name
    docp_geom2_smart_platten = join(workpath, docn_geom2_smart_platten + ".FCStd")
    doco_geom2_smart_platten = App.open(docp_geom2_platten_floor)
    doco_geom2_smart_platten.saveAs(docp_geom2_smart_platten)
    # remove leere geschosse
    smartdatadiff_tools.remove_empty_grps(doco_geom2_smart_platten)
    doco_geom2_smart_platten.save()

    # copy intersections
    # https://forum.freecadweb.org/viewtopic.php?style=4&p=608750#p608750
    # copy = target_doc.copyObject(source_doc.getObject('aBox'), True)
    # TODO copiere ueber identifier der returned wurde anstatt oder mit doc
    inter_copy_geom2_geschoss = doco_geom2_smart_platten.copyObject(grpinter_geom2_geschoss, True)
    inter_copy_geom2_zweit = doco_geom2_smart_platten.copyObject(grpinter_geom2_zweit, True)
    main_site_grp_geom2 = doco_geom2_smart_platten.getObject("Group")  # haaaack
    if inter_copy_geom2_geschoss:
        main_site_grp_geom2.addObject(inter_copy_geom2_geschoss)
    if inter_copy_geom2_zweit:
        main_site_grp_geom2.addObject(inter_copy_geom2_zweit)
    # TODO move materials too
    doco_geom2_smart_platten.recompute()
    doco_geom2_smart_platten.save()
    # export ifc
    export_geom2_ifcfile = join(workpath, docn_geom2_smart_platten + ".ifc")
    smartdatadiff_tools.export_to_ifc(main_site_grp_geom2, export_geom2_ifcfile)

    # close open Documents
    App.closeDocument(doco_inter_geom2vt_geom1pl.Name)
    App.closeDocument(doco_geom2_smart_platten.Name)
"""

import FreeCAD as App
import Arch


# ************************************************************************************************
def get_archobj_by_guid(doc, target_guid):

    if target_guid is None:
        print("GUID None will never be found ;-).")
        return None

    # returns the first obj found
    for obj in doc.Objects:
        if (
            hasattr(obj, "GlobalId")
            and obj.GlobalId == target_guid
        ):
            print(obj.Name, obj.GlobalId)
            return obj
    else:
        print("Not found objekt with GUID: {}".format(target_guid))
        return None

# ************************************************************************************************
def calculate_group_intersection(doc, grpbpl, grpwae, inter_grp_name="InterGroup"):

    # es wird nicht hinabgestiegen, group in group wie building part
    # todo pruefen

    doc.recompute()

    # to be sure the Arch obj is added to corrct document
    App.ActiveDocument = doc
    grpinter = Arch.makeFloor(name=inter_grp_name)
    doc.recompute()

    if grpbpl is None or grpwae is None:
        # create a group in any case
        return grpinter

    for obpl in grpbpl.Group:
        # Part.show(obpl.Shape)
        for owae in grpwae.Group:
            # Part.show(owae.Shape)
            intersh = None
            try:
                # intersh = obpl.Shape.common(owae.Shape, 0.4)  # TEEEEEEEST
                intersh = obpl.Shape.common(owae.Shape)
            except Exception:
                print("error on intersection")
            if (
                intersh.isNull() is False
                and intersh.isValid() is True  # wenn an einer nullshape isValid() ausgefuehrt wird ... abbruch ohne catch !!!
                and len(intersh.Vertexes) > 0
                and intersh.Volume > 0
            ):
                # print(len(intersh.Vertexes))
                # Part.show(intersh)
                interobj = Arch.makeComponent(name="Inter")
                interobj.Shape = intersh
                interobj.Label = owae.Label
                interobj.IfcType = owae.IfcType
                interobj.ObjectType = owae.ObjectType
                #interobj.Material = owae.Material
                interobj.ViewObject.ShapeColor = owae.ViewObject.ShapeColor
                # add to intergroup
                grpinter.addObject(interobj)
                doc.recompute()
                print("Found Intersection shape. Arch Component added.")
            else:
                pass
                # print("No Shape")
            # break
        # break
    doc.recompute()
    # doc.save()
    return grpinter


# ************************************************************************************************
def export_to_ifc(groupobj_to_export, export_ifcfile):

    exportprefs = {
        'DEBUG': True,
        'CREATE_CLONES': False,
        'FORCE_BREP': True,
        'STORE_UID': True,
        'SERIALIZE': False,
        'EXPORT_2D': True,
        'FULL_PARAMETRIC': False,
        'ADD_DEFAULT_SITE': True,
        'ADD_DEFAULT_BUILDING': True,
        'ADD_DEFAULT_STOREY': True,
        'IFC_UNIT': 'metre',
        'SCALE_FACTOR': 0.001,
        'GET_STANDARD': False,
        'EXPORT_MODEL': 'arch',
        'SCHEMA': 'IFC2X3'
    }
    import exportIFC
    exportIFC.export([groupobj_to_export], export_ifcfile, "", preferences=exportprefs)


# ************************************************************************************************
def remove_empty_grps(doc):

    # remove leere groups
    for obj in doc.Objects:
        if (
            hasattr(obj, "Group")
            and len(obj.Group) == 0
        ):
            doc.removeObject(obj.Name)
            doc.recompute()
    return True


# ************************************************************************************************
def merge_intersection_storeys_into_base_data(ifcfile_p, mergefile_p, outfile_p):

    import ifcopenshell

    ifcfile_o = ifcopenshell.open(ifcfile_p)
    mergefile_o = ifcopenshell.open(mergefile_p)

    # nimm die geschosse aus mergefile_o und fuege sie mit children zu ifcfile_o hinzu
    # erstelle link der geschosse zu dem ersten gebaeude
    new_s_objs = []
    for s_obj in mergefile_o.by_type("IfcBuildingStorey"):
        if s_obj.ContainsElements:
            s_rel = s_obj.ContainsElements[0]
            new_s_rel = ifcfile_o.add(s_rel)
            new_s_obj = new_s_rel.RelatingStructure
            new_s_objs.append(new_s_obj)
        else:
            # leeres Geschoss, wenn keine intersections
            new_s_obj = ifcfile_o.add(s_obj)
            new_s_objs.append(new_s_obj)

    ifcfile_o.createIfcRelAggregates(
        ifcopenshell.guid.new(),
        ifcfile_o.by_type("IfcOwnerHistory")[0],
        'building container for intersection storeys',
        '',
        ifcfile_o.by_type("IfcBuilding")[0],
        new_s_objs
    )

    ifcfile_o.write(outfile_p)
    return True


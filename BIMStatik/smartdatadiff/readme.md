### Ablauf
+ 1. serialize
    + Output:
        + *_ROH__Vertikalbauteile_Alle.ifc
        + *_ROH__Plattenbauteile_Gesch.ifc
        + *_TRW__Vertikalbauteile_Alle.ifc
        + *_TRW__Plattenbauteile_Gesch.ifc
        + zu allen auch *.FCStd
    + aus TRW und ROH komplett
    + Immer nur Ortbeton, heisst alles nicht Ortbeton entfernen
        + einfach bei TRW
        + im ROH sind die Fertigteile auch Beton
    + Vertikalbauteile
        + Alle NichtPlatten
    + Platten
        + Je Geschoss Plattenbauteile
        + bei BPL mit Liftschacht komplett
+ 2. Intersection
    + allen output oben in eine Verzeichnis
    + alle operationen an FC documenten
    + evtl ein monsterdoc und immer save as
    +
    + doc1
        + open *_ROHM01_Vertikalbauteile_Alle
        + merge *_TRW_Plattenbauteile_Gesch
        + Intersections
        + save doc1
        + close doc1
    + doc2
        + open *_ROHM01_Plattenbauteile_Gesch
        + copy Intersections documentuebergreifend
        + save doc2
        + export ifc
    + doc3
        + ...
    + doc4
        + ...
    + dann ist 3. schon erledigt :-)
+ 3. smart diff data
    + Output:
        + *_ROHM01_SmartPlattenbauteile_Gesch.ifc
        + *_TRW_SmartPlattenbauteile_Gesch.ifc
        + und *.FCStd
+ 4. smart diff result
    + mit den Daten aus 3. standard BHA diff durchfuehren
    + ifc ins Diff kopieren, run starten


### Idee
+ idee Vergleich
    + Der Vergleich mit den abgeschnittenen Waenden
    + Alles ausschalten
    + Minterbauteile TRW Decken einschalten
    + Decken TRW einschalten
    + spielen
    + Alles ausschalten
    + Minderbauteile ROH Decken einschalten
    + Decken ROH einschalten
    + spielen
    + evtl. Waende gar nicht aus FC exportieren
+ Richtig Smart
    + Intersection TRW BPL und ROH Waende. Dies zu ROH BPL hinzufuegen
    + Intersection TRW Waende und ROH BPL. Dies zu TRW BPL hinzufuegen
    + nun nur BPL vergleichen
    + dann sind unterschiedliche Modellierungen egalisiert
    + Vor alles diese intersection sollten keine Fehler bringen, da "wirkliche" Intersection
+
+ obiges koennte als Monsterkoerper (alle vereinigen) und dann diff funktionieren
    + der ist nicht sooo monstroes
+
+ zwei Deckenvergleiche Decke UG
    + nur Decken
    + Decken mit Waende UG und EG abgeschnittenen, Wanddiffs nicht in Vergleich exportiert


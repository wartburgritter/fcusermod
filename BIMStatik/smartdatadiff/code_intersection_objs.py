#
# box.common(shape2, tolerance=0.4)

import Arch
doc = App.ActiveDocument
doc.recompute()
grpbpl = doc.BuildingPart
grpwae = doc.BuildingPart009
#
App.ActiveDocument = doc
#
grpinter = Arch.makeFloor(name="InterGroup")
for obpl in grpbpl.Group:
    # Part.show(obpl.Shape)
    for owae in grpwae.Group:
        # Part.show(owae.Shape)
        intersh = None
        #intersh = obpl.Shape.common(owae.Shape, 0.4)
        try:
            intersh = obpl.Shape.common(owae.Shape)
        except Exception:
            print("error on intersection")
        if (
            intersh.isValid() is True
            and intersh.isNull() is False
            and len(intersh.Vertexes) > 0
            and intersh.Volume > 0
        ):
            # print(len(intersh.Vertexes))
            # Part.show(intersh)
            interobj = Arch.makeComponent(name="Inter")
            interobj.Shape = intersh
            interobj.Label = owae.Label
            interobj.IfcType = owae.IfcType
            interobj.ObjectType = owae.ObjectType
            interobj.Material = owae.Material
            interobj.ViewObject.ShapeColor = owae.ViewObject.ShapeColor
            # add to intergroup
            grpinter.addObject(interobj)
            doc.recompute()
            print("Found Intersection shape. Arch Component added.")
        else:
            pass
            # print("No Shape")
        # break
    # break

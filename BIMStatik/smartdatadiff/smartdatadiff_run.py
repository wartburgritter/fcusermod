"""

# load project data first, see lines to copy in prj data module

import importlib
import smartdatadiff.smartdatadiff_run as runsmdiffdata
importlib.reload(runsmdiffdata)


runsmdiffdata.ifc2fcstd()
runsmdiffdata.run_all(prjobj)


# rezu alle geschosse in liste, auch zum rauskopieren
runsmdiffdata.run_all(prjobj, geschosse = ["01_UG2", "02_UG1", "11_EG", "21_OG1", "22_OG2", "23_AG3", "24_DA4"])


# ein gesamtes geschoss
# div geschossnamen, je nach bedarf anpassen
runsmdiffdata.run_all(prjobj, geschosse = "01_UG1")
runsmdiffdata.run_all(prjobj, geschosse = "23_AG3")
runsmdiffdata.run_all(prjobj, geschosse = "24_DA4")


# nur vertikale oder platten fuer ein geschoss
# div geschossnamen, je nach bedarf anpassen
runsmdiffdata.run_vertikale(prjobj, "01_UG")
runsmdiffdata.run_platten(prjobj, "01_UG")
runsmdiffdata.run_platten(prjobj, "11_EG")

"""


# nur step 2, get the smart data for bimgeomdiff


# hinweis:
# es gibt einen smart vertikalkoerper der innerhalb der decke liegt,
# wenn im modell fehler existieren, bei denen vertikalkoerper in die decke reinreichen
#
# TODO fix: FC generiert anscheind fuer export neue GUIDs fuer Stockwerke
# aber irgendwie nur beim Serializer
#
# TODO: evtl. alle dateien *.FCStd1 und *.FCStd2 leoschen
# manuell nach Typ sortieren markieren delete sind 2 sekunden ...


import importlib
import os
from os.path import join

import FreeCAD as App

import smartdatadiff.smartdatadiff_get_data as getsmartdatadiffdata
importlib.reload(getsmartdatadiffdata)

import some_ifc_tools.serializer.ifcos_serializer_tools as sertools
importlib.reload(sertools)

# std_workpath = join("C:", os.sep, "0_BHA_privat", "BIMGeomSmartDataDiff")
std_workpath = join("C:", os.sep, "0_BHA_privat", "BIMGeomAllSerializerSmartDataDiff")


def ifc2fcstd(workpath=std_workpath):

    # load all ifc into FreeCAD and save them as FCStd
    for filen in os.listdir(workpath):
        if os.path.splitext(filen)[1] != ".ifc":
            continue
        ifc_filep = join(workpath, filen)
        fcstd_filep = join(workpath, os.path.splitext(filen)[0] + ".FCStd")
        print(ifc_filep)
        # flush ...
        if os.path.isfile(fcstd_filep):
            print(fcstd_filep)
            continue
        doc = sertools.import_in_fc_with_skip_and_cut(ifc_filep)
        doc.saveAs(fcstd_filep)
        App.closeDocument(doc.Name)

    return True


# spatial_data darf nicht None sein, aber da es hinten steht braucht es eine initwert
def run_all(prjdata, workpath=std_workpath, geschosse=None):

    spatial_data = prjdata.SpatialData

    if geschosse is None:
        geschosse = [geschoss for geschoss in spatial_data.keys() if geschoss is not None]
    elif isinstance(geschosse, str):
        geschosse = [geschosse]
    print(geschosse)

    oberstes_geschoss = list(spatial_data.keys())[-2]  # wirklich -2 und nicht -1 ...
    unterstes_geschoss = list(spatial_data.keys())[0]

    for geschoss in geschosse:
        if geschoss is not None:

            if geschoss != unterstes_geschoss:
                run_platten(prjdata, geschoss, workpath)
            else:
                print(
                    "Unterstes Geschosse hat kein Geschoss darunter: {}"
                    .format(unterstes_geschoss)
                )

            # das oberste geschoss vertikal gibt es nicht, da nur platten
            if geschoss != oberstes_geschoss:
                run_vertikale(prjdata, geschoss, workpath)
            else:
                print(
                    "Oberstes Geschosse hat keine Vertikale (ignorieren): {}"
                    .format(oberstes_geschoss)
                )

    return True


def run_vertikale(prjdata, geschoss, workpath=std_workpath):

    # intersection vertikale mit dem selben geschoss platten und dem geschoss darueber platten
    print("Vertikale geschoss: {}".format(geschoss))

    spatial_data = prjdata.SpatialData

    ge_zweit = spatial_data[geschoss]["darueber"]
    getsmartdatadiffdata.get_smart_intersection_data_vertikale(
        workpath = workpath,
        diff_floor = geschoss,
        roh_geschoss_guid = spatial_data[geschoss]["guid_roh"],
        roh_ge_zweit_guid = spatial_data[ge_zweit]["guid_roh"],
        trw_geschoss_guid = spatial_data[geschoss]["guid_trw"],
        trw_ge_zweit_guid = spatial_data[ge_zweit]["guid_trw"],
    )
    return True


def run_platten(prjdata, geschoss, workpath=std_workpath):

    # intersection platten mit dem geschoss darunter vertikale und dem selben geschoss vertikale
    print("Platten geschoss: {}".format(geschoss))

    spatial_data = prjdata.SpatialData
    ge_zweit = spatial_data[geschoss]["darunter"]

    # print(geschoss)
    # print(spatial_data[geschoss])
    # print(ge_zweit)
    # print(spatial_data[ge_zweit])

    getsmartdatadiffdata.get_smart_intersection_data_platten(
        workpath = workpath,
        diff_floor = geschoss,
        roh_geschoss_guid = spatial_data[geschoss]["guid_roh"],
        roh_ge_zweit_guid = spatial_data[ge_zweit]["guid_roh"],
        trw_geschoss_guid = spatial_data[geschoss]["guid_trw"],
        trw_ge_zweit_guid = spatial_data[ge_zweit]["guid_trw"],
    )
    return True

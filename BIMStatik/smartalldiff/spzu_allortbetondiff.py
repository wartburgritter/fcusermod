"""
import importlib
import smartalldiff.spzu_allortbetondiff as spzuallortbetondiff

importlib.reload(spzuallortbetondiff)

spzuallortbetondiff.run()

"""


# VERALTET, da noc auf FC und nicht auf IfcPatch aufbaut


import FreeCADGui as fcgui

import importlib

import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_arc_roh as runspzuroh
import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_ing_trw as runspzutrw
importlib.reload(runspzuroh)
importlib.reload(runspzutrw)

import bimstatiktools.bimgeomdiff_run as bimgeomdiffrun
importlib.reload(bimgeomdiffrun)


the_workpath = "C:/0_BHA_privat/BIMGeomAllSerializerSmartDataDiff"


def run():

    # ser
    runspzuroh.spzu_arc_roh_ortbeton_alles_fuer_rohvergleich_serialise(True, the_workpath)
    fcgui.updateGui()
    runspzutrw.spzu_ing_trw_ortbeton_alles_fuer_rohvergleich_serialise(True, the_workpath)
    fcgui.updateGui()

    # diff
    bimgeomdiffrun.run_bimgeomdiff(
        [
            "Alle_Ortbetonbauteile_fuer_rohvergleich_ING_TRW",
            "Alle_Ortbetonbauteile_fuer_trwvergleich_ARC_ROH",
        ],
        workpath=the_workpath,
    )

    print("ALL ortbeton diff finished SPZU")
    return True

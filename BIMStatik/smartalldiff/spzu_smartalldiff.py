"""
import importlib
import smartalldiff.spzu_smartalldiff as smartdiffspzu

importlib.reload(smartdiffspzu)

smartdiffspzu.run_all()


# Zeilen ALLE aktiveren bei einzelruns !!!
smartdiffspzu.run_og1_platten()

smartdiffspzu.run_eg_vertikale()
smartdiffspzu.run_eg_platten()

smartdiffspzu.run_ug_vertikale()
smartdiffspzu.run_ug_platten()

"""

# es soll auch moeglich sein, nur fuer UG Platten laufen zu lassen
# dann sollten auch nur diejenigen laufen ....
# bloed dabei ich lasse jeweils erst alles laufen und dann weiter
# besser jedes geschoss und fertig, dann uber alle geschosse


import importlib
import os
from os.path import join

import FreeCADGui as fcgui

import some_ifc_tools.serializer.ifcos_ifcpatch_for_smartdiff_prj_spzu_0run as runspzu
importlib.reload(runspzu)

import smartdatadiff.smartdatadiff_run as runsmdiffdata
importlib.reload(runsmdiffdata)

import bimstatiktools.bimgeomdiff_all as runallbimgeomdiff
importlib.reload(runallbimgeomdiff)

import bimstatiktools.bimgeomdiff_run as bimgeomdiffrun
importlib.reload(bimgeomdiffrun)

import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_arc_roh as runspzuroh
import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_ing_trw as runspzutrw
importlib.reload(runspzuroh)
importlib.reload(runspzutrw)


std_workpath = join("C:", os.sep, "0_BHA_privat", "BIMGeomAllSerializerSmartDataDiff")


def run_all():

    # patch
    runspzu.run_all(workpath=std_workpath)
    fcgui.updateGui()

    # convert ifc to FCStd
    runsmdiffdata.ifc2fcstd(std_workpath)
    fcgui.updateGui()

    # get smart data
    runsmdiffdata.run_all(std_workpath)
    fcgui.updateGui()

    # diff
    runallbimgeomdiff.run_all(std_workpath)
    fcgui.updateGui()

    print("ALL diff finished SPZU")
    return True


def run_og1_platten():

    # serialise
    runspzuroh.spzu_arc_roh_ortbeton_eg_decke_serialise(True, the_workpath)
    fcgui.updateGui()
    runspzutrw.spzu_ing_trw_ortbeton_eg_decke_serialise(True, the_workpath)
    fcgui.updateGui()
    #runspzuroh.spzu_arc_roh_ortbeton_alle_vertikale_serialise(True, the_workpath)
    fcgui.updateGui()
    #runspzutrw.spzu_ing_trw_ortbeton_alle_vertikale_serialise(True, the_workpath)
    fcgui.updateGui()

    # get smart diff data
    runsmdiffdata.run_og1_platten(the_workpath)
    fcgui.updateGui()

    # run diff
    runallbimgeomdiff.run_og1_platten(the_workpath)
    fcgui.updateGui()


def run_eg_vertikale():

    # serialise
    runspzuroh.spzu_arc_roh_ortbeton_eg_waende_serialise(True, the_workpath)
    fcgui.updateGui()
    runspzutrw.spzu_ing_trw_ortbeton_eg_waende_serialise(True, the_workpath)
    fcgui.updateGui()
    #runspzuroh.spzu_arc_roh_ortbeton_alle_platten_serialise(True, the_workpath)
    fcgui.updateGui()
    #runspzutrw.spzu_ing_trw_ortbeton_alle_platten_serialise(True, the_workpath)
    fcgui.updateGui()

    # get smart diff data
    runsmdiffdata.run_eg_vertikale(the_workpath)
    fcgui.updateGui()

    # run diff
    runallbimgeomdiff.run_eg_vertikale(the_workpath)
    fcgui.updateGui()


def run_eg_platten():

    # serialise
    runspzuroh.spzu_arc_roh_ortbeton_ug_decke_serialise(True, the_workpath)
    fcgui.updateGui()
    runspzutrw.spzu_ing_trw_ortbeton_ug_decke_serialise(True, the_workpath)
    fcgui.updateGui()
    #runspzuroh.spzu_arc_roh_ortbeton_alle_vertikale_serialise(True, the_workpath)
    fcgui.updateGui()
    #runspzutrw.spzu_ing_trw_ortbeton_alle_vertikale_serialise(True, the_workpath)
    fcgui.updateGui()

    # get smart diff data
    runsmdiffdata.run_eg_platten(the_workpath)
    fcgui.updateGui()

    # run diff
    runallbimgeomdiff.run_eg_platten(the_workpath)
    fcgui.updateGui()


def run_ug_vertikale():

    # serialise
    runspzuroh.spzu_arc_roh_ortbeton_ug_waende_serialise(True, the_workpath)
    fcgui.updateGui()
    runspzutrw.spzu_ing_trw_ortbeton_ug_waende_serialise(True, the_workpath)
    fcgui.updateGui()
    #runspzuroh.spzu_arc_roh_ortbeton_alle_platten_serialise(True, the_workpath)
    fcgui.updateGui()
    #runspzutrw.spzu_ing_trw_ortbeton_alle_platten_serialise(True, the_workpath)
    fcgui.updateGui()

    # get smart diff data
    runsmdiffdata.run_ug_vertikale(the_workpath)
    fcgui.updateGui()

    # run diff
    runallbimgeomdiff.run_ug_vertikale(the_workpath)
    fcgui.updateGui()


def run_ug_platten():

    # serialise
    runspzuroh.spzu_arc_roh_ortbeton_ug_bpl_serialise(True, the_workpath)
    fcgui.updateGui()
    runspzutrw.spzu_ing_trw_ortbeton_ug_bpl_serialise(True, the_workpath)
    fcgui.updateGui()
    #runspzuroh.spzu_arc_roh_ortbeton_alle_vertikale_serialise(True, the_workpath)
    fcgui.updateGui()
    #runspzutrw.spzu_ing_trw_ortbeton_alle_vertikale_serialise(True, the_workpath)
    fcgui.updateGui()

    # get smart diff data
    runsmdiffdata.run_ug_platten(the_workpath)
    fcgui.updateGui()

    # run diff
    runallbimgeomdiff.run_ug_platten(the_workpath)
    fcgui.updateGui()

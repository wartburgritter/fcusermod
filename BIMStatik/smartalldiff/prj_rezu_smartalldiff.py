"""
# codelines to copy fuer bimgeomsmartdiff REZU

import importlib

import some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_rezu_data as prjmodule
importlib.reload(prjmodule)

import some_ifc_tools.serializer.bimgeomsmartdiffprojectdataclass as classprjobj
importlib.reload(classprjobj)
prjobj = classprjobj.BimGeomSmartDiffProjectData(prjmodule)


import smartalldiff.bimgeomsmartdiff as smartdiffrun
importlib.reload(smartdiffrun)


smartdiffrun.run_all(prjobj, "01_UG2")
smartdiffrun.run_all(prjobj, "11_EG")
smartdiffrun.run_all(prjobj, "22_OG2")
smartdiffrun.run_all(prjobj, ["01_UG2", "11_EG"])


smartdiffrun.run_all(prjobj)

"""


pass

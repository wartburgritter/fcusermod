"""
import importlib
import smartalldiff.spzu_allortbetondiff as spzuallortbetondiff

importlib.reload(spzuallortbetondiff)

spzuallortbetondiff.run()

"""


# das ist  all diff, heisst nicht auf geschosse und horizontal und vertikal aufgeteilt
# logisch, da es im Verzeichnis smartalldiff ist
# es gibt smartalldiff und smartdatadiff


import importlib
import os
from os.path import join

import FreeCADGui as fcgui

import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_arc_roh as runspzuroh
import some_ifc_tools.serializer.ifcos_serializer_prj_spzu_ing_trw as runspzutrw
importlib.reload(runspzuroh)
importlib.reload(runspzutrw)

import bimstatiktools.bimgeomdiff_run as bimgeomdiffrun
importlib.reload(bimgeomdiffrun)


std_workpath = join("C:", os.sep, "0_BHA_privat", "BIMGeomAllSerializerSmartDataDiff")


def run():

    # ser
    runspzuroh.spzu_arc_roh_ortbeton_alles_fuer_rohvergleich_serialise(True, the_workpath)
    fcgui.updateGui()
    runspzutrw.spzu_ing_trw_ortbeton_alles_fuer_rohvergleich_serialise(True, the_workpath)
    fcgui.updateGui()

    # diff
    bimgeomdiffrun.run_bimgeomdiff(
        [
            "Alle_Ortbetonbauteile_fuer_rohvergleich_ING_TRW",
            "Alle_Ortbetonbauteile_fuer_trwvergleich_ARC_ROH",
        ],
        workpath=the_workpath,
    )

    print("ALL ortbeton diff finished SPZU")
    return True

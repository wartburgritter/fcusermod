"""
fuer codelines to copy siehe projekt module

spzu ... spzu_smartalldiff
"""


import importlib
import os
from os.path import join

import FreeCADGui as fcgui

import some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_0run as runifcpatchforsmartdiff
importlib.reload(runifcpatchforsmartdiff)

import smartdatadiff.smartdatadiff_run as runsmdiffdata
importlib.reload(runsmdiffdata)

import bimstatiktools.bimgeomdiff_all as runallbimgeomdiff
importlib.reload(runallbimgeomdiff)


std_workpath = join("C:", os.sep, "0_BHA_privat", "BIMGeomAllSerializerSmartDataDiff")


# ************************************************************************************************
# ist komplett projektunabhaengig :-)
# run_all laeuft es nacheinander: ifcpath, smart data, diff
# wenn man nur ein Geschoss will, dann "UG" uebergeben
# wenn man nur EG und UG will unbedingt ["UG", "EG"] uebergeben, nie zweimal laufen lassen
# wenn nur geschosszuordnung von bauteilen aendert, die Alle_... dateien einfach nicht loeschen
# wenn nur ein schritt, dann in dem ensprechenden modul schauen
# fuer schritt 2 muss schritt 1 vorhanden sein usw.
def run_all(prjdata_module, geschosse=None):

    # patch alle plat und vert bauteile und je geschoss plat und vert bauteile
    runifcpatchforsmartdiff.run_all(prjdata_module, geschosse=geschosse, outpath=std_workpath)
    fcgui.updateGui()

    # convert ifc to FCStd falls FCStd nicht existiert
    runsmdiffdata.ifc2fcstd(std_workpath)
    fcgui.updateGui()

    # get smart data
    runsmdiffdata.run_all(prjdata_module, std_workpath, geschosse=geschosse)
    fcgui.updateGui()

    # diff
    runallbimgeomdiff.run_all(prjdata_module, std_workpath, geschosse=geschosse)
    fcgui.updateGui()

    print("ALL diff finished BIMGeomAllSerializerSmartDataDiff")
    return True

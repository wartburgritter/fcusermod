"""
# veraltet!!!
# unterer code
# einzeln mit ifcos_serializer
# wenn alle_vert oder alle_plat serialiseirt vorhanden sind, dann ser_alle=False

import importlib

import smartalldiff.spzu_smartalldiff as smartdiffspzu
importlib.reload(smartdiffspzu)

smartdiffspzu.run_einzeln("UG", "plat", ser_alle=True)  # ODER False
smartdiffspzu.run_einzeln("UG", "vert", ser_alle=True)  # ODER False

smartdiffspzu.run_einzeln("EG", "plat", ser_alle=False)
smartdiffspzu.run_einzeln("EG", "vert", ser_alle=False)

smartdiffspzu.run_einzeln("OG1", "plat", ser_alle=False)

"""


import importlib
import os
from os.path import join

import FreeCADGui as fcgui

import smartdatadiff.smartdatadiff_run as runsmdiffdata
importlib.reload(runsmdiffdata)

import bimstatiktools.bimgeomdiff_all as runallbimgeomdiff
importlib.reload(runallbimgeomdiff)


std_workpath = join("C:", os.sep, "0_BHA_privat", "BIMGeomAllSerializerSmartDataDiff")


# ************************************************************************************************
# hat noch projektabhaengigen code und imports
# daher hier im spzu module
# benutzt ifcos serialise, nicht ifcpatch. daher langsam und veraltet
# get smart data und diff sind identisch mit neuem ifcpatch tool
# serialize nur bis plat OG1 = Decke EG eingerichetet
# daher auch hier nur bis Decke EG machbar
def run_einzeln(geschoss="UG", plat_vert="plat", ser_alle=False):

    # da projektabhaengige imports innerhalb der def
    import some_ifc_tools.serializer.for_smartdiff_ifcos_prj_spzu_arc_roh as runifcosserializerspzuroh
    import some_ifc_tools.serializer.for_smartdiff_ifcos_prj_spzu_ing_trw as runifcosserializerspzutrw
    importlib.reload(runifcosserializerspzuroh)
    importlib.reload(runifcosserializerspzutrw)

    # serialise geschoss
    runifcosserializerspzuroh.spzu_arc_roh_ortbeton_geschosse_plat_vert_serialise(geschoss, plat_vert, True, std_workpath)
    fcgui.updateGui()
    runifcosserializerspzutrw.spzu_ing_trw_ortbeton_geschosse_plat_vert_serialise(geschoss, plat_vert, True, std_workpath)
    fcgui.updateGui()

    # serialise alle
    if plat_vert == "plat" and ser_alle is True:
        # fuer plat braucht es alle vertikale
        runifcosserializerspzuroh.spzu_arc_roh_ortbeton_alle_vertikale_serialise(True, std_workpath)
        fcgui.updateGui()
        runifcosserializerspzutrw.spzu_ing_trw_ortbeton_alle_vertikale_serialise(True, std_workpath)
        fcgui.updateGui()
    elif plat_vert == "vert" and ser_alle is True:
        # fuer vert braucht es alle platten
        runifcosserializerspzuroh.spzu_arc_roh_ortbeton_alle_platten_serialise(True, std_workpath)
        fcgui.updateGui()
        runifcosserializerspzutrw.spzu_ing_trw_ortbeton_alle_platten_serialise(True, std_workpath)
        fcgui.updateGui()

    # convert ifc to FCStd
    runsmdiffdata.ifc2fcstd(std_workpath)
    fcgui.updateGui()

    # get smart diff data
    if plat_vert == "plat":
        runsmdiffdata.run_platten(geschoss, std_workpath)
    elif plat_vert == "vert":
        runsmdiffdata.run_vertikale(geschoss, std_workpath)
    fcgui.updateGui()

    # run diff 
    if plat_vert == "plat":
        runallbimgeomdiff.run_platten(geschoss, std_workpath)
    elif plat_vert == "vert":
        runallbimgeomdiff.run_vertikale(geschoss, std_workpath)
    fcgui.updateGui()

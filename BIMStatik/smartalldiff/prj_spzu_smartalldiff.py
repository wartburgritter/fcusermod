"""
# komplettes superdiff :-)
# codelines to copy fuer bimgeomsmartdiff SPZU
# TRW und ROH kopieren nach C:\0_BHA_privat\BIMGeomAllSerializerSmartDataDiff\infiles
# start FreeCAD and copy paste

import importlib

import some_ifc_tools.serializer.for_smartdiff_ifcpatch_prj_spzu_data as prjmodule
importlib.reload(prjmodule)

import some_ifc_tools.serializer.bimgeomsmartdiffprojectdataclass as classprjobj
importlib.reload(classprjobj)
prjobj = classprjobj.BimGeomSmartDiffProjectData(prjmodule)


import smartalldiff.bimgeomsmartdiff as smartdiffrun
importlib.reload(smartdiffrun)


smartdiffrun.run_all(prjobj, "01_UG")
smartdiffrun.run_all(prjobj, "11_EG")
smartdiffrun.run_all(prjobj, ["01_UG", "11_EG"])


smartdiffrun.run_all(prjobj)

"""


pass
